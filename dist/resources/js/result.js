$(document).ready(function () {
    var ctx = document.getElementById('myChart').getContext('2d')
    var radar_gradient = ctx.createLinearGradient(0, 0, 0, 600)
    radar_gradient.addColorStop(0, 'rgba(255, 180, 0, 0.9)')
    radar_gradient.addColorStop(1, 'rgba(255, 72, 0,0.2)')
    // Chart.defaults.global.defaultFontFamily = "ariak"

    var level = ['Beginner', 'Intermediate','Advanced','Expert','Superr'];
    var leveldesc = ['You have some experience, but you can gain more','test01,test01','test02,test02','test03,test03','test04,test04'];

    var products = [
      {
        beginner:[{
          name:"beginner01",
          description:"beginnerdesc01<br />beginnerdesc01",
          thumbnail:"../dist/resources/images/result-page/maemanee.png",
          link:"#"
        },{
          name:"beginner02",
          description:"beginnerdesc02<br />beginnerdesc02",
          thumbnail:"../dist/resources/images/result-page/maemanee.png",
          link:"#"
        },{
          name:"beginner03",
          description:"beginnerdesc03<br />beginnerdesc03",
          thumbnail:"../dist/resources/images/result-page/maemanee.png",
          link:"#"
        }],
        intermediate:[{
          name:"intermediate01",
          description:"intermediatedesc01<br />intermediatedesc01",
          thumbnail:"../dist/resources/images/result-page/maemanee.png",
          link:"#"
        },{
          name:"intermediate02",
          description:"intermediatedesc02<br />intermediatedesc02",
          thumbnail:"../dist/resources/images/result-page/maemanee.png",
          link:"#"
        },{
          name:"intermediate03",
          description:"intermediatedesc03<br />intermediatedesc03",
          thumbnail:"../dist/resources/images/result-page/maemanee.png",
          link:"#"
        }],
        advanced:[{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        }],
        expert:[{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        }],
        supreme:[{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        }]
      },
      {
        beginner:[{
          name:"beginner01",
          description:"beginnerdesc01<br />beginnerdesc01",
          thumbnail:"../dist/resources/images/result-page/maemanee.png",
          link:"#"
        },{
          name:"beginner02",
          description:"beginnerdesc02<br />beginnerdesc02",
          thumbnail:"../dist/resources/images/result-page/maemanee.png",
          link:"#"
        },{
          name:"beginner03",
          description:"beginnerdesc03<br />beginnerdesc03",
          thumbnail:"../dist/resources/images/result-page/maemanee.png",
          link:"#"
        }],
        intermediate:[{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        }],
        advanced:[{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        }],
        expert:[{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        }],
        supreme:[{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        }]
      },
      {
        beginner:[{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        }],
        intermediate:[{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        }],
        advanced:[{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        }],
        expert:[{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        }],
        supreme:[{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        }]
      },
      {
        beginner:[{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        }],
        intermediate:[{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        }],
        advanced:[{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        }],
        expert:[{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        }],
        supreme:[{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        }]
      },
      {
        beginner:[{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        }],
        intermediate:[{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        }],
        advanced:[{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        }],
        expert:[{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        }],
        supreme:[{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        },{
          name:"",
          description:"",
          thumbnail:"",
          link:""
        }]
      }
    ]

    if(getUrlVars()['score'] != null && getUrlVars()['level'] != null && getUrlVars()['name'] != null){
      var score = getUrlVars()['score'].split(',');
      var result01 = 0;
      var result02 = 0;
      var result03 = 0;
    
      for (var i = 0; i < score.length; i++) {
        if(score[i].includes('0:')){
          result01 = (4*parseInt(score[i].split(':')[1])/100);
        }  
        
        if(score[i].includes('1:')){
          result02 = (4*parseInt(score[i].split(':')[1])/100);
        } 
        
        if(score[i].includes('2:')){
          result03 = (4*parseInt(score[i].split(':')[1])/100);
        }  
      }

      $('.yourbusinesslevel').html(getUrlVars()['level']);
      $('.yourbusinessname').html('‘'+getUrlVars()['name'].toString().replace("-"," ")+'’');

      var findIndex = level.indexOf(getUrlVars()['level']);
      var findWord = leveldesc[findIndex];
      $('#experiece1').html(findWord.split(',')[0]);
      $('#experiece2').html(findWord.split(',')[1]);

      var htmlcode='';
      var leveltxt = getUrlVars()['level'].toString().toLowerCase();
      for (var j = 0; j < products.length; j++) {
        htmlcode='';
        for (var i = 0; i < products[j][leveltxt].length; i++) {
          htmlcode += '<div class="content-slick">';
          htmlcode += '  <div id="bg-txt">';
          htmlcode += '    <div class="txt-header">';
          htmlcode += '      <img src="../dist/resources/images/result-page/correct.png" id="slick1" width="28"';
          htmlcode += '        height="28" alt="">';
          htmlcode += '      <p class="text-ktextbold extralarge white" id="maemaneeapp">'+products[j][leveltxt][i].name+'</p>';
          htmlcode += '    </div>';
          htmlcode += '    <div class="txt-middle">';
          htmlcode += '      <p class="text-ktreg  white" id="moneyelec">'+products[j][leveltxt][i].description+'</p>';
          htmlcode += '      <a href="'+products[j][leveltxt][i].link+'" id="btn-1" class="text-ktreg white"><u>ดูเพิ่มเติม</u></a>';
          htmlcode += '      <img src="'+products[j][leveltxt][i].thumbnail+'" id="maemanee" alt="">';
          htmlcode += '    </div>';
          htmlcode += '  </div>';
          htmlcode += '</div>';
        }

        $('#manage-list0'+(j+1)).html(htmlcode); 
      }
    }else{
      window.location='landing.html';
    } 
  
    var config = {
      type: 'radar',
      data: {
        labels: ['การทำธุรกิจแบบดิจิตัล', 'ความคุ้มครองทางธุรกิจ', 'การขยายธุรกิจ'],
        datasets: [{
          label: '',
          backgroundColor: radar_gradient,
          // borderColor: window.chartColors.red,
          pointBackgroundColor: [
            'rgba(255, 225, 225, 1)',
            'rgba(255, 225, 225, 1)',
            'rgba(255, 225, 225, 1)'
          ],
          data: [
            result01, result02, result03
          ]
        }]
      },
      options: {
        legend: {
          display: false,
          position: 'center'
        },
        title: {
          display: false,
          text: ''
        },
        tooltips: {
          enabled: true,
          titleFontFamily: 'KittithadaErgo_light',
          titleFontSize: 18,
          bodyFontFamily: 'KittithadaErgo_light',
          bodyFontSize: 22,
          callbacks: {
            title: function () {
              return 'คะแนนที่ได้'
            },
            label: function (tooltipItem, data) {
              return data.labels[tooltipItem.index] + ' : ' + tooltipItem.yLabel
            }
          }
        },
        scale: {
          ticks: {
            display: false,
            beginAtZero: true,
            maxTicksLimit: 6
          },
          pointLabels: {
            fontColor: '#ffffff',
            fontSize: 24,
            fontFamily: 'KittithadaErgo_light'
          },
          angleLines: {
            color: '#c9c9c9',
            lineWidth: 1.5
          },
          gridLines: {
            color: '#c9c9c9',
            lineWidth: 1.5
          }
        }
      }
    }
    window.myRadar = new Chart(ctx, config)
  })
  
  $(document).ready(function () {
    $('#arrow-up').click(function () {
      $('html, body').animate({scrollTop: 0}, 1000)
    // window.scrollTo(0, 0)
    })
    $('#arrow-down').click(function () {
      $('html, body').animate({
        scrollTop: $('.content-all').offset().top - 80}, 1000)
    })
  })
  
  function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
        vars[key] = value;
    });
    return vars;
}
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJyZXN1bHQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xyXG4gICAgdmFyIGN0eCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdteUNoYXJ0JykuZ2V0Q29udGV4dCgnMmQnKVxyXG4gICAgdmFyIHJhZGFyX2dyYWRpZW50ID0gY3R4LmNyZWF0ZUxpbmVhckdyYWRpZW50KDAsIDAsIDAsIDYwMClcclxuICAgIHJhZGFyX2dyYWRpZW50LmFkZENvbG9yU3RvcCgwLCAncmdiYSgyNTUsIDE4MCwgMCwgMC45KScpXHJcbiAgICByYWRhcl9ncmFkaWVudC5hZGRDb2xvclN0b3AoMSwgJ3JnYmEoMjU1LCA3MiwgMCwwLjIpJylcclxuICAgIC8vIENoYXJ0LmRlZmF1bHRzLmdsb2JhbC5kZWZhdWx0Rm9udEZhbWlseSA9IFwiYXJpYWtcIlxyXG5cclxuICAgIHZhciBsZXZlbCA9IFsnQmVnaW5uZXInLCAnSW50ZXJtZWRpYXRlJywnQWR2YW5jZWQnLCdFeHBlcnQnLCdTdXBlcnInXTtcclxuICAgIHZhciBsZXZlbGRlc2MgPSBbJ1lvdSBoYXZlIHNvbWUgZXhwZXJpZW5jZSwgYnV0IHlvdSBjYW4gZ2FpbiBtb3JlJywndGVzdDAxLHRlc3QwMScsJ3Rlc3QwMix0ZXN0MDInLCd0ZXN0MDMsdGVzdDAzJywndGVzdDA0LHRlc3QwNCddO1xyXG5cclxuICAgIHZhciBwcm9kdWN0cyA9IFtcclxuICAgICAge1xyXG4gICAgICAgIGJlZ2lubmVyOlt7XHJcbiAgICAgICAgICBuYW1lOlwiYmVnaW5uZXIwMVwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJiZWdpbm5lcmRlc2MwMTxiciAvPmJlZ2lubmVyZGVzYzAxXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCIuLi9kaXN0L3Jlc291cmNlcy9pbWFnZXMvcmVzdWx0LXBhZ2UvbWFlbWFuZWUucG5nXCIsXHJcbiAgICAgICAgICBsaW5rOlwiI1wiXHJcbiAgICAgICAgfSx7XHJcbiAgICAgICAgICBuYW1lOlwiYmVnaW5uZXIwMlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJiZWdpbm5lcmRlc2MwMjxiciAvPmJlZ2lubmVyZGVzYzAyXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCIuLi9kaXN0L3Jlc291cmNlcy9pbWFnZXMvcmVzdWx0LXBhZ2UvbWFlbWFuZWUucG5nXCIsXHJcbiAgICAgICAgICBsaW5rOlwiI1wiXHJcbiAgICAgICAgfSx7XHJcbiAgICAgICAgICBuYW1lOlwiYmVnaW5uZXIwM1wiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJiZWdpbm5lcmRlc2MwMzxiciAvPmJlZ2lubmVyZGVzYzAzXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCIuLi9kaXN0L3Jlc291cmNlcy9pbWFnZXMvcmVzdWx0LXBhZ2UvbWFlbWFuZWUucG5nXCIsXHJcbiAgICAgICAgICBsaW5rOlwiI1wiXHJcbiAgICAgICAgfV0sXHJcbiAgICAgICAgaW50ZXJtZWRpYXRlOlt7XHJcbiAgICAgICAgICBuYW1lOlwiaW50ZXJtZWRpYXRlMDFcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiaW50ZXJtZWRpYXRlZGVzYzAxPGJyIC8+aW50ZXJtZWRpYXRlZGVzYzAxXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCIuLi9kaXN0L3Jlc291cmNlcy9pbWFnZXMvcmVzdWx0LXBhZ2UvbWFlbWFuZWUucG5nXCIsXHJcbiAgICAgICAgICBsaW5rOlwiI1wiXHJcbiAgICAgICAgfSx7XHJcbiAgICAgICAgICBuYW1lOlwiaW50ZXJtZWRpYXRlMDJcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiaW50ZXJtZWRpYXRlZGVzYzAyPGJyIC8+aW50ZXJtZWRpYXRlZGVzYzAyXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCIuLi9kaXN0L3Jlc291cmNlcy9pbWFnZXMvcmVzdWx0LXBhZ2UvbWFlbWFuZWUucG5nXCIsXHJcbiAgICAgICAgICBsaW5rOlwiI1wiXHJcbiAgICAgICAgfSx7XHJcbiAgICAgICAgICBuYW1lOlwiaW50ZXJtZWRpYXRlMDNcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiaW50ZXJtZWRpYXRlZGVzYzAzPGJyIC8+aW50ZXJtZWRpYXRlZGVzYzAzXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCIuLi9kaXN0L3Jlc291cmNlcy9pbWFnZXMvcmVzdWx0LXBhZ2UvbWFlbWFuZWUucG5nXCIsXHJcbiAgICAgICAgICBsaW5rOlwiI1wiXHJcbiAgICAgICAgfV0sXHJcbiAgICAgICAgYWR2YW5jZWQ6W3tcclxuICAgICAgICAgIG5hbWU6XCJcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCJcIixcclxuICAgICAgICAgIGxpbms6XCJcIlxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgbmFtZTpcIlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJcIixcclxuICAgICAgICAgIHRodW1ibmFpbDpcIlwiLFxyXG4gICAgICAgICAgbGluazpcIlwiXHJcbiAgICAgICAgfSx7XHJcbiAgICAgICAgICBuYW1lOlwiXCIsXHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcIlwiLFxyXG4gICAgICAgICAgdGh1bWJuYWlsOlwiXCIsXHJcbiAgICAgICAgICBsaW5rOlwiXCJcclxuICAgICAgICB9XSxcclxuICAgICAgICBleHBlcnQ6W3tcclxuICAgICAgICAgIG5hbWU6XCJcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCJcIixcclxuICAgICAgICAgIGxpbms6XCJcIlxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgbmFtZTpcIlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJcIixcclxuICAgICAgICAgIHRodW1ibmFpbDpcIlwiLFxyXG4gICAgICAgICAgbGluazpcIlwiXHJcbiAgICAgICAgfSx7XHJcbiAgICAgICAgICBuYW1lOlwiXCIsXHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcIlwiLFxyXG4gICAgICAgICAgdGh1bWJuYWlsOlwiXCIsXHJcbiAgICAgICAgICBsaW5rOlwiXCJcclxuICAgICAgICB9XSxcclxuICAgICAgICBzdXByZW1lOlt7XHJcbiAgICAgICAgICBuYW1lOlwiXCIsXHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcIlwiLFxyXG4gICAgICAgICAgdGh1bWJuYWlsOlwiXCIsXHJcbiAgICAgICAgICBsaW5rOlwiXCJcclxuICAgICAgICB9LHtcclxuICAgICAgICAgIG5hbWU6XCJcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCJcIixcclxuICAgICAgICAgIGxpbms6XCJcIlxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgbmFtZTpcIlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJcIixcclxuICAgICAgICAgIHRodW1ibmFpbDpcIlwiLFxyXG4gICAgICAgICAgbGluazpcIlwiXHJcbiAgICAgICAgfV1cclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIGJlZ2lubmVyOlt7XHJcbiAgICAgICAgICBuYW1lOlwiYmVnaW5uZXIwMVwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJiZWdpbm5lcmRlc2MwMTxiciAvPmJlZ2lubmVyZGVzYzAxXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCIuLi9kaXN0L3Jlc291cmNlcy9pbWFnZXMvcmVzdWx0LXBhZ2UvbWFlbWFuZWUucG5nXCIsXHJcbiAgICAgICAgICBsaW5rOlwiI1wiXHJcbiAgICAgICAgfSx7XHJcbiAgICAgICAgICBuYW1lOlwiYmVnaW5uZXIwMlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJiZWdpbm5lcmRlc2MwMjxiciAvPmJlZ2lubmVyZGVzYzAyXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCIuLi9kaXN0L3Jlc291cmNlcy9pbWFnZXMvcmVzdWx0LXBhZ2UvbWFlbWFuZWUucG5nXCIsXHJcbiAgICAgICAgICBsaW5rOlwiI1wiXHJcbiAgICAgICAgfSx7XHJcbiAgICAgICAgICBuYW1lOlwiYmVnaW5uZXIwM1wiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJiZWdpbm5lcmRlc2MwMzxiciAvPmJlZ2lubmVyZGVzYzAzXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCIuLi9kaXN0L3Jlc291cmNlcy9pbWFnZXMvcmVzdWx0LXBhZ2UvbWFlbWFuZWUucG5nXCIsXHJcbiAgICAgICAgICBsaW5rOlwiI1wiXHJcbiAgICAgICAgfV0sXHJcbiAgICAgICAgaW50ZXJtZWRpYXRlOlt7XHJcbiAgICAgICAgICBuYW1lOlwiXCIsXHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcIlwiLFxyXG4gICAgICAgICAgdGh1bWJuYWlsOlwiXCIsXHJcbiAgICAgICAgICBsaW5rOlwiXCJcclxuICAgICAgICB9LHtcclxuICAgICAgICAgIG5hbWU6XCJcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCJcIixcclxuICAgICAgICAgIGxpbms6XCJcIlxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgbmFtZTpcIlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJcIixcclxuICAgICAgICAgIHRodW1ibmFpbDpcIlwiLFxyXG4gICAgICAgICAgbGluazpcIlwiXHJcbiAgICAgICAgfV0sXHJcbiAgICAgICAgYWR2YW5jZWQ6W3tcclxuICAgICAgICAgIG5hbWU6XCJcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCJcIixcclxuICAgICAgICAgIGxpbms6XCJcIlxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgbmFtZTpcIlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJcIixcclxuICAgICAgICAgIHRodW1ibmFpbDpcIlwiLFxyXG4gICAgICAgICAgbGluazpcIlwiXHJcbiAgICAgICAgfSx7XHJcbiAgICAgICAgICBuYW1lOlwiXCIsXHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcIlwiLFxyXG4gICAgICAgICAgdGh1bWJuYWlsOlwiXCIsXHJcbiAgICAgICAgICBsaW5rOlwiXCJcclxuICAgICAgICB9XSxcclxuICAgICAgICBleHBlcnQ6W3tcclxuICAgICAgICAgIG5hbWU6XCJcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCJcIixcclxuICAgICAgICAgIGxpbms6XCJcIlxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgbmFtZTpcIlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJcIixcclxuICAgICAgICAgIHRodW1ibmFpbDpcIlwiLFxyXG4gICAgICAgICAgbGluazpcIlwiXHJcbiAgICAgICAgfSx7XHJcbiAgICAgICAgICBuYW1lOlwiXCIsXHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcIlwiLFxyXG4gICAgICAgICAgdGh1bWJuYWlsOlwiXCIsXHJcbiAgICAgICAgICBsaW5rOlwiXCJcclxuICAgICAgICB9XSxcclxuICAgICAgICBzdXByZW1lOlt7XHJcbiAgICAgICAgICBuYW1lOlwiXCIsXHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcIlwiLFxyXG4gICAgICAgICAgdGh1bWJuYWlsOlwiXCIsXHJcbiAgICAgICAgICBsaW5rOlwiXCJcclxuICAgICAgICB9LHtcclxuICAgICAgICAgIG5hbWU6XCJcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCJcIixcclxuICAgICAgICAgIGxpbms6XCJcIlxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgbmFtZTpcIlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJcIixcclxuICAgICAgICAgIHRodW1ibmFpbDpcIlwiLFxyXG4gICAgICAgICAgbGluazpcIlwiXHJcbiAgICAgICAgfV1cclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIGJlZ2lubmVyOlt7XHJcbiAgICAgICAgICBuYW1lOlwiXCIsXHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcIlwiLFxyXG4gICAgICAgICAgdGh1bWJuYWlsOlwiXCIsXHJcbiAgICAgICAgICBsaW5rOlwiXCJcclxuICAgICAgICB9LHtcclxuICAgICAgICAgIG5hbWU6XCJcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCJcIixcclxuICAgICAgICAgIGxpbms6XCJcIlxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgbmFtZTpcIlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJcIixcclxuICAgICAgICAgIHRodW1ibmFpbDpcIlwiLFxyXG4gICAgICAgICAgbGluazpcIlwiXHJcbiAgICAgICAgfV0sXHJcbiAgICAgICAgaW50ZXJtZWRpYXRlOlt7XHJcbiAgICAgICAgICBuYW1lOlwiXCIsXHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcIlwiLFxyXG4gICAgICAgICAgdGh1bWJuYWlsOlwiXCIsXHJcbiAgICAgICAgICBsaW5rOlwiXCJcclxuICAgICAgICB9LHtcclxuICAgICAgICAgIG5hbWU6XCJcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCJcIixcclxuICAgICAgICAgIGxpbms6XCJcIlxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgbmFtZTpcIlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJcIixcclxuICAgICAgICAgIHRodW1ibmFpbDpcIlwiLFxyXG4gICAgICAgICAgbGluazpcIlwiXHJcbiAgICAgICAgfV0sXHJcbiAgICAgICAgYWR2YW5jZWQ6W3tcclxuICAgICAgICAgIG5hbWU6XCJcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCJcIixcclxuICAgICAgICAgIGxpbms6XCJcIlxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgbmFtZTpcIlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJcIixcclxuICAgICAgICAgIHRodW1ibmFpbDpcIlwiLFxyXG4gICAgICAgICAgbGluazpcIlwiXHJcbiAgICAgICAgfSx7XHJcbiAgICAgICAgICBuYW1lOlwiXCIsXHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcIlwiLFxyXG4gICAgICAgICAgdGh1bWJuYWlsOlwiXCIsXHJcbiAgICAgICAgICBsaW5rOlwiXCJcclxuICAgICAgICB9XSxcclxuICAgICAgICBleHBlcnQ6W3tcclxuICAgICAgICAgIG5hbWU6XCJcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCJcIixcclxuICAgICAgICAgIGxpbms6XCJcIlxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgbmFtZTpcIlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJcIixcclxuICAgICAgICAgIHRodW1ibmFpbDpcIlwiLFxyXG4gICAgICAgICAgbGluazpcIlwiXHJcbiAgICAgICAgfSx7XHJcbiAgICAgICAgICBuYW1lOlwiXCIsXHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcIlwiLFxyXG4gICAgICAgICAgdGh1bWJuYWlsOlwiXCIsXHJcbiAgICAgICAgICBsaW5rOlwiXCJcclxuICAgICAgICB9XSxcclxuICAgICAgICBzdXByZW1lOlt7XHJcbiAgICAgICAgICBuYW1lOlwiXCIsXHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcIlwiLFxyXG4gICAgICAgICAgdGh1bWJuYWlsOlwiXCIsXHJcbiAgICAgICAgICBsaW5rOlwiXCJcclxuICAgICAgICB9LHtcclxuICAgICAgICAgIG5hbWU6XCJcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCJcIixcclxuICAgICAgICAgIGxpbms6XCJcIlxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgbmFtZTpcIlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJcIixcclxuICAgICAgICAgIHRodW1ibmFpbDpcIlwiLFxyXG4gICAgICAgICAgbGluazpcIlwiXHJcbiAgICAgICAgfV1cclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIGJlZ2lubmVyOlt7XHJcbiAgICAgICAgICBuYW1lOlwiXCIsXHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcIlwiLFxyXG4gICAgICAgICAgdGh1bWJuYWlsOlwiXCIsXHJcbiAgICAgICAgICBsaW5rOlwiXCJcclxuICAgICAgICB9LHtcclxuICAgICAgICAgIG5hbWU6XCJcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCJcIixcclxuICAgICAgICAgIGxpbms6XCJcIlxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgbmFtZTpcIlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJcIixcclxuICAgICAgICAgIHRodW1ibmFpbDpcIlwiLFxyXG4gICAgICAgICAgbGluazpcIlwiXHJcbiAgICAgICAgfV0sXHJcbiAgICAgICAgaW50ZXJtZWRpYXRlOlt7XHJcbiAgICAgICAgICBuYW1lOlwiXCIsXHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcIlwiLFxyXG4gICAgICAgICAgdGh1bWJuYWlsOlwiXCIsXHJcbiAgICAgICAgICBsaW5rOlwiXCJcclxuICAgICAgICB9LHtcclxuICAgICAgICAgIG5hbWU6XCJcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCJcIixcclxuICAgICAgICAgIGxpbms6XCJcIlxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgbmFtZTpcIlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJcIixcclxuICAgICAgICAgIHRodW1ibmFpbDpcIlwiLFxyXG4gICAgICAgICAgbGluazpcIlwiXHJcbiAgICAgICAgfV0sXHJcbiAgICAgICAgYWR2YW5jZWQ6W3tcclxuICAgICAgICAgIG5hbWU6XCJcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCJcIixcclxuICAgICAgICAgIGxpbms6XCJcIlxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgbmFtZTpcIlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJcIixcclxuICAgICAgICAgIHRodW1ibmFpbDpcIlwiLFxyXG4gICAgICAgICAgbGluazpcIlwiXHJcbiAgICAgICAgfSx7XHJcbiAgICAgICAgICBuYW1lOlwiXCIsXHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcIlwiLFxyXG4gICAgICAgICAgdGh1bWJuYWlsOlwiXCIsXHJcbiAgICAgICAgICBsaW5rOlwiXCJcclxuICAgICAgICB9XSxcclxuICAgICAgICBleHBlcnQ6W3tcclxuICAgICAgICAgIG5hbWU6XCJcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCJcIixcclxuICAgICAgICAgIGxpbms6XCJcIlxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgbmFtZTpcIlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJcIixcclxuICAgICAgICAgIHRodW1ibmFpbDpcIlwiLFxyXG4gICAgICAgICAgbGluazpcIlwiXHJcbiAgICAgICAgfSx7XHJcbiAgICAgICAgICBuYW1lOlwiXCIsXHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcIlwiLFxyXG4gICAgICAgICAgdGh1bWJuYWlsOlwiXCIsXHJcbiAgICAgICAgICBsaW5rOlwiXCJcclxuICAgICAgICB9XSxcclxuICAgICAgICBzdXByZW1lOlt7XHJcbiAgICAgICAgICBuYW1lOlwiXCIsXHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcIlwiLFxyXG4gICAgICAgICAgdGh1bWJuYWlsOlwiXCIsXHJcbiAgICAgICAgICBsaW5rOlwiXCJcclxuICAgICAgICB9LHtcclxuICAgICAgICAgIG5hbWU6XCJcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCJcIixcclxuICAgICAgICAgIGxpbms6XCJcIlxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgbmFtZTpcIlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJcIixcclxuICAgICAgICAgIHRodW1ibmFpbDpcIlwiLFxyXG4gICAgICAgICAgbGluazpcIlwiXHJcbiAgICAgICAgfV1cclxuICAgICAgfSxcclxuICAgICAge1xyXG4gICAgICAgIGJlZ2lubmVyOlt7XHJcbiAgICAgICAgICBuYW1lOlwiXCIsXHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcIlwiLFxyXG4gICAgICAgICAgdGh1bWJuYWlsOlwiXCIsXHJcbiAgICAgICAgICBsaW5rOlwiXCJcclxuICAgICAgICB9LHtcclxuICAgICAgICAgIG5hbWU6XCJcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCJcIixcclxuICAgICAgICAgIGxpbms6XCJcIlxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgbmFtZTpcIlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJcIixcclxuICAgICAgICAgIHRodW1ibmFpbDpcIlwiLFxyXG4gICAgICAgICAgbGluazpcIlwiXHJcbiAgICAgICAgfV0sXHJcbiAgICAgICAgaW50ZXJtZWRpYXRlOlt7XHJcbiAgICAgICAgICBuYW1lOlwiXCIsXHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcIlwiLFxyXG4gICAgICAgICAgdGh1bWJuYWlsOlwiXCIsXHJcbiAgICAgICAgICBsaW5rOlwiXCJcclxuICAgICAgICB9LHtcclxuICAgICAgICAgIG5hbWU6XCJcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCJcIixcclxuICAgICAgICAgIGxpbms6XCJcIlxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgbmFtZTpcIlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJcIixcclxuICAgICAgICAgIHRodW1ibmFpbDpcIlwiLFxyXG4gICAgICAgICAgbGluazpcIlwiXHJcbiAgICAgICAgfV0sXHJcbiAgICAgICAgYWR2YW5jZWQ6W3tcclxuICAgICAgICAgIG5hbWU6XCJcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCJcIixcclxuICAgICAgICAgIGxpbms6XCJcIlxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgbmFtZTpcIlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJcIixcclxuICAgICAgICAgIHRodW1ibmFpbDpcIlwiLFxyXG4gICAgICAgICAgbGluazpcIlwiXHJcbiAgICAgICAgfSx7XHJcbiAgICAgICAgICBuYW1lOlwiXCIsXHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcIlwiLFxyXG4gICAgICAgICAgdGh1bWJuYWlsOlwiXCIsXHJcbiAgICAgICAgICBsaW5rOlwiXCJcclxuICAgICAgICB9XSxcclxuICAgICAgICBleHBlcnQ6W3tcclxuICAgICAgICAgIG5hbWU6XCJcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCJcIixcclxuICAgICAgICAgIGxpbms6XCJcIlxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgbmFtZTpcIlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJcIixcclxuICAgICAgICAgIHRodW1ibmFpbDpcIlwiLFxyXG4gICAgICAgICAgbGluazpcIlwiXHJcbiAgICAgICAgfSx7XHJcbiAgICAgICAgICBuYW1lOlwiXCIsXHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcIlwiLFxyXG4gICAgICAgICAgdGh1bWJuYWlsOlwiXCIsXHJcbiAgICAgICAgICBsaW5rOlwiXCJcclxuICAgICAgICB9XSxcclxuICAgICAgICBzdXByZW1lOlt7XHJcbiAgICAgICAgICBuYW1lOlwiXCIsXHJcbiAgICAgICAgICBkZXNjcmlwdGlvbjpcIlwiLFxyXG4gICAgICAgICAgdGh1bWJuYWlsOlwiXCIsXHJcbiAgICAgICAgICBsaW5rOlwiXCJcclxuICAgICAgICB9LHtcclxuICAgICAgICAgIG5hbWU6XCJcIixcclxuICAgICAgICAgIGRlc2NyaXB0aW9uOlwiXCIsXHJcbiAgICAgICAgICB0aHVtYm5haWw6XCJcIixcclxuICAgICAgICAgIGxpbms6XCJcIlxyXG4gICAgICAgIH0se1xyXG4gICAgICAgICAgbmFtZTpcIlwiLFxyXG4gICAgICAgICAgZGVzY3JpcHRpb246XCJcIixcclxuICAgICAgICAgIHRodW1ibmFpbDpcIlwiLFxyXG4gICAgICAgICAgbGluazpcIlwiXHJcbiAgICAgICAgfV1cclxuICAgICAgfVxyXG4gICAgXVxyXG5cclxuICAgIGlmKGdldFVybFZhcnMoKVsnc2NvcmUnXSAhPSBudWxsICYmIGdldFVybFZhcnMoKVsnbGV2ZWwnXSAhPSBudWxsICYmIGdldFVybFZhcnMoKVsnbmFtZSddICE9IG51bGwpe1xyXG4gICAgICB2YXIgc2NvcmUgPSBnZXRVcmxWYXJzKClbJ3Njb3JlJ10uc3BsaXQoJywnKTtcclxuICAgICAgdmFyIHJlc3VsdDAxID0gMDtcclxuICAgICAgdmFyIHJlc3VsdDAyID0gMDtcclxuICAgICAgdmFyIHJlc3VsdDAzID0gMDtcclxuICAgIFxyXG4gICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHNjb3JlLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgaWYoc2NvcmVbaV0uaW5jbHVkZXMoJzA6Jykpe1xyXG4gICAgICAgICAgcmVzdWx0MDEgPSAoNCpwYXJzZUludChzY29yZVtpXS5zcGxpdCgnOicpWzFdKS8xMDApO1xyXG4gICAgICAgIH0gIFxyXG4gICAgICAgIFxyXG4gICAgICAgIGlmKHNjb3JlW2ldLmluY2x1ZGVzKCcxOicpKXtcclxuICAgICAgICAgIHJlc3VsdDAyID0gKDQqcGFyc2VJbnQoc2NvcmVbaV0uc3BsaXQoJzonKVsxXSkvMTAwKTtcclxuICAgICAgICB9IFxyXG4gICAgICAgIFxyXG4gICAgICAgIGlmKHNjb3JlW2ldLmluY2x1ZGVzKCcyOicpKXtcclxuICAgICAgICAgIHJlc3VsdDAzID0gKDQqcGFyc2VJbnQoc2NvcmVbaV0uc3BsaXQoJzonKVsxXSkvMTAwKTtcclxuICAgICAgICB9ICBcclxuICAgICAgfVxyXG5cclxuICAgICAgJCgnLnlvdXJidXNpbmVzc2xldmVsJykuaHRtbChnZXRVcmxWYXJzKClbJ2xldmVsJ10pO1xyXG4gICAgICAkKCcueW91cmJ1c2luZXNzbmFtZScpLmh0bWwoJ+KAmCcrZ2V0VXJsVmFycygpWyduYW1lJ10udG9TdHJpbmcoKS5yZXBsYWNlKFwiLVwiLFwiIFwiKSsn4oCZJyk7XHJcblxyXG4gICAgICB2YXIgZmluZEluZGV4ID0gbGV2ZWwuaW5kZXhPZihnZXRVcmxWYXJzKClbJ2xldmVsJ10pO1xyXG4gICAgICB2YXIgZmluZFdvcmQgPSBsZXZlbGRlc2NbZmluZEluZGV4XTtcclxuICAgICAgJCgnI2V4cGVyaWVjZTEnKS5odG1sKGZpbmRXb3JkLnNwbGl0KCcsJylbMF0pO1xyXG4gICAgICAkKCcjZXhwZXJpZWNlMicpLmh0bWwoZmluZFdvcmQuc3BsaXQoJywnKVsxXSk7XHJcblxyXG4gICAgICB2YXIgaHRtbGNvZGU9Jyc7XHJcbiAgICAgIHZhciBsZXZlbHR4dCA9IGdldFVybFZhcnMoKVsnbGV2ZWwnXS50b1N0cmluZygpLnRvTG93ZXJDYXNlKCk7XHJcbiAgICAgIGZvciAodmFyIGogPSAwOyBqIDwgcHJvZHVjdHMubGVuZ3RoOyBqKyspIHtcclxuICAgICAgICBodG1sY29kZT0nJztcclxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHByb2R1Y3RzW2pdW2xldmVsdHh0XS5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgaHRtbGNvZGUgKz0gJzxkaXYgY2xhc3M9XCJjb250ZW50LXNsaWNrXCI+JztcclxuICAgICAgICAgIGh0bWxjb2RlICs9ICcgIDxkaXYgaWQ9XCJiZy10eHRcIj4nO1xyXG4gICAgICAgICAgaHRtbGNvZGUgKz0gJyAgICA8ZGl2IGNsYXNzPVwidHh0LWhlYWRlclwiPic7XHJcbiAgICAgICAgICBodG1sY29kZSArPSAnICAgICAgPGltZyBzcmM9XCIuLi9kaXN0L3Jlc291cmNlcy9pbWFnZXMvcmVzdWx0LXBhZ2UvY29ycmVjdC5wbmdcIiBpZD1cInNsaWNrMVwiIHdpZHRoPVwiMjhcIic7XHJcbiAgICAgICAgICBodG1sY29kZSArPSAnICAgICAgICBoZWlnaHQ9XCIyOFwiIGFsdD1cIlwiPic7XHJcbiAgICAgICAgICBodG1sY29kZSArPSAnICAgICAgPHAgY2xhc3M9XCJ0ZXh0LWt0ZXh0Ym9sZCBleHRyYWxhcmdlIHdoaXRlXCIgaWQ9XCJtYWVtYW5lZWFwcFwiPicrcHJvZHVjdHNbal1bbGV2ZWx0eHRdW2ldLm5hbWUrJzwvcD4nO1xyXG4gICAgICAgICAgaHRtbGNvZGUgKz0gJyAgICA8L2Rpdj4nO1xyXG4gICAgICAgICAgaHRtbGNvZGUgKz0gJyAgICA8ZGl2IGNsYXNzPVwidHh0LW1pZGRsZVwiPic7XHJcbiAgICAgICAgICBodG1sY29kZSArPSAnICAgICAgPHAgY2xhc3M9XCJ0ZXh0LWt0cmVnICB3aGl0ZVwiIGlkPVwibW9uZXllbGVjXCI+Jytwcm9kdWN0c1tqXVtsZXZlbHR4dF1baV0uZGVzY3JpcHRpb24rJzwvcD4nO1xyXG4gICAgICAgICAgaHRtbGNvZGUgKz0gJyAgICAgIDxhIGhyZWY9XCInK3Byb2R1Y3RzW2pdW2xldmVsdHh0XVtpXS5saW5rKydcIiBpZD1cImJ0bi0xXCIgY2xhc3M9XCJ0ZXh0LWt0cmVnIHdoaXRlXCI+PHU+4LiU4Li54LmA4Lie4Li04LmI4Lih4LmA4LiV4Li04LihPC91PjwvYT4nO1xyXG4gICAgICAgICAgaHRtbGNvZGUgKz0gJyAgICAgIDxpbWcgc3JjPVwiJytwcm9kdWN0c1tqXVtsZXZlbHR4dF1baV0udGh1bWJuYWlsKydcIiBpZD1cIm1hZW1hbmVlXCIgYWx0PVwiXCI+JztcclxuICAgICAgICAgIGh0bWxjb2RlICs9ICcgICAgPC9kaXY+JztcclxuICAgICAgICAgIGh0bWxjb2RlICs9ICcgIDwvZGl2Pic7XHJcbiAgICAgICAgICBodG1sY29kZSArPSAnPC9kaXY+JztcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgICQoJyNtYW5hZ2UtbGlzdDAnKyhqKzEpKS5odG1sKGh0bWxjb2RlKTsgXHJcbiAgICAgIH1cclxuICAgIH1lbHNle1xyXG4gICAgICB3aW5kb3cubG9jYXRpb249J2xhbmRpbmcuaHRtbCc7XHJcbiAgICB9IFxyXG4gIFxyXG4gICAgdmFyIGNvbmZpZyA9IHtcclxuICAgICAgdHlwZTogJ3JhZGFyJyxcclxuICAgICAgZGF0YToge1xyXG4gICAgICAgIGxhYmVsczogWyfguIHguLLguKPguJfguLPguJjguLjguKPguIHguLTguIjguYHguJrguJrguJTguLTguIjguLTguJXguLHguKUnLCAn4LiE4Lin4Liy4Lih4LiE4Li44LmJ4Lih4LiE4Lij4Lit4LiH4LiX4Liy4LiH4LiY4Li44Lij4LiB4Li04LiIJywgJ+C4geC4suC4o+C4guC4ouC4suC4ouC4mOC4uOC4o+C4geC4tOC4iCddLFxyXG4gICAgICAgIGRhdGFzZXRzOiBbe1xyXG4gICAgICAgICAgbGFiZWw6ICcnLFxyXG4gICAgICAgICAgYmFja2dyb3VuZENvbG9yOiByYWRhcl9ncmFkaWVudCxcclxuICAgICAgICAgIC8vIGJvcmRlckNvbG9yOiB3aW5kb3cuY2hhcnRDb2xvcnMucmVkLFxyXG4gICAgICAgICAgcG9pbnRCYWNrZ3JvdW5kQ29sb3I6IFtcclxuICAgICAgICAgICAgJ3JnYmEoMjU1LCAyMjUsIDIyNSwgMSknLFxyXG4gICAgICAgICAgICAncmdiYSgyNTUsIDIyNSwgMjI1LCAxKScsXHJcbiAgICAgICAgICAgICdyZ2JhKDI1NSwgMjI1LCAyMjUsIDEpJ1xyXG4gICAgICAgICAgXSxcclxuICAgICAgICAgIGRhdGE6IFtcclxuICAgICAgICAgICAgcmVzdWx0MDEsIHJlc3VsdDAyLCByZXN1bHQwM1xyXG4gICAgICAgICAgXVxyXG4gICAgICAgIH1dXHJcbiAgICAgIH0sXHJcbiAgICAgIG9wdGlvbnM6IHtcclxuICAgICAgICBsZWdlbmQ6IHtcclxuICAgICAgICAgIGRpc3BsYXk6IGZhbHNlLFxyXG4gICAgICAgICAgcG9zaXRpb246ICdjZW50ZXInXHJcbiAgICAgICAgfSxcclxuICAgICAgICB0aXRsZToge1xyXG4gICAgICAgICAgZGlzcGxheTogZmFsc2UsXHJcbiAgICAgICAgICB0ZXh0OiAnJ1xyXG4gICAgICAgIH0sXHJcbiAgICAgICAgdG9vbHRpcHM6IHtcclxuICAgICAgICAgIGVuYWJsZWQ6IHRydWUsXHJcbiAgICAgICAgICB0aXRsZUZvbnRGYW1pbHk6ICdLaXR0aXRoYWRhRXJnb19saWdodCcsXHJcbiAgICAgICAgICB0aXRsZUZvbnRTaXplOiAxOCxcclxuICAgICAgICAgIGJvZHlGb250RmFtaWx5OiAnS2l0dGl0aGFkYUVyZ29fbGlnaHQnLFxyXG4gICAgICAgICAgYm9keUZvbnRTaXplOiAyMixcclxuICAgICAgICAgIGNhbGxiYWNrczoge1xyXG4gICAgICAgICAgICB0aXRsZTogZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgIHJldHVybiAn4LiE4Liw4LmB4LiZ4LiZ4LiX4Li14LmI4LmE4LiU4LmJJ1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBsYWJlbDogZnVuY3Rpb24gKHRvb2x0aXBJdGVtLCBkYXRhKSB7XHJcbiAgICAgICAgICAgICAgcmV0dXJuIGRhdGEubGFiZWxzW3Rvb2x0aXBJdGVtLmluZGV4XSArICcgOiAnICsgdG9vbHRpcEl0ZW0ueUxhYmVsXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgIH1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHNjYWxlOiB7XHJcbiAgICAgICAgICB0aWNrczoge1xyXG4gICAgICAgICAgICBkaXNwbGF5OiBmYWxzZSxcclxuICAgICAgICAgICAgYmVnaW5BdFplcm86IHRydWUsXHJcbiAgICAgICAgICAgIG1heFRpY2tzTGltaXQ6IDZcclxuICAgICAgICAgIH0sXHJcbiAgICAgICAgICBwb2ludExhYmVsczoge1xyXG4gICAgICAgICAgICBmb250Q29sb3I6ICcjZmZmZmZmJyxcclxuICAgICAgICAgICAgZm9udFNpemU6IDI0LFxyXG4gICAgICAgICAgICBmb250RmFtaWx5OiAnS2l0dGl0aGFkYUVyZ29fbGlnaHQnXHJcbiAgICAgICAgICB9LFxyXG4gICAgICAgICAgYW5nbGVMaW5lczoge1xyXG4gICAgICAgICAgICBjb2xvcjogJyNjOWM5YzknLFxyXG4gICAgICAgICAgICBsaW5lV2lkdGg6IDEuNVxyXG4gICAgICAgICAgfSxcclxuICAgICAgICAgIGdyaWRMaW5lczoge1xyXG4gICAgICAgICAgICBjb2xvcjogJyNjOWM5YzknLFxyXG4gICAgICAgICAgICBsaW5lV2lkdGg6IDEuNVxyXG4gICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgfVxyXG4gICAgfVxyXG4gICAgd2luZG93Lm15UmFkYXIgPSBuZXcgQ2hhcnQoY3R4LCBjb25maWcpXHJcbiAgfSlcclxuICBcclxuICAkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XHJcbiAgICAkKCcjYXJyb3ctdXAnKS5jbGljayhmdW5jdGlvbiAoKSB7XHJcbiAgICAgICQoJ2h0bWwsIGJvZHknKS5hbmltYXRlKHtzY3JvbGxUb3A6IDB9LCAxMDAwKVxyXG4gICAgLy8gd2luZG93LnNjcm9sbFRvKDAsIDApXHJcbiAgICB9KVxyXG4gICAgJCgnI2Fycm93LWRvd24nKS5jbGljayhmdW5jdGlvbiAoKSB7XHJcbiAgICAgICQoJ2h0bWwsIGJvZHknKS5hbmltYXRlKHtcclxuICAgICAgICBzY3JvbGxUb3A6ICQoJy5jb250ZW50LWFsbCcpLm9mZnNldCgpLnRvcCAtIDgwfSwgMTAwMClcclxuICAgIH0pXHJcbiAgfSlcclxuICBcclxuICBmdW5jdGlvbiBnZXRVcmxWYXJzKCkge1xyXG4gICAgdmFyIHZhcnMgPSB7fTtcclxuICAgIHZhciBwYXJ0cyA9IHdpbmRvdy5sb2NhdGlvbi5ocmVmLnJlcGxhY2UoL1s/Jl0rKFtePSZdKyk9KFteJl0qKS9naSwgZnVuY3Rpb24obSxrZXksdmFsdWUpIHtcclxuICAgICAgICB2YXJzW2tleV0gPSB2YWx1ZTtcclxuICAgIH0pO1xyXG4gICAgcmV0dXJuIHZhcnM7XHJcbn0iXSwiZmlsZSI6InJlc3VsdC5qcyJ9
