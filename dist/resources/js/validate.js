window.Parsley
    .addValidator('mobileNumber', {
        requirementType: 'integer',
        validateString: function(value) {
            var mobileRegexp = /^(0)(8|9|6|2)[0-9]{8}([0-9]{1})?$/;
            return mobileRegexp.test(value);
        },
        messages: {
            en: 'Wrong Format With Mobile Number ',
        }
});

window.Parsley
    .addValidator('alphabet', {
        requirementType: 'string',
        validateString: function(value) {
            var onlyString = /^([^0-9]*)$/;
            return onlyString.test(value);
        },
        messages: {
            en: 'Only Alphabet ',
        }
});


function phoneCheck() { 
    // Grabs text input
    var inputText = document.getElementById("field-phone").value;
    var regButton = document.getElementById('submit');
    // strips out non-numbers to check length
    var inputStripped = inputText.replace(/\D/g, '');;
    // Grabs the Results div
    var res = document.getElementById("container");

    // RegEx Stuff
    var phoneReg = /^[0-9()-.\s]+$/

    // Pulls out class for Validation Check
    res.className = ""; 
    // Checks type and length
 
}
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJ2YWxpZGF0ZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyJ3aW5kb3cuUGFyc2xleVxyXG4gICAgLmFkZFZhbGlkYXRvcignbW9iaWxlTnVtYmVyJywge1xyXG4gICAgICAgIHJlcXVpcmVtZW50VHlwZTogJ2ludGVnZXInLFxyXG4gICAgICAgIHZhbGlkYXRlU3RyaW5nOiBmdW5jdGlvbih2YWx1ZSkge1xyXG4gICAgICAgICAgICB2YXIgbW9iaWxlUmVnZXhwID0gL14oMCkoOHw5fDZ8MilbMC05XXs4fShbMC05XXsxfSk/JC87XHJcbiAgICAgICAgICAgIHJldHVybiBtb2JpbGVSZWdleHAudGVzdCh2YWx1ZSk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBtZXNzYWdlczoge1xyXG4gICAgICAgICAgICBlbjogJ1dyb25nIEZvcm1hdCBXaXRoIE1vYmlsZSBOdW1iZXIgJyxcclxuICAgICAgICB9XHJcbn0pO1xyXG5cclxud2luZG93LlBhcnNsZXlcclxuICAgIC5hZGRWYWxpZGF0b3IoJ2FscGhhYmV0Jywge1xyXG4gICAgICAgIHJlcXVpcmVtZW50VHlwZTogJ3N0cmluZycsXHJcbiAgICAgICAgdmFsaWRhdGVTdHJpbmc6IGZ1bmN0aW9uKHZhbHVlKSB7XHJcbiAgICAgICAgICAgIHZhciBvbmx5U3RyaW5nID0gL14oW14wLTldKikkLztcclxuICAgICAgICAgICAgcmV0dXJuIG9ubHlTdHJpbmcudGVzdCh2YWx1ZSk7XHJcbiAgICAgICAgfSxcclxuICAgICAgICBtZXNzYWdlczoge1xyXG4gICAgICAgICAgICBlbjogJ09ubHkgQWxwaGFiZXQgJyxcclxuICAgICAgICB9XHJcbn0pO1xyXG5cclxuXHJcbmZ1bmN0aW9uIHBob25lQ2hlY2soKSB7IFxyXG4gICAgLy8gR3JhYnMgdGV4dCBpbnB1dFxyXG4gICAgdmFyIGlucHV0VGV4dCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiZmllbGQtcGhvbmVcIikudmFsdWU7XHJcbiAgICB2YXIgcmVnQnV0dG9uID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3N1Ym1pdCcpO1xyXG4gICAgLy8gc3RyaXBzIG91dCBub24tbnVtYmVycyB0byBjaGVjayBsZW5ndGhcclxuICAgIHZhciBpbnB1dFN0cmlwcGVkID0gaW5wdXRUZXh0LnJlcGxhY2UoL1xcRC9nLCAnJyk7O1xyXG4gICAgLy8gR3JhYnMgdGhlIFJlc3VsdHMgZGl2XHJcbiAgICB2YXIgcmVzID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJjb250YWluZXJcIik7XHJcblxyXG4gICAgLy8gUmVnRXggU3R1ZmZcclxuICAgIHZhciBwaG9uZVJlZyA9IC9eWzAtOSgpLS5cXHNdKyQvXHJcblxyXG4gICAgLy8gUHVsbHMgb3V0IGNsYXNzIGZvciBWYWxpZGF0aW9uIENoZWNrXHJcbiAgICByZXMuY2xhc3NOYW1lID0gXCJcIjsgXHJcbiAgICAvLyBDaGVja3MgdHlwZSBhbmQgbGVuZ3RoXHJcbiBcclxufSJdLCJmaWxlIjoidmFsaWRhdGUuanMifQ==
