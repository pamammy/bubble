$(document).ready(function () {
	
	$(".ans-result > .page-title span.yourbusinessname").addClass("text-ktextbold extralarge").removeClass("text-enbold large");
	var checkQ3 = getUrlVars()['product'].split(',')
	if (checkQ3.indexOf('d1') != -1) {
		$("#resultPage .result-suggest .content-info:nth-child(4)").show();
	}
	var ctx = document.getElementById('myChart').getContext('2d')
	var radar_gradient = ctx.createLinearGradient(0, 0, 0, 600)
	radar_gradient.addColorStop(0, 'rgba(255, 180, 0, 0.9)')
	radar_gradient.addColorStop(1, 'rgba(255, 72, 0,0.2)')
	setTimeout(function () {
		$('#myChart').before('<div class="chart-img-top">' +
			'<img class="chart-img" src="/dist/resources/images/result-page/manage-text.png" alt="">' +
			'</div>' +
			'<div  class="chart-img-middle">' +
			'<div class="chart-img-col">' +
			'<div>' +
			'<img class="chart-img" src="/dist/resources/images/result-page/invest-text.png" alt=""></div>' +
			'</div>' +
			'<div class="chart-img-col">' +
			'<div>' +
			'<img class="chart-img" src="/dist/resources/images/result-page/protect-text.png" alt=""></div>' +
			'</div>' +
			'</div>' +
			'<div  class="chart-img-bottom">' +
			'<div class="chart-img-col">' +
			'<div style="display:inline-block;">' +
			'<img class="chart-img" src="/dist/resources/images/result-page/knowledge-text.png" alt=""></div>' +
			'</div>' +
			'<div class="chart-img-col">' +
			'<div style="display:inline-block;margin-left:50px"></div>' +
			'</div>' +
			'<div class="chart-img-col">' +
			'<div style="display:inline-block;">' +
			'<img class="chart-img" src="/dist/resources/images/result-page/partner-text.png" alt=""></div>' +
			'</div>' +
			'</div>');

	}, 200);


	var level = ['Beginner', 'Intermediate', 'Advanced', 'Expert', 'Professional'];
	var leveldesc = ['Lorem ipsum dolor sit, amet consectetur <br />adipisicing elit. Distinctio consectetur id quae ullam soluta dolore 1111',
		'Lorem ipsum dolor sit, amet consectetur<br />adipisicing elit. Distinctio consectetur id quae ullam soluta dolore 2222',
		'Lorem ipsum dolor sit, amet <br />consectetur<br />adipisicing elit. Distinctio consectetur id quae ullam soluta dolore 333!',
		'Lorem ipsum dolor sit, <br />amet consectetur<br />adipisicing elit. Distinctio consectetur id quae ullam 444 ',
		'Lorem ipsum dolor sit, amet <br />consectetur adipisicing elit. <br />Distinctio consectetur id quae ullam 555'
	];

	var imgPath = '../dist/resources/images/main-page/slide-hand.png';

	//Create a new Image object.
	var imgObj = document.getElementById('eiei');

	//Set the src of this Image object.
	// imgObj.src = imgPath;

	//the x coordinates
	var x = 0;

	//the y coordinates
	var y = 0;

	//When our image has loaded.\ 
	var products = [{
		id: "d1",
		name: "Headlineeeee1",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Distinctio",
		thumbnail: "../dist/resources/images/result-page/easy.png",
		link: "#"
	}, {
		id: "d2",
		name: "Headlineeeee2",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Distinctio",
		thumbnail: "../dist/resources/images/result-page/easy.png",
		link: "#"
	}, {
		id: "d3",
		name: "Headlineeeee3",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Distinctio",
		thumbnail: "../dist/resources/images/result-page/easy.png",
		link: "#/corporate-banking/digital-banking-services/business-anywhere.html"
	}, {
		id: "d4",
		name: "Headlineeeee4",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Distinctio",
		thumbnail: "../dist/resources/images/result-page/11.png",
		link: "#/business-solution/google-my-business.html"
	}, {
		id: "d5",
		name: "Headlineeeee5",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Distinctio",
		thumbnail: "../dist/resources/images/result-page/22.png",
		link: "#/payroll.html"
	}, {
		id: "d6",
		name: "Headlineeeee6",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Distinctio",
		thumbnail: "../dist/resources/images/result-page/EDC.png",
		link: "#/personal-banking/payment/for-merchant/merchant-acquisition.html"
	}, {
		id: "d7",
		name: "Headlineeeee7",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Distinctio",
		thumbnail: "../dist/resources/images/result-page/QRCS.png",
		link: "#/personal-banking/payment/for-merchant/easy-pay.html"
	}, {
		id: "e1",
		name: "Headlineeeee8",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Distinctio",
		thumbnail: "../dist/resources/images/result-page/lending.png",
		link: "#"
	}, {
		id: "i1",
		name: "Headlineeeee9",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Distinctio",
		thumbnail: "../dist/resources/images/result-page/insurance.png",
		link: "#"
	}, {
		id: "p1",
		name: "Headlineeeee10",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Distinctio",
		thumbnail: "../dist/resources/images/result-page/wechat.png",
		link: "#/business-solution/total-merchant-solution.html"
	}, {
		id: "p2",
		name: "Headlineeeee11",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Distinctio",
		thumbnail: "../dist/resources/images/result-page/wongnai.png",
		link: "#/business-solution/restaurant-solution.html"
	}, {
		id: "p3",
		name: "Headlineeeee12",
		description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Distinctio",
		thumbnail: "../dist/resources/images/result-page/get.png",
		link: "#/business-solution/restaurant-solution.html"
	}, {
		id: "n1",
		name: "Headlineeeee13",
		description: "",
		thumbnail: "../dist/resources/images/result-page/industry-fashion.jpg",
		link: ""
	}, {
		id: "n2",
		name: "Headlineeeee14",
		description: "",
		thumbnail: "../dist/resources/images/result-page/industry-beauty.jpg",
		link: ""
	}, {
		id: "n3",
		name: "Headlineeeee15",
		description: "",
		thumbnail: "../dist/resources/images/result-page/industry-mom&baby.jpg",
		link: ""
	}, {
		id: "n4",
		name: "Headlineeeee16",
		description: "",
		thumbnail: "../dist/resources/images/result-page/industry-electronic.jpg",
		link: ""
	}, {
		id: "n5",
		name: "Headlineeeee17",
		description: "",
		thumbnail: "../dist/resources/images/result-page/industry-food&beverage.jpg",
		link: ""
	}, {
		id: "n6",
		name: "Headlineeeee18",
		description: "",
		thumbnail: "../dist/resources/images/result-page/industry-pharmacy.jpg",
		link: ""
	}, {
		id: "n7",
		name: "Headlineeeee19",
		description: "",
		thumbnail: "../dist/resources/images/result-page/industry-construction.jpg",
		link: ""
	}, {
		id: "n8",
		name: "Headlineeeee20",
		description: "",
		thumbnail: "../dist/resources/images/result-page/industry-hotel.jpg",
		link: ""
	}, {
		id: "n9",
		name: "Headlineeeee",
		description: "",
		thumbnail: "../dist/resources/images/result-page/industry-other.jpg",
		link: ""
	}, {
		id: "n10",
		name: "Headlineeeee",
		description: "",
		thumbnail: "../dist/resources/images/result-page/industry-used-automotive.jpg",
		link: ""
	}, {
		id: "k1",
		name: "Headlineeeee",
		description: "* Sunt suscipit, molestiae tempora est rerum quae tenetur incidunt laudantium? Quod reiciendis quo molestias.",
		thumbnail: "../dist/resources/images/result-page/industry-used-automotive.jpg",
		link: "#"
	}]

	if (getUrlVars()['score'] != null && getUrlVars()['level'] != null && getUrlVars()['name'] != null) {
		var score = getUrlVars()['score'].split(',');
		var result01 = 0;
		var result02 = 0;
		var result03 = 0;
		var result04 = 0;
		var result05 = 0;

		for (var i = 0; i < score.length; i++) {
			if (score[i].includes('0:')) {
				//result01 = parseInt(score[i].split(':')[1]);
				result01 = 100;
			}

			if (score[i].includes('1:')) {
				//result02 = parseInt(score[i].split(':')[1]);
				result02 = 100;
			}

			if (score[i].includes('2:')) {
				//result03 = parseInt(score[i].split(':')[1]);
				result03 = 100;
			}

			if (score[i].includes('3:')) {
				//result04 = parseInt(score[i].split(':')[1]);
				result04 = 100;
			}

			if (score[i].includes('4:')) {
				//result05 = parseInt(score[i].split(':')[1]);
				result04 = 60;
			}
		}

		$('.yourbusinesslevel').html(getUrlVars()['level'].toString().toUpperCase());
		$('.yourbusinessname').html('‘' + decodeURIComponent(getUrlVars()['name'].toString().replace("-", " ")) + '’');

		var findIndex = level.indexOf(getUrlVars()['level']);
		var findWord = leveldesc[findIndex];
		var fingImgThumbnail = getUrlVars()['level'].toString().toLowerCase();
		$('.yourbusinessdesc').html(findWord);
		$('.yourbusinessImg').html('<img src="../dist/resources/images/result-page/' + fingImgThumbnail + '.jpg" alt="' + fingImgThumbnail + '">');

		var product = getUrlVars()['product'].split(',')
		var product1 = []
		var product2 = []
		var product3 = []
		var product4 = []
		var product5 = []
		var product6 = []
		for (var i = 0; i < product.length; i++) {
			if (product[i].includes("d") && product1.indexOf(product[i]) == -1) {
				if (product.indexOf("d1") != -1) {
					product1.push(product[i])
				} else if ((product.indexOf("d2") != -1 || product.indexOf("d3") != -1) && product[i] != 'd7') {
					product1.push(product[i])
				}
			}
			if (product[i].includes("e") && product2.indexOf(product[i]) == -1) {
				product2.push(product[i])
			}
			if (product[i].includes("i") && product3.indexOf(product[i]) == -1) {
				product3.push(product[i])
			}
			if (product[i].includes("p") && product4.indexOf(product[i]) == -1) {
				product4.push(product[i])
			}
			if (product[i].includes("n") && product5.indexOf(product[i]) == -1) {
				product5.push(product[i])
			}
			if (product[i].includes("k") && product6.indexOf(product[i]) == -1) {
				product6.push(product[i])
			}
		}

		if (product1.length > 0) {
			$("#payrolldiv").html("");
			var htmlproduct1 = "";
			for (var i = 0; i < product1.length; i++) {
				var obj = products.filter(function (item) {
					if (item.id == product1[i]) {
						return item
					}
				})
				htmlproduct1 += '<div class="detail-box">';
				htmlproduct1 += '	<div class="detail-title text-enbold white">' + obj[0].name + '</div>';
				htmlproduct1 += '	<div class="detail-desc text-pslbold white">';
				htmlproduct1 += obj[0].description;
				htmlproduct1 += '	</div>';
				htmlproduct1 += '	<a href="' + obj[0].link + '" target="_blank" class="more-info white text-pslbold text-center">ดูเพิ่มเติม</a>';
				htmlproduct1 += '	<div class="partner"><img src="' + obj[0].thumbnail + '" alt="partner"></div>';
				htmlproduct1 += '</div>';
			}
			$("#payrolldiv").html(htmlproduct1);
		} else {
			$("#invest").remove();
		}

		if (product2.length > 0) {
			$("#investdiv").html("");
			var htmlproduct2 = "";
			for (var i = 0; i < product2.length; i++) {
				var obj = products.filter(function (item) {
					if (item.id == product2[i]) {
						return item
					}
				})
				htmlproduct2 += '<div class="detail-box">';
				htmlproduct2 += '	<div class="detail-title text-enbold white">' + obj[0].name + '</div>';
				htmlproduct2 += '	<div class="detail-desc text-pslbold white">';
				htmlproduct2 += obj[0].description;
				htmlproduct2 += '	</div>';
				htmlproduct2 += '	<a href="' + obj[0].link + '" target="_blank" class="more-info white text-pslbold text-center">ดูเพิ่มเติม</a>';
				htmlproduct2 += '	<div class="partner"><img src="' + obj[0].thumbnail + '" alt="partner"></div>';
				htmlproduct2 += '</div>';
			}
			$("#investdiv").html(htmlproduct2);
		} else {
			$("#invest").remove();
		}

		if (product3.length > 0) {
			$("#protectdiv").html("");
			var htmlproduct3 = "";
			for (var i = 0; i < product3.length; i++) {
				var obj = products.filter(function (item) {
					if (item.id == product3[i]) {
						return item
					}
				})
				htmlproduct3 += '<div class="detail-box">';
				htmlproduct3 += '	<div class="detail-title text-enbold white">' + obj[0].name + '</div>';
				htmlproduct3 += '	<div class="detail-desc text-pslbold white">';
				htmlproduct3 += obj[0].description;
				htmlproduct3 += '	</div>';
				htmlproduct3 += '	<a href="' + obj[0].link + '" target="_blank" class="more-info white text-pslbold text-center">ดูเพิ่มเติม</a>';
				htmlproduct3 += '	<div class="partner"><img src="' + obj[0].thumbnail + '" alt="partner"></div>';
				htmlproduct3 += '</div>';
			}
			$("#protectdiv").html(htmlproduct3);
		} else {
			$("#protect").remove();
		}

		if (product4.length > 0) {
			$("#partnerdiv").html("");
			var htmlproduct4 = "";
			for (var i = 0; i < product4.length; i++) {
				var obj = products.filter(function (item) {
					if (item.id == product4[i]) {
						return item
					}
				})
				htmlproduct4 += '<div class="detail-box">';
				htmlproduct4 += '	<div class="detail-title text-enbold white">' + obj[0].name + '</div>';
				htmlproduct4 += '	<div class="detail-desc text-pslbold white">';
				htmlproduct4 += obj[0].description;
				htmlproduct4 += '	</div>';
				htmlproduct4 += '	<a href="' + obj[0].link + '" target="_blank" class="more-info white text-pslbold text-center">ดูเพิ่มเติม</a>';
				htmlproduct4 += '	<div class="partner"><img src="' + obj[0].thumbnail + '" alt="partner"></div>';
				htmlproduct4 += '</div>';
			}
			$("#partnerdiv").html(htmlproduct4);
		} else {
			$("#partner").remove();
		}

		if (product6.length > 0) {
			$("#knowledgediv").html("");
			var htmlproduct6 = "";
			for (var i = 0; i < product6.length; i++) {
				var obj = products.filter(function (item) {
					if (item.id == product6[i]) {
						return item
					}
				})
				htmlproduct6 += '<div class="detail-box">';
				htmlproduct6 += '	<div class="detail-title text-enbold white">' + obj[0].name + '</div>';
				htmlproduct6 += '	<div class="detail-desc text-pslbold white">';
				htmlproduct6 += obj[0].description;
				htmlproduct6 += '	</div>';
				htmlproduct6 += '	<a href="' + obj[0].link + '" target="_blank" class="more-info white text-pslbold text-center">ดูเพิ่มเติม</a>';
				htmlproduct6 += '	<div class="partner"><img src="' + obj[0].thumbnail + '" alt="partner"></div>';
				htmlproduct6 += '</div>';
			}
			$("#knowledgediv").html(htmlproduct6);
		} else {
			$("#knowledge").remove();
		}

		if (product5.length > 0) {

			var obj = products.filter(function (item) {
				if (item.id == product5[0]) {
					return item
				}
			})

			var htmlproduct5 = "";

			htmlproduct5 += '<img src="' + obj[0].thumbnail + '" alt="' + obj[0].name + '">';
			htmlproduct5 += '<div class="businesstitle text-enbold white extralarge">';
			htmlproduct5 += obj[0].name;
			htmlproduct5 += '</div>';

			$(".subscrib").html(htmlproduct5);
		}

	} else {
		window.location = 'landing.html';
	}

	setTimeout(function () {
		Chart.defaut
		var graphHeight = $('#myChart').height();
		var paddingY;
		if ($(window).width() < 480) {
			paddingY = (graphHeight * 7.5) / 100;
		} else {
			paddingY = (graphHeight * 20) / 100;
		}

		var config = {
			type: 'radar',
			data: {
				labels: ['', '', '', '', ''],
				datasets: [{
					label: '',
					backgroundColor: radar_gradient,
					pointBackgroundColor: [
						'rgb(255, 255, 255)',
						'rgb(255, 255, 255)',
						'rgb(255, 255, 255)',
						'rgb(255, 255, 255)',
						'rgb(255, 255, 255)'
					],
					data: [
						result01, result05, result03, result04, result02
					]
				}]
			},
			options: {
				legend: {
					display: false,
					position: 'center'
				},
				title: {
					display: false,
					text: ''
				},
				tooltips: {
					enabled: true,
					titleFontFamily: 'KittithadaErgo_light',
					titleFontSize: 18,
					bodyFontFamily: 'KittithadaErgo_light',
					bodyFontSize: 22,
					callbacks: {
						title: function () {
							return 'คะแนนที่ได้'
						},
						label: function (tooltipItem, data) {
							return data.labels[tooltipItem.index] + ' : ' + tooltipItem.yLabel + ' %'
						}
					}
				},
				scale: {
					ticks: {
						display: false,
						beginAtZero: true,
						max: 100,
						stepSize: 20
					},
					pointLabels: {
						fontColor: '#ffffff',
						fontSize: 20,
						fontFamily: 'KittithadaErgo_light'
					},
					angleLines: {
						color: '#c9c9c9',
						lineWidth: 1.5
					},
					gridLines: {
						color: '#c9c9c9',
						lineWidth: 1.5
					}
				},
				layout: {
					padding: {
						left: 0,
						right: 0,
						top: paddingY,
						bottom: 0
					}
				}
			}
		}
		window.myRadar = new Chart(ctx, config)
		console.log("config", config)
		console.log("myRadar ", window.myRadar)
	}, 2000)


})

$(document).ready(function () {
	$("#arrow-down .down").on("click", function () {
		$("html, body").delay(100).animate({
			scrollTop: $(".result-ingredient").offset().top - 110
		}, 1500);
	});
	$(".product-box-js").each(function () {
		var detailCount = $(this).find(".wrapper-box .detail-box").length;
		if (detailCount > 3) {
			$(this).find(".wrapper-box").addClass("grid");
		} else {
			$(this).find(".wrapper-box").addClass("flex");
		}
	})

	$("#gototop").on("click", function () {
		$("html, body").delay(100).animate({
			scrollTop: 0
		}, 2700);
	});
	$(".result-product .detail-box .detail-title").addClass("extralarge");
})

function getUrlVars() {
	var vars = {};
	var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
		vars[key] = value;
	});
	return vars;
}

function draw_outside_label() {
	Chart.defaults.derivedRadar = Chart.defaults.radar;
	var custom = Chart.controllers.radar.extend({
		draw: function (ease) {

			Chart.controllers.radar.prototype.draw.call(this, ease);

			var meta = this.chart.chart.getDatasetMeta(0).data;
			var datasets = this.chart.chart;
			var ctx = this.chart.chart.ctx;
			var graphWidth = datasets.width;
			var graphHeight = datasets.height;
			console.log(graphWidth)
			for (var i = 0; i < this.chart.chart.getDatasetMeta(0).data.length; i++) {
				if (i == 0) {
					var x = ((graphWidth * 50) / 100);
					var y = (graphHeight * 0) / 100;
					var img = document.getElementById("bank");
					if ($(window).width() < 480) {
						ctx.drawImage(img, x - 20, y - 2, 45, 45);
					} else {
						ctx.drawImage(img, x - 30, y - 5, 70, 70);
					}

				} else if (i == 1) {
					var x = ((graphWidth * 25) / 100);
					var y = (graphHeight * 77.5) / 100;
					var img = document.getElementById("graph");
					if ($(window).width() < 480) {
						ctx.drawImage(img, x - 10, y - 70, 45, 45);
					} else {
						ctx.drawImage(img, x - 25, y - 200, 70, 70);
					}
				} else if (i == 2) {
					var x = ((graphWidth * 70) / 100);
					var y = (graphHeight * 77.5) / 100;
					var img = document.getElementById("shield");
					if ($(window).width() < 480) {
						ctx.drawImage(img, x - 18, y - 70, 45, 45);
					} else {
						ctx.drawImage(img, x + 10, y - 200, 70, 70);
					}
				} else if (i == 3) {
					var x = ((graphWidth * 30) / 100);
					var y = (graphHeight * 77.5) / 100;
					var img = document.getElementById("brain");
					if ($(window).width() < 480) {
						ctx.drawImage(img, x - 5, y, 45, 45);
					} else {
						ctx.drawImage(img, x - 10, y + 20, 70, 70);
					}
				} else if (i == 4) {
					var x = ((graphWidth * 60) / 100);
					var y = (graphHeight * 77.5) / 100;
					var img = document.getElementById("people");
					if ($(window).width() < 480) {
						ctx.drawImage(img, x + 2, y, 45, 45);
					} else {
						ctx.drawImage(img, x + 40, y + 20, 70, 70);
					}
				}

			}

		}
	});
	Chart.controllers.derivedRadar = custom;

}
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJyZXN1bHQtcGFnZS5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIkKGRvY3VtZW50KS5yZWFkeShmdW5jdGlvbiAoKSB7XHJcblx0XHJcblx0JChcIi5hbnMtcmVzdWx0ID4gLnBhZ2UtdGl0bGUgc3Bhbi55b3VyYnVzaW5lc3NuYW1lXCIpLmFkZENsYXNzKFwidGV4dC1rdGV4dGJvbGQgZXh0cmFsYXJnZVwiKS5yZW1vdmVDbGFzcyhcInRleHQtZW5ib2xkIGxhcmdlXCIpO1xyXG5cdHZhciBjaGVja1EzID0gZ2V0VXJsVmFycygpWydwcm9kdWN0J10uc3BsaXQoJywnKVxyXG5cdGlmIChjaGVja1EzLmluZGV4T2YoJ2QxJykgIT0gLTEpIHtcclxuXHRcdCQoXCIjcmVzdWx0UGFnZSAucmVzdWx0LXN1Z2dlc3QgLmNvbnRlbnQtaW5mbzpudGgtY2hpbGQoNClcIikuc2hvdygpO1xyXG5cdH1cclxuXHR2YXIgY3R4ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ215Q2hhcnQnKS5nZXRDb250ZXh0KCcyZCcpXHJcblx0dmFyIHJhZGFyX2dyYWRpZW50ID0gY3R4LmNyZWF0ZUxpbmVhckdyYWRpZW50KDAsIDAsIDAsIDYwMClcclxuXHRyYWRhcl9ncmFkaWVudC5hZGRDb2xvclN0b3AoMCwgJ3JnYmEoMjU1LCAxODAsIDAsIDAuOSknKVxyXG5cdHJhZGFyX2dyYWRpZW50LmFkZENvbG9yU3RvcCgxLCAncmdiYSgyNTUsIDcyLCAwLDAuMiknKVxyXG5cdHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG5cdFx0JCgnI215Q2hhcnQnKS5iZWZvcmUoJzxkaXYgY2xhc3M9XCJjaGFydC1pbWctdG9wXCI+JyArXHJcblx0XHRcdCc8aW1nIGNsYXNzPVwiY2hhcnQtaW1nXCIgc3JjPVwiL2Rpc3QvcmVzb3VyY2VzL2ltYWdlcy9yZXN1bHQtcGFnZS9tYW5hZ2UtdGV4dC5wbmdcIiBhbHQ9XCJcIj4nICtcclxuXHRcdFx0JzwvZGl2PicgK1xyXG5cdFx0XHQnPGRpdiAgY2xhc3M9XCJjaGFydC1pbWctbWlkZGxlXCI+JyArXHJcblx0XHRcdCc8ZGl2IGNsYXNzPVwiY2hhcnQtaW1nLWNvbFwiPicgK1xyXG5cdFx0XHQnPGRpdj4nICtcclxuXHRcdFx0JzxpbWcgY2xhc3M9XCJjaGFydC1pbWdcIiBzcmM9XCIvZGlzdC9yZXNvdXJjZXMvaW1hZ2VzL3Jlc3VsdC1wYWdlL2ludmVzdC10ZXh0LnBuZ1wiIGFsdD1cIlwiPjwvZGl2PicgK1xyXG5cdFx0XHQnPC9kaXY+JyArXHJcblx0XHRcdCc8ZGl2IGNsYXNzPVwiY2hhcnQtaW1nLWNvbFwiPicgK1xyXG5cdFx0XHQnPGRpdj4nICtcclxuXHRcdFx0JzxpbWcgY2xhc3M9XCJjaGFydC1pbWdcIiBzcmM9XCIvZGlzdC9yZXNvdXJjZXMvaW1hZ2VzL3Jlc3VsdC1wYWdlL3Byb3RlY3QtdGV4dC5wbmdcIiBhbHQ9XCJcIj48L2Rpdj4nICtcclxuXHRcdFx0JzwvZGl2PicgK1xyXG5cdFx0XHQnPC9kaXY+JyArXHJcblx0XHRcdCc8ZGl2ICBjbGFzcz1cImNoYXJ0LWltZy1ib3R0b21cIj4nICtcclxuXHRcdFx0JzxkaXYgY2xhc3M9XCJjaGFydC1pbWctY29sXCI+JyArXHJcblx0XHRcdCc8ZGl2IHN0eWxlPVwiZGlzcGxheTppbmxpbmUtYmxvY2s7XCI+JyArXHJcblx0XHRcdCc8aW1nIGNsYXNzPVwiY2hhcnQtaW1nXCIgc3JjPVwiL2Rpc3QvcmVzb3VyY2VzL2ltYWdlcy9yZXN1bHQtcGFnZS9rbm93bGVkZ2UtdGV4dC5wbmdcIiBhbHQ9XCJcIj48L2Rpdj4nICtcclxuXHRcdFx0JzwvZGl2PicgK1xyXG5cdFx0XHQnPGRpdiBjbGFzcz1cImNoYXJ0LWltZy1jb2xcIj4nICtcclxuXHRcdFx0JzxkaXYgc3R5bGU9XCJkaXNwbGF5OmlubGluZS1ibG9jazttYXJnaW4tbGVmdDo1MHB4XCI+PC9kaXY+JyArXHJcblx0XHRcdCc8L2Rpdj4nICtcclxuXHRcdFx0JzxkaXYgY2xhc3M9XCJjaGFydC1pbWctY29sXCI+JyArXHJcblx0XHRcdCc8ZGl2IHN0eWxlPVwiZGlzcGxheTppbmxpbmUtYmxvY2s7XCI+JyArXHJcblx0XHRcdCc8aW1nIGNsYXNzPVwiY2hhcnQtaW1nXCIgc3JjPVwiL2Rpc3QvcmVzb3VyY2VzL2ltYWdlcy9yZXN1bHQtcGFnZS9wYXJ0bmVyLXRleHQucG5nXCIgYWx0PVwiXCI+PC9kaXY+JyArXHJcblx0XHRcdCc8L2Rpdj4nICtcclxuXHRcdFx0JzwvZGl2PicpO1xyXG5cclxuXHR9LCAyMDApO1xyXG5cclxuXHJcblx0dmFyIGxldmVsID0gWydCZWdpbm5lcicsICdJbnRlcm1lZGlhdGUnLCAnQWR2YW5jZWQnLCAnRXhwZXJ0JywgJ1Byb2Zlc3Npb25hbCddO1xyXG5cdHZhciBsZXZlbGRlc2MgPSBbJ0xvcmVtIGlwc3VtIGRvbG9yIHNpdCwgYW1ldCBjb25zZWN0ZXR1ciA8YnIgLz5hZGlwaXNpY2luZyBlbGl0LiBEaXN0aW5jdGlvIGNvbnNlY3RldHVyIGlkIHF1YWUgdWxsYW0gc29sdXRhIGRvbG9yZSAxMTExJyxcclxuXHRcdCdMb3JlbSBpcHN1bSBkb2xvciBzaXQsIGFtZXQgY29uc2VjdGV0dXI8YnIgLz5hZGlwaXNpY2luZyBlbGl0LiBEaXN0aW5jdGlvIGNvbnNlY3RldHVyIGlkIHF1YWUgdWxsYW0gc29sdXRhIGRvbG9yZSAyMjIyJyxcclxuXHRcdCdMb3JlbSBpcHN1bSBkb2xvciBzaXQsIGFtZXQgPGJyIC8+Y29uc2VjdGV0dXI8YnIgLz5hZGlwaXNpY2luZyBlbGl0LiBEaXN0aW5jdGlvIGNvbnNlY3RldHVyIGlkIHF1YWUgdWxsYW0gc29sdXRhIGRvbG9yZSAzMzMhJyxcclxuXHRcdCdMb3JlbSBpcHN1bSBkb2xvciBzaXQsIDxiciAvPmFtZXQgY29uc2VjdGV0dXI8YnIgLz5hZGlwaXNpY2luZyBlbGl0LiBEaXN0aW5jdGlvIGNvbnNlY3RldHVyIGlkIHF1YWUgdWxsYW0gNDQ0ICcsXHJcblx0XHQnTG9yZW0gaXBzdW0gZG9sb3Igc2l0LCBhbWV0IDxiciAvPmNvbnNlY3RldHVyIGFkaXBpc2ljaW5nIGVsaXQuIDxiciAvPkRpc3RpbmN0aW8gY29uc2VjdGV0dXIgaWQgcXVhZSB1bGxhbSA1NTUnXHJcblx0XTtcclxuXHJcblx0dmFyIGltZ1BhdGggPSAnLi4vZGlzdC9yZXNvdXJjZXMvaW1hZ2VzL21haW4tcGFnZS9zbGlkZS1oYW5kLnBuZyc7XHJcblxyXG5cdC8vQ3JlYXRlIGEgbmV3IEltYWdlIG9iamVjdC5cclxuXHR2YXIgaW1nT2JqID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2VpZWknKTtcclxuXHJcblx0Ly9TZXQgdGhlIHNyYyBvZiB0aGlzIEltYWdlIG9iamVjdC5cclxuXHQvLyBpbWdPYmouc3JjID0gaW1nUGF0aDtcclxuXHJcblx0Ly90aGUgeCBjb29yZGluYXRlc1xyXG5cdHZhciB4ID0gMDtcclxuXHJcblx0Ly90aGUgeSBjb29yZGluYXRlc1xyXG5cdHZhciB5ID0gMDtcclxuXHJcblx0Ly9XaGVuIG91ciBpbWFnZSBoYXMgbG9hZGVkLlxcIFxyXG5cdHZhciBwcm9kdWN0cyA9IFt7XHJcblx0XHRpZDogXCJkMVwiLFxyXG5cdFx0bmFtZTogXCJIZWFkbGluZWVlZWUxXCIsXHJcblx0XHRkZXNjcmlwdGlvbjogXCJMb3JlbSBpcHN1bSBkb2xvciBzaXQsIGFtZXQgY29uc2VjdGV0dXIgYWRpcGlzaWNpbmcgZWxpdC4gRGlzdGluY3Rpb1wiLFxyXG5cdFx0dGh1bWJuYWlsOiBcIi4uL2Rpc3QvcmVzb3VyY2VzL2ltYWdlcy9yZXN1bHQtcGFnZS9lYXN5LnBuZ1wiLFxyXG5cdFx0bGluazogXCIjXCJcclxuXHR9LCB7XHJcblx0XHRpZDogXCJkMlwiLFxyXG5cdFx0bmFtZTogXCJIZWFkbGluZWVlZWUyXCIsXHJcblx0XHRkZXNjcmlwdGlvbjogXCJMb3JlbSBpcHN1bSBkb2xvciBzaXQsIGFtZXQgY29uc2VjdGV0dXIgYWRpcGlzaWNpbmcgZWxpdC4gRGlzdGluY3Rpb1wiLFxyXG5cdFx0dGh1bWJuYWlsOiBcIi4uL2Rpc3QvcmVzb3VyY2VzL2ltYWdlcy9yZXN1bHQtcGFnZS9lYXN5LnBuZ1wiLFxyXG5cdFx0bGluazogXCIjXCJcclxuXHR9LCB7XHJcblx0XHRpZDogXCJkM1wiLFxyXG5cdFx0bmFtZTogXCJIZWFkbGluZWVlZWUzXCIsXHJcblx0XHRkZXNjcmlwdGlvbjogXCJMb3JlbSBpcHN1bSBkb2xvciBzaXQsIGFtZXQgY29uc2VjdGV0dXIgYWRpcGlzaWNpbmcgZWxpdC4gRGlzdGluY3Rpb1wiLFxyXG5cdFx0dGh1bWJuYWlsOiBcIi4uL2Rpc3QvcmVzb3VyY2VzL2ltYWdlcy9yZXN1bHQtcGFnZS9lYXN5LnBuZ1wiLFxyXG5cdFx0bGluazogXCIjL2NvcnBvcmF0ZS1iYW5raW5nL2RpZ2l0YWwtYmFua2luZy1zZXJ2aWNlcy9idXNpbmVzcy1hbnl3aGVyZS5odG1sXCJcclxuXHR9LCB7XHJcblx0XHRpZDogXCJkNFwiLFxyXG5cdFx0bmFtZTogXCJIZWFkbGluZWVlZWU0XCIsXHJcblx0XHRkZXNjcmlwdGlvbjogXCJMb3JlbSBpcHN1bSBkb2xvciBzaXQsIGFtZXQgY29uc2VjdGV0dXIgYWRpcGlzaWNpbmcgZWxpdC4gRGlzdGluY3Rpb1wiLFxyXG5cdFx0dGh1bWJuYWlsOiBcIi4uL2Rpc3QvcmVzb3VyY2VzL2ltYWdlcy9yZXN1bHQtcGFnZS8xMS5wbmdcIixcclxuXHRcdGxpbms6IFwiIy9idXNpbmVzcy1zb2x1dGlvbi9nb29nbGUtbXktYnVzaW5lc3MuaHRtbFwiXHJcblx0fSwge1xyXG5cdFx0aWQ6IFwiZDVcIixcclxuXHRcdG5hbWU6IFwiSGVhZGxpbmVlZWVlNVwiLFxyXG5cdFx0ZGVzY3JpcHRpb246IFwiTG9yZW0gaXBzdW0gZG9sb3Igc2l0LCBhbWV0IGNvbnNlY3RldHVyIGFkaXBpc2ljaW5nIGVsaXQuIERpc3RpbmN0aW9cIixcclxuXHRcdHRodW1ibmFpbDogXCIuLi9kaXN0L3Jlc291cmNlcy9pbWFnZXMvcmVzdWx0LXBhZ2UvMjIucG5nXCIsXHJcblx0XHRsaW5rOiBcIiMvcGF5cm9sbC5odG1sXCJcclxuXHR9LCB7XHJcblx0XHRpZDogXCJkNlwiLFxyXG5cdFx0bmFtZTogXCJIZWFkbGluZWVlZWU2XCIsXHJcblx0XHRkZXNjcmlwdGlvbjogXCJMb3JlbSBpcHN1bSBkb2xvciBzaXQsIGFtZXQgY29uc2VjdGV0dXIgYWRpcGlzaWNpbmcgZWxpdC4gRGlzdGluY3Rpb1wiLFxyXG5cdFx0dGh1bWJuYWlsOiBcIi4uL2Rpc3QvcmVzb3VyY2VzL2ltYWdlcy9yZXN1bHQtcGFnZS9FREMucG5nXCIsXHJcblx0XHRsaW5rOiBcIiMvcGVyc29uYWwtYmFua2luZy9wYXltZW50L2Zvci1tZXJjaGFudC9tZXJjaGFudC1hY3F1aXNpdGlvbi5odG1sXCJcclxuXHR9LCB7XHJcblx0XHRpZDogXCJkN1wiLFxyXG5cdFx0bmFtZTogXCJIZWFkbGluZWVlZWU3XCIsXHJcblx0XHRkZXNjcmlwdGlvbjogXCJMb3JlbSBpcHN1bSBkb2xvciBzaXQsIGFtZXQgY29uc2VjdGV0dXIgYWRpcGlzaWNpbmcgZWxpdC4gRGlzdGluY3Rpb1wiLFxyXG5cdFx0dGh1bWJuYWlsOiBcIi4uL2Rpc3QvcmVzb3VyY2VzL2ltYWdlcy9yZXN1bHQtcGFnZS9RUkNTLnBuZ1wiLFxyXG5cdFx0bGluazogXCIjL3BlcnNvbmFsLWJhbmtpbmcvcGF5bWVudC9mb3ItbWVyY2hhbnQvZWFzeS1wYXkuaHRtbFwiXHJcblx0fSwge1xyXG5cdFx0aWQ6IFwiZTFcIixcclxuXHRcdG5hbWU6IFwiSGVhZGxpbmVlZWVlOFwiLFxyXG5cdFx0ZGVzY3JpcHRpb246IFwiTG9yZW0gaXBzdW0gZG9sb3Igc2l0LCBhbWV0IGNvbnNlY3RldHVyIGFkaXBpc2ljaW5nIGVsaXQuIERpc3RpbmN0aW9cIixcclxuXHRcdHRodW1ibmFpbDogXCIuLi9kaXN0L3Jlc291cmNlcy9pbWFnZXMvcmVzdWx0LXBhZ2UvbGVuZGluZy5wbmdcIixcclxuXHRcdGxpbms6IFwiI1wiXHJcblx0fSwge1xyXG5cdFx0aWQ6IFwiaTFcIixcclxuXHRcdG5hbWU6IFwiSGVhZGxpbmVlZWVlOVwiLFxyXG5cdFx0ZGVzY3JpcHRpb246IFwiTG9yZW0gaXBzdW0gZG9sb3Igc2l0LCBhbWV0IGNvbnNlY3RldHVyIGFkaXBpc2ljaW5nIGVsaXQuIERpc3RpbmN0aW9cIixcclxuXHRcdHRodW1ibmFpbDogXCIuLi9kaXN0L3Jlc291cmNlcy9pbWFnZXMvcmVzdWx0LXBhZ2UvaW5zdXJhbmNlLnBuZ1wiLFxyXG5cdFx0bGluazogXCIjXCJcclxuXHR9LCB7XHJcblx0XHRpZDogXCJwMVwiLFxyXG5cdFx0bmFtZTogXCJIZWFkbGluZWVlZWUxMFwiLFxyXG5cdFx0ZGVzY3JpcHRpb246IFwiTG9yZW0gaXBzdW0gZG9sb3Igc2l0LCBhbWV0IGNvbnNlY3RldHVyIGFkaXBpc2ljaW5nIGVsaXQuIERpc3RpbmN0aW9cIixcclxuXHRcdHRodW1ibmFpbDogXCIuLi9kaXN0L3Jlc291cmNlcy9pbWFnZXMvcmVzdWx0LXBhZ2Uvd2VjaGF0LnBuZ1wiLFxyXG5cdFx0bGluazogXCIjL2J1c2luZXNzLXNvbHV0aW9uL3RvdGFsLW1lcmNoYW50LXNvbHV0aW9uLmh0bWxcIlxyXG5cdH0sIHtcclxuXHRcdGlkOiBcInAyXCIsXHJcblx0XHRuYW1lOiBcIkhlYWRsaW5lZWVlZTExXCIsXHJcblx0XHRkZXNjcmlwdGlvbjogXCJMb3JlbSBpcHN1bSBkb2xvciBzaXQsIGFtZXQgY29uc2VjdGV0dXIgYWRpcGlzaWNpbmcgZWxpdC4gRGlzdGluY3Rpb1wiLFxyXG5cdFx0dGh1bWJuYWlsOiBcIi4uL2Rpc3QvcmVzb3VyY2VzL2ltYWdlcy9yZXN1bHQtcGFnZS93b25nbmFpLnBuZ1wiLFxyXG5cdFx0bGluazogXCIjL2J1c2luZXNzLXNvbHV0aW9uL3Jlc3RhdXJhbnQtc29sdXRpb24uaHRtbFwiXHJcblx0fSwge1xyXG5cdFx0aWQ6IFwicDNcIixcclxuXHRcdG5hbWU6IFwiSGVhZGxpbmVlZWVlMTJcIixcclxuXHRcdGRlc2NyaXB0aW9uOiBcIkxvcmVtIGlwc3VtIGRvbG9yIHNpdCwgYW1ldCBjb25zZWN0ZXR1ciBhZGlwaXNpY2luZyBlbGl0LiBEaXN0aW5jdGlvXCIsXHJcblx0XHR0aHVtYm5haWw6IFwiLi4vZGlzdC9yZXNvdXJjZXMvaW1hZ2VzL3Jlc3VsdC1wYWdlL2dldC5wbmdcIixcclxuXHRcdGxpbms6IFwiIy9idXNpbmVzcy1zb2x1dGlvbi9yZXN0YXVyYW50LXNvbHV0aW9uLmh0bWxcIlxyXG5cdH0sIHtcclxuXHRcdGlkOiBcIm4xXCIsXHJcblx0XHRuYW1lOiBcIkhlYWRsaW5lZWVlZTEzXCIsXHJcblx0XHRkZXNjcmlwdGlvbjogXCJcIixcclxuXHRcdHRodW1ibmFpbDogXCIuLi9kaXN0L3Jlc291cmNlcy9pbWFnZXMvcmVzdWx0LXBhZ2UvaW5kdXN0cnktZmFzaGlvbi5qcGdcIixcclxuXHRcdGxpbms6IFwiXCJcclxuXHR9LCB7XHJcblx0XHRpZDogXCJuMlwiLFxyXG5cdFx0bmFtZTogXCJIZWFkbGluZWVlZWUxNFwiLFxyXG5cdFx0ZGVzY3JpcHRpb246IFwiXCIsXHJcblx0XHR0aHVtYm5haWw6IFwiLi4vZGlzdC9yZXNvdXJjZXMvaW1hZ2VzL3Jlc3VsdC1wYWdlL2luZHVzdHJ5LWJlYXV0eS5qcGdcIixcclxuXHRcdGxpbms6IFwiXCJcclxuXHR9LCB7XHJcblx0XHRpZDogXCJuM1wiLFxyXG5cdFx0bmFtZTogXCJIZWFkbGluZWVlZWUxNVwiLFxyXG5cdFx0ZGVzY3JpcHRpb246IFwiXCIsXHJcblx0XHR0aHVtYm5haWw6IFwiLi4vZGlzdC9yZXNvdXJjZXMvaW1hZ2VzL3Jlc3VsdC1wYWdlL2luZHVzdHJ5LW1vbSZiYWJ5LmpwZ1wiLFxyXG5cdFx0bGluazogXCJcIlxyXG5cdH0sIHtcclxuXHRcdGlkOiBcIm40XCIsXHJcblx0XHRuYW1lOiBcIkhlYWRsaW5lZWVlZTE2XCIsXHJcblx0XHRkZXNjcmlwdGlvbjogXCJcIixcclxuXHRcdHRodW1ibmFpbDogXCIuLi9kaXN0L3Jlc291cmNlcy9pbWFnZXMvcmVzdWx0LXBhZ2UvaW5kdXN0cnktZWxlY3Ryb25pYy5qcGdcIixcclxuXHRcdGxpbms6IFwiXCJcclxuXHR9LCB7XHJcblx0XHRpZDogXCJuNVwiLFxyXG5cdFx0bmFtZTogXCJIZWFkbGluZWVlZWUxN1wiLFxyXG5cdFx0ZGVzY3JpcHRpb246IFwiXCIsXHJcblx0XHR0aHVtYm5haWw6IFwiLi4vZGlzdC9yZXNvdXJjZXMvaW1hZ2VzL3Jlc3VsdC1wYWdlL2luZHVzdHJ5LWZvb2QmYmV2ZXJhZ2UuanBnXCIsXHJcblx0XHRsaW5rOiBcIlwiXHJcblx0fSwge1xyXG5cdFx0aWQ6IFwibjZcIixcclxuXHRcdG5hbWU6IFwiSGVhZGxpbmVlZWVlMThcIixcclxuXHRcdGRlc2NyaXB0aW9uOiBcIlwiLFxyXG5cdFx0dGh1bWJuYWlsOiBcIi4uL2Rpc3QvcmVzb3VyY2VzL2ltYWdlcy9yZXN1bHQtcGFnZS9pbmR1c3RyeS1waGFybWFjeS5qcGdcIixcclxuXHRcdGxpbms6IFwiXCJcclxuXHR9LCB7XHJcblx0XHRpZDogXCJuN1wiLFxyXG5cdFx0bmFtZTogXCJIZWFkbGluZWVlZWUxOVwiLFxyXG5cdFx0ZGVzY3JpcHRpb246IFwiXCIsXHJcblx0XHR0aHVtYm5haWw6IFwiLi4vZGlzdC9yZXNvdXJjZXMvaW1hZ2VzL3Jlc3VsdC1wYWdlL2luZHVzdHJ5LWNvbnN0cnVjdGlvbi5qcGdcIixcclxuXHRcdGxpbms6IFwiXCJcclxuXHR9LCB7XHJcblx0XHRpZDogXCJuOFwiLFxyXG5cdFx0bmFtZTogXCJIZWFkbGluZWVlZWUyMFwiLFxyXG5cdFx0ZGVzY3JpcHRpb246IFwiXCIsXHJcblx0XHR0aHVtYm5haWw6IFwiLi4vZGlzdC9yZXNvdXJjZXMvaW1hZ2VzL3Jlc3VsdC1wYWdlL2luZHVzdHJ5LWhvdGVsLmpwZ1wiLFxyXG5cdFx0bGluazogXCJcIlxyXG5cdH0sIHtcclxuXHRcdGlkOiBcIm45XCIsXHJcblx0XHRuYW1lOiBcIkhlYWRsaW5lZWVlZVwiLFxyXG5cdFx0ZGVzY3JpcHRpb246IFwiXCIsXHJcblx0XHR0aHVtYm5haWw6IFwiLi4vZGlzdC9yZXNvdXJjZXMvaW1hZ2VzL3Jlc3VsdC1wYWdlL2luZHVzdHJ5LW90aGVyLmpwZ1wiLFxyXG5cdFx0bGluazogXCJcIlxyXG5cdH0sIHtcclxuXHRcdGlkOiBcIm4xMFwiLFxyXG5cdFx0bmFtZTogXCJIZWFkbGluZWVlZWVcIixcclxuXHRcdGRlc2NyaXB0aW9uOiBcIlwiLFxyXG5cdFx0dGh1bWJuYWlsOiBcIi4uL2Rpc3QvcmVzb3VyY2VzL2ltYWdlcy9yZXN1bHQtcGFnZS9pbmR1c3RyeS11c2VkLWF1dG9tb3RpdmUuanBnXCIsXHJcblx0XHRsaW5rOiBcIlwiXHJcblx0fSwge1xyXG5cdFx0aWQ6IFwiazFcIixcclxuXHRcdG5hbWU6IFwiSGVhZGxpbmVlZWVlXCIsXHJcblx0XHRkZXNjcmlwdGlvbjogXCIqIFN1bnQgc3VzY2lwaXQsIG1vbGVzdGlhZSB0ZW1wb3JhIGVzdCByZXJ1bSBxdWFlIHRlbmV0dXIgaW5jaWR1bnQgbGF1ZGFudGl1bT8gUXVvZCByZWljaWVuZGlzIHF1byBtb2xlc3RpYXMuXCIsXHJcblx0XHR0aHVtYm5haWw6IFwiLi4vZGlzdC9yZXNvdXJjZXMvaW1hZ2VzL3Jlc3VsdC1wYWdlL2luZHVzdHJ5LXVzZWQtYXV0b21vdGl2ZS5qcGdcIixcclxuXHRcdGxpbms6IFwiI1wiXHJcblx0fV1cclxuXHJcblx0aWYgKGdldFVybFZhcnMoKVsnc2NvcmUnXSAhPSBudWxsICYmIGdldFVybFZhcnMoKVsnbGV2ZWwnXSAhPSBudWxsICYmIGdldFVybFZhcnMoKVsnbmFtZSddICE9IG51bGwpIHtcclxuXHRcdHZhciBzY29yZSA9IGdldFVybFZhcnMoKVsnc2NvcmUnXS5zcGxpdCgnLCcpO1xyXG5cdFx0dmFyIHJlc3VsdDAxID0gMDtcclxuXHRcdHZhciByZXN1bHQwMiA9IDA7XHJcblx0XHR2YXIgcmVzdWx0MDMgPSAwO1xyXG5cdFx0dmFyIHJlc3VsdDA0ID0gMDtcclxuXHRcdHZhciByZXN1bHQwNSA9IDA7XHJcblxyXG5cdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCBzY29yZS5sZW5ndGg7IGkrKykge1xyXG5cdFx0XHRpZiAoc2NvcmVbaV0uaW5jbHVkZXMoJzA6JykpIHtcclxuXHRcdFx0XHQvL3Jlc3VsdDAxID0gcGFyc2VJbnQoc2NvcmVbaV0uc3BsaXQoJzonKVsxXSk7XHJcblx0XHRcdFx0cmVzdWx0MDEgPSAxMDA7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlmIChzY29yZVtpXS5pbmNsdWRlcygnMTonKSkge1xyXG5cdFx0XHRcdC8vcmVzdWx0MDIgPSBwYXJzZUludChzY29yZVtpXS5zcGxpdCgnOicpWzFdKTtcclxuXHRcdFx0XHRyZXN1bHQwMiA9IDEwMDtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0aWYgKHNjb3JlW2ldLmluY2x1ZGVzKCcyOicpKSB7XHJcblx0XHRcdFx0Ly9yZXN1bHQwMyA9IHBhcnNlSW50KHNjb3JlW2ldLnNwbGl0KCc6JylbMV0pO1xyXG5cdFx0XHRcdHJlc3VsdDAzID0gMTAwO1xyXG5cdFx0XHR9XHJcblxyXG5cdFx0XHRpZiAoc2NvcmVbaV0uaW5jbHVkZXMoJzM6JykpIHtcclxuXHRcdFx0XHQvL3Jlc3VsdDA0ID0gcGFyc2VJbnQoc2NvcmVbaV0uc3BsaXQoJzonKVsxXSk7XHJcblx0XHRcdFx0cmVzdWx0MDQgPSAxMDA7XHJcblx0XHRcdH1cclxuXHJcblx0XHRcdGlmIChzY29yZVtpXS5pbmNsdWRlcygnNDonKSkge1xyXG5cdFx0XHRcdC8vcmVzdWx0MDUgPSBwYXJzZUludChzY29yZVtpXS5zcGxpdCgnOicpWzFdKTtcclxuXHRcdFx0XHRyZXN1bHQwNCA9IDYwO1xyXG5cdFx0XHR9XHJcblx0XHR9XHJcblxyXG5cdFx0JCgnLnlvdXJidXNpbmVzc2xldmVsJykuaHRtbChnZXRVcmxWYXJzKClbJ2xldmVsJ10udG9TdHJpbmcoKS50b1VwcGVyQ2FzZSgpKTtcclxuXHRcdCQoJy55b3VyYnVzaW5lc3NuYW1lJykuaHRtbCgn4oCYJyArIGRlY29kZVVSSUNvbXBvbmVudChnZXRVcmxWYXJzKClbJ25hbWUnXS50b1N0cmluZygpLnJlcGxhY2UoXCItXCIsIFwiIFwiKSkgKyAn4oCZJyk7XHJcblxyXG5cdFx0dmFyIGZpbmRJbmRleCA9IGxldmVsLmluZGV4T2YoZ2V0VXJsVmFycygpWydsZXZlbCddKTtcclxuXHRcdHZhciBmaW5kV29yZCA9IGxldmVsZGVzY1tmaW5kSW5kZXhdO1xyXG5cdFx0dmFyIGZpbmdJbWdUaHVtYm5haWwgPSBnZXRVcmxWYXJzKClbJ2xldmVsJ10udG9TdHJpbmcoKS50b0xvd2VyQ2FzZSgpO1xyXG5cdFx0JCgnLnlvdXJidXNpbmVzc2Rlc2MnKS5odG1sKGZpbmRXb3JkKTtcclxuXHRcdCQoJy55b3VyYnVzaW5lc3NJbWcnKS5odG1sKCc8aW1nIHNyYz1cIi4uL2Rpc3QvcmVzb3VyY2VzL2ltYWdlcy9yZXN1bHQtcGFnZS8nICsgZmluZ0ltZ1RodW1ibmFpbCArICcuanBnXCIgYWx0PVwiJyArIGZpbmdJbWdUaHVtYm5haWwgKyAnXCI+Jyk7XHJcblxyXG5cdFx0dmFyIHByb2R1Y3QgPSBnZXRVcmxWYXJzKClbJ3Byb2R1Y3QnXS5zcGxpdCgnLCcpXHJcblx0XHR2YXIgcHJvZHVjdDEgPSBbXVxyXG5cdFx0dmFyIHByb2R1Y3QyID0gW11cclxuXHRcdHZhciBwcm9kdWN0MyA9IFtdXHJcblx0XHR2YXIgcHJvZHVjdDQgPSBbXVxyXG5cdFx0dmFyIHByb2R1Y3Q1ID0gW11cclxuXHRcdHZhciBwcm9kdWN0NiA9IFtdXHJcblx0XHRmb3IgKHZhciBpID0gMDsgaSA8IHByb2R1Y3QubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0aWYgKHByb2R1Y3RbaV0uaW5jbHVkZXMoXCJkXCIpICYmIHByb2R1Y3QxLmluZGV4T2YocHJvZHVjdFtpXSkgPT0gLTEpIHtcclxuXHRcdFx0XHRpZiAocHJvZHVjdC5pbmRleE9mKFwiZDFcIikgIT0gLTEpIHtcclxuXHRcdFx0XHRcdHByb2R1Y3QxLnB1c2gocHJvZHVjdFtpXSlcclxuXHRcdFx0XHR9IGVsc2UgaWYgKChwcm9kdWN0LmluZGV4T2YoXCJkMlwiKSAhPSAtMSB8fCBwcm9kdWN0LmluZGV4T2YoXCJkM1wiKSAhPSAtMSkgJiYgcHJvZHVjdFtpXSAhPSAnZDcnKSB7XHJcblx0XHRcdFx0XHRwcm9kdWN0MS5wdXNoKHByb2R1Y3RbaV0pXHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9XHJcblx0XHRcdGlmIChwcm9kdWN0W2ldLmluY2x1ZGVzKFwiZVwiKSAmJiBwcm9kdWN0Mi5pbmRleE9mKHByb2R1Y3RbaV0pID09IC0xKSB7XHJcblx0XHRcdFx0cHJvZHVjdDIucHVzaChwcm9kdWN0W2ldKVxyXG5cdFx0XHR9XHJcblx0XHRcdGlmIChwcm9kdWN0W2ldLmluY2x1ZGVzKFwiaVwiKSAmJiBwcm9kdWN0My5pbmRleE9mKHByb2R1Y3RbaV0pID09IC0xKSB7XHJcblx0XHRcdFx0cHJvZHVjdDMucHVzaChwcm9kdWN0W2ldKVxyXG5cdFx0XHR9XHJcblx0XHRcdGlmIChwcm9kdWN0W2ldLmluY2x1ZGVzKFwicFwiKSAmJiBwcm9kdWN0NC5pbmRleE9mKHByb2R1Y3RbaV0pID09IC0xKSB7XHJcblx0XHRcdFx0cHJvZHVjdDQucHVzaChwcm9kdWN0W2ldKVxyXG5cdFx0XHR9XHJcblx0XHRcdGlmIChwcm9kdWN0W2ldLmluY2x1ZGVzKFwiblwiKSAmJiBwcm9kdWN0NS5pbmRleE9mKHByb2R1Y3RbaV0pID09IC0xKSB7XHJcblx0XHRcdFx0cHJvZHVjdDUucHVzaChwcm9kdWN0W2ldKVxyXG5cdFx0XHR9XHJcblx0XHRcdGlmIChwcm9kdWN0W2ldLmluY2x1ZGVzKFwia1wiKSAmJiBwcm9kdWN0Ni5pbmRleE9mKHByb2R1Y3RbaV0pID09IC0xKSB7XHJcblx0XHRcdFx0cHJvZHVjdDYucHVzaChwcm9kdWN0W2ldKVxyXG5cdFx0XHR9XHJcblx0XHR9XHJcblxyXG5cdFx0aWYgKHByb2R1Y3QxLmxlbmd0aCA+IDApIHtcclxuXHRcdFx0JChcIiNwYXlyb2xsZGl2XCIpLmh0bWwoXCJcIik7XHJcblx0XHRcdHZhciBodG1scHJvZHVjdDEgPSBcIlwiO1xyXG5cdFx0XHRmb3IgKHZhciBpID0gMDsgaSA8IHByb2R1Y3QxLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdFx0dmFyIG9iaiA9IHByb2R1Y3RzLmZpbHRlcihmdW5jdGlvbiAoaXRlbSkge1xyXG5cdFx0XHRcdFx0aWYgKGl0ZW0uaWQgPT0gcHJvZHVjdDFbaV0pIHtcclxuXHRcdFx0XHRcdFx0cmV0dXJuIGl0ZW1cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9KVxyXG5cdFx0XHRcdGh0bWxwcm9kdWN0MSArPSAnPGRpdiBjbGFzcz1cImRldGFpbC1ib3hcIj4nO1xyXG5cdFx0XHRcdGh0bWxwcm9kdWN0MSArPSAnXHQ8ZGl2IGNsYXNzPVwiZGV0YWlsLXRpdGxlIHRleHQtZW5ib2xkIHdoaXRlXCI+JyArIG9ialswXS5uYW1lICsgJzwvZGl2Pic7XHJcblx0XHRcdFx0aHRtbHByb2R1Y3QxICs9ICdcdDxkaXYgY2xhc3M9XCJkZXRhaWwtZGVzYyB0ZXh0LXBzbGJvbGQgd2hpdGVcIj4nO1xyXG5cdFx0XHRcdGh0bWxwcm9kdWN0MSArPSBvYmpbMF0uZGVzY3JpcHRpb247XHJcblx0XHRcdFx0aHRtbHByb2R1Y3QxICs9ICdcdDwvZGl2Pic7XHJcblx0XHRcdFx0aHRtbHByb2R1Y3QxICs9ICdcdDxhIGhyZWY9XCInICsgb2JqWzBdLmxpbmsgKyAnXCIgdGFyZ2V0PVwiX2JsYW5rXCIgY2xhc3M9XCJtb3JlLWluZm8gd2hpdGUgdGV4dC1wc2xib2xkIHRleHQtY2VudGVyXCI+4LiU4Li54LmA4Lie4Li04LmI4Lih4LmA4LiV4Li04LihPC9hPic7XHJcblx0XHRcdFx0aHRtbHByb2R1Y3QxICs9ICdcdDxkaXYgY2xhc3M9XCJwYXJ0bmVyXCI+PGltZyBzcmM9XCInICsgb2JqWzBdLnRodW1ibmFpbCArICdcIiBhbHQ9XCJwYXJ0bmVyXCI+PC9kaXY+JztcclxuXHRcdFx0XHRodG1scHJvZHVjdDEgKz0gJzwvZGl2Pic7XHJcblx0XHRcdH1cclxuXHRcdFx0JChcIiNwYXlyb2xsZGl2XCIpLmh0bWwoaHRtbHByb2R1Y3QxKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdCQoXCIjaW52ZXN0XCIpLnJlbW92ZSgpO1xyXG5cdFx0fVxyXG5cclxuXHRcdGlmIChwcm9kdWN0Mi5sZW5ndGggPiAwKSB7XHJcblx0XHRcdCQoXCIjaW52ZXN0ZGl2XCIpLmh0bWwoXCJcIik7XHJcblx0XHRcdHZhciBodG1scHJvZHVjdDIgPSBcIlwiO1xyXG5cdFx0XHRmb3IgKHZhciBpID0gMDsgaSA8IHByb2R1Y3QyLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdFx0dmFyIG9iaiA9IHByb2R1Y3RzLmZpbHRlcihmdW5jdGlvbiAoaXRlbSkge1xyXG5cdFx0XHRcdFx0aWYgKGl0ZW0uaWQgPT0gcHJvZHVjdDJbaV0pIHtcclxuXHRcdFx0XHRcdFx0cmV0dXJuIGl0ZW1cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9KVxyXG5cdFx0XHRcdGh0bWxwcm9kdWN0MiArPSAnPGRpdiBjbGFzcz1cImRldGFpbC1ib3hcIj4nO1xyXG5cdFx0XHRcdGh0bWxwcm9kdWN0MiArPSAnXHQ8ZGl2IGNsYXNzPVwiZGV0YWlsLXRpdGxlIHRleHQtZW5ib2xkIHdoaXRlXCI+JyArIG9ialswXS5uYW1lICsgJzwvZGl2Pic7XHJcblx0XHRcdFx0aHRtbHByb2R1Y3QyICs9ICdcdDxkaXYgY2xhc3M9XCJkZXRhaWwtZGVzYyB0ZXh0LXBzbGJvbGQgd2hpdGVcIj4nO1xyXG5cdFx0XHRcdGh0bWxwcm9kdWN0MiArPSBvYmpbMF0uZGVzY3JpcHRpb247XHJcblx0XHRcdFx0aHRtbHByb2R1Y3QyICs9ICdcdDwvZGl2Pic7XHJcblx0XHRcdFx0aHRtbHByb2R1Y3QyICs9ICdcdDxhIGhyZWY9XCInICsgb2JqWzBdLmxpbmsgKyAnXCIgdGFyZ2V0PVwiX2JsYW5rXCIgY2xhc3M9XCJtb3JlLWluZm8gd2hpdGUgdGV4dC1wc2xib2xkIHRleHQtY2VudGVyXCI+4LiU4Li54LmA4Lie4Li04LmI4Lih4LmA4LiV4Li04LihPC9hPic7XHJcblx0XHRcdFx0aHRtbHByb2R1Y3QyICs9ICdcdDxkaXYgY2xhc3M9XCJwYXJ0bmVyXCI+PGltZyBzcmM9XCInICsgb2JqWzBdLnRodW1ibmFpbCArICdcIiBhbHQ9XCJwYXJ0bmVyXCI+PC9kaXY+JztcclxuXHRcdFx0XHRodG1scHJvZHVjdDIgKz0gJzwvZGl2Pic7XHJcblx0XHRcdH1cclxuXHRcdFx0JChcIiNpbnZlc3RkaXZcIikuaHRtbChodG1scHJvZHVjdDIpO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0JChcIiNpbnZlc3RcIikucmVtb3ZlKCk7XHJcblx0XHR9XHJcblxyXG5cdFx0aWYgKHByb2R1Y3QzLmxlbmd0aCA+IDApIHtcclxuXHRcdFx0JChcIiNwcm90ZWN0ZGl2XCIpLmh0bWwoXCJcIik7XHJcblx0XHRcdHZhciBodG1scHJvZHVjdDMgPSBcIlwiO1xyXG5cdFx0XHRmb3IgKHZhciBpID0gMDsgaSA8IHByb2R1Y3QzLmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdFx0dmFyIG9iaiA9IHByb2R1Y3RzLmZpbHRlcihmdW5jdGlvbiAoaXRlbSkge1xyXG5cdFx0XHRcdFx0aWYgKGl0ZW0uaWQgPT0gcHJvZHVjdDNbaV0pIHtcclxuXHRcdFx0XHRcdFx0cmV0dXJuIGl0ZW1cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9KVxyXG5cdFx0XHRcdGh0bWxwcm9kdWN0MyArPSAnPGRpdiBjbGFzcz1cImRldGFpbC1ib3hcIj4nO1xyXG5cdFx0XHRcdGh0bWxwcm9kdWN0MyArPSAnXHQ8ZGl2IGNsYXNzPVwiZGV0YWlsLXRpdGxlIHRleHQtZW5ib2xkIHdoaXRlXCI+JyArIG9ialswXS5uYW1lICsgJzwvZGl2Pic7XHJcblx0XHRcdFx0aHRtbHByb2R1Y3QzICs9ICdcdDxkaXYgY2xhc3M9XCJkZXRhaWwtZGVzYyB0ZXh0LXBzbGJvbGQgd2hpdGVcIj4nO1xyXG5cdFx0XHRcdGh0bWxwcm9kdWN0MyArPSBvYmpbMF0uZGVzY3JpcHRpb247XHJcblx0XHRcdFx0aHRtbHByb2R1Y3QzICs9ICdcdDwvZGl2Pic7XHJcblx0XHRcdFx0aHRtbHByb2R1Y3QzICs9ICdcdDxhIGhyZWY9XCInICsgb2JqWzBdLmxpbmsgKyAnXCIgdGFyZ2V0PVwiX2JsYW5rXCIgY2xhc3M9XCJtb3JlLWluZm8gd2hpdGUgdGV4dC1wc2xib2xkIHRleHQtY2VudGVyXCI+4LiU4Li54LmA4Lie4Li04LmI4Lih4LmA4LiV4Li04LihPC9hPic7XHJcblx0XHRcdFx0aHRtbHByb2R1Y3QzICs9ICdcdDxkaXYgY2xhc3M9XCJwYXJ0bmVyXCI+PGltZyBzcmM9XCInICsgb2JqWzBdLnRodW1ibmFpbCArICdcIiBhbHQ9XCJwYXJ0bmVyXCI+PC9kaXY+JztcclxuXHRcdFx0XHRodG1scHJvZHVjdDMgKz0gJzwvZGl2Pic7XHJcblx0XHRcdH1cclxuXHRcdFx0JChcIiNwcm90ZWN0ZGl2XCIpLmh0bWwoaHRtbHByb2R1Y3QzKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdCQoXCIjcHJvdGVjdFwiKS5yZW1vdmUoKTtcclxuXHRcdH1cclxuXHJcblx0XHRpZiAocHJvZHVjdDQubGVuZ3RoID4gMCkge1xyXG5cdFx0XHQkKFwiI3BhcnRuZXJkaXZcIikuaHRtbChcIlwiKTtcclxuXHRcdFx0dmFyIGh0bWxwcm9kdWN0NCA9IFwiXCI7XHJcblx0XHRcdGZvciAodmFyIGkgPSAwOyBpIDwgcHJvZHVjdDQubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0XHR2YXIgb2JqID0gcHJvZHVjdHMuZmlsdGVyKGZ1bmN0aW9uIChpdGVtKSB7XHJcblx0XHRcdFx0XHRpZiAoaXRlbS5pZCA9PSBwcm9kdWN0NFtpXSkge1xyXG5cdFx0XHRcdFx0XHRyZXR1cm4gaXRlbVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0pXHJcblx0XHRcdFx0aHRtbHByb2R1Y3Q0ICs9ICc8ZGl2IGNsYXNzPVwiZGV0YWlsLWJveFwiPic7XHJcblx0XHRcdFx0aHRtbHByb2R1Y3Q0ICs9ICdcdDxkaXYgY2xhc3M9XCJkZXRhaWwtdGl0bGUgdGV4dC1lbmJvbGQgd2hpdGVcIj4nICsgb2JqWzBdLm5hbWUgKyAnPC9kaXY+JztcclxuXHRcdFx0XHRodG1scHJvZHVjdDQgKz0gJ1x0PGRpdiBjbGFzcz1cImRldGFpbC1kZXNjIHRleHQtcHNsYm9sZCB3aGl0ZVwiPic7XHJcblx0XHRcdFx0aHRtbHByb2R1Y3Q0ICs9IG9ialswXS5kZXNjcmlwdGlvbjtcclxuXHRcdFx0XHRodG1scHJvZHVjdDQgKz0gJ1x0PC9kaXY+JztcclxuXHRcdFx0XHRodG1scHJvZHVjdDQgKz0gJ1x0PGEgaHJlZj1cIicgKyBvYmpbMF0ubGluayArICdcIiB0YXJnZXQ9XCJfYmxhbmtcIiBjbGFzcz1cIm1vcmUtaW5mbyB3aGl0ZSB0ZXh0LXBzbGJvbGQgdGV4dC1jZW50ZXJcIj7guJTguLnguYDguJ7guLTguYjguKHguYDguJXguLTguKE8L2E+JztcclxuXHRcdFx0XHRodG1scHJvZHVjdDQgKz0gJ1x0PGRpdiBjbGFzcz1cInBhcnRuZXJcIj48aW1nIHNyYz1cIicgKyBvYmpbMF0udGh1bWJuYWlsICsgJ1wiIGFsdD1cInBhcnRuZXJcIj48L2Rpdj4nO1xyXG5cdFx0XHRcdGh0bWxwcm9kdWN0NCArPSAnPC9kaXY+JztcclxuXHRcdFx0fVxyXG5cdFx0XHQkKFwiI3BhcnRuZXJkaXZcIikuaHRtbChodG1scHJvZHVjdDQpO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0JChcIiNwYXJ0bmVyXCIpLnJlbW92ZSgpO1xyXG5cdFx0fVxyXG5cclxuXHRcdGlmIChwcm9kdWN0Ni5sZW5ndGggPiAwKSB7XHJcblx0XHRcdCQoXCIja25vd2xlZGdlZGl2XCIpLmh0bWwoXCJcIik7XHJcblx0XHRcdHZhciBodG1scHJvZHVjdDYgPSBcIlwiO1xyXG5cdFx0XHRmb3IgKHZhciBpID0gMDsgaSA8IHByb2R1Y3Q2Lmxlbmd0aDsgaSsrKSB7XHJcblx0XHRcdFx0dmFyIG9iaiA9IHByb2R1Y3RzLmZpbHRlcihmdW5jdGlvbiAoaXRlbSkge1xyXG5cdFx0XHRcdFx0aWYgKGl0ZW0uaWQgPT0gcHJvZHVjdDZbaV0pIHtcclxuXHRcdFx0XHRcdFx0cmV0dXJuIGl0ZW1cclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9KVxyXG5cdFx0XHRcdGh0bWxwcm9kdWN0NiArPSAnPGRpdiBjbGFzcz1cImRldGFpbC1ib3hcIj4nO1xyXG5cdFx0XHRcdGh0bWxwcm9kdWN0NiArPSAnXHQ8ZGl2IGNsYXNzPVwiZGV0YWlsLXRpdGxlIHRleHQtZW5ib2xkIHdoaXRlXCI+JyArIG9ialswXS5uYW1lICsgJzwvZGl2Pic7XHJcblx0XHRcdFx0aHRtbHByb2R1Y3Q2ICs9ICdcdDxkaXYgY2xhc3M9XCJkZXRhaWwtZGVzYyB0ZXh0LXBzbGJvbGQgd2hpdGVcIj4nO1xyXG5cdFx0XHRcdGh0bWxwcm9kdWN0NiArPSBvYmpbMF0uZGVzY3JpcHRpb247XHJcblx0XHRcdFx0aHRtbHByb2R1Y3Q2ICs9ICdcdDwvZGl2Pic7XHJcblx0XHRcdFx0aHRtbHByb2R1Y3Q2ICs9ICdcdDxhIGhyZWY9XCInICsgb2JqWzBdLmxpbmsgKyAnXCIgdGFyZ2V0PVwiX2JsYW5rXCIgY2xhc3M9XCJtb3JlLWluZm8gd2hpdGUgdGV4dC1wc2xib2xkIHRleHQtY2VudGVyXCI+4LiU4Li54LmA4Lie4Li04LmI4Lih4LmA4LiV4Li04LihPC9hPic7XHJcblx0XHRcdFx0aHRtbHByb2R1Y3Q2ICs9ICdcdDxkaXYgY2xhc3M9XCJwYXJ0bmVyXCI+PGltZyBzcmM9XCInICsgb2JqWzBdLnRodW1ibmFpbCArICdcIiBhbHQ9XCJwYXJ0bmVyXCI+PC9kaXY+JztcclxuXHRcdFx0XHRodG1scHJvZHVjdDYgKz0gJzwvZGl2Pic7XHJcblx0XHRcdH1cclxuXHRcdFx0JChcIiNrbm93bGVkZ2VkaXZcIikuaHRtbChodG1scHJvZHVjdDYpO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0JChcIiNrbm93bGVkZ2VcIikucmVtb3ZlKCk7XHJcblx0XHR9XHJcblxyXG5cdFx0aWYgKHByb2R1Y3Q1Lmxlbmd0aCA+IDApIHtcclxuXHJcblx0XHRcdHZhciBvYmogPSBwcm9kdWN0cy5maWx0ZXIoZnVuY3Rpb24gKGl0ZW0pIHtcclxuXHRcdFx0XHRpZiAoaXRlbS5pZCA9PSBwcm9kdWN0NVswXSkge1xyXG5cdFx0XHRcdFx0cmV0dXJuIGl0ZW1cclxuXHRcdFx0XHR9XHJcblx0XHRcdH0pXHJcblxyXG5cdFx0XHR2YXIgaHRtbHByb2R1Y3Q1ID0gXCJcIjtcclxuXHJcblx0XHRcdGh0bWxwcm9kdWN0NSArPSAnPGltZyBzcmM9XCInICsgb2JqWzBdLnRodW1ibmFpbCArICdcIiBhbHQ9XCInICsgb2JqWzBdLm5hbWUgKyAnXCI+JztcclxuXHRcdFx0aHRtbHByb2R1Y3Q1ICs9ICc8ZGl2IGNsYXNzPVwiYnVzaW5lc3N0aXRsZSB0ZXh0LWVuYm9sZCB3aGl0ZSBleHRyYWxhcmdlXCI+JztcclxuXHRcdFx0aHRtbHByb2R1Y3Q1ICs9IG9ialswXS5uYW1lO1xyXG5cdFx0XHRodG1scHJvZHVjdDUgKz0gJzwvZGl2Pic7XHJcblxyXG5cdFx0XHQkKFwiLnN1YnNjcmliXCIpLmh0bWwoaHRtbHByb2R1Y3Q1KTtcclxuXHRcdH1cclxuXHJcblx0fSBlbHNlIHtcclxuXHRcdHdpbmRvdy5sb2NhdGlvbiA9ICdsYW5kaW5nLmh0bWwnO1xyXG5cdH1cclxuXHJcblx0c2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcblx0XHRDaGFydC5kZWZhdXRcclxuXHRcdHZhciBncmFwaEhlaWdodCA9ICQoJyNteUNoYXJ0JykuaGVpZ2h0KCk7XHJcblx0XHR2YXIgcGFkZGluZ1k7XHJcblx0XHRpZiAoJCh3aW5kb3cpLndpZHRoKCkgPCA0ODApIHtcclxuXHRcdFx0cGFkZGluZ1kgPSAoZ3JhcGhIZWlnaHQgKiA3LjUpIC8gMTAwO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0cGFkZGluZ1kgPSAoZ3JhcGhIZWlnaHQgKiAyMCkgLyAxMDA7XHJcblx0XHR9XHJcblxyXG5cdFx0dmFyIGNvbmZpZyA9IHtcclxuXHRcdFx0dHlwZTogJ3JhZGFyJyxcclxuXHRcdFx0ZGF0YToge1xyXG5cdFx0XHRcdGxhYmVsczogWycnLCAnJywgJycsICcnLCAnJ10sXHJcblx0XHRcdFx0ZGF0YXNldHM6IFt7XHJcblx0XHRcdFx0XHRsYWJlbDogJycsXHJcblx0XHRcdFx0XHRiYWNrZ3JvdW5kQ29sb3I6IHJhZGFyX2dyYWRpZW50LFxyXG5cdFx0XHRcdFx0cG9pbnRCYWNrZ3JvdW5kQ29sb3I6IFtcclxuXHRcdFx0XHRcdFx0J3JnYigyNTUsIDI1NSwgMjU1KScsXHJcblx0XHRcdFx0XHRcdCdyZ2IoMjU1LCAyNTUsIDI1NSknLFxyXG5cdFx0XHRcdFx0XHQncmdiKDI1NSwgMjU1LCAyNTUpJyxcclxuXHRcdFx0XHRcdFx0J3JnYigyNTUsIDI1NSwgMjU1KScsXHJcblx0XHRcdFx0XHRcdCdyZ2IoMjU1LCAyNTUsIDI1NSknXHJcblx0XHRcdFx0XHRdLFxyXG5cdFx0XHRcdFx0ZGF0YTogW1xyXG5cdFx0XHRcdFx0XHRyZXN1bHQwMSwgcmVzdWx0MDUsIHJlc3VsdDAzLCByZXN1bHQwNCwgcmVzdWx0MDJcclxuXHRcdFx0XHRcdF1cclxuXHRcdFx0XHR9XVxyXG5cdFx0XHR9LFxyXG5cdFx0XHRvcHRpb25zOiB7XHJcblx0XHRcdFx0bGVnZW5kOiB7XHJcblx0XHRcdFx0XHRkaXNwbGF5OiBmYWxzZSxcclxuXHRcdFx0XHRcdHBvc2l0aW9uOiAnY2VudGVyJ1xyXG5cdFx0XHRcdH0sXHJcblx0XHRcdFx0dGl0bGU6IHtcclxuXHRcdFx0XHRcdGRpc3BsYXk6IGZhbHNlLFxyXG5cdFx0XHRcdFx0dGV4dDogJydcclxuXHRcdFx0XHR9LFxyXG5cdFx0XHRcdHRvb2x0aXBzOiB7XHJcblx0XHRcdFx0XHRlbmFibGVkOiB0cnVlLFxyXG5cdFx0XHRcdFx0dGl0bGVGb250RmFtaWx5OiAnS2l0dGl0aGFkYUVyZ29fbGlnaHQnLFxyXG5cdFx0XHRcdFx0dGl0bGVGb250U2l6ZTogMTgsXHJcblx0XHRcdFx0XHRib2R5Rm9udEZhbWlseTogJ0tpdHRpdGhhZGFFcmdvX2xpZ2h0JyxcclxuXHRcdFx0XHRcdGJvZHlGb250U2l6ZTogMjIsXHJcblx0XHRcdFx0XHRjYWxsYmFja3M6IHtcclxuXHRcdFx0XHRcdFx0dGl0bGU6IGZ1bmN0aW9uICgpIHtcclxuXHRcdFx0XHRcdFx0XHRyZXR1cm4gJ+C4hOC4sOC5geC4meC4meC4l+C4teC5iOC5hOC4lOC5iSdcclxuXHRcdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdFx0bGFiZWw6IGZ1bmN0aW9uICh0b29sdGlwSXRlbSwgZGF0YSkge1xyXG5cdFx0XHRcdFx0XHRcdHJldHVybiBkYXRhLmxhYmVsc1t0b29sdGlwSXRlbS5pbmRleF0gKyAnIDogJyArIHRvb2x0aXBJdGVtLnlMYWJlbCArICcgJSdcclxuXHRcdFx0XHRcdFx0fVxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH0sXHJcblx0XHRcdFx0c2NhbGU6IHtcclxuXHRcdFx0XHRcdHRpY2tzOiB7XHJcblx0XHRcdFx0XHRcdGRpc3BsYXk6IGZhbHNlLFxyXG5cdFx0XHRcdFx0XHRiZWdpbkF0WmVybzogdHJ1ZSxcclxuXHRcdFx0XHRcdFx0bWF4OiAxMDAsXHJcblx0XHRcdFx0XHRcdHN0ZXBTaXplOiAyMFxyXG5cdFx0XHRcdFx0fSxcclxuXHRcdFx0XHRcdHBvaW50TGFiZWxzOiB7XHJcblx0XHRcdFx0XHRcdGZvbnRDb2xvcjogJyNmZmZmZmYnLFxyXG5cdFx0XHRcdFx0XHRmb250U2l6ZTogMjAsXHJcblx0XHRcdFx0XHRcdGZvbnRGYW1pbHk6ICdLaXR0aXRoYWRhRXJnb19saWdodCdcclxuXHRcdFx0XHRcdH0sXHJcblx0XHRcdFx0XHRhbmdsZUxpbmVzOiB7XHJcblx0XHRcdFx0XHRcdGNvbG9yOiAnI2M5YzljOScsXHJcblx0XHRcdFx0XHRcdGxpbmVXaWR0aDogMS41XHJcblx0XHRcdFx0XHR9LFxyXG5cdFx0XHRcdFx0Z3JpZExpbmVzOiB7XHJcblx0XHRcdFx0XHRcdGNvbG9yOiAnI2M5YzljOScsXHJcblx0XHRcdFx0XHRcdGxpbmVXaWR0aDogMS41XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSxcclxuXHRcdFx0XHRsYXlvdXQ6IHtcclxuXHRcdFx0XHRcdHBhZGRpbmc6IHtcclxuXHRcdFx0XHRcdFx0bGVmdDogMCxcclxuXHRcdFx0XHRcdFx0cmlnaHQ6IDAsXHJcblx0XHRcdFx0XHRcdHRvcDogcGFkZGluZ1ksXHJcblx0XHRcdFx0XHRcdGJvdHRvbTogMFxyXG5cdFx0XHRcdFx0fVxyXG5cdFx0XHRcdH1cclxuXHRcdFx0fVxyXG5cdFx0fVxyXG5cdFx0d2luZG93Lm15UmFkYXIgPSBuZXcgQ2hhcnQoY3R4LCBjb25maWcpXHJcblx0XHRjb25zb2xlLmxvZyhcImNvbmZpZ1wiLCBjb25maWcpXHJcblx0XHRjb25zb2xlLmxvZyhcIm15UmFkYXIgXCIsIHdpbmRvdy5teVJhZGFyKVxyXG5cdH0sIDIwMDApXHJcblxyXG5cclxufSlcclxuXHJcbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uICgpIHtcclxuXHQkKFwiI2Fycm93LWRvd24gLmRvd25cIikub24oXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XHJcblx0XHQkKFwiaHRtbCwgYm9keVwiKS5kZWxheSgxMDApLmFuaW1hdGUoe1xyXG5cdFx0XHRzY3JvbGxUb3A6ICQoXCIucmVzdWx0LWluZ3JlZGllbnRcIikub2Zmc2V0KCkudG9wIC0gMTEwXHJcblx0XHR9LCAxNTAwKTtcclxuXHR9KTtcclxuXHQkKFwiLnByb2R1Y3QtYm94LWpzXCIpLmVhY2goZnVuY3Rpb24gKCkge1xyXG5cdFx0dmFyIGRldGFpbENvdW50ID0gJCh0aGlzKS5maW5kKFwiLndyYXBwZXItYm94IC5kZXRhaWwtYm94XCIpLmxlbmd0aDtcclxuXHRcdGlmIChkZXRhaWxDb3VudCA+IDMpIHtcclxuXHRcdFx0JCh0aGlzKS5maW5kKFwiLndyYXBwZXItYm94XCIpLmFkZENsYXNzKFwiZ3JpZFwiKTtcclxuXHRcdH0gZWxzZSB7XHJcblx0XHRcdCQodGhpcykuZmluZChcIi53cmFwcGVyLWJveFwiKS5hZGRDbGFzcyhcImZsZXhcIik7XHJcblx0XHR9XHJcblx0fSlcclxuXHJcblx0JChcIiNnb3RvdG9wXCIpLm9uKFwiY2xpY2tcIiwgZnVuY3Rpb24gKCkge1xyXG5cdFx0JChcImh0bWwsIGJvZHlcIikuZGVsYXkoMTAwKS5hbmltYXRlKHtcclxuXHRcdFx0c2Nyb2xsVG9wOiAwXHJcblx0XHR9LCAyNzAwKTtcclxuXHR9KTtcclxuXHQkKFwiLnJlc3VsdC1wcm9kdWN0IC5kZXRhaWwtYm94IC5kZXRhaWwtdGl0bGVcIikuYWRkQ2xhc3MoXCJleHRyYWxhcmdlXCIpO1xyXG59KVxyXG5cclxuZnVuY3Rpb24gZ2V0VXJsVmFycygpIHtcclxuXHR2YXIgdmFycyA9IHt9O1xyXG5cdHZhciBwYXJ0cyA9IHdpbmRvdy5sb2NhdGlvbi5ocmVmLnJlcGxhY2UoL1s/Jl0rKFtePSZdKyk9KFteJl0qKS9naSwgZnVuY3Rpb24gKG0sIGtleSwgdmFsdWUpIHtcclxuXHRcdHZhcnNba2V5XSA9IHZhbHVlO1xyXG5cdH0pO1xyXG5cdHJldHVybiB2YXJzO1xyXG59XHJcblxyXG5mdW5jdGlvbiBkcmF3X291dHNpZGVfbGFiZWwoKSB7XHJcblx0Q2hhcnQuZGVmYXVsdHMuZGVyaXZlZFJhZGFyID0gQ2hhcnQuZGVmYXVsdHMucmFkYXI7XHJcblx0dmFyIGN1c3RvbSA9IENoYXJ0LmNvbnRyb2xsZXJzLnJhZGFyLmV4dGVuZCh7XHJcblx0XHRkcmF3OiBmdW5jdGlvbiAoZWFzZSkge1xyXG5cclxuXHRcdFx0Q2hhcnQuY29udHJvbGxlcnMucmFkYXIucHJvdG90eXBlLmRyYXcuY2FsbCh0aGlzLCBlYXNlKTtcclxuXHJcblx0XHRcdHZhciBtZXRhID0gdGhpcy5jaGFydC5jaGFydC5nZXREYXRhc2V0TWV0YSgwKS5kYXRhO1xyXG5cdFx0XHR2YXIgZGF0YXNldHMgPSB0aGlzLmNoYXJ0LmNoYXJ0O1xyXG5cdFx0XHR2YXIgY3R4ID0gdGhpcy5jaGFydC5jaGFydC5jdHg7XHJcblx0XHRcdHZhciBncmFwaFdpZHRoID0gZGF0YXNldHMud2lkdGg7XHJcblx0XHRcdHZhciBncmFwaEhlaWdodCA9IGRhdGFzZXRzLmhlaWdodDtcclxuXHRcdFx0Y29uc29sZS5sb2coZ3JhcGhXaWR0aClcclxuXHRcdFx0Zm9yICh2YXIgaSA9IDA7IGkgPCB0aGlzLmNoYXJ0LmNoYXJ0LmdldERhdGFzZXRNZXRhKDApLmRhdGEubGVuZ3RoOyBpKyspIHtcclxuXHRcdFx0XHRpZiAoaSA9PSAwKSB7XHJcblx0XHRcdFx0XHR2YXIgeCA9ICgoZ3JhcGhXaWR0aCAqIDUwKSAvIDEwMCk7XHJcblx0XHRcdFx0XHR2YXIgeSA9IChncmFwaEhlaWdodCAqIDApIC8gMTAwO1xyXG5cdFx0XHRcdFx0dmFyIGltZyA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKFwiYmFua1wiKTtcclxuXHRcdFx0XHRcdGlmICgkKHdpbmRvdykud2lkdGgoKSA8IDQ4MCkge1xyXG5cdFx0XHRcdFx0XHRjdHguZHJhd0ltYWdlKGltZywgeCAtIDIwLCB5IC0gMiwgNDUsIDQ1KTtcclxuXHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdGN0eC5kcmF3SW1hZ2UoaW1nLCB4IC0gMzAsIHkgLSA1LCA3MCwgNzApO1xyXG5cdFx0XHRcdFx0fVxyXG5cclxuXHRcdFx0XHR9IGVsc2UgaWYgKGkgPT0gMSkge1xyXG5cdFx0XHRcdFx0dmFyIHggPSAoKGdyYXBoV2lkdGggKiAyNSkgLyAxMDApO1xyXG5cdFx0XHRcdFx0dmFyIHkgPSAoZ3JhcGhIZWlnaHQgKiA3Ny41KSAvIDEwMDtcclxuXHRcdFx0XHRcdHZhciBpbWcgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImdyYXBoXCIpO1xyXG5cdFx0XHRcdFx0aWYgKCQod2luZG93KS53aWR0aCgpIDwgNDgwKSB7XHJcblx0XHRcdFx0XHRcdGN0eC5kcmF3SW1hZ2UoaW1nLCB4IC0gMTAsIHkgLSA3MCwgNDUsIDQ1KTtcclxuXHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdGN0eC5kcmF3SW1hZ2UoaW1nLCB4IC0gMjUsIHkgLSAyMDAsIDcwLCA3MCk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fSBlbHNlIGlmIChpID09IDIpIHtcclxuXHRcdFx0XHRcdHZhciB4ID0gKChncmFwaFdpZHRoICogNzApIC8gMTAwKTtcclxuXHRcdFx0XHRcdHZhciB5ID0gKGdyYXBoSGVpZ2h0ICogNzcuNSkgLyAxMDA7XHJcblx0XHRcdFx0XHR2YXIgaW1nID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoXCJzaGllbGRcIik7XHJcblx0XHRcdFx0XHRpZiAoJCh3aW5kb3cpLndpZHRoKCkgPCA0ODApIHtcclxuXHRcdFx0XHRcdFx0Y3R4LmRyYXdJbWFnZShpbWcsIHggLSAxOCwgeSAtIDcwLCA0NSwgNDUpO1xyXG5cdFx0XHRcdFx0fSBlbHNlIHtcclxuXHRcdFx0XHRcdFx0Y3R4LmRyYXdJbWFnZShpbWcsIHggKyAxMCwgeSAtIDIwMCwgNzAsIDcwKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9IGVsc2UgaWYgKGkgPT0gMykge1xyXG5cdFx0XHRcdFx0dmFyIHggPSAoKGdyYXBoV2lkdGggKiAzMCkgLyAxMDApO1xyXG5cdFx0XHRcdFx0dmFyIHkgPSAoZ3JhcGhIZWlnaHQgKiA3Ny41KSAvIDEwMDtcclxuXHRcdFx0XHRcdHZhciBpbWcgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcImJyYWluXCIpO1xyXG5cdFx0XHRcdFx0aWYgKCQod2luZG93KS53aWR0aCgpIDwgNDgwKSB7XHJcblx0XHRcdFx0XHRcdGN0eC5kcmF3SW1hZ2UoaW1nLCB4IC0gNSwgeSwgNDUsIDQ1KTtcclxuXHRcdFx0XHRcdH0gZWxzZSB7XHJcblx0XHRcdFx0XHRcdGN0eC5kcmF3SW1hZ2UoaW1nLCB4IC0gMTAsIHkgKyAyMCwgNzAsIDcwKTtcclxuXHRcdFx0XHRcdH1cclxuXHRcdFx0XHR9IGVsc2UgaWYgKGkgPT0gNCkge1xyXG5cdFx0XHRcdFx0dmFyIHggPSAoKGdyYXBoV2lkdGggKiA2MCkgLyAxMDApO1xyXG5cdFx0XHRcdFx0dmFyIHkgPSAoZ3JhcGhIZWlnaHQgKiA3Ny41KSAvIDEwMDtcclxuXHRcdFx0XHRcdHZhciBpbWcgPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZChcInBlb3BsZVwiKTtcclxuXHRcdFx0XHRcdGlmICgkKHdpbmRvdykud2lkdGgoKSA8IDQ4MCkge1xyXG5cdFx0XHRcdFx0XHRjdHguZHJhd0ltYWdlKGltZywgeCArIDIsIHksIDQ1LCA0NSk7XHJcblx0XHRcdFx0XHR9IGVsc2Uge1xyXG5cdFx0XHRcdFx0XHRjdHguZHJhd0ltYWdlKGltZywgeCArIDQwLCB5ICsgMjAsIDcwLCA3MCk7XHJcblx0XHRcdFx0XHR9XHJcblx0XHRcdFx0fVxyXG5cclxuXHRcdFx0fVxyXG5cclxuXHRcdH1cclxuXHR9KTtcclxuXHRDaGFydC5jb250cm9sbGVycy5kZXJpdmVkUmFkYXIgPSBjdXN0b207XHJcblxyXG59Il0sImZpbGUiOiJyZXN1bHQtcGFnZS5qcyJ9
