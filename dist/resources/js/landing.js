var checkAvailable;
var overallScore = [];
var productArray = []; // Product will show at result page
var checkLevel = "";
var businessName = "";
var digitalBusinessScore = 0;
var q3Score = 0;
var industry = ""; 
 
var questions = [{
    type: "choice",
    maxchoice: 1,
    title: "BUBBLE ที่คุณชอบเป็นแบบไหน?<br/><span class=\"sbttl\">(หากBUBBLEคือ BUBBLE 5 ข้อการเลือกสีให้คุณโดยอัตโนมัติ)</span>",
    colorelm: 'radial-gradient(ellipse at 6% 20%, #1bc579 12%,#722ab8 75%)',
    answers: [{
            title: "BUBBLE 1",
            desktoptopleft: "50%,23%",
            mobiletopleft: "28%,46%",
            bubblestylescale: "b1,180",
            productrelate: []
        },
        {
            title: "BUBBLE 2",
            desktoptopleft: "39%,42%",
            mobiletopleft: "35%,90%",
            bubblestylescale: "b5,165",
            productrelate: []
        },
        {
            title: "BUBBLE 3",
            desktoptopleft: "55%,55%",
            mobiletopleft: "48%,35%",
            bubblestylescale: "b3,170",
            productrelate: []
        },
        {
            title: "BUBBLE 4",
            desktoptopleft: "40%,85%",
            mobiletopleft: "70%,40%",
            bubblestylescale: "b4,150",
            productrelate: []
        },
        {
            title: "BUBBLE 5",
            desktoptopleft: "48%,70%",
            mobiletopleft: "60%,80%",
            bubblestylescale: "b2,210",
            productrelate: ['p2', 'n5']
        }
    ],
    currentanswer: "",

},
{
    type: "choice",
    maxchoice: 1,
    title: "สีของคุณคือ..",
    colorelm: 'linear-gradient(140deg, #fea924 12%, #e72ba2 60%)',
    answers: [{
            title: "แดง",
            desktoptopleft: "30%,20%",
            mobiletopleft: "30%,30%",
            bubblestylescale: "b2,190",
            productrelate: ["n1"]
        },
        {
            title: "เขียว",
            desktoptopleft: "33%,43%",
            mobiletopleft: "25%,65%",
            bubblestylescale: "b4,170",
            productrelate: ["n2"]
        },
        {
            title: "เหลือง",
            desktoptopleft: "30%,85%",
            mobiletopleft: "30%,95%",
            bubblestylescale: "b6,150",
            productrelate: ["n3"]
        },
        {
            title: "ส้ม",
            desktoptopleft: "60%,20%",
            mobiletopleft: "45%,30%",
            bubblestylescale: "b3,190",
            productrelate: ["n4"]
        },
        {
            title: "ชมพูววว",
            desktoptopleft: "46%,31%",
            mobiletopleft: "41%,60%",
            bubblestylescale: "b5,160",
            productrelate: ["n5"]
        },
        {
            title: "น้ำตาล",
            desktoptopleft: "33%,60%",
            mobiletopleft: "45%,95%",
            bubblestylescale: "b1,165",
            productrelate: ["n6"]
        },
        {
            title: "น้ำเงิน",
            desktoptopleft: "60%,50%",
            mobiletopleft: "66%,32%",
            bubblestylescale: "b4,170",
            productrelate: ["n7"]
        },
        {
            title: "ทอง",
            desktoptopleft: "67%,37%",
            mobiletopleft: "55%,60%",
            bubblestylescale: "b3,140",
            productrelate: ["n8"]
        },
        {
            title: "ไม่มีสี",
            desktoptopleft: "60%,64%",
            mobiletopleft: "65%,95%",
            bubblestylescale: "b2,160",
            productrelate: ["n9"]
        },
        {
            title: "ผสมผสาน",
            desktoptopleft: "61%,84%",
            mobiletopleft: "75%,70%",
            bubblestylescale: "b1,230",
            productrelate: ["n10"]
        }
    ],
    currentanswer: ""
},
{
    type: "choice",
    maxchoice: 1,
    title: "Where is your BUBBLE stay?",
    colorelm: 'radial-gradient(ellipse at 6% 20%, #df39a2 12%,rgb(78, 29, 111) 63%, #384186 103%)',
    answers: [{
            title: "SKY",
            desktoptopleft: "55%,32%",
            mobiletopleft: "29%,60%",
            bubblestylescale: "b2,180",
            productrelate: []
        },
        {
            title: "INTERNET",
            desktoptopleft: "33%,52%",
            mobiletopleft: "47%,35%",
            bubblestylescale: "b5,180",
            productrelate: ['d4']
        },
        {
            title: "HOME",
            desktoptopleft: "57%,68%",
            mobiletopleft: "64%,80%",
            bubblestylescale: "b4,210",
            productrelate: ['d4']
        }
    ],
    currentanswer: "",

},
{
    type: "choice",
    maxchoice: 1,
    title: "How BUBBLE prefer to drink?",
    colorelm: 'radial-gradient(ellipse at 6% 10%, #fa3060 12%, #2e73ff 90%)',
    answers: [{
            title: "Cola",
            desktoptopleft: "55%,32%",
            mobiletopleft: "32%,83%",
            bubblestylescale: "b2,180",
            productrelate: ['d1']
        },
        {
            title: "Fanta",
            desktoptopleft: "33%,52%",
            mobiletopleft: "47%,38%",
            bubblestylescale: "b5,205",
            productrelate: ['d2']
        },
        {
            title: "Water",
            desktoptopleft: "57%,68%",
            mobiletopleft: "71%,72%",
            bubblestylescale: "b4,220",
            productrelate: ['d3']
        }
    ],
    currentanswer: "",

},
{
    type: "choice",
    maxchoice: 5,
    title: "BUBBLE say?<br/><span class=\"sbttl\">(เลือกตอบได้มากกว่า 1)</span>",
    colorelm: 'radial-gradient(at 6% 10%, #ed5c4a 12%, #5f2e5d 73%)',
    answers: [{
            title: "wowwww",
            desktoptopleft: "60%,20%",
            mobiletopleft: "31%,30%",
            bubblestylescale: "b1,160",
            productrelate: ['d6', 'd7']

        },
        {
            title: "ewwww",
            desktoptopleft: "36%,31%",
            mobiletopleft: "37%,90%",
            bubblestylescale: "b3,140",
            productrelate: ['d6', 'd7']

        },
        {
            title: "heyy",
            desktoptopleft: "45%,47%",
            mobiletopleft: "52%,39%",
            bubblestylescale: "b4,160",
            productrelate: ['d6', 'd7']

        },
        {
            title: "Ouchhhhh",
            desktoptopleft: "52%,80%",
            mobiletopleft: "60%,84%",
            bubblestylescale: "b2,210",
            productrelate: ['d6', 'd7']

        },
        {
            title: "...",
            desktoptopleft: "42%,65%",
            mobiletopleft: "76%,50%",
            bubblestylescale: "b2,185",
            productrelate: []

        }
    ],
    currentanswer: ""
},
{
    type: "choice",
    maxchoice: 6,
    title: "Personality of bubble <br/><span class=\"sbttl\">(เลือกตอบได้มากกว่า 1)</span>",
    colorelm: 'radial-gradient(ellipse at 6% 20%, #01a1d6 12%,#6d32d7 75%)',
    answers: [{
            title: "strong",
            desktoptopleft: "60%,20%",
            mobiletopleft: "31%,30%",
            bubblestylescale: "b1,160",
            productrelate: ['d6', 'd7']

        },
        {
            title: "thinker",
            desktoptopleft: "36%,31%",
            mobiletopleft: "34%,90%",
            bubblestylescale: "b3,140",
            productrelate: ['d6', 'd7']

        },
        {
            title: "Feeler",
            desktoptopleft: "46%,41%",
            mobiletopleft: "45%,52%",
            bubblestylescale: "b4,160",
            productrelate: ['d6', 'd7']

        },
        {
            title: "Introvert",
            desktoptopleft: "52%,80%",
            mobiletopleft: "60%,84%",
            bubblestylescale: "b2,210",
            productrelate: ['d6', 'd7']

        },
        {
            title: "Extrovert",
            desktoptopleft: "42%,65%",
            mobiletopleft: "76%,50%",
            bubblestylescale: "b5,185",
            productrelate: []

        },
        {
            title: "Kind",
            desktoptopleft: "66%,57%",
            mobiletopleft: "61%,33%",
            bubblestylescale: "b6,200",
            productrelate: []

        }
    ],
    currentanswer: ""
}, {
    type: "choice",
    maxchoice: 5,
    title: "BUBBLE HOBBY?​<br/><span class=\"sbttl\">(เลือกตอบได้มากกว่า 1)</span>",
    colorelm: 'radial-gradient(ellipse at 6% 20%, #ff7200 12%,#b32601 75%)',
    answers: [{
            title: "Run",
            desktoptopleft: "60%,20%",
            mobiletopleft: "31%,30%",
            bubblestylescale: "b1,160",
            productrelate: []

        },
        {
            title: "Sleep",
            desktoptopleft: "36%,31%",
            mobiletopleft: "37%,90%",
            bubblestylescale: "b3,140",
            productrelate: []

        },
        {
            title: "Game",
            desktoptopleft: "45%,47%",
            mobiletopleft: "50%,40%",
            bubblestylescale: "b4,160",
            productrelate: []

        },
        {
            title: "Fly",
            desktoptopleft: "52%,80%",
            mobiletopleft: "60%,84%",
            bubblestylescale: "b2,210",
            productrelate: ['p3']

        },
        {
            title: "Eat",
            desktoptopleft: "42%,65%",
            mobiletopleft: "76%,50%",
            bubblestylescale: "b2,185",
            productrelate: []

        }
    ],
    currentanswer: ""
},
{
    type: "slide",
    maxchoice: 1,
    title: " BUBBLE Level up/month",
    colorelm: 'linear-gradient(0deg, rgb(28, 183, 132) 83%, rgb(86, 177, 239) 100%)',
    answers: [{
            title: "น้อยกว่า 1 ครั้ง​",
            desktoptopleft: "60%,20%",
            mobiletopleft: "10%,20%",
            bubblestylescale: "b1,0.75",
            productrelate: ['k1']
        },
        {
            title: "1-3 ครั้ง​",
            desktoptopleft: "60%,20%",
            mobiletopleft: "10%,20%",
            bubblestylescale: "b1,0.75",
            productrelate: ['k1']
        },
        {
            title: "3-5 ครั้ง​",
            desktoptopleft: "60%,20%",
            mobiletopleft: "10%,20%",
            bubblestylescale: "b1,0.75",
            productrelate: ['k1']
        },
        {
            title: "มากกว่า 5 ครั้ง​",
            desktoptopleft: "60%,20%",
            mobiletopleft: "10%,20%",
            bubblestylescale: "b1,0.75",
            productrelate: ['k1']
        }
    ],
    currentanswer: ""
},
{
    type: "choice",
    maxchoice: 1,
    title: "Age of Bubble",
    colorelm: 'radial-gradient(at 6% 20%, #f51976 12%, #51116d 75%)',
    answers: [{
            title: "Baby",
            desktoptopleft: "51%,23%",
            mobiletopleft: "22%,75%",
            bubblestylescale: "b3,160",
            productrelate: ['e1']
        },
        {
            title: "Toddle",
            desktoptopleft: "51%,43%",
            mobiletopleft: "35%,45%",
            bubblestylescale: "b3,160",
            productrelate: ['e1']
        },
        {
            title: "Young dumb and brokeee",
            desktoptopleft: "48%,60%",
            mobiletopleft: "47%,85%",
            bubblestylescale: "b2,175",
            productrelate: ['e1']
        },
        {
            title: "Adult",
            desktoptopleft: "49%,80%",
            mobiletopleft: "58%,50%",
            bubblestylescale: "b4,190",
            productrelate: ['e1']
        }
    ],
    currentanswer: ""
},
{
    type: "choice",
    maxchoice: 3,
    title: "BUBLE NUMBER <br/><span class=\"sbttl\">(เลือกตอบได้มากกว่า 1)</span>",
    colorelm: 'radial-gradient(at 6% 20%, #069f92 12%, #005a80 75%)',
    answers: [{
            title: "1",
            desktoptopleft: "51%,23%",
            mobiletopleft: "28%,40%",
            bubblestylescale: "b3,160",
            productrelate: ['i1']
        },
        {
            title: "0",
            desktoptopleft: "36%,43%",
            mobiletopleft: "47%,85%",
            bubblestylescale: "b2,175",
            productrelate: ['i1']
        },
        {
            title: "011001",
            desktoptopleft: "49%,58%",
            mobiletopleft: "50%,44%",
            bubblestylescale: "b4,200",
            productrelate: ['i1']
        },
        {
            title: "none of above",
            desktoptopleft: "49%,80%",
            mobiletopleft: "72%,60%",
            bubblestylescale: "b1,150",
            productrelate: ['i1']
        }
    ],
    currentanswer: ""
},
{
    type: "choice",
    maxchoice: 8,
    title: "Way you want for your bubble<br/><span class=\"sbttl\">(เลือกตอบได้มากกว่า 1)</span>",
    colorelm: 'radial-gradient(at 6% 20%, #ffaf02 12%, #ff2d6e 75%)',
    answers: [{
            title: "ต้องการเงินลงทุนเพิ่ม",
            desktoptopleft: "51%,23%",
            mobiletopleft: "22%,36%",
            bubblestylescale: "b3,160",
            productrelate: ['e1']
        },
        {
            title: "Become king of bubble world",
            desktoptopleft: "36%,38%",
            mobiletopleft: "28%,82%",
            bubblestylescale: "b2,205",
            productrelate: ['p3']
        },
        {
            title: "Sleep",
            desktoptopleft: "65%,41%",
            mobiletopleft: "38%,55%",
            bubblestylescale: "b4,215",
            productrelate: ['d6']
        },
        {
            title: "Stay",
            desktoptopleft: "38%,54%",
            mobiletopleft: "48%,88%",
            bubblestylescale: "b1,185",
            productrelate: ['d7']
        },
        {
            title: "Go to the zoo",
            desktoptopleft: "68%,57%",
            mobiletopleft: "55%,34%",
            bubblestylescale: "b6,215",
            productrelate: ['d5']
        },
        {
            title: "Play some video game",
            desktoptopleft: "38%,67%",
            mobiletopleft: "70%,77%",
            bubblestylescale: "b5,200",
            productrelate: ['i1']
        },
        {
            title: "Watch netflix",
            desktoptopleft: "42%,82%",
            mobiletopleft: "75%,49%",
            bubblestylescale: "b3,190",
            productrelate: ['p1']
        },
        {
            title: "Go to the moon",
            desktoptopleft: "68%,72%",
            mobiletopleft: "57%,59%",
            bubblestylescale: "b1,210",
            productrelate: ['k1']
        }
    ],
    currentanswer: ""
},
];

var currentQuestion = 0;
var count = 0;
// $(function () {
//     $("#info-form").parsley()
//         .on("form:success", function () {
//             event.preventDefault();

//             /* ปุ่มหน้าform */
//             $("#btn-next-js").click();

//             setTimeout(function () {
//                 $('.btnwrap').fadeIn();
//                 $('.bottlewrap,#bubbleCanvas,.counterwrap').css({
//                     zIndex: 6,
//                 })
//                 $('.bottlewrap').addClass('show');
//             }, 1200);

//             return false;
//         })
//         .on("form:error", function () {
//             $("input#submit").show();
//             return false;
//         })
// })



$(document).ready(function () {
    $('#landingPage #mask_div .mask_div_wrapper .remark_container .remarktxt').append('<span>© สงวนลิขสิทธิ์ 2563</span>');
    $('.slick-items').slick({
        infinite: true,
        loop: true,
        slidesToShow: 10,
        slidesToScroll: 10,
        autoplay: true,
        autoplaySpeed: 5000,
        dots: true,
        arrows: false,
        responsive: [{
            breakpoint: 480,
            settings: {
                infinite: true,
                autoplay: true,
                autoplaySpeed: 5000,
                slidesToShow: 5,
                slidesToScroll: 5,
                dots: true,
            }
        }, ]
    });
    $(".more-info").click();
    setTimeout(function () {
        if ($(".lity").hasClass("lity-opened")) {
            $(".lity-close").click();
        }

    }, 5000);
    setTimeout(function () {
        $(".lity-opened").fadeIn();
    }, 200);
    initBackgroundAndElm(); //bg & bottle
    if (getUrlVars()['form'] != null && getUrlVars()['form'] == 1) {
        $("#mask_div,.mask_div_outer").addClass("wave_hide");
        $("#form").addClass("active");
        setTimeout(function () {
            $('#mask_div,.mask_div_outer').delay(500).hide();
        }, 5000);
    }
    if (getUrlVars()['form'] != null && getUrlVars()['form'] == 2) {
        $(".lity-close").click();
        setTimeout(function () {
            $("#btn-start-js").click();
        }, 500);
    }
    $(window).resize(function () {
        if ($('#bubbleCanvas .bubblechoice').length) {
            setTimeout(function () {
                initPositionBubble();
                positionBubble();
            }, 500);

        }
    });
    /*test*/
    $("#test").click(function () {
        shake_stge0();
    });

    $('.btn-start-js2').click(function () {

    })

    /* ปุ่ม popup หน้าแรก */
    if ($(".lity").hasClass("lity-opened")) {
        $("#lightbox_vdo .mae-manee")[0].play();
    } else {}

    /* หน้ากระป๋องกลับไปหน้าคำถาม */
    $('#btn-prev-result-js').click(function () {
        var current;
        var bg_current;
        var prev_question;

        current = $(".question.active");
        bg_current = current.find('.bg_gradient');
        prev_question = count - 1;
        bg_current.addClass('next-stage');
        bg_current.removeClass('current-stage');
        $(".bg_gradient-11").removeClass('prev-stage');
        $(".bg_gradient-11").addClass('current-stage');
        setTimeout(function () {
            $("#question_11").addClass("active");
        }, 500);
        setTimeout(function () {
            bg_current.closest('.question').removeClass("active");
        }, 600);


        $('.bottlewrap').addClass('show');
        $('#result-page').removeAttr('style').hide();
        $('.btnwrap, .counterwrap').fadeIn();
        // alert("show next btn");
        $('.bottlewrap,#bubbleCanvas,.counterwrap').css({
            zIndex: 6,
        });
        setTimeout(function () {
            initPositionBubble();
            gotoQuestion(currentQuestion);
            checkBubbleActive(count)
        }, 1000);

    });


    /* ปุ่มเริ่มหน้าแรก */
    $("#btn-start-js").click(function () {
        $('.mask_div_outer').addClass("wave_hide");
        $(".header_container_left a").removeClass("active");
        $(".btn-start-js2").addClass("active");
        $("input.menu-btn").prop('checked', false);
        // setTimeout(function () {
        //     $('video').removeClass("video_transition");
        // }, 200);
        setTimeout(function () {
            $('.mask_div_outer').fadeOut();
        }, 100);
        /* ปุ่มหน้าform */
        $("#btn-next-js").click();

        setTimeout(function () {
            $('.btnwrap').fadeIn();
            $('#footer').fadeIn();
            $('.bottlewrap,#bubbleCanvas,.counterwrap').css({
                zIndex: 6,
            })
            $('.bottlewrap').addClass('show');
        }, 1000);
        // $("#btn-next-js").click();

    });

    /* ปุ่มหน้าถัดไป */
    $('.btnwrap').hide();
    $("#btn-next-js").click(function (e) {

        $(".bubblechoice").addClass("eventnone");
        var current;
        var bg_current;
        var next_bg;
        var next_question;
        if (currentQuestion + 1 < questions.length && questions[parseInt(currentQuestion) + 1].currentanswer != '') {
            setTimeout(function () {
                $('#btn-next-js').removeClass('hide');
            }, 3000);
        }
        if (currentQuestion + 1 == 1 && questions[1].currentanswer != '' && parseInt(questions[0].currentanswer) != 4) {
            productArray = productArray.concat(questions[1].answers[parseInt(questions[1].currentanswer)].productrelate);

        }

        if ($(".question").hasClass("active")) {
            current = $(".question.active");
            bg_current = current.find('.bg_gradient');
            next_question = current.data("question");
            $('#btn-next-js,#btn-prev-js').addClass('hide');

            if (next_question == 1) {
                overlayElm(1500);
                /* คำถามที่1 */
                initPositionBubble();
                gotoQuestion(0);
            } else if (next_question > questions.length) {
                overlayElm(3000);
                bubbleDown();
                $('.bottlewrap').removeClass('show');
                resultCalculator();
                setTimeout(function () {
                    removeAllBubble();
                    $('#bubbleCanvas,.counterwrap').removeAttr('style').hide();
                    shake_stge0();
                    $('#result-page, #btn-prev-js').removeClass('hide');
                }, 1000);

                /* กดย้อนกลับ ล้างกระะป๋อง clear all in result state when click back */
                $('#businessname').val('');
                $("span.stage-0").removeClass("hide-elm");
                $("div.business_name").removeClass("hide-elm");
                $("p.business_name").addClass("hide-elm");
                $("#btn-show-result-js").addClass("hidden");
                $("div#btn").addClass("hide");
                $("span.stage-1").addClass("hide-elm");
                $(".img img").addClass("stage-1").removeClass("stage-2");
                $('#btn-shake-submit-js').addClass("hidden");
            } else {
                overlayElm(3000);
                if (questions[currentQuestion].type == 'slide') {
                    questions[currentQuestion].currentanswer = checkAvailable;
                }

                submitAns();
                bubbleDown();
                setTimeout(function () {
                    initPositionBubble();
                    gotoQuestion(currentQuestion + 1);
                }, 1200);
                count++;
            }

            next_bg = current.data("bg");
            bg_current.addClass('prev-stage');
            bg_current.removeClass('current-stage');
            $(".bg_gradient-" + next_question).removeClass('next-stage');
            $(".bg_gradient-" + next_question).addClass('current-stage');
            setTimeout(function () {
                $("#question_" + next_question).addClass("active");
                $('#btdragdealern-next-js').addClass("hide-btn");
            }, 500);
            setTimeout(function () {
                bg_current.closest('.question').removeClass("active");
            }, 600);

        }
        drag_val();
        resultCalculator();

    });

    /* overlay elm when click button (แก้เรื่องปุ่มคลิ๊กซ้ำซ้อนหลายทีเเล้วบัคข้ามข้อ)*/
    function overlayElm(time) {
        var elm = '<div class="overlayElm"></div>';
        $(elm).prependTo('#landingPage');
        setTimeout(function () {
            $('.overlayElm').remove();
        }, time);

    }
    /* ปุ่มย้อนกลับ */
    $("#btn-prev-js").click(function () {


        $(".main-page").addClass("none-event");
        var current;
        var bg_current;
        var prev_question;
        count--;
        checkBubbleActive(count)

        $('#btn-next-js').addClass('hide');


        if (currentQuestion > 0) {
            setTimeout(function () {
                $('#btn-next-js').removeClass('hide');
            }, 3000);
        }


        if ((currentQuestion == 2 && questions[0].currentanswer == 4) || currentQuestion - 1 == 0) {
            $('#btn-prev-js').addClass('hide');
        }

        if (currentQuestion - 1 == 0 && questions[1].currentanswer != "") {

            var aaaa = productArray.splice(productArray.indexOf(questions[1].answers[parseInt(questions[1].currentanswer)].productrelate[0]), 1);


        }

        if ($(".question").hasClass("active")) {



            current = $(".question.active");
            bg_current = current.find('.bg_gradient');
            prev_question = current.data("question") - 2;
            bg_current.addClass('next-stage');
            bg_current.removeClass('current-stage');
            $(".bg_gradient-" + prev_question).removeClass('prev-stage');
            $(".bg_gradient-" + prev_question).addClass('current-stage');




            if (currentQuestion - 1 == -1) {
                $('#btn-prev-js').addClass('hide');
                $(".form_validate").addClass("active");
                if ($(".main-page").hasClass("none-event")) {

                    setTimeout(function () {
                        $(".main-page").removeClass("none-event");
                    }, 600);
                }
                overlayElm(1000);
                bubbleDown();

                setTimeout(function () {
                    removeAllBubble();

                    $("input#submit").show();
                    $("#btn-next-js").addClass("hide");
                }, 600);
                setTimeout(function () {
                    $(".form_validate").addClass("active");
                }, 300);

            } else if (prev_question == 2) {
                overlayElm(3000);
                if (questions[0].currentanswer == 4) {
                    // $('#btn-next-js').removeClass('hide');
                    backAns(2);
                    bubbleDown();
                    $('.bubblechoice').removeClass('active');
                    setTimeout(function () {
                        initPositionBubble();
                        gotoQuestion(currentQuestion - 2);

                        $("#question_2").removeClass("active");
                        $("#question_2 .bg_gradient-2").removeClass("current-stage", "prev-stage").addClass("next-stage");
                        $("#question_1").addClass("active");
                        $("#question_1 .bg_gradient-1").removeClass("next-stage", "prev-stage").addClass("current-stage");

                    }, 1200);
                } else {
                    // $('#btn-next-js').removeClass('hide');
                    backAns(1);
                    bubbleDown();

                    setTimeout(function () {
                        initPositionBubble();
                        gotoQuestion(currentQuestion - 1);

                    }, 1300);
                }
            } else if (prev_question == questions.length) {
                overlayElm(3000);
                $('#btn-prev-js').removeClass('hide');
            } else {
                overlayElm(3000);
                $('#btn-next-js').addClass('hide');
                backAns(1);
                bubbleDown();

                setTimeout(function () {
                    initPositionBubble();
                    gotoQuestion(currentQuestion - 1);

                }, 1500);
            }
        }

        setTimeout(function () {
            $("#question_" + prev_question).addClass("active");
        }, 500);
        setTimeout(function () {
            bg_current.closest('.question').removeClass("active");
            // $(".main-page").removeClass("none-event");
        }, 600);
        setTimeout(function () {
            // $(".main-page").removeClass("none-event");
        }, 1200);
    });


    $(".accept-js").on("click", function () {
        $(".accept").toggleClass("active");
        var checkBoxes = $("input[name='accpet']");
        if (checkBoxes.prop("checked") == true)
            checkBoxes.prop("checked", false);
        else {
            checkBoxes.prop("checked", true)
        }
        $("input#submit").toggleClass("disable");

    });

    /* button submit form (page 2 on landing) */
    $("input#submit").on("click", function () {
        $("input#submit").hide();
        if (questions[currentQuestion].currentanswer != "") {
            setTimeout(function () {
                $("#btn-next-js").removeClass("hide");
            }, 500);

        }
    });

    $(".to_homepage,#btn-logo-js").on("click", function () {
        $(".header_container_left a").removeClass("active");
        $(".header_container_left li:first-child a").addClass("active");
        $('.mask_div_outer').removeAttr("style");
        $('#footer').fadeOut();
        $("input.menu-btn").prop('checked', false);
        setTimeout(function () {
            $('.mask_div_outer').removeClass("wave_hide");
        }, 200);

    });


    /* หน้า shake*/
    /* แสดงปุ่มกด*/
    $('#businessname').on('input', function () {
        if ($.trim(this.value).length > 0) {
            $('#btn-shake-submit-js').removeClass("hidden");
        } else {
            $('#btn-shake-submit-js').addClass("hidden");
        }
    });

    /* กดแล้วซ่อนเสตจ0 ไปสเตจ1*/
    $("#btn-shake-submit-js").click(function () {
        $(this).addClass("hidden");
        $("p.business_name").html("'" + $("input#businessname").val() + "'")
        businessName = $("input#businessname").val().toString().replace(" ", "-");
        setTimeout(function () {
            $("span.stage-0").addClass("hide-elm");
            $("div.business_name").addClass("hide-elm");
            $("p.business_name").removeClass("hide-elm");
            $("#btn-show-result-js").removeClass("hidden");
            $("div#btn").removeClass("hide");

        }, 500);
        setTimeout(function () {
            $("span.stage-1").removeClass("hide-elm");
            $(".img img").removeClass("stage-1").addClass("stage-2");
        }, 350);
    });

    /* กดเพื่อ shake*/
    $("#btn-show-result-js").click(function () {
        $("#mb-shake").addClass("shake");
        $("#btn").css('transform', 'translate(-50%,-50%) scale(0)');
        setTimeout(function () {
            window.location = "result-page.html?level=" + checkLevel + "&name=" + businessName + "&score=" + overallScore + "&product=" + productArray;
        }, 800);
    });

    /* when click bubble Insurance */
    $(document).on("click", ".bubblewrap", function (event) {
        if (count == 9) {
            bubbleInsurance();
        }

    });




    ////////////// function ////////////////
    function drag_val() {
        var range = $('.input-range'),
            value = $('.range-value');

        value.html(range.attr('value'));

        range.on('input', function () {
            value.html(this.value);
        });
    }
    ///////////// question function //////////

    var posDrag;

    function gotoQuestion(num) {
        if (num == 1 && questions[0].currentanswer == 3) {
            questions[2].answers[0].productrelate = ['d4']
        } else {
            questions[2].answers[0].productrelate = []
        }
        if (num == 8) {
            productArray.push("k1");
        }
        currentQuestion = num;
        var question = questions[num];
        countPage(currentQuestion);
        if (currentQuestion != 0) {
            setTimeout(function () {
                $('#btn-prev-js').removeClass('hide');
            }, 500);
        }

        if (question.type == "choice") {
            $('#sliderCanvas').fadeOut();
            $('#bubbleCanvas').fadeIn();
            var htmlbubble = "";

            /*generate bubble array*/
            for (var i = 0; i < question.answers.length; i++) {
                var bubbleStyle = question.answers[i].bubblestylescale.split(',')[0];
                htmlbubble += '<div class="bubblewrap">';
                htmlbubble += '<div class="bubblechoice ' + bubbleStyle + '" style="background:' + question.colorelm + '"><span class="bubble_text text">' + question.answers[i].title + '</span></div>';
                htmlbubble += '</div>';
            }

            var htmltitlebb = "";
            htmltitlebb += '<div class="headbubble text-ktreg extralarge white">';
            htmltitlebb += question.title;
            htmltitlebb += '</div>';


            $("#bubbleCanvas").html(htmlbubble);
            $("#bubbleCanvas").prepend(htmltitlebb);

            $(".bubblechoice").on("click", function (e) {
                $('#bubbleCanvas').show();
                checkBubbleActive(currentQuestion)
                //ดึงข้อปัจจุบันออกมา
                var question = questions[currentQuestion];

                var currentanswerArray = question.currentanswer.split(','); //array เก็บตัวที่เคยเลือกไว้ใน currentanswer

                var selectIndex = "" + $('.bubblechoice').index(this);
                var isSelected = false;

                isSelected = currentanswerArray.includes("" + selectIndex); // is select will remove           

                if (isSelected) { //กด bubble ที่กดไปแล้ว = Unchecked ต้องลบ product id ที่เก็บไว้ใน product array ออก

                    // 
                    var currentChoice = question.answers[selectIndex]

                    //วนเอา id ของ relate product ใน choice ที่เลือก
                    for (var i = 0; i < currentChoice.productrelate.length; i++) {

                        //เอา id มาหา index ใน product array แล้วลบทิ้ง
                        productArray.splice(productArray.indexOf(currentChoice.productrelate[i]), 1)
                    }

                    //

                    /*if selected(not first time)*/
                    currentanswerArray.splice(currentanswerArray.indexOf("" + selectIndex), 1);
                    question.currentanswer = currentanswerArray.join(",");


                } else {
                    // add to productArray
                    // productArray = productArray.concat(question.answers[selectIndex].productrelate)

                    if (question.currentanswer != "") { //ถ้าข้อนั้นเป็น multiple choice แล้วกด bubble อันที่สองจะเข้าอันนี้ || หรือถ้าเป็น single choice จะเข้าอันนี้ ถ้าไม่ได้ uncheck ตัวเลือกก่อนหน้า


                        if (question.maxchoice == 1) { //กรณี Single choice


                            //
                            var prevChoice = question.answers[parseInt(question.currentanswer)]

                            //วนเอา id ของ relate product ใน choice ที่เลือกก่อนหน้านี้
                            for (var i = 0; i < prevChoice.productrelate.length; i++) {

                                //เอา id มาหา index ใน product array แล้วลบทิ้ง
                                productArray.splice(productArray.indexOf(prevChoice.productrelate[i]), 1)
                            }
                            productArray = productArray.concat(question.answers[selectIndex].productrelate) //เอา related product ของ bubble ที่เลือกไปเก็บใน product array

                            //

                            /*for maxchoice = 1*/
                            $(".bubblechoice").removeClass('active');
                            question.currentanswer = "" + selectIndex;

                        } else {

                            /*for multiplechoice*/
                            if (currentanswerArray.length < question.maxchoice) { //เช็คว่าเลือกมากกว่า maximum หรือยัง
                                currentanswerArray.push("" + selectIndex);
                                question.currentanswer = currentanswerArray.join(","); //เอา index ของ bubble ที่กดเก็บเข้าไปใน current answer ของข้อนั้น
                                productArray = productArray.concat(question.answers[selectIndex].productrelate) //เอา related product ของ bubble ที่เลือกไปเก็บใน product array

                            } else {
                                isSelected = true;
                            }

                        }
                    } else { //ถ้าข้อนั้น ๆ Uncheck bubble ทั้งหมด แล้วกดเลือกเป็นอันแรกจะเข้าอันนี้
                        question.currentanswer = "" + selectIndex; //เอา index ของ bubble ที่กดเก็บเข้าไปใน current answer ของข้อนั้น
                        productArray = productArray.concat(question.answers[selectIndex].productrelate) //เอา product id จาก related product เก็บใส่ product array (product array ใช้ส่งไปหน้า result)

                    }
                }

                var ele = $(this)
                if (!isSelected) { //false
                    $(this).addClass('bbpop');
                    setTimeout(function () {
                        ele.removeClass('bbpop');
                        ele.addClass('active');
                    }, 100);
                    pop(e.pageX, e.pageY, 12);
                } else {
                    ele.removeClass('active');
                }

            });
            setCurrentActive();
            positionBubble();
            if (num == 1) {
                /* ข้อ 2 */
                if (questions[0].currentanswer == 0) {
                    /* ค้าปลีก */
                    $('.bubblewrap:eq(7)').hide();

                    //เครื่องใช้ไฟฟ้า/อิเล็กทรอนิกส์
                    questions[1].answers[3].mobiletopleft = "45%,30%";
                    //อาหาร/เครื่องดื่ม
                    questions[1].answers[4].desktoptopleft = "56%,36%";
                    questions[1].answers[4].mobiletopleft = "50%,60%";
                    //วัสดุก่อสร้าง
                    questions[1].answers[6].desktoptopleft = "60%,50%";
                    questions[1].answers[6].mobiletopleft = "66%,32%";
                    //สินค้า/บริการอื่นๆ
                    questions[1].answers[8].desktoptopleft = "60%,64%";
                    questions[1].answers[8].mobiletopleft = "65%,95%";
                } else if (questions[0].currentanswer == 1) {
                    /* ค้าส่ง */
                    $('.bubblewrap:eq(7),.bubblewrap:eq(9)').hide();

                    //สุขภาพและความงาม
                    questions[1].answers[1].desktoptopleft = "33%,43%";
                    //เครื่องใช้ไฟฟ้า/อิเล็กทรอนิกส์
                    questions[1].answers[3].mobiletopleft = "46%,37%";
                    //อาหาร/เครื่องดื่ม
                    questions[1].answers[4].desktoptopleft = "62%,41%";
                    questions[1].answers[4].mobiletopleft = "61%,60%";
                    //วัสดุก่อสร้าง
                    questions[1].answers[6].desktoptopleft = "60%,58%";
                    questions[1].answers[6].mobiletopleft = "74%,36%";
                    //สินค้า/บริการอื่นๆ
                    questions[1].answers[8].desktoptopleft = "65%,78%";
                    questions[1].answers[8].mobiletopleft = "65%,95%";

                } else if (questions[0].currentanswer == 2) {
                    /* บริการ */
                    $('.bubblewrap:eq(0),.bubblewrap:eq(1),.bubblewrap:eq(2),.bubblewrap:eq(3),.bubblewrap:eq(5),.bubblewrap:eq(6),.bubblewrap:eq(8),.bubblewrap:eq(9)').hide();

                    //อาหาร/เครื่องดื่ม
                    questions[1].answers[4].desktoptopleft = "42%,42%";
                    questions[1].answers[4].mobiletopleft = "36%,49%";
                    //โรงแรม/ที่พัก
                    questions[1].answers[7].desktoptopleft = "51%,68%";
                    questions[1].answers[7].mobiletopleft = "56%,76%";
                } else if (questions[0].currentanswer == 3) {
                    /* ผลิตสินค้า */
                    $('.bubblewrap:eq(7),.bubblewrap:eq(9)').hide();
                    questions[1].answers[4].desktoptopleft = "48%,35%";


                    //เครื่องใช้ไฟฟ้า/อิเล็กทรอนิกส์
                    questions[1].answers[3].mobiletopleft = "48%,48%";
                    //สุขภาพและความงาม
                    questions[1].answers[1].desktoptopleft = "60%,50%";
                    //วัสดุก่อสร้าง
                    questions[1].answers[6].desktoptopleft = "62%,68%";
                    questions[1].answers[6].mobiletopleft = "66%,32%";
                    //สินค้า/บริการอื่นๆ
                    questions[1].answers[8].desktoptopleft = "60%,87%";
                    questions[1].answers[8].mobiletopleft = "65%,95%";
                } else if (questions[0].currentanswer == 4) {
                    /* ร้านอาหารและเครื่องดื่ม */

                    // $('.bubblewrap:eq(4)').click();
                    $('#btn-next-js').click();
                }

            } else if (num == 9) {
                bubbleInsurance();
            } else if (num == 10) {
                // var temp = questions[3].currentanswer.split(',');
                if (questions[3].currentanswer == '1' || questions[3].currentanswer == '2') {
                    $('.bubblewrap:eq(3)').hide();
                }
            }

        } else if (question.type == "slide") {

            $(".question_dragging_demo").prepend("<img class='tips_shake' src='../dist/resources/images/main-page/slide-hand.png'/>");
            $(".question_dragging_demo").addClass("show_tips");

            $('.handle').click(function () {
                $(".question_dragging_demo img.tips_shake").fadeOut();
            });

            setTimeout(function () {
                $(".question_dragging_demo img.tips_shake").fadeOut("slow", function () {
                    $(".question_dragging_demo img.tips_shake").remove();
                    $(".question_dragging_demo").removeClass("show_tips");
                });
            }, 3000);

            $('.monitor_title').html(question.title);
            removeAllBubble();
            $('#sliderCanvas').show();
            setTimeout(function () {
                $('#btn-next-js,#btn-prev-js').removeClass('hide');
            }, 500);
            var selector;

            if (question.currentanswer == '') {
                selector = 1
            } else {

                selector = posDrag;
                count = false;
            }

            if (num == 2) {} else {
                for (var i = 0; i < question.answers.length; i++) {
                    $('.overlay_radius').css({
                        "background": "rgb(28,183,132)",
                        "background": question.colorelm,
                    })
                }
            }
            new Dragdealer('content-scroller', {
                horizontal: false,
                vertical: true,
                y: selector,
                animationCallback: function (x, y) {
                    var handleTop = $('.handle').offset().top;
                    var titlelengthpercent = 100 / question.answers.length;

                    if ($(window).width() > 767) {
                        $('.overlay_radius').css({
                            'top': handleTop - 46
                        })
                    } else {
                        $('.overlay_radius').css({
                            'top': handleTop - 26
                        })
                    }

                    for (var i = 0; i < question.answers.length; i++) {
                        if ((((y * 100) - 100) * (-1)) < (titlelengthpercent * (i + 1))) {
                            $('.overlay_indicator').html(question.answers[i].title);
                            checkAvailable = i;
                            break
                        }
                    }

                    if (y < 0.1) {
                        $('.overlay_radius').css({
                            'border-radius': '50% 50% 0px 0px',
                            'width': '120%'
                        })
                    } else if (y < 0.2) {
                        $('.overlay_radius').css({
                            'border-radius': '50% 50% 0px 0px',
                            'width': '130%'
                        })
                    } else if (y < 0.3) {
                        $('.overlay_radius').css({
                            'border-radius': '50% 50% 0px 0px',
                            'width': '140%'
                        })
                    } else if (y < 0.4) {
                        $('.overlay_radius').css({
                            'border-radius': '50% 50% 0px 0px',
                            'width': '150%'
                        })
                    } else if (y < 0.5) {
                        $('.overlay_radius').css({
                            'border-radius': '50% 50% 0px 0px',
                            'width': '160%'
                        })
                    } else if (y < 0.6) {
                        $('.overlay_radius').css({
                            'border-radius': '50% 50% 0px 0px',
                            'width': '170%'
                        })
                    } else if (y < 0.7) {
                        $('.overlay_radius').css({
                            'border-radius': '50% 50% 0px 0px',
                            'width': '180%'
                        })
                    } else if (y < 0.8) {
                        $('.overlay_radius').css({
                            'border-radius': '50% 50% 0px 0px',
                            'width': '200%'
                        })
                    } else if (y < 0.9) {
                        $('.overlay_radius').css({
                            'border-radius': '50% 50% 0px 0px',
                            'width': '225%'
                        })
                    } else {
                        $('.overlay_radius').css({
                            'border-radius': 0
                        });
                    }
                    posDrag = y;
                }
            });
        }

        if ($(".main-page").hasClass("none-event")) {

            setTimeout(function () {
                $(".main-page").removeClass("none-event");
            }, 600);
        }

    }

    function bubbleInsurance() {
        setTimeout(function () {
            if ($('.bubblewrap:eq(0)').find('.bubblechoice').hasClass('active') || $('.bubblewrap:eq(1)').find('.bubblechoice').hasClass('active') || $('.bubblewrap:eq(2)').find('.bubblechoice').hasClass('active')) {
                $('.bubblewrap:eq(3)').css({
                    pointerEvents: 'none',
                })
            } else if (!$('.bubblewrap:eq(0)').find('.bubblechoice').hasClass('active') && !$('.bubblewrap:eq(1)').find('.bubblechoice').hasClass('active') && !$('.bubblewrap:eq(2)').find('.bubblechoice').hasClass('active')) {
                $('.bubblewrap:eq(3)').css({
                    pointerEvents: 'auto',
                })
            }

            if ($('.bubblewrap:eq(3)').find('.bubblechoice').hasClass('active')) {
                $('.bubblewrap:eq(0), .bubblewrap:eq(1), .bubblewrap:eq(2)').css({
                    pointerEvents: 'none',
                })
            } else if (!$('.bubblewrap:eq(3)').find('.bubblechoice').hasClass('active')) {
                $('.bubblewrap:eq(0), .bubblewrap:eq(1), .bubblewrap:eq(2)').css({
                    pointerEvents: 'auto',
                })
            }
        }, 300);
    }

    function initPositionBubble() {
        setTimeout(function () {
            var question = questions[currentQuestion];
            var bbScale;
            if (currentQuestion == 10) {
                bbScale = 0.1;
            } else {
                bbScale = 0;
            }
            for (var i = 0; i < question.answers.length; i++) {
                var scale = question.answers[i].bubblestylescale.split(',')[1];

                if ($(window).width() >= 1150) {
                    var left = question.answers[i].desktoptopleft.split(',')[1];
                    $(".bubblewrap:eq(" + i + ")").css({
                        left: left,
                        width: scale,
                        transform: 'translate(-50%,-50%) scale(0.9)',
                    });
                } else if ($(window).width() > 768 && $(window).width() < 1100) {
                    var left = question.answers[i].desktoptopleft.split(',')[1];
                    $(".bubblewrap:eq(" + i + ")").css({
                        left: left,
                        width: scale,
                        transform: 'translate(-50%,-50%) scale(' + (0.85 - bbScale) + ')',
                    });
                } else if ($(window).width() <= 768 && $(window).width() >= 375 && $(window).height() > 790) {
                    var left = question.answers[i].mobiletopleft.split(',')[1];
                    $(".bubblewrap:eq(" + i + ")").css({
                        left: left,
                        width: scale,
                        transform: 'translate(-50%,-50%) scale(' + (0.68 - bbScale) + ')',
                    });
                } else {
                    var left = question.answers[i].mobiletopleft.split(',')[1];
                    $(".bubblewrap:eq(" + i + ")").css({
                        left: left,
                        width: scale,
                        transform: 'translate(-50%,-50%) scale(' + (0.62 - bbScale) + ')',
                    });
                }
                $(".bubblewrap:eq(" + i + ")").find(".bubblechoice").css({
                    width: scale,
                    height: scale,
                });
            }
        }, 100);
    }

    function positionBubble() {
        var question = questions[currentQuestion];
        for (var i = 0; i < question.answers.length; i++) {
            /* random timeout (bubble slide up)*/
            (function (i) {
                setTimeout(function () {
                    if ($(window).width() > 767) {
                        var top = question.answers[i].desktoptopleft.split(',')[0];
                        var left = question.answers[i].desktoptopleft.split(',')[1];
                    } else {
                        var top = question.answers[i].mobiletopleft.split(',')[0];
                        var left = question.answers[i].mobiletopleft.split(',')[1];
                    }
                    $(".bubblewrap:eq(" + i + ")").animate({
                        top: top,
                        left: left,
                    }, 700);
                }, Math.floor(Math.random() * 901) + 200);

            })(i);
        }

    }

    function bubbleDown() {
        var question = questions[currentQuestion];
        $('#bubbleCanvas').find('.bubblechoice:not(.active)').addClass('hide');
        for (var i = 0; i < question.answers.length; i++) {
            /* random timeout (bubble slide up)*/
            (function (i) {
                setTimeout(function () {
                    $(".bubblewrap:eq(" + i + ")").animate({
                        top: '115%',
                        opacity: '0'
                    }, 600);
                }, Math.floor(Math.random() * 901) + 200);

            })(i);
        }
    }

    function setCurrentActive() {
        var question = questions[currentQuestion];

        var currentanswerArray = question.currentanswer.split(',');

        for (var i = 0; i < question.answers.length; i++) {
            if (isSelected = currentanswerArray.includes("" + i)) {
                $(".bubblechoice:eq(" + i + ")").addClass("active");
            }
        }
    }

    function checkBubbleActive(val) {
        /*check bubble active (show and hide button) */
        var question = questions[val];
        if (val < 0) {

        } else {
            if (question.type == "choice") {

                if (question.maxchoice == 1) {
                    $('#btn-next-js').addClass('hide');
                    setTimeout(function () {
                        if ($('#bubbleCanvas').find('.bubblechoice.active').length) {
                            $('#btn-next-js').removeClass('hide');
                        }
                    }, 600);
                } else {
                    setTimeout(function () {
                        $('#btn-next-js').addClass('hide');
                        if ($('#bubbleCanvas').find('.bubblechoice.active').length) {
                            $('#btn-next-js').removeClass('hide');
                        }
                    }, 600);
                }
            } else {

            }
        }
    }

    function countPage(cur) {
        var countcurrent = cur + 1;
        $('.counterwrap .current-choice').text(countcurrent);
        $('.counterwrap .all-choice').text(questions.length);
    }

    function removeAllBubble() {
        $('#bubbleCanvas').empty().hide();
    }

    /*animate debris when click bubble*/
    function r2d(x) {
        return x / (Math.PI / 180);
    }

    function d2r(x) {
        return x * (Math.PI / 180);
    }

    function pop(start_x, start_y, particle_count) {
        arr = [];
        angle = 0;
        particles = [];
        offset_x = $("#dummy_debris").width() / 500;
        offset_y = $("#dummy_debris").height() / 500;

        for (i = 0; i < particle_count; i++) {
            rad = d2r(angle);
            x = Math.cos(rad) * (100 + Math.random() * 20);
            y = Math.sin(rad) * (100 + Math.random() * 20);
            arr.push([start_x + x, start_y + y]);
            z = $('<div class="debris" />');
            z.css({
                "left": start_x - offset_x,
                "top": start_y - offset_x
            }).appendTo($("#debris-wrap"));
            particles.push(z);
            angle += 360 / (particle_count - 6.5);
        }

        $.each(particles, function (i, v) {
            $(v).show();
            $(v).stop().animate({
                top: arr[i][1],
                left: arr[i][0],
                width: 4,
                height: 4,
                opacity: 0
            }, 600, function () {
                $(v).remove()
            });
        });
    }
    /*end animate debris when click bubble*/

    /*bottle*/
    var countAns = 0;

    function submitAns() {
        countAns += 1;
        $(".main-page [data-layer=" + countAns + "]").show().css({
            opacity: 1,
            visibility: 'visible',
        }).addClass("action");
    }

    function backAns(num) {
        $(".main-page [data-layer=" + countAns + "]").hide().removeClass("action");
        var minus = num;
        countAns -= minus;
        // alert(countAns);
        if (countAns == 0) {
            $(".main-page  .layer").hide().removeClass("action");
        }
    }
    /*bottle*/


    /* Calculate answer */
    var resultArray = [];

    function resultCalculator() {
        resultArray = [];
        digitalBusinessScore = 0;
        overallScore = [];


        var result = multiMode(calAnswer3(), calAnswer4(), calAnswer5and6(), calAnswer7(), calAnswer8()); //sort max string in array
        calAnswer9()
        calAnswer10()
        calAnswer11()
        checkLevel = result;
    }

    /* ข้อ 1 (maxchoice = 1)*/
    function calAnswer1() {

        var expr = questions[0].currentanswer; //please change num in array
        /* 1 maxanswer*/
        switch (expr) {
            /*question[i].answers*/
            case '0':
                resultArray.push('Beginner', 'Intermediate', 'Advanced');
                break;
            case '1':
                resultArray.push('Beginner', 'Intermediate', 'Advanced');
                break;
            case '2':
                resultArray.push('Advanced', 'Expert', 'Professional');
                break;
            default:
        }
        return resultArray;

    }

    /* ข้อ 2 (maxchoice >= 1)*/
    function calAnswer2() {
        // Beginner
        // Intermediate
        // Advanced
        // Expert 
        // Professional
        var expr = questions[1].currentanswer; //please change num in array

        var arrayTemp = expr.split(',');
        /*multiple answer*/
        for (i = 0; i < arrayTemp.length; i++) {
            switch (arrayTemp[i]) {
                /* question[i].answers result */
                case '0':
                    resultArray.push('Beginner', 'Intermediate', 'Advanced');
                    break;
                case '1':
                    resultArray.push('Beginner', 'Intermediate', 'Advanced');
                    break;
                case '2':
                    resultArray.push('Advanced', 'Expert', 'Professional');
                    break;
                case '3':
                    resultArray.push('Advanced', 'Expert', 'Professional');
                    break;
                default:
            }
        }
        return resultArray;

    }

    /* ข้อ 3 (maxchoice = 1)*/
    function calAnswer3() {
        // Beginner
        // Intermediate
        // Advanced
        // Expert 
        // Professional
        var expr = questions[2].currentanswer; //please change num in array
        /* 1 maxanswer*/
        switch (expr) {
            /*question[i].answers*/
            case '0':
                resultArray.push('Beginner', 'Intermediate', 'Advanced');
                digitalBusinessScore += 100;
                q3Score = 50;
                break;
            case '1':
                resultArray.push('Beginner', 'Intermediate', 'Advanced');
                digitalBusinessScore += 0;
                q3Score = 0;
                break;
            case '2':
                resultArray.push('Advanced', 'Expert', 'Professional');
                digitalBusinessScore += 100;
                q3Score = 50;
                break;
            default:
                // code block
        }

        return resultArray;

    }

    /* ข้อ 4 (maxchoice = 1)*/
    function calAnswer4() {
        // Beginner
        // Intermediate
        // Advanced
        // Expert 
        // Professional
        var expr = questions[3].currentanswer; //please change num in array
        /* 1 maxanswer*/
        switch (expr) {
            /*question[i].answers*/
            case '0':
                resultArray.push('Beginner', 'Intermediate');
                break;
            case '1':
                resultArray.push('Advanced', 'Expert', 'Professional');
                break;
            case '2':
                resultArray.push('Advanced', 'Expert', 'Professional');
                break;
            default:
                // code block
        }

        return resultArray;

    }

    /* ข้อ 5,6 คิดรวมกัน (maxchoice >= 1) นับข้อเข้าเคส ex. เลือก 2 (จาก 8) วิธี = Beginner  */
    function calAnswer5and6() {
        // Beginner
        // Intermediate
        // Advanced
        // Expert 
        // Professional

        /* 1 maxanswer นับข้อเข้าเคส ex. เลือก 2 (จาก 8) วิธี = Beginner */
        var expr1 = questions[4].currentanswer; //please change num in array
        var expr2 = questions[5].currentanswer;

        if (expr1 != "" && expr2 != "") {
            var expr = expr1 + ',' + expr2;
            var arrayTemp = expr.split(',');
            if (arrayTemp.length > 1 && arrayTemp.length <= 3) {
                resultArray.push('Beginner');
            } else if (arrayTemp.length > 3 && arrayTemp.length <= 4) {
                resultArray.push('Intermediate');
            } else if (arrayTemp.length > 4 && arrayTemp.length <= 6) {
                resultArray.push('Advanced');
            } else if (arrayTemp.length == 7) {
                resultArray.push('Expert');
            } else if (arrayTemp.length >= 8) {
                resultArray.push('Professional');
            }

            var q5ans = expr1.split(',');
            var q6ans = expr2.split(',');
            var joinans = "q5(";

            for (var i = 0; i < q5ans.length; i++) {
                if (i != 0) {
                    joinans += ',';
                }
                joinans += q5ans[i];

                if (i == q5ans.length - 1) {
                    joinans += "),q6(";
                }
            }

            for (var i = 0; i < q6ans.length; i++) {
                if (i != 0) {
                    joinans += ',';
                }
                joinans += q6ans[i];

                if (i == q6ans.length - 1) {
                    joinans += ")";
                }
            }

            var splitansq5 = joinans.split('),')[0].replace('q5(', '').split(',');
            var splitansq6 = joinans.split('),')[1].replace('q6(', '').replace(')', '').split(',');
            var q5score = 0;
            var q6score = 0;
            var total = 0;
            var count = 0;

            var digitalPercentage = 0;

            //Cal คะแนนข้อ 6
            if (splitansq5.includes("0")) {
                q5score += 0;
                count++;
            }
            if (splitansq5.includes("1")) {
                q5score += 0;
                count++;
            }
            if (splitansq5.includes("2")) {
                q5score += 0;
                count++;
            }
            if (splitansq5.includes("3")) {
                q5score += 100;
                count++;
                digitalPercentage++;
            }
            if (splitansq5.includes("4")) {
                q5score += 50;
                count++;
                digitalPercentage++;
            }

            q5score = q5score / count;
            count = 0;

            //Cal คะแนนข้อ 6
            if (splitansq6.includes("0")) {
                q6score += 0;
                count++;
            }
            if (splitansq6.includes("1")) {
                q6score += 50;
                count++;
            }
            if (splitansq6.includes("2")) {
                q6score += 0;
                count++;
            }
            if (splitansq6.includes("3")) {
                q6score += 100;
                count++;
                digitalPercentage++;
            }
            if (splitansq6.includes("4")) {
                q6score += 50;
                count++;
                digitalPercentage++;
            }
            if (splitansq6.includes("5")) {
                q6score += 100;
                count++;
                digitalPercentage++;
            }

            q6score = q6score / count;
            total = (q5score + q6score) / 2;
            digitalBusinessScore = (digitalBusinessScore + total) / 2;

            //คะแนนข้อ 5,6
            digitalPercentage = digitalPercentage * 10
            if (digitalPercentage == 40) {
                digitalPercentage += 10
            }

            //คะแนน final แกน Digital ข้อ 5,6 + ข้อ 3
            overallScore.push('0:' + Math.round(digitalPercentage + q3Score));
        }

        return resultArray;

    }
    /* ข้อ 7 (maxchoice >= 1) นับข้อเข้าเคส ex. เลือก 2 (จาก 8) วิธี = Beginner  */
    function calAnswer7() {
        // Beginner
        // Intermediate
        // Advanced
        // Expert 
        // Professional

        var expr = questions[6].currentanswer.toString(); //please change num in array
        expr = expr.split(',')


        overallScore.push('2:' + expr.length * 20);


    }

    /* ข้อ 8 */
    function calAnswer8() {
        var expr = questions[7].currentanswer; //please change num in array

        overallScore.push('3:' + (parseInt(expr) + 1) * 25);

    }

    /* ข้อ 9 */
    function calAnswer9() {
        var expr = questions[8].currentanswer; //please change num in array
        var resultScore = 0;

        switch (expr) {
            /*question[i].answers*/
            case '0':
                resultScore = 25;
                break;
            case '1':
                resultScore = 100;
                break;
            case '2':
                resultScore = 75;
                break;
            case '3':
                resultScore = 50;
                break;
            default:
                // code block
        }

        overallScore.push('1:' + resultScore);
    }

    /* ข้อ 10 */
    function calAnswer10() {
        var expr = questions[9].currentanswer; //please change num in array
        var count = expr.split(',');
        var resultScore = 0;

        if (expr.includes("3")) {
            resultScore = 0;
        } else {
            if (count.length == 1) {
                resultScore = 50;
            } else if (count.length == 2) {
                resultScore = 75;
            } else if (count.length == 3) {
                resultScore = 100;
            }
        }

        overallScore.push('4:' + resultScore);
    }

    /* ข้อ 11 */
    function calAnswer11() {
        var expr = questions[10].currentanswer; //please change num in array
        var count = expr.split(',');
    }

    /* sort max string in array */
    if (!Object.values) Object.values = function (ob) {
        var arr = [];
        for (var k in ob)
            if (ob.hasOwnProperty(k))
                arr.push(ob[k]);
        return arr;
    };

    /* find the level */
    function multiMode(arr) {
        var map = arr.reduce(function (map, item) {
            if (!(item in map)) map[item] = 0;
            return map[item]++, map;
        }, {});
        var max = Math.max.apply(null, Object.values(map)),
            arr2 = [];
        Object.keys(map).forEach(function (k) {
            if (map[k] === max) arr2.push(k);
        });



        var resultlevel = arr2;
        if (resultlevel.includes('Professional')) {
            return 'Professional'
        } else if (resultlevel.includes('Expert')) {
            return 'Expert'
        } else if (resultlevel.includes('Advanced')) {
            return 'Advanced'
        } else if (resultlevel.includes('Intermediate')) {
            return 'Intermediate'
        } else if (resultlevel.includes('Beginner')) {
            return 'Beginner'
        }

        return arr2;
    }
    /* end sort max string in array */

    /* Calculate answer */

    function initBackgroundAndElm() {
        for (var i = 0; i <= questions.length; i++) {
            /* for background gradient */
            var htmlbggradient = "";
            var num = i + 1;
            var numq = i + 2;
            if (i < questions.length) {


                htmlbggradient += '<div id="question_' + num + '" class="question" data-question="' + numq + '">';
                htmlbggradient += '<div class="layer_0  bg_gradient bg_gradient-' + num + '"></div>';

                htmlbggradient += '</div>';

                /* for bottle in main-page */
                var htmlbottle = "";
                htmlbottle += '<img class="layer" data-layer="' + num + '" src="../dist/resources/images/bottle/layer' + num + '.png" alt="layer' + num + '"></img>'
                $(".main-page .bottlewrap .bottle").append(htmlbottle);
                /* for bottle */

                /* for bottle in result */
                if (i < questions.length - 1) {
                    var htmlbottleresult = "";
                    htmlbottleresult += '<img class="layer" data-layer="' + numq + '" src="../dist/resources/images/bottle/layer' + numq + '.png" alt="layer' + numq + '"></img>'
                    $("#result-page .bottlewrap .bottle").append(htmlbottleresult);
                }
                /* for bottle result */
            } else {
                htmlbggradient += '<div id="question_' + num + '" class="question result-page" data-question="result-page">';
                htmlbggradient += '<div class="layer_0  bg_gradient bg_gradient-' + num + '"></div>';
                htmlbggradient += '</div>';
            }

            $(".question_main").append(htmlbggradient);
        }
    }
});


function shake_stge0() {

    if (!/Android/.test(navigator.userAgent)) {
        $("#btn-1").remove();
        $("#btn-2").remove();
    };

    // waitingForResultPage = true
    var shakeEvent = new Shake({
        threshold: 15
    });
    shakeEvent.start();
    window.addEventListener('shake', function (e) {
        e.preventDefault();
        setTimeout(function () {
            $("#mb-shake").addClass("shake");
            $("#btn").css('transform', 'translate(-50%,-50%) scale(0)');
            setTimeout(function () {

                window.location = "result-page.html?level=" + checkLevel + "&name=" + businessName + "&score=" + overallScore + "&product=" + productArray;

            }, 800);
        }, 2000);
    }, false);

    //stop listening
    function stopShake() {
        shakeEvent.stop();
    }

    //check if shake is supported or not.
    if (!("ondevicemotion" in window)) {
        alert("Not Supported");
    }

    $('#result-page').fadeIn(600);

    var img1 = $("#img-shake-1");
    var img2 = $("#img-shake-2");
    var inp = $(".business_name");
    var slide = $("#result-page img.layer"),
        cur = 0;
    setTimeout(function () {
        img1.removeClass("stage-0");
        img2.removeClass("stage-0");
        $("h1 span.stage-0").removeClass("hide-elm");
        setInterval(function () {
            $(slide).eq((++cur)).addClass('action');
        }, 200);
    }, 600);
    setTimeout(function () {
        inp.removeClass("stage-0");
    }, 2000);

}

function getUrlVars() {
    var vars = {};
    var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
        vars[key] = value;
    });
    return vars;
}
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJsYW5kaW5nLmpzIl0sInNvdXJjZXNDb250ZW50IjpbInZhciBjaGVja0F2YWlsYWJsZTtcclxudmFyIG92ZXJhbGxTY29yZSA9IFtdO1xyXG52YXIgcHJvZHVjdEFycmF5ID0gW107IC8vIFByb2R1Y3Qgd2lsbCBzaG93IGF0IHJlc3VsdCBwYWdlXHJcbnZhciBjaGVja0xldmVsID0gXCJcIjtcclxudmFyIGJ1c2luZXNzTmFtZSA9IFwiXCI7XHJcbnZhciBkaWdpdGFsQnVzaW5lc3NTY29yZSA9IDA7XHJcbnZhciBxM1Njb3JlID0gMDtcclxudmFyIGluZHVzdHJ5ID0gXCJcIjsgXHJcbiBcclxudmFyIHF1ZXN0aW9ucyA9IFt7XHJcbiAgICB0eXBlOiBcImNob2ljZVwiLFxyXG4gICAgbWF4Y2hvaWNlOiAxLFxyXG4gICAgdGl0bGU6IFwiQlVCQkxFIOC4l+C4teC5iOC4hOC4uOC4k+C4iuC4reC4muC5gOC4m+C5h+C4meC5geC4muC4muC5hOC4q+C4mT88YnIvPjxzcGFuIGNsYXNzPVxcXCJzYnR0bFxcXCI+KOC4q+C4suC4gUJVQkJMReC4hOC4t+C4rSBCVUJCTEUgNSDguILguYnguK3guIHguLLguKPguYDguKXguLfguK3guIHguKrguLXguYPguKvguYnguITguLjguJPguYLguJTguKLguK3guLHguJXguYLguJnguKHguLHguJXguLQpPC9zcGFuPlwiLFxyXG4gICAgY29sb3JlbG06ICdyYWRpYWwtZ3JhZGllbnQoZWxsaXBzZSBhdCA2JSAyMCUsICMxYmM1NzkgMTIlLCM3MjJhYjggNzUlKScsXHJcbiAgICBhbnN3ZXJzOiBbe1xyXG4gICAgICAgICAgICB0aXRsZTogXCJCVUJCTEUgMVwiLFxyXG4gICAgICAgICAgICBkZXNrdG9wdG9wbGVmdDogXCI1MCUsMjMlXCIsXHJcbiAgICAgICAgICAgIG1vYmlsZXRvcGxlZnQ6IFwiMjglLDQ2JVwiLFxyXG4gICAgICAgICAgICBidWJibGVzdHlsZXNjYWxlOiBcImIxLDE4MFwiLFxyXG4gICAgICAgICAgICBwcm9kdWN0cmVsYXRlOiBbXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aXRsZTogXCJCVUJCTEUgMlwiLFxyXG4gICAgICAgICAgICBkZXNrdG9wdG9wbGVmdDogXCIzOSUsNDIlXCIsXHJcbiAgICAgICAgICAgIG1vYmlsZXRvcGxlZnQ6IFwiMzUlLDkwJVwiLFxyXG4gICAgICAgICAgICBidWJibGVzdHlsZXNjYWxlOiBcImI1LDE2NVwiLFxyXG4gICAgICAgICAgICBwcm9kdWN0cmVsYXRlOiBbXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aXRsZTogXCJCVUJCTEUgM1wiLFxyXG4gICAgICAgICAgICBkZXNrdG9wdG9wbGVmdDogXCI1NSUsNTUlXCIsXHJcbiAgICAgICAgICAgIG1vYmlsZXRvcGxlZnQ6IFwiNDglLDM1JVwiLFxyXG4gICAgICAgICAgICBidWJibGVzdHlsZXNjYWxlOiBcImIzLDE3MFwiLFxyXG4gICAgICAgICAgICBwcm9kdWN0cmVsYXRlOiBbXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aXRsZTogXCJCVUJCTEUgNFwiLFxyXG4gICAgICAgICAgICBkZXNrdG9wdG9wbGVmdDogXCI0MCUsODUlXCIsXHJcbiAgICAgICAgICAgIG1vYmlsZXRvcGxlZnQ6IFwiNzAlLDQwJVwiLFxyXG4gICAgICAgICAgICBidWJibGVzdHlsZXNjYWxlOiBcImI0LDE1MFwiLFxyXG4gICAgICAgICAgICBwcm9kdWN0cmVsYXRlOiBbXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aXRsZTogXCJCVUJCTEUgNVwiLFxyXG4gICAgICAgICAgICBkZXNrdG9wdG9wbGVmdDogXCI0OCUsNzAlXCIsXHJcbiAgICAgICAgICAgIG1vYmlsZXRvcGxlZnQ6IFwiNjAlLDgwJVwiLFxyXG4gICAgICAgICAgICBidWJibGVzdHlsZXNjYWxlOiBcImIyLDIxMFwiLFxyXG4gICAgICAgICAgICBwcm9kdWN0cmVsYXRlOiBbJ3AyJywgJ241J11cclxuICAgICAgICB9XHJcbiAgICBdLFxyXG4gICAgY3VycmVudGFuc3dlcjogXCJcIixcclxuXHJcbn0sXHJcbntcclxuICAgIHR5cGU6IFwiY2hvaWNlXCIsXHJcbiAgICBtYXhjaG9pY2U6IDEsXHJcbiAgICB0aXRsZTogXCLguKrguLXguILguK3guIfguITguLjguJPguITguLfguK0uLlwiLFxyXG4gICAgY29sb3JlbG06ICdsaW5lYXItZ3JhZGllbnQoMTQwZGVnLCAjZmVhOTI0IDEyJSwgI2U3MmJhMiA2MCUpJyxcclxuICAgIGFuc3dlcnM6IFt7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIuC5geC4lOC4h1wiLFxyXG4gICAgICAgICAgICBkZXNrdG9wdG9wbGVmdDogXCIzMCUsMjAlXCIsXHJcbiAgICAgICAgICAgIG1vYmlsZXRvcGxlZnQ6IFwiMzAlLDMwJVwiLFxyXG4gICAgICAgICAgICBidWJibGVzdHlsZXNjYWxlOiBcImIyLDE5MFwiLFxyXG4gICAgICAgICAgICBwcm9kdWN0cmVsYXRlOiBbXCJuMVwiXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aXRsZTogXCLguYDguILguLXguKLguKdcIixcclxuICAgICAgICAgICAgZGVza3RvcHRvcGxlZnQ6IFwiMzMlLDQzJVwiLFxyXG4gICAgICAgICAgICBtb2JpbGV0b3BsZWZ0OiBcIjI1JSw2NSVcIixcclxuICAgICAgICAgICAgYnViYmxlc3R5bGVzY2FsZTogXCJiNCwxNzBcIixcclxuICAgICAgICAgICAgcHJvZHVjdHJlbGF0ZTogW1wibjJcIl1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgdGl0bGU6IFwi4LmA4Lir4Lil4Li34Lit4LiHXCIsXHJcbiAgICAgICAgICAgIGRlc2t0b3B0b3BsZWZ0OiBcIjMwJSw4NSVcIixcclxuICAgICAgICAgICAgbW9iaWxldG9wbGVmdDogXCIzMCUsOTUlXCIsXHJcbiAgICAgICAgICAgIGJ1YmJsZXN0eWxlc2NhbGU6IFwiYjYsMTUwXCIsXHJcbiAgICAgICAgICAgIHByb2R1Y3RyZWxhdGU6IFtcIm4zXCJdXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIuC4quC5ieC4oVwiLFxyXG4gICAgICAgICAgICBkZXNrdG9wdG9wbGVmdDogXCI2MCUsMjAlXCIsXHJcbiAgICAgICAgICAgIG1vYmlsZXRvcGxlZnQ6IFwiNDUlLDMwJVwiLFxyXG4gICAgICAgICAgICBidWJibGVzdHlsZXNjYWxlOiBcImIzLDE5MFwiLFxyXG4gICAgICAgICAgICBwcm9kdWN0cmVsYXRlOiBbXCJuNFwiXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aXRsZTogXCLguIrguKHguJ7guLnguKfguKfguKdcIixcclxuICAgICAgICAgICAgZGVza3RvcHRvcGxlZnQ6IFwiNDYlLDMxJVwiLFxyXG4gICAgICAgICAgICBtb2JpbGV0b3BsZWZ0OiBcIjQxJSw2MCVcIixcclxuICAgICAgICAgICAgYnViYmxlc3R5bGVzY2FsZTogXCJiNSwxNjBcIixcclxuICAgICAgICAgICAgcHJvZHVjdHJlbGF0ZTogW1wibjVcIl1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgdGl0bGU6IFwi4LiZ4LmJ4Liz4LiV4Liy4LilXCIsXHJcbiAgICAgICAgICAgIGRlc2t0b3B0b3BsZWZ0OiBcIjMzJSw2MCVcIixcclxuICAgICAgICAgICAgbW9iaWxldG9wbGVmdDogXCI0NSUsOTUlXCIsXHJcbiAgICAgICAgICAgIGJ1YmJsZXN0eWxlc2NhbGU6IFwiYjEsMTY1XCIsXHJcbiAgICAgICAgICAgIHByb2R1Y3RyZWxhdGU6IFtcIm42XCJdXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIuC4meC5ieC4s+C5gOC4h+C4tOC4mVwiLFxyXG4gICAgICAgICAgICBkZXNrdG9wdG9wbGVmdDogXCI2MCUsNTAlXCIsXHJcbiAgICAgICAgICAgIG1vYmlsZXRvcGxlZnQ6IFwiNjYlLDMyJVwiLFxyXG4gICAgICAgICAgICBidWJibGVzdHlsZXNjYWxlOiBcImI0LDE3MFwiLFxyXG4gICAgICAgICAgICBwcm9kdWN0cmVsYXRlOiBbXCJuN1wiXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aXRsZTogXCLguJfguK3guIdcIixcclxuICAgICAgICAgICAgZGVza3RvcHRvcGxlZnQ6IFwiNjclLDM3JVwiLFxyXG4gICAgICAgICAgICBtb2JpbGV0b3BsZWZ0OiBcIjU1JSw2MCVcIixcclxuICAgICAgICAgICAgYnViYmxlc3R5bGVzY2FsZTogXCJiMywxNDBcIixcclxuICAgICAgICAgICAgcHJvZHVjdHJlbGF0ZTogW1wibjhcIl1cclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgdGl0bGU6IFwi4LmE4Lih4LmI4Lih4Li14Liq4Li1XCIsXHJcbiAgICAgICAgICAgIGRlc2t0b3B0b3BsZWZ0OiBcIjYwJSw2NCVcIixcclxuICAgICAgICAgICAgbW9iaWxldG9wbGVmdDogXCI2NSUsOTUlXCIsXHJcbiAgICAgICAgICAgIGJ1YmJsZXN0eWxlc2NhbGU6IFwiYjIsMTYwXCIsXHJcbiAgICAgICAgICAgIHByb2R1Y3RyZWxhdGU6IFtcIm45XCJdXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIuC4nOC4quC4oeC4nOC4quC4suC4mVwiLFxyXG4gICAgICAgICAgICBkZXNrdG9wdG9wbGVmdDogXCI2MSUsODQlXCIsXHJcbiAgICAgICAgICAgIG1vYmlsZXRvcGxlZnQ6IFwiNzUlLDcwJVwiLFxyXG4gICAgICAgICAgICBidWJibGVzdHlsZXNjYWxlOiBcImIxLDIzMFwiLFxyXG4gICAgICAgICAgICBwcm9kdWN0cmVsYXRlOiBbXCJuMTBcIl1cclxuICAgICAgICB9XHJcbiAgICBdLFxyXG4gICAgY3VycmVudGFuc3dlcjogXCJcIlxyXG59LFxyXG57XHJcbiAgICB0eXBlOiBcImNob2ljZVwiLFxyXG4gICAgbWF4Y2hvaWNlOiAxLFxyXG4gICAgdGl0bGU6IFwiV2hlcmUgaXMgeW91ciBCVUJCTEUgc3RheT9cIixcclxuICAgIGNvbG9yZWxtOiAncmFkaWFsLWdyYWRpZW50KGVsbGlwc2UgYXQgNiUgMjAlLCAjZGYzOWEyIDEyJSxyZ2IoNzgsIDI5LCAxMTEpIDYzJSwgIzM4NDE4NiAxMDMlKScsXHJcbiAgICBhbnN3ZXJzOiBbe1xyXG4gICAgICAgICAgICB0aXRsZTogXCJTS1lcIixcclxuICAgICAgICAgICAgZGVza3RvcHRvcGxlZnQ6IFwiNTUlLDMyJVwiLFxyXG4gICAgICAgICAgICBtb2JpbGV0b3BsZWZ0OiBcIjI5JSw2MCVcIixcclxuICAgICAgICAgICAgYnViYmxlc3R5bGVzY2FsZTogXCJiMiwxODBcIixcclxuICAgICAgICAgICAgcHJvZHVjdHJlbGF0ZTogW11cclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgdGl0bGU6IFwiSU5URVJORVRcIixcclxuICAgICAgICAgICAgZGVza3RvcHRvcGxlZnQ6IFwiMzMlLDUyJVwiLFxyXG4gICAgICAgICAgICBtb2JpbGV0b3BsZWZ0OiBcIjQ3JSwzNSVcIixcclxuICAgICAgICAgICAgYnViYmxlc3R5bGVzY2FsZTogXCJiNSwxODBcIixcclxuICAgICAgICAgICAgcHJvZHVjdHJlbGF0ZTogWydkNCddXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIkhPTUVcIixcclxuICAgICAgICAgICAgZGVza3RvcHRvcGxlZnQ6IFwiNTclLDY4JVwiLFxyXG4gICAgICAgICAgICBtb2JpbGV0b3BsZWZ0OiBcIjY0JSw4MCVcIixcclxuICAgICAgICAgICAgYnViYmxlc3R5bGVzY2FsZTogXCJiNCwyMTBcIixcclxuICAgICAgICAgICAgcHJvZHVjdHJlbGF0ZTogWydkNCddXHJcbiAgICAgICAgfVxyXG4gICAgXSxcclxuICAgIGN1cnJlbnRhbnN3ZXI6IFwiXCIsXHJcblxyXG59LFxyXG57XHJcbiAgICB0eXBlOiBcImNob2ljZVwiLFxyXG4gICAgbWF4Y2hvaWNlOiAxLFxyXG4gICAgdGl0bGU6IFwiSG93IEJVQkJMRSBwcmVmZXIgdG8gZHJpbms/XCIsXHJcbiAgICBjb2xvcmVsbTogJ3JhZGlhbC1ncmFkaWVudChlbGxpcHNlIGF0IDYlIDEwJSwgI2ZhMzA2MCAxMiUsICMyZTczZmYgOTAlKScsXHJcbiAgICBhbnN3ZXJzOiBbe1xyXG4gICAgICAgICAgICB0aXRsZTogXCJDb2xhXCIsXHJcbiAgICAgICAgICAgIGRlc2t0b3B0b3BsZWZ0OiBcIjU1JSwzMiVcIixcclxuICAgICAgICAgICAgbW9iaWxldG9wbGVmdDogXCIzMiUsODMlXCIsXHJcbiAgICAgICAgICAgIGJ1YmJsZXN0eWxlc2NhbGU6IFwiYjIsMTgwXCIsXHJcbiAgICAgICAgICAgIHByb2R1Y3RyZWxhdGU6IFsnZDEnXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aXRsZTogXCJGYW50YVwiLFxyXG4gICAgICAgICAgICBkZXNrdG9wdG9wbGVmdDogXCIzMyUsNTIlXCIsXHJcbiAgICAgICAgICAgIG1vYmlsZXRvcGxlZnQ6IFwiNDclLDM4JVwiLFxyXG4gICAgICAgICAgICBidWJibGVzdHlsZXNjYWxlOiBcImI1LDIwNVwiLFxyXG4gICAgICAgICAgICBwcm9kdWN0cmVsYXRlOiBbJ2QyJ11cclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgdGl0bGU6IFwiV2F0ZXJcIixcclxuICAgICAgICAgICAgZGVza3RvcHRvcGxlZnQ6IFwiNTclLDY4JVwiLFxyXG4gICAgICAgICAgICBtb2JpbGV0b3BsZWZ0OiBcIjcxJSw3MiVcIixcclxuICAgICAgICAgICAgYnViYmxlc3R5bGVzY2FsZTogXCJiNCwyMjBcIixcclxuICAgICAgICAgICAgcHJvZHVjdHJlbGF0ZTogWydkMyddXHJcbiAgICAgICAgfVxyXG4gICAgXSxcclxuICAgIGN1cnJlbnRhbnN3ZXI6IFwiXCIsXHJcblxyXG59LFxyXG57XHJcbiAgICB0eXBlOiBcImNob2ljZVwiLFxyXG4gICAgbWF4Y2hvaWNlOiA1LFxyXG4gICAgdGl0bGU6IFwiQlVCQkxFIHNheT88YnIvPjxzcGFuIGNsYXNzPVxcXCJzYnR0bFxcXCI+KOC5gOC4peC4t+C4reC4geC4leC4reC4muC5hOC4lOC5ieC4oeC4suC4geC4geC4p+C5iOC4siAxKTwvc3Bhbj5cIixcclxuICAgIGNvbG9yZWxtOiAncmFkaWFsLWdyYWRpZW50KGF0IDYlIDEwJSwgI2VkNWM0YSAxMiUsICM1ZjJlNWQgNzMlKScsXHJcbiAgICBhbnN3ZXJzOiBbe1xyXG4gICAgICAgICAgICB0aXRsZTogXCJ3b3d3d3dcIixcclxuICAgICAgICAgICAgZGVza3RvcHRvcGxlZnQ6IFwiNjAlLDIwJVwiLFxyXG4gICAgICAgICAgICBtb2JpbGV0b3BsZWZ0OiBcIjMxJSwzMCVcIixcclxuICAgICAgICAgICAgYnViYmxlc3R5bGVzY2FsZTogXCJiMSwxNjBcIixcclxuICAgICAgICAgICAgcHJvZHVjdHJlbGF0ZTogWydkNicsICdkNyddXHJcblxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aXRsZTogXCJld3d3d1wiLFxyXG4gICAgICAgICAgICBkZXNrdG9wdG9wbGVmdDogXCIzNiUsMzElXCIsXHJcbiAgICAgICAgICAgIG1vYmlsZXRvcGxlZnQ6IFwiMzclLDkwJVwiLFxyXG4gICAgICAgICAgICBidWJibGVzdHlsZXNjYWxlOiBcImIzLDE0MFwiLFxyXG4gICAgICAgICAgICBwcm9kdWN0cmVsYXRlOiBbJ2Q2JywgJ2Q3J11cclxuXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcImhleXlcIixcclxuICAgICAgICAgICAgZGVza3RvcHRvcGxlZnQ6IFwiNDUlLDQ3JVwiLFxyXG4gICAgICAgICAgICBtb2JpbGV0b3BsZWZ0OiBcIjUyJSwzOSVcIixcclxuICAgICAgICAgICAgYnViYmxlc3R5bGVzY2FsZTogXCJiNCwxNjBcIixcclxuICAgICAgICAgICAgcHJvZHVjdHJlbGF0ZTogWydkNicsICdkNyddXHJcblxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aXRsZTogXCJPdWNoaGhoaFwiLFxyXG4gICAgICAgICAgICBkZXNrdG9wdG9wbGVmdDogXCI1MiUsODAlXCIsXHJcbiAgICAgICAgICAgIG1vYmlsZXRvcGxlZnQ6IFwiNjAlLDg0JVwiLFxyXG4gICAgICAgICAgICBidWJibGVzdHlsZXNjYWxlOiBcImIyLDIxMFwiLFxyXG4gICAgICAgICAgICBwcm9kdWN0cmVsYXRlOiBbJ2Q2JywgJ2Q3J11cclxuXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIi4uLlwiLFxyXG4gICAgICAgICAgICBkZXNrdG9wdG9wbGVmdDogXCI0MiUsNjUlXCIsXHJcbiAgICAgICAgICAgIG1vYmlsZXRvcGxlZnQ6IFwiNzYlLDUwJVwiLFxyXG4gICAgICAgICAgICBidWJibGVzdHlsZXNjYWxlOiBcImIyLDE4NVwiLFxyXG4gICAgICAgICAgICBwcm9kdWN0cmVsYXRlOiBbXVxyXG5cclxuICAgICAgICB9XHJcbiAgICBdLFxyXG4gICAgY3VycmVudGFuc3dlcjogXCJcIlxyXG59LFxyXG57XHJcbiAgICB0eXBlOiBcImNob2ljZVwiLFxyXG4gICAgbWF4Y2hvaWNlOiA2LFxyXG4gICAgdGl0bGU6IFwiUGVyc29uYWxpdHkgb2YgYnViYmxlIDxici8+PHNwYW4gY2xhc3M9XFxcInNidHRsXFxcIj4o4LmA4Lil4Li34Lit4LiB4LiV4Lit4Lia4LmE4LiU4LmJ4Lih4Liy4LiB4LiB4Lin4LmI4LiyIDEpPC9zcGFuPlwiLFxyXG4gICAgY29sb3JlbG06ICdyYWRpYWwtZ3JhZGllbnQoZWxsaXBzZSBhdCA2JSAyMCUsICMwMWExZDYgMTIlLCM2ZDMyZDcgNzUlKScsXHJcbiAgICBhbnN3ZXJzOiBbe1xyXG4gICAgICAgICAgICB0aXRsZTogXCJzdHJvbmdcIixcclxuICAgICAgICAgICAgZGVza3RvcHRvcGxlZnQ6IFwiNjAlLDIwJVwiLFxyXG4gICAgICAgICAgICBtb2JpbGV0b3BsZWZ0OiBcIjMxJSwzMCVcIixcclxuICAgICAgICAgICAgYnViYmxlc3R5bGVzY2FsZTogXCJiMSwxNjBcIixcclxuICAgICAgICAgICAgcHJvZHVjdHJlbGF0ZTogWydkNicsICdkNyddXHJcblxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aXRsZTogXCJ0aGlua2VyXCIsXHJcbiAgICAgICAgICAgIGRlc2t0b3B0b3BsZWZ0OiBcIjM2JSwzMSVcIixcclxuICAgICAgICAgICAgbW9iaWxldG9wbGVmdDogXCIzNCUsOTAlXCIsXHJcbiAgICAgICAgICAgIGJ1YmJsZXN0eWxlc2NhbGU6IFwiYjMsMTQwXCIsXHJcbiAgICAgICAgICAgIHByb2R1Y3RyZWxhdGU6IFsnZDYnLCAnZDcnXVxyXG5cclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgdGl0bGU6IFwiRmVlbGVyXCIsXHJcbiAgICAgICAgICAgIGRlc2t0b3B0b3BsZWZ0OiBcIjQ2JSw0MSVcIixcclxuICAgICAgICAgICAgbW9iaWxldG9wbGVmdDogXCI0NSUsNTIlXCIsXHJcbiAgICAgICAgICAgIGJ1YmJsZXN0eWxlc2NhbGU6IFwiYjQsMTYwXCIsXHJcbiAgICAgICAgICAgIHByb2R1Y3RyZWxhdGU6IFsnZDYnLCAnZDcnXVxyXG5cclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgdGl0bGU6IFwiSW50cm92ZXJ0XCIsXHJcbiAgICAgICAgICAgIGRlc2t0b3B0b3BsZWZ0OiBcIjUyJSw4MCVcIixcclxuICAgICAgICAgICAgbW9iaWxldG9wbGVmdDogXCI2MCUsODQlXCIsXHJcbiAgICAgICAgICAgIGJ1YmJsZXN0eWxlc2NhbGU6IFwiYjIsMjEwXCIsXHJcbiAgICAgICAgICAgIHByb2R1Y3RyZWxhdGU6IFsnZDYnLCAnZDcnXVxyXG5cclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgdGl0bGU6IFwiRXh0cm92ZXJ0XCIsXHJcbiAgICAgICAgICAgIGRlc2t0b3B0b3BsZWZ0OiBcIjQyJSw2NSVcIixcclxuICAgICAgICAgICAgbW9iaWxldG9wbGVmdDogXCI3NiUsNTAlXCIsXHJcbiAgICAgICAgICAgIGJ1YmJsZXN0eWxlc2NhbGU6IFwiYjUsMTg1XCIsXHJcbiAgICAgICAgICAgIHByb2R1Y3RyZWxhdGU6IFtdXHJcblxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aXRsZTogXCJLaW5kXCIsXHJcbiAgICAgICAgICAgIGRlc2t0b3B0b3BsZWZ0OiBcIjY2JSw1NyVcIixcclxuICAgICAgICAgICAgbW9iaWxldG9wbGVmdDogXCI2MSUsMzMlXCIsXHJcbiAgICAgICAgICAgIGJ1YmJsZXN0eWxlc2NhbGU6IFwiYjYsMjAwXCIsXHJcbiAgICAgICAgICAgIHByb2R1Y3RyZWxhdGU6IFtdXHJcblxyXG4gICAgICAgIH1cclxuICAgIF0sXHJcbiAgICBjdXJyZW50YW5zd2VyOiBcIlwiXHJcbn0sIHtcclxuICAgIHR5cGU6IFwiY2hvaWNlXCIsXHJcbiAgICBtYXhjaG9pY2U6IDUsXHJcbiAgICB0aXRsZTogXCJCVUJCTEUgSE9CQlk/4oCLPGJyLz48c3BhbiBjbGFzcz1cXFwic2J0dGxcXFwiPijguYDguKXguLfguK3guIHguJXguK3guJrguYTguJTguYnguKHguLLguIHguIHguKfguYjguLIgMSk8L3NwYW4+XCIsXHJcbiAgICBjb2xvcmVsbTogJ3JhZGlhbC1ncmFkaWVudChlbGxpcHNlIGF0IDYlIDIwJSwgI2ZmNzIwMCAxMiUsI2IzMjYwMSA3NSUpJyxcclxuICAgIGFuc3dlcnM6IFt7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIlJ1blwiLFxyXG4gICAgICAgICAgICBkZXNrdG9wdG9wbGVmdDogXCI2MCUsMjAlXCIsXHJcbiAgICAgICAgICAgIG1vYmlsZXRvcGxlZnQ6IFwiMzElLDMwJVwiLFxyXG4gICAgICAgICAgICBidWJibGVzdHlsZXNjYWxlOiBcImIxLDE2MFwiLFxyXG4gICAgICAgICAgICBwcm9kdWN0cmVsYXRlOiBbXVxyXG5cclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgdGl0bGU6IFwiU2xlZXBcIixcclxuICAgICAgICAgICAgZGVza3RvcHRvcGxlZnQ6IFwiMzYlLDMxJVwiLFxyXG4gICAgICAgICAgICBtb2JpbGV0b3BsZWZ0OiBcIjM3JSw5MCVcIixcclxuICAgICAgICAgICAgYnViYmxlc3R5bGVzY2FsZTogXCJiMywxNDBcIixcclxuICAgICAgICAgICAgcHJvZHVjdHJlbGF0ZTogW11cclxuXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIkdhbWVcIixcclxuICAgICAgICAgICAgZGVza3RvcHRvcGxlZnQ6IFwiNDUlLDQ3JVwiLFxyXG4gICAgICAgICAgICBtb2JpbGV0b3BsZWZ0OiBcIjUwJSw0MCVcIixcclxuICAgICAgICAgICAgYnViYmxlc3R5bGVzY2FsZTogXCJiNCwxNjBcIixcclxuICAgICAgICAgICAgcHJvZHVjdHJlbGF0ZTogW11cclxuXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIkZseVwiLFxyXG4gICAgICAgICAgICBkZXNrdG9wdG9wbGVmdDogXCI1MiUsODAlXCIsXHJcbiAgICAgICAgICAgIG1vYmlsZXRvcGxlZnQ6IFwiNjAlLDg0JVwiLFxyXG4gICAgICAgICAgICBidWJibGVzdHlsZXNjYWxlOiBcImIyLDIxMFwiLFxyXG4gICAgICAgICAgICBwcm9kdWN0cmVsYXRlOiBbJ3AzJ11cclxuXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIkVhdFwiLFxyXG4gICAgICAgICAgICBkZXNrdG9wdG9wbGVmdDogXCI0MiUsNjUlXCIsXHJcbiAgICAgICAgICAgIG1vYmlsZXRvcGxlZnQ6IFwiNzYlLDUwJVwiLFxyXG4gICAgICAgICAgICBidWJibGVzdHlsZXNjYWxlOiBcImIyLDE4NVwiLFxyXG4gICAgICAgICAgICBwcm9kdWN0cmVsYXRlOiBbXVxyXG5cclxuICAgICAgICB9XHJcbiAgICBdLFxyXG4gICAgY3VycmVudGFuc3dlcjogXCJcIlxyXG59LFxyXG57XHJcbiAgICB0eXBlOiBcInNsaWRlXCIsXHJcbiAgICBtYXhjaG9pY2U6IDEsXHJcbiAgICB0aXRsZTogXCIgQlVCQkxFIExldmVsIHVwL21vbnRoXCIsXHJcbiAgICBjb2xvcmVsbTogJ2xpbmVhci1ncmFkaWVudCgwZGVnLCByZ2IoMjgsIDE4MywgMTMyKSA4MyUsIHJnYig4NiwgMTc3LCAyMzkpIDEwMCUpJyxcclxuICAgIGFuc3dlcnM6IFt7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIuC4meC5ieC4reC4ouC4geC4p+C5iOC4siAxIOC4hOC4o+C4seC5ieC4h+KAi1wiLFxyXG4gICAgICAgICAgICBkZXNrdG9wdG9wbGVmdDogXCI2MCUsMjAlXCIsXHJcbiAgICAgICAgICAgIG1vYmlsZXRvcGxlZnQ6IFwiMTAlLDIwJVwiLFxyXG4gICAgICAgICAgICBidWJibGVzdHlsZXNjYWxlOiBcImIxLDAuNzVcIixcclxuICAgICAgICAgICAgcHJvZHVjdHJlbGF0ZTogWydrMSddXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIjEtMyDguITguKPguLHguYnguIfigItcIixcclxuICAgICAgICAgICAgZGVza3RvcHRvcGxlZnQ6IFwiNjAlLDIwJVwiLFxyXG4gICAgICAgICAgICBtb2JpbGV0b3BsZWZ0OiBcIjEwJSwyMCVcIixcclxuICAgICAgICAgICAgYnViYmxlc3R5bGVzY2FsZTogXCJiMSwwLjc1XCIsXHJcbiAgICAgICAgICAgIHByb2R1Y3RyZWxhdGU6IFsnazEnXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aXRsZTogXCIzLTUg4LiE4Lij4Lix4LmJ4LiH4oCLXCIsXHJcbiAgICAgICAgICAgIGRlc2t0b3B0b3BsZWZ0OiBcIjYwJSwyMCVcIixcclxuICAgICAgICAgICAgbW9iaWxldG9wbGVmdDogXCIxMCUsMjAlXCIsXHJcbiAgICAgICAgICAgIGJ1YmJsZXN0eWxlc2NhbGU6IFwiYjEsMC43NVwiLFxyXG4gICAgICAgICAgICBwcm9kdWN0cmVsYXRlOiBbJ2sxJ11cclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgdGl0bGU6IFwi4Lih4Liy4LiB4LiB4Lin4LmI4LiyIDUg4LiE4Lij4Lix4LmJ4LiH4oCLXCIsXHJcbiAgICAgICAgICAgIGRlc2t0b3B0b3BsZWZ0OiBcIjYwJSwyMCVcIixcclxuICAgICAgICAgICAgbW9iaWxldG9wbGVmdDogXCIxMCUsMjAlXCIsXHJcbiAgICAgICAgICAgIGJ1YmJsZXN0eWxlc2NhbGU6IFwiYjEsMC43NVwiLFxyXG4gICAgICAgICAgICBwcm9kdWN0cmVsYXRlOiBbJ2sxJ11cclxuICAgICAgICB9XHJcbiAgICBdLFxyXG4gICAgY3VycmVudGFuc3dlcjogXCJcIlxyXG59LFxyXG57XHJcbiAgICB0eXBlOiBcImNob2ljZVwiLFxyXG4gICAgbWF4Y2hvaWNlOiAxLFxyXG4gICAgdGl0bGU6IFwiQWdlIG9mIEJ1YmJsZVwiLFxyXG4gICAgY29sb3JlbG06ICdyYWRpYWwtZ3JhZGllbnQoYXQgNiUgMjAlLCAjZjUxOTc2IDEyJSwgIzUxMTE2ZCA3NSUpJyxcclxuICAgIGFuc3dlcnM6IFt7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIkJhYnlcIixcclxuICAgICAgICAgICAgZGVza3RvcHRvcGxlZnQ6IFwiNTElLDIzJVwiLFxyXG4gICAgICAgICAgICBtb2JpbGV0b3BsZWZ0OiBcIjIyJSw3NSVcIixcclxuICAgICAgICAgICAgYnViYmxlc3R5bGVzY2FsZTogXCJiMywxNjBcIixcclxuICAgICAgICAgICAgcHJvZHVjdHJlbGF0ZTogWydlMSddXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIlRvZGRsZVwiLFxyXG4gICAgICAgICAgICBkZXNrdG9wdG9wbGVmdDogXCI1MSUsNDMlXCIsXHJcbiAgICAgICAgICAgIG1vYmlsZXRvcGxlZnQ6IFwiMzUlLDQ1JVwiLFxyXG4gICAgICAgICAgICBidWJibGVzdHlsZXNjYWxlOiBcImIzLDE2MFwiLFxyXG4gICAgICAgICAgICBwcm9kdWN0cmVsYXRlOiBbJ2UxJ11cclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgdGl0bGU6IFwiWW91bmcgZHVtYiBhbmQgYnJva2VlZVwiLFxyXG4gICAgICAgICAgICBkZXNrdG9wdG9wbGVmdDogXCI0OCUsNjAlXCIsXHJcbiAgICAgICAgICAgIG1vYmlsZXRvcGxlZnQ6IFwiNDclLDg1JVwiLFxyXG4gICAgICAgICAgICBidWJibGVzdHlsZXNjYWxlOiBcImIyLDE3NVwiLFxyXG4gICAgICAgICAgICBwcm9kdWN0cmVsYXRlOiBbJ2UxJ11cclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgdGl0bGU6IFwiQWR1bHRcIixcclxuICAgICAgICAgICAgZGVza3RvcHRvcGxlZnQ6IFwiNDklLDgwJVwiLFxyXG4gICAgICAgICAgICBtb2JpbGV0b3BsZWZ0OiBcIjU4JSw1MCVcIixcclxuICAgICAgICAgICAgYnViYmxlc3R5bGVzY2FsZTogXCJiNCwxOTBcIixcclxuICAgICAgICAgICAgcHJvZHVjdHJlbGF0ZTogWydlMSddXHJcbiAgICAgICAgfVxyXG4gICAgXSxcclxuICAgIGN1cnJlbnRhbnN3ZXI6IFwiXCJcclxufSxcclxue1xyXG4gICAgdHlwZTogXCJjaG9pY2VcIixcclxuICAgIG1heGNob2ljZTogMyxcclxuICAgIHRpdGxlOiBcIkJVQkxFIE5VTUJFUiA8YnIvPjxzcGFuIGNsYXNzPVxcXCJzYnR0bFxcXCI+KOC5gOC4peC4t+C4reC4geC4leC4reC4muC5hOC4lOC5ieC4oeC4suC4geC4geC4p+C5iOC4siAxKTwvc3Bhbj5cIixcclxuICAgIGNvbG9yZWxtOiAncmFkaWFsLWdyYWRpZW50KGF0IDYlIDIwJSwgIzA2OWY5MiAxMiUsICMwMDVhODAgNzUlKScsXHJcbiAgICBhbnN3ZXJzOiBbe1xyXG4gICAgICAgICAgICB0aXRsZTogXCIxXCIsXHJcbiAgICAgICAgICAgIGRlc2t0b3B0b3BsZWZ0OiBcIjUxJSwyMyVcIixcclxuICAgICAgICAgICAgbW9iaWxldG9wbGVmdDogXCIyOCUsNDAlXCIsXHJcbiAgICAgICAgICAgIGJ1YmJsZXN0eWxlc2NhbGU6IFwiYjMsMTYwXCIsXHJcbiAgICAgICAgICAgIHByb2R1Y3RyZWxhdGU6IFsnaTEnXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aXRsZTogXCIwXCIsXHJcbiAgICAgICAgICAgIGRlc2t0b3B0b3BsZWZ0OiBcIjM2JSw0MyVcIixcclxuICAgICAgICAgICAgbW9iaWxldG9wbGVmdDogXCI0NyUsODUlXCIsXHJcbiAgICAgICAgICAgIGJ1YmJsZXN0eWxlc2NhbGU6IFwiYjIsMTc1XCIsXHJcbiAgICAgICAgICAgIHByb2R1Y3RyZWxhdGU6IFsnaTEnXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aXRsZTogXCIwMTEwMDFcIixcclxuICAgICAgICAgICAgZGVza3RvcHRvcGxlZnQ6IFwiNDklLDU4JVwiLFxyXG4gICAgICAgICAgICBtb2JpbGV0b3BsZWZ0OiBcIjUwJSw0NCVcIixcclxuICAgICAgICAgICAgYnViYmxlc3R5bGVzY2FsZTogXCJiNCwyMDBcIixcclxuICAgICAgICAgICAgcHJvZHVjdHJlbGF0ZTogWydpMSddXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIm5vbmUgb2YgYWJvdmVcIixcclxuICAgICAgICAgICAgZGVza3RvcHRvcGxlZnQ6IFwiNDklLDgwJVwiLFxyXG4gICAgICAgICAgICBtb2JpbGV0b3BsZWZ0OiBcIjcyJSw2MCVcIixcclxuICAgICAgICAgICAgYnViYmxlc3R5bGVzY2FsZTogXCJiMSwxNTBcIixcclxuICAgICAgICAgICAgcHJvZHVjdHJlbGF0ZTogWydpMSddXHJcbiAgICAgICAgfVxyXG4gICAgXSxcclxuICAgIGN1cnJlbnRhbnN3ZXI6IFwiXCJcclxufSxcclxue1xyXG4gICAgdHlwZTogXCJjaG9pY2VcIixcclxuICAgIG1heGNob2ljZTogOCxcclxuICAgIHRpdGxlOiBcIldheSB5b3Ugd2FudCBmb3IgeW91ciBidWJibGU8YnIvPjxzcGFuIGNsYXNzPVxcXCJzYnR0bFxcXCI+KOC5gOC4peC4t+C4reC4geC4leC4reC4muC5hOC4lOC5ieC4oeC4suC4geC4geC4p+C5iOC4siAxKTwvc3Bhbj5cIixcclxuICAgIGNvbG9yZWxtOiAncmFkaWFsLWdyYWRpZW50KGF0IDYlIDIwJSwgI2ZmYWYwMiAxMiUsICNmZjJkNmUgNzUlKScsXHJcbiAgICBhbnN3ZXJzOiBbe1xyXG4gICAgICAgICAgICB0aXRsZTogXCLguJXguYnguK3guIfguIHguLLguKPguYDguIfguLTguJnguKXguIfguJfguLjguJnguYDguJ7guLTguYjguKFcIixcclxuICAgICAgICAgICAgZGVza3RvcHRvcGxlZnQ6IFwiNTElLDIzJVwiLFxyXG4gICAgICAgICAgICBtb2JpbGV0b3BsZWZ0OiBcIjIyJSwzNiVcIixcclxuICAgICAgICAgICAgYnViYmxlc3R5bGVzY2FsZTogXCJiMywxNjBcIixcclxuICAgICAgICAgICAgcHJvZHVjdHJlbGF0ZTogWydlMSddXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIkJlY29tZSBraW5nIG9mIGJ1YmJsZSB3b3JsZFwiLFxyXG4gICAgICAgICAgICBkZXNrdG9wdG9wbGVmdDogXCIzNiUsMzglXCIsXHJcbiAgICAgICAgICAgIG1vYmlsZXRvcGxlZnQ6IFwiMjglLDgyJVwiLFxyXG4gICAgICAgICAgICBidWJibGVzdHlsZXNjYWxlOiBcImIyLDIwNVwiLFxyXG4gICAgICAgICAgICBwcm9kdWN0cmVsYXRlOiBbJ3AzJ11cclxuICAgICAgICB9LFxyXG4gICAgICAgIHtcclxuICAgICAgICAgICAgdGl0bGU6IFwiU2xlZXBcIixcclxuICAgICAgICAgICAgZGVza3RvcHRvcGxlZnQ6IFwiNjUlLDQxJVwiLFxyXG4gICAgICAgICAgICBtb2JpbGV0b3BsZWZ0OiBcIjM4JSw1NSVcIixcclxuICAgICAgICAgICAgYnViYmxlc3R5bGVzY2FsZTogXCJiNCwyMTVcIixcclxuICAgICAgICAgICAgcHJvZHVjdHJlbGF0ZTogWydkNiddXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIlN0YXlcIixcclxuICAgICAgICAgICAgZGVza3RvcHRvcGxlZnQ6IFwiMzglLDU0JVwiLFxyXG4gICAgICAgICAgICBtb2JpbGV0b3BsZWZ0OiBcIjQ4JSw4OCVcIixcclxuICAgICAgICAgICAgYnViYmxlc3R5bGVzY2FsZTogXCJiMSwxODVcIixcclxuICAgICAgICAgICAgcHJvZHVjdHJlbGF0ZTogWydkNyddXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIkdvIHRvIHRoZSB6b29cIixcclxuICAgICAgICAgICAgZGVza3RvcHRvcGxlZnQ6IFwiNjglLDU3JVwiLFxyXG4gICAgICAgICAgICBtb2JpbGV0b3BsZWZ0OiBcIjU1JSwzNCVcIixcclxuICAgICAgICAgICAgYnViYmxlc3R5bGVzY2FsZTogXCJiNiwyMTVcIixcclxuICAgICAgICAgICAgcHJvZHVjdHJlbGF0ZTogWydkNSddXHJcbiAgICAgICAgfSxcclxuICAgICAgICB7XHJcbiAgICAgICAgICAgIHRpdGxlOiBcIlBsYXkgc29tZSB2aWRlbyBnYW1lXCIsXHJcbiAgICAgICAgICAgIGRlc2t0b3B0b3BsZWZ0OiBcIjM4JSw2NyVcIixcclxuICAgICAgICAgICAgbW9iaWxldG9wbGVmdDogXCI3MCUsNzclXCIsXHJcbiAgICAgICAgICAgIGJ1YmJsZXN0eWxlc2NhbGU6IFwiYjUsMjAwXCIsXHJcbiAgICAgICAgICAgIHByb2R1Y3RyZWxhdGU6IFsnaTEnXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aXRsZTogXCJXYXRjaCBuZXRmbGl4XCIsXHJcbiAgICAgICAgICAgIGRlc2t0b3B0b3BsZWZ0OiBcIjQyJSw4MiVcIixcclxuICAgICAgICAgICAgbW9iaWxldG9wbGVmdDogXCI3NSUsNDklXCIsXHJcbiAgICAgICAgICAgIGJ1YmJsZXN0eWxlc2NhbGU6IFwiYjMsMTkwXCIsXHJcbiAgICAgICAgICAgIHByb2R1Y3RyZWxhdGU6IFsncDEnXVxyXG4gICAgICAgIH0sXHJcbiAgICAgICAge1xyXG4gICAgICAgICAgICB0aXRsZTogXCJHbyB0byB0aGUgbW9vblwiLFxyXG4gICAgICAgICAgICBkZXNrdG9wdG9wbGVmdDogXCI2OCUsNzIlXCIsXHJcbiAgICAgICAgICAgIG1vYmlsZXRvcGxlZnQ6IFwiNTclLDU5JVwiLFxyXG4gICAgICAgICAgICBidWJibGVzdHlsZXNjYWxlOiBcImIxLDIxMFwiLFxyXG4gICAgICAgICAgICBwcm9kdWN0cmVsYXRlOiBbJ2sxJ11cclxuICAgICAgICB9XHJcbiAgICBdLFxyXG4gICAgY3VycmVudGFuc3dlcjogXCJcIlxyXG59LFxyXG5dO1xyXG5cclxudmFyIGN1cnJlbnRRdWVzdGlvbiA9IDA7XHJcbnZhciBjb3VudCA9IDA7XHJcbi8vICQoZnVuY3Rpb24gKCkge1xyXG4vLyAgICAgJChcIiNpbmZvLWZvcm1cIikucGFyc2xleSgpXHJcbi8vICAgICAgICAgLm9uKFwiZm9ybTpzdWNjZXNzXCIsIGZ1bmN0aW9uICgpIHtcclxuLy8gICAgICAgICAgICAgZXZlbnQucHJldmVudERlZmF1bHQoKTtcclxuXHJcbi8vICAgICAgICAgICAgIC8qIOC4m+C4uOC5iOC4oeC4q+C4meC5ieC4smZvcm0gKi9cclxuLy8gICAgICAgICAgICAgJChcIiNidG4tbmV4dC1qc1wiKS5jbGljaygpO1xyXG5cclxuLy8gICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbi8vICAgICAgICAgICAgICAgICAkKCcuYnRud3JhcCcpLmZhZGVJbigpO1xyXG4vLyAgICAgICAgICAgICAgICAgJCgnLmJvdHRsZXdyYXAsI2J1YmJsZUNhbnZhcywuY291bnRlcndyYXAnKS5jc3Moe1xyXG4vLyAgICAgICAgICAgICAgICAgICAgIHpJbmRleDogNixcclxuLy8gICAgICAgICAgICAgICAgIH0pXHJcbi8vICAgICAgICAgICAgICAgICAkKCcuYm90dGxld3JhcCcpLmFkZENsYXNzKCdzaG93Jyk7XHJcbi8vICAgICAgICAgICAgIH0sIDEyMDApO1xyXG5cclxuLy8gICAgICAgICAgICAgcmV0dXJuIGZhbHNlO1xyXG4vLyAgICAgICAgIH0pXHJcbi8vICAgICAgICAgLm9uKFwiZm9ybTplcnJvclwiLCBmdW5jdGlvbiAoKSB7XHJcbi8vICAgICAgICAgICAgICQoXCJpbnB1dCNzdWJtaXRcIikuc2hvdygpO1xyXG4vLyAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbi8vICAgICAgICAgfSlcclxuLy8gfSlcclxuXHJcblxyXG5cclxuJChkb2N1bWVudCkucmVhZHkoZnVuY3Rpb24gKCkge1xyXG4gICAgJCgnI2xhbmRpbmdQYWdlICNtYXNrX2RpdiAubWFza19kaXZfd3JhcHBlciAucmVtYXJrX2NvbnRhaW5lciAucmVtYXJrdHh0JykuYXBwZW5kKCc8c3Bhbj7CqSDguKrguIfguKfguJnguKXguLTguILguKrguLTguJfguJjguLTguYwgMjU2Mzwvc3Bhbj4nKTtcclxuICAgICQoJy5zbGljay1pdGVtcycpLnNsaWNrKHtcclxuICAgICAgICBpbmZpbml0ZTogdHJ1ZSxcclxuICAgICAgICBsb29wOiB0cnVlLFxyXG4gICAgICAgIHNsaWRlc1RvU2hvdzogMTAsXHJcbiAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDEwLFxyXG4gICAgICAgIGF1dG9wbGF5OiB0cnVlLFxyXG4gICAgICAgIGF1dG9wbGF5U3BlZWQ6IDUwMDAsXHJcbiAgICAgICAgZG90czogdHJ1ZSxcclxuICAgICAgICBhcnJvd3M6IGZhbHNlLFxyXG4gICAgICAgIHJlc3BvbnNpdmU6IFt7XHJcbiAgICAgICAgICAgIGJyZWFrcG9pbnQ6IDQ4MCxcclxuICAgICAgICAgICAgc2V0dGluZ3M6IHtcclxuICAgICAgICAgICAgICAgIGluZmluaXRlOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgYXV0b3BsYXk6IHRydWUsXHJcbiAgICAgICAgICAgICAgICBhdXRvcGxheVNwZWVkOiA1MDAwLFxyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TaG93OiA1LFxyXG4gICAgICAgICAgICAgICAgc2xpZGVzVG9TY3JvbGw6IDUsXHJcbiAgICAgICAgICAgICAgICBkb3RzOiB0cnVlLFxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgXVxyXG4gICAgfSk7XHJcbiAgICAkKFwiLm1vcmUtaW5mb1wiKS5jbGljaygpO1xyXG4gICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgaWYgKCQoXCIubGl0eVwiKS5oYXNDbGFzcyhcImxpdHktb3BlbmVkXCIpKSB7XHJcbiAgICAgICAgICAgICQoXCIubGl0eS1jbG9zZVwiKS5jbGljaygpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9LCA1MDAwKTtcclxuICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICQoXCIubGl0eS1vcGVuZWRcIikuZmFkZUluKCk7XHJcbiAgICB9LCAyMDApO1xyXG4gICAgaW5pdEJhY2tncm91bmRBbmRFbG0oKTsgLy9iZyAmIGJvdHRsZVxyXG4gICAgaWYgKGdldFVybFZhcnMoKVsnZm9ybSddICE9IG51bGwgJiYgZ2V0VXJsVmFycygpWydmb3JtJ10gPT0gMSkge1xyXG4gICAgICAgICQoXCIjbWFza19kaXYsLm1hc2tfZGl2X291dGVyXCIpLmFkZENsYXNzKFwid2F2ZV9oaWRlXCIpO1xyXG4gICAgICAgICQoXCIjZm9ybVwiKS5hZGRDbGFzcyhcImFjdGl2ZVwiKTtcclxuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgJCgnI21hc2tfZGl2LC5tYXNrX2Rpdl9vdXRlcicpLmRlbGF5KDUwMCkuaGlkZSgpO1xyXG4gICAgICAgIH0sIDUwMDApO1xyXG4gICAgfVxyXG4gICAgaWYgKGdldFVybFZhcnMoKVsnZm9ybSddICE9IG51bGwgJiYgZ2V0VXJsVmFycygpWydmb3JtJ10gPT0gMikge1xyXG4gICAgICAgICQoXCIubGl0eS1jbG9zZVwiKS5jbGljaygpO1xyXG4gICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAkKFwiI2J0bi1zdGFydC1qc1wiKS5jbGljaygpO1xyXG4gICAgICAgIH0sIDUwMCk7XHJcbiAgICB9XHJcbiAgICAkKHdpbmRvdykucmVzaXplKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBpZiAoJCgnI2J1YmJsZUNhbnZhcyAuYnViYmxlY2hvaWNlJykubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgaW5pdFBvc2l0aW9uQnViYmxlKCk7XHJcbiAgICAgICAgICAgICAgICBwb3NpdGlvbkJ1YmJsZSgpO1xyXG4gICAgICAgICAgICB9LCA1MDApO1xyXG5cclxuICAgICAgICB9XHJcbiAgICB9KTtcclxuICAgIC8qdGVzdCovXHJcbiAgICAkKFwiI3Rlc3RcIikuY2xpY2soZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHNoYWtlX3N0Z2UwKCk7XHJcbiAgICB9KTtcclxuXHJcbiAgICAkKCcuYnRuLXN0YXJ0LWpzMicpLmNsaWNrKGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgICB9KVxyXG5cclxuICAgIC8qIOC4m+C4uOC5iOC4oSBwb3B1cCDguKvguJnguYnguLLguYHguKPguIEgKi9cclxuICAgIGlmICgkKFwiLmxpdHlcIikuaGFzQ2xhc3MoXCJsaXR5LW9wZW5lZFwiKSkge1xyXG4gICAgICAgICQoXCIjbGlnaHRib3hfdmRvIC5tYWUtbWFuZWVcIilbMF0ucGxheSgpO1xyXG4gICAgfSBlbHNlIHt9XHJcblxyXG4gICAgLyog4Lir4LiZ4LmJ4Liy4LiB4Lij4Liw4Lib4LmL4Lit4LiH4LiB4Lil4Lix4Lia4LmE4Lib4Lir4LiZ4LmJ4Liy4LiE4Liz4LiW4Liy4LihICovXHJcbiAgICAkKCcjYnRuLXByZXYtcmVzdWx0LWpzJykuY2xpY2soZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIHZhciBjdXJyZW50O1xyXG4gICAgICAgIHZhciBiZ19jdXJyZW50O1xyXG4gICAgICAgIHZhciBwcmV2X3F1ZXN0aW9uO1xyXG5cclxuICAgICAgICBjdXJyZW50ID0gJChcIi5xdWVzdGlvbi5hY3RpdmVcIik7XHJcbiAgICAgICAgYmdfY3VycmVudCA9IGN1cnJlbnQuZmluZCgnLmJnX2dyYWRpZW50Jyk7XHJcbiAgICAgICAgcHJldl9xdWVzdGlvbiA9IGNvdW50IC0gMTtcclxuICAgICAgICBiZ19jdXJyZW50LmFkZENsYXNzKCduZXh0LXN0YWdlJyk7XHJcbiAgICAgICAgYmdfY3VycmVudC5yZW1vdmVDbGFzcygnY3VycmVudC1zdGFnZScpO1xyXG4gICAgICAgICQoXCIuYmdfZ3JhZGllbnQtMTFcIikucmVtb3ZlQ2xhc3MoJ3ByZXYtc3RhZ2UnKTtcclxuICAgICAgICAkKFwiLmJnX2dyYWRpZW50LTExXCIpLmFkZENsYXNzKCdjdXJyZW50LXN0YWdlJyk7XHJcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICQoXCIjcXVlc3Rpb25fMTFcIikuYWRkQ2xhc3MoXCJhY3RpdmVcIik7XHJcbiAgICAgICAgfSwgNTAwKTtcclxuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgYmdfY3VycmVudC5jbG9zZXN0KCcucXVlc3Rpb24nKS5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtcclxuICAgICAgICB9LCA2MDApO1xyXG5cclxuXHJcbiAgICAgICAgJCgnLmJvdHRsZXdyYXAnKS5hZGRDbGFzcygnc2hvdycpO1xyXG4gICAgICAgICQoJyNyZXN1bHQtcGFnZScpLnJlbW92ZUF0dHIoJ3N0eWxlJykuaGlkZSgpO1xyXG4gICAgICAgICQoJy5idG53cmFwLCAuY291bnRlcndyYXAnKS5mYWRlSW4oKTtcclxuICAgICAgICAvLyBhbGVydChcInNob3cgbmV4dCBidG5cIik7XHJcbiAgICAgICAgJCgnLmJvdHRsZXdyYXAsI2J1YmJsZUNhbnZhcywuY291bnRlcndyYXAnKS5jc3Moe1xyXG4gICAgICAgICAgICB6SW5kZXg6IDYsXHJcbiAgICAgICAgfSk7XHJcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIGluaXRQb3NpdGlvbkJ1YmJsZSgpO1xyXG4gICAgICAgICAgICBnb3RvUXVlc3Rpb24oY3VycmVudFF1ZXN0aW9uKTtcclxuICAgICAgICAgICAgY2hlY2tCdWJibGVBY3RpdmUoY291bnQpXHJcbiAgICAgICAgfSwgMTAwMCk7XHJcblxyXG4gICAgfSk7XHJcblxyXG5cclxuICAgIC8qIOC4m+C4uOC5iOC4oeC5gOC4o+C4tOC5iOC4oeC4q+C4meC5ieC4suC5geC4o+C4gSAqL1xyXG4gICAgJChcIiNidG4tc3RhcnQtanNcIikuY2xpY2soZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICQoJy5tYXNrX2Rpdl9vdXRlcicpLmFkZENsYXNzKFwid2F2ZV9oaWRlXCIpO1xyXG4gICAgICAgICQoXCIuaGVhZGVyX2NvbnRhaW5lcl9sZWZ0IGFcIikucmVtb3ZlQ2xhc3MoXCJhY3RpdmVcIik7XHJcbiAgICAgICAgJChcIi5idG4tc3RhcnQtanMyXCIpLmFkZENsYXNzKFwiYWN0aXZlXCIpO1xyXG4gICAgICAgICQoXCJpbnB1dC5tZW51LWJ0blwiKS5wcm9wKCdjaGVja2VkJywgZmFsc2UpO1xyXG4gICAgICAgIC8vIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgIC8vICAgICAkKCd2aWRlbycpLnJlbW92ZUNsYXNzKFwidmlkZW9fdHJhbnNpdGlvblwiKTtcclxuICAgICAgICAvLyB9LCAyMDApO1xyXG4gICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAkKCcubWFza19kaXZfb3V0ZXInKS5mYWRlT3V0KCk7XHJcbiAgICAgICAgfSwgMTAwKTtcclxuICAgICAgICAvKiDguJvguLjguYjguKHguKvguJnguYnguLJmb3JtICovXHJcbiAgICAgICAgJChcIiNidG4tbmV4dC1qc1wiKS5jbGljaygpO1xyXG5cclxuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgJCgnLmJ0bndyYXAnKS5mYWRlSW4oKTtcclxuICAgICAgICAgICAgJCgnI2Zvb3RlcicpLmZhZGVJbigpO1xyXG4gICAgICAgICAgICAkKCcuYm90dGxld3JhcCwjYnViYmxlQ2FudmFzLC5jb3VudGVyd3JhcCcpLmNzcyh7XHJcbiAgICAgICAgICAgICAgICB6SW5kZXg6IDYsXHJcbiAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICQoJy5ib3R0bGV3cmFwJykuYWRkQ2xhc3MoJ3Nob3cnKTtcclxuICAgICAgICB9LCAxMDAwKTtcclxuICAgICAgICAvLyAkKFwiI2J0bi1uZXh0LWpzXCIpLmNsaWNrKCk7XHJcblxyXG4gICAgfSk7XHJcblxyXG4gICAgLyog4Lib4Li44LmI4Lih4Lir4LiZ4LmJ4Liy4LiW4Lix4LiU4LmE4LibICovXHJcbiAgICAkKCcuYnRud3JhcCcpLmhpZGUoKTtcclxuICAgICQoXCIjYnRuLW5leHQtanNcIikuY2xpY2soZnVuY3Rpb24gKGUpIHtcclxuXHJcbiAgICAgICAgJChcIi5idWJibGVjaG9pY2VcIikuYWRkQ2xhc3MoXCJldmVudG5vbmVcIik7XHJcbiAgICAgICAgdmFyIGN1cnJlbnQ7XHJcbiAgICAgICAgdmFyIGJnX2N1cnJlbnQ7XHJcbiAgICAgICAgdmFyIG5leHRfYmc7XHJcbiAgICAgICAgdmFyIG5leHRfcXVlc3Rpb247XHJcbiAgICAgICAgaWYgKGN1cnJlbnRRdWVzdGlvbiArIDEgPCBxdWVzdGlvbnMubGVuZ3RoICYmIHF1ZXN0aW9uc1twYXJzZUludChjdXJyZW50UXVlc3Rpb24pICsgMV0uY3VycmVudGFuc3dlciAhPSAnJykge1xyXG4gICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICQoJyNidG4tbmV4dC1qcycpLnJlbW92ZUNsYXNzKCdoaWRlJyk7XHJcbiAgICAgICAgICAgIH0sIDMwMDApO1xyXG4gICAgICAgIH1cclxuICAgICAgICBpZiAoY3VycmVudFF1ZXN0aW9uICsgMSA9PSAxICYmIHF1ZXN0aW9uc1sxXS5jdXJyZW50YW5zd2VyICE9ICcnICYmIHBhcnNlSW50KHF1ZXN0aW9uc1swXS5jdXJyZW50YW5zd2VyKSAhPSA0KSB7XHJcbiAgICAgICAgICAgIHByb2R1Y3RBcnJheSA9IHByb2R1Y3RBcnJheS5jb25jYXQocXVlc3Rpb25zWzFdLmFuc3dlcnNbcGFyc2VJbnQocXVlc3Rpb25zWzFdLmN1cnJlbnRhbnN3ZXIpXS5wcm9kdWN0cmVsYXRlKTtcclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoJChcIi5xdWVzdGlvblwiKS5oYXNDbGFzcyhcImFjdGl2ZVwiKSkge1xyXG4gICAgICAgICAgICBjdXJyZW50ID0gJChcIi5xdWVzdGlvbi5hY3RpdmVcIik7XHJcbiAgICAgICAgICAgIGJnX2N1cnJlbnQgPSBjdXJyZW50LmZpbmQoJy5iZ19ncmFkaWVudCcpO1xyXG4gICAgICAgICAgICBuZXh0X3F1ZXN0aW9uID0gY3VycmVudC5kYXRhKFwicXVlc3Rpb25cIik7XHJcbiAgICAgICAgICAgICQoJyNidG4tbmV4dC1qcywjYnRuLXByZXYtanMnKS5hZGRDbGFzcygnaGlkZScpO1xyXG5cclxuICAgICAgICAgICAgaWYgKG5leHRfcXVlc3Rpb24gPT0gMSkge1xyXG4gICAgICAgICAgICAgICAgb3ZlcmxheUVsbSgxNTAwKTtcclxuICAgICAgICAgICAgICAgIC8qIOC4hOC4s+C4luC4suC4oeC4l+C4teC5iDEgKi9cclxuICAgICAgICAgICAgICAgIGluaXRQb3NpdGlvbkJ1YmJsZSgpO1xyXG4gICAgICAgICAgICAgICAgZ290b1F1ZXN0aW9uKDApO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKG5leHRfcXVlc3Rpb24gPiBxdWVzdGlvbnMubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICBvdmVybGF5RWxtKDMwMDApO1xyXG4gICAgICAgICAgICAgICAgYnViYmxlRG93bigpO1xyXG4gICAgICAgICAgICAgICAgJCgnLmJvdHRsZXdyYXAnKS5yZW1vdmVDbGFzcygnc2hvdycpO1xyXG4gICAgICAgICAgICAgICAgcmVzdWx0Q2FsY3VsYXRvcigpO1xyXG4gICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVtb3ZlQWxsQnViYmxlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgJCgnI2J1YmJsZUNhbnZhcywuY291bnRlcndyYXAnKS5yZW1vdmVBdHRyKCdzdHlsZScpLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgICAgICBzaGFrZV9zdGdlMCgpO1xyXG4gICAgICAgICAgICAgICAgICAgICQoJyNyZXN1bHQtcGFnZSwgI2J0bi1wcmV2LWpzJykucmVtb3ZlQ2xhc3MoJ2hpZGUnKTtcclxuICAgICAgICAgICAgICAgIH0sIDEwMDApO1xyXG5cclxuICAgICAgICAgICAgICAgIC8qIOC4geC4lOC4ouC5ieC4reC4meC4geC4peC4seC4miDguKXguYnguLLguIfguIHguKPguLDguLDguJvguYvguK3guIcgY2xlYXIgYWxsIGluIHJlc3VsdCBzdGF0ZSB3aGVuIGNsaWNrIGJhY2sgKi9cclxuICAgICAgICAgICAgICAgICQoJyNidXNpbmVzc25hbWUnKS52YWwoJycpO1xyXG4gICAgICAgICAgICAgICAgJChcInNwYW4uc3RhZ2UtMFwiKS5yZW1vdmVDbGFzcyhcImhpZGUtZWxtXCIpO1xyXG4gICAgICAgICAgICAgICAgJChcImRpdi5idXNpbmVzc19uYW1lXCIpLnJlbW92ZUNsYXNzKFwiaGlkZS1lbG1cIik7XHJcbiAgICAgICAgICAgICAgICAkKFwicC5idXNpbmVzc19uYW1lXCIpLmFkZENsYXNzKFwiaGlkZS1lbG1cIik7XHJcbiAgICAgICAgICAgICAgICAkKFwiI2J0bi1zaG93LXJlc3VsdC1qc1wiKS5hZGRDbGFzcyhcImhpZGRlblwiKTtcclxuICAgICAgICAgICAgICAgICQoXCJkaXYjYnRuXCIpLmFkZENsYXNzKFwiaGlkZVwiKTtcclxuICAgICAgICAgICAgICAgICQoXCJzcGFuLnN0YWdlLTFcIikuYWRkQ2xhc3MoXCJoaWRlLWVsbVwiKTtcclxuICAgICAgICAgICAgICAgICQoXCIuaW1nIGltZ1wiKS5hZGRDbGFzcyhcInN0YWdlLTFcIikucmVtb3ZlQ2xhc3MoXCJzdGFnZS0yXCIpO1xyXG4gICAgICAgICAgICAgICAgJCgnI2J0bi1zaGFrZS1zdWJtaXQtanMnKS5hZGRDbGFzcyhcImhpZGRlblwiKTtcclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIG92ZXJsYXlFbG0oMzAwMCk7XHJcbiAgICAgICAgICAgICAgICBpZiAocXVlc3Rpb25zW2N1cnJlbnRRdWVzdGlvbl0udHlwZSA9PSAnc2xpZGUnKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcXVlc3Rpb25zW2N1cnJlbnRRdWVzdGlvbl0uY3VycmVudGFuc3dlciA9IGNoZWNrQXZhaWxhYmxlO1xyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHN1Ym1pdEFucygpO1xyXG4gICAgICAgICAgICAgICAgYnViYmxlRG93bigpO1xyXG4gICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaW5pdFBvc2l0aW9uQnViYmxlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgZ290b1F1ZXN0aW9uKGN1cnJlbnRRdWVzdGlvbiArIDEpO1xyXG4gICAgICAgICAgICAgICAgfSwgMTIwMCk7XHJcbiAgICAgICAgICAgICAgICBjb3VudCsrO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBuZXh0X2JnID0gY3VycmVudC5kYXRhKFwiYmdcIik7XHJcbiAgICAgICAgICAgIGJnX2N1cnJlbnQuYWRkQ2xhc3MoJ3ByZXYtc3RhZ2UnKTtcclxuICAgICAgICAgICAgYmdfY3VycmVudC5yZW1vdmVDbGFzcygnY3VycmVudC1zdGFnZScpO1xyXG4gICAgICAgICAgICAkKFwiLmJnX2dyYWRpZW50LVwiICsgbmV4dF9xdWVzdGlvbikucmVtb3ZlQ2xhc3MoJ25leHQtc3RhZ2UnKTtcclxuICAgICAgICAgICAgJChcIi5iZ19ncmFkaWVudC1cIiArIG5leHRfcXVlc3Rpb24pLmFkZENsYXNzKCdjdXJyZW50LXN0YWdlJyk7XHJcbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgJChcIiNxdWVzdGlvbl9cIiArIG5leHRfcXVlc3Rpb24pLmFkZENsYXNzKFwiYWN0aXZlXCIpO1xyXG4gICAgICAgICAgICAgICAgJCgnI2J0ZHJhZ2RlYWxlcm4tbmV4dC1qcycpLmFkZENsYXNzKFwiaGlkZS1idG5cIik7XHJcbiAgICAgICAgICAgIH0sIDUwMCk7XHJcbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgYmdfY3VycmVudC5jbG9zZXN0KCcucXVlc3Rpb24nKS5yZW1vdmVDbGFzcyhcImFjdGl2ZVwiKTtcclxuICAgICAgICAgICAgfSwgNjAwKTtcclxuXHJcbiAgICAgICAgfVxyXG4gICAgICAgIGRyYWdfdmFsKCk7XHJcbiAgICAgICAgcmVzdWx0Q2FsY3VsYXRvcigpO1xyXG5cclxuICAgIH0pO1xyXG5cclxuICAgIC8qIG92ZXJsYXkgZWxtIHdoZW4gY2xpY2sgYnV0dG9uICjguYHguIHguYnguYDguKPguLfguYjguK3guIfguJvguLjguYjguKHguITguKXguLTguYrguIHguIvguYnguLPguIvguYnguK3guJnguKvguKXguLLguKLguJfguLXguYDguYDguKXguYnguKfguJrguLHguITguILguYnguLLguKHguILguYnguK0pKi9cclxuICAgIGZ1bmN0aW9uIG92ZXJsYXlFbG0odGltZSkge1xyXG4gICAgICAgIHZhciBlbG0gPSAnPGRpdiBjbGFzcz1cIm92ZXJsYXlFbG1cIj48L2Rpdj4nO1xyXG4gICAgICAgICQoZWxtKS5wcmVwZW5kVG8oJyNsYW5kaW5nUGFnZScpO1xyXG4gICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAkKCcub3ZlcmxheUVsbScpLnJlbW92ZSgpO1xyXG4gICAgICAgIH0sIHRpbWUpO1xyXG5cclxuICAgIH1cclxuICAgIC8qIOC4m+C4uOC5iOC4oeC4ouC5ieC4reC4meC4geC4peC4seC4miAqL1xyXG4gICAgJChcIiNidG4tcHJldi1qc1wiKS5jbGljayhmdW5jdGlvbiAoKSB7XHJcblxyXG5cclxuICAgICAgICAkKFwiLm1haW4tcGFnZVwiKS5hZGRDbGFzcyhcIm5vbmUtZXZlbnRcIik7XHJcbiAgICAgICAgdmFyIGN1cnJlbnQ7XHJcbiAgICAgICAgdmFyIGJnX2N1cnJlbnQ7XHJcbiAgICAgICAgdmFyIHByZXZfcXVlc3Rpb247XHJcbiAgICAgICAgY291bnQtLTtcclxuICAgICAgICBjaGVja0J1YmJsZUFjdGl2ZShjb3VudClcclxuXHJcbiAgICAgICAgJCgnI2J0bi1uZXh0LWpzJykuYWRkQ2xhc3MoJ2hpZGUnKTtcclxuXHJcblxyXG4gICAgICAgIGlmIChjdXJyZW50UXVlc3Rpb24gPiAwKSB7XHJcbiAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgJCgnI2J0bi1uZXh0LWpzJykucmVtb3ZlQ2xhc3MoJ2hpZGUnKTtcclxuICAgICAgICAgICAgfSwgMzAwMCk7XHJcbiAgICAgICAgfVxyXG5cclxuXHJcbiAgICAgICAgaWYgKChjdXJyZW50UXVlc3Rpb24gPT0gMiAmJiBxdWVzdGlvbnNbMF0uY3VycmVudGFuc3dlciA9PSA0KSB8fCBjdXJyZW50UXVlc3Rpb24gLSAxID09IDApIHtcclxuICAgICAgICAgICAgJCgnI2J0bi1wcmV2LWpzJykuYWRkQ2xhc3MoJ2hpZGUnKTtcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIGlmIChjdXJyZW50UXVlc3Rpb24gLSAxID09IDAgJiYgcXVlc3Rpb25zWzFdLmN1cnJlbnRhbnN3ZXIgIT0gXCJcIikge1xyXG5cclxuICAgICAgICAgICAgdmFyIGFhYWEgPSBwcm9kdWN0QXJyYXkuc3BsaWNlKHByb2R1Y3RBcnJheS5pbmRleE9mKHF1ZXN0aW9uc1sxXS5hbnN3ZXJzW3BhcnNlSW50KHF1ZXN0aW9uc1sxXS5jdXJyZW50YW5zd2VyKV0ucHJvZHVjdHJlbGF0ZVswXSksIDEpO1xyXG5cclxuXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBpZiAoJChcIi5xdWVzdGlvblwiKS5oYXNDbGFzcyhcImFjdGl2ZVwiKSkge1xyXG5cclxuXHJcblxyXG4gICAgICAgICAgICBjdXJyZW50ID0gJChcIi5xdWVzdGlvbi5hY3RpdmVcIik7XHJcbiAgICAgICAgICAgIGJnX2N1cnJlbnQgPSBjdXJyZW50LmZpbmQoJy5iZ19ncmFkaWVudCcpO1xyXG4gICAgICAgICAgICBwcmV2X3F1ZXN0aW9uID0gY3VycmVudC5kYXRhKFwicXVlc3Rpb25cIikgLSAyO1xyXG4gICAgICAgICAgICBiZ19jdXJyZW50LmFkZENsYXNzKCduZXh0LXN0YWdlJyk7XHJcbiAgICAgICAgICAgIGJnX2N1cnJlbnQucmVtb3ZlQ2xhc3MoJ2N1cnJlbnQtc3RhZ2UnKTtcclxuICAgICAgICAgICAgJChcIi5iZ19ncmFkaWVudC1cIiArIHByZXZfcXVlc3Rpb24pLnJlbW92ZUNsYXNzKCdwcmV2LXN0YWdlJyk7XHJcbiAgICAgICAgICAgICQoXCIuYmdfZ3JhZGllbnQtXCIgKyBwcmV2X3F1ZXN0aW9uKS5hZGRDbGFzcygnY3VycmVudC1zdGFnZScpO1xyXG5cclxuXHJcblxyXG5cclxuICAgICAgICAgICAgaWYgKGN1cnJlbnRRdWVzdGlvbiAtIDEgPT0gLTEpIHtcclxuICAgICAgICAgICAgICAgICQoJyNidG4tcHJldi1qcycpLmFkZENsYXNzKCdoaWRlJyk7XHJcbiAgICAgICAgICAgICAgICAkKFwiLmZvcm1fdmFsaWRhdGVcIikuYWRkQ2xhc3MoXCJhY3RpdmVcIik7XHJcbiAgICAgICAgICAgICAgICBpZiAoJChcIi5tYWluLXBhZ2VcIikuaGFzQ2xhc3MoXCJub25lLWV2ZW50XCIpKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKFwiLm1haW4tcGFnZVwiKS5yZW1vdmVDbGFzcyhcIm5vbmUtZXZlbnRcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgNjAwKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIG92ZXJsYXlFbG0oMTAwMCk7XHJcbiAgICAgICAgICAgICAgICBidWJibGVEb3duKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmVtb3ZlQWxsQnViYmxlKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICQoXCJpbnB1dCNzdWJtaXRcIikuc2hvdygpO1xyXG4gICAgICAgICAgICAgICAgICAgICQoXCIjYnRuLW5leHQtanNcIikuYWRkQ2xhc3MoXCJoaWRlXCIpO1xyXG4gICAgICAgICAgICAgICAgfSwgNjAwKTtcclxuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICQoXCIuZm9ybV92YWxpZGF0ZVwiKS5hZGRDbGFzcyhcImFjdGl2ZVwiKTtcclxuICAgICAgICAgICAgICAgIH0sIDMwMCk7XHJcblxyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHByZXZfcXVlc3Rpb24gPT0gMikge1xyXG4gICAgICAgICAgICAgICAgb3ZlcmxheUVsbSgzMDAwKTtcclxuICAgICAgICAgICAgICAgIGlmIChxdWVzdGlvbnNbMF0uY3VycmVudGFuc3dlciA9PSA0KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gJCgnI2J0bi1uZXh0LWpzJykucmVtb3ZlQ2xhc3MoJ2hpZGUnKTtcclxuICAgICAgICAgICAgICAgICAgICBiYWNrQW5zKDIpO1xyXG4gICAgICAgICAgICAgICAgICAgIGJ1YmJsZURvd24oKTtcclxuICAgICAgICAgICAgICAgICAgICAkKCcuYnViYmxlY2hvaWNlJykucmVtb3ZlQ2xhc3MoJ2FjdGl2ZScpO1xyXG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpbml0UG9zaXRpb25CdWJibGUoKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZ290b1F1ZXN0aW9uKGN1cnJlbnRRdWVzdGlvbiAtIDIpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNxdWVzdGlvbl8yXCIpLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKFwiI3F1ZXN0aW9uXzIgLmJnX2dyYWRpZW50LTJcIikucmVtb3ZlQ2xhc3MoXCJjdXJyZW50LXN0YWdlXCIsIFwicHJldi1zdGFnZVwiKS5hZGRDbGFzcyhcIm5leHQtc3RhZ2VcIik7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjcXVlc3Rpb25fMVwiKS5hZGRDbGFzcyhcImFjdGl2ZVwiKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNxdWVzdGlvbl8xIC5iZ19ncmFkaWVudC0xXCIpLnJlbW92ZUNsYXNzKFwibmV4dC1zdGFnZVwiLCBcInByZXYtc3RhZ2VcIikuYWRkQ2xhc3MoXCJjdXJyZW50LXN0YWdlXCIpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICB9LCAxMjAwKTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gJCgnI2J0bi1uZXh0LWpzJykucmVtb3ZlQ2xhc3MoJ2hpZGUnKTtcclxuICAgICAgICAgICAgICAgICAgICBiYWNrQW5zKDEpO1xyXG4gICAgICAgICAgICAgICAgICAgIGJ1YmJsZURvd24oKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGluaXRQb3NpdGlvbkJ1YmJsZSgpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBnb3RvUXVlc3Rpb24oY3VycmVudFF1ZXN0aW9uIC0gMSk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIH0sIDEzMDApO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKHByZXZfcXVlc3Rpb24gPT0gcXVlc3Rpb25zLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgb3ZlcmxheUVsbSgzMDAwKTtcclxuICAgICAgICAgICAgICAgICQoJyNidG4tcHJldi1qcycpLnJlbW92ZUNsYXNzKCdoaWRlJyk7XHJcbiAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBvdmVybGF5RWxtKDMwMDApO1xyXG4gICAgICAgICAgICAgICAgJCgnI2J0bi1uZXh0LWpzJykuYWRkQ2xhc3MoJ2hpZGUnKTtcclxuICAgICAgICAgICAgICAgIGJhY2tBbnMoMSk7XHJcbiAgICAgICAgICAgICAgICBidWJibGVEb3duKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgaW5pdFBvc2l0aW9uQnViYmxlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgZ290b1F1ZXN0aW9uKGN1cnJlbnRRdWVzdGlvbiAtIDEpO1xyXG5cclxuICAgICAgICAgICAgICAgIH0sIDE1MDApO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgJChcIiNxdWVzdGlvbl9cIiArIHByZXZfcXVlc3Rpb24pLmFkZENsYXNzKFwiYWN0aXZlXCIpO1xyXG4gICAgICAgIH0sIDUwMCk7XHJcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIGJnX2N1cnJlbnQuY2xvc2VzdCgnLnF1ZXN0aW9uJykucmVtb3ZlQ2xhc3MoXCJhY3RpdmVcIik7XHJcbiAgICAgICAgICAgIC8vICQoXCIubWFpbi1wYWdlXCIpLnJlbW92ZUNsYXNzKFwibm9uZS1ldmVudFwiKTtcclxuICAgICAgICB9LCA2MDApO1xyXG4gICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAvLyAkKFwiLm1haW4tcGFnZVwiKS5yZW1vdmVDbGFzcyhcIm5vbmUtZXZlbnRcIik7XHJcbiAgICAgICAgfSwgMTIwMCk7XHJcbiAgICB9KTtcclxuXHJcblxyXG4gICAgJChcIi5hY2NlcHQtanNcIikub24oXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgJChcIi5hY2NlcHRcIikudG9nZ2xlQ2xhc3MoXCJhY3RpdmVcIik7XHJcbiAgICAgICAgdmFyIGNoZWNrQm94ZXMgPSAkKFwiaW5wdXRbbmFtZT0nYWNjcGV0J11cIik7XHJcbiAgICAgICAgaWYgKGNoZWNrQm94ZXMucHJvcChcImNoZWNrZWRcIikgPT0gdHJ1ZSlcclxuICAgICAgICAgICAgY2hlY2tCb3hlcy5wcm9wKFwiY2hlY2tlZFwiLCBmYWxzZSk7XHJcbiAgICAgICAgZWxzZSB7XHJcbiAgICAgICAgICAgIGNoZWNrQm94ZXMucHJvcChcImNoZWNrZWRcIiwgdHJ1ZSlcclxuICAgICAgICB9XHJcbiAgICAgICAgJChcImlucHV0I3N1Ym1pdFwiKS50b2dnbGVDbGFzcyhcImRpc2FibGVcIik7XHJcblxyXG4gICAgfSk7XHJcblxyXG4gICAgLyogYnV0dG9uIHN1Ym1pdCBmb3JtIChwYWdlIDIgb24gbGFuZGluZykgKi9cclxuICAgICQoXCJpbnB1dCNzdWJtaXRcIikub24oXCJjbGlja1wiLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgJChcImlucHV0I3N1Ym1pdFwiKS5oaWRlKCk7XHJcbiAgICAgICAgaWYgKHF1ZXN0aW9uc1tjdXJyZW50UXVlc3Rpb25dLmN1cnJlbnRhbnN3ZXIgIT0gXCJcIikge1xyXG4gICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICQoXCIjYnRuLW5leHQtanNcIikucmVtb3ZlQ2xhc3MoXCJoaWRlXCIpO1xyXG4gICAgICAgICAgICB9LCA1MDApO1xyXG5cclxuICAgICAgICB9XHJcbiAgICB9KTtcclxuXHJcbiAgICAkKFwiLnRvX2hvbWVwYWdlLCNidG4tbG9nby1qc1wiKS5vbihcImNsaWNrXCIsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAkKFwiLmhlYWRlcl9jb250YWluZXJfbGVmdCBhXCIpLnJlbW92ZUNsYXNzKFwiYWN0aXZlXCIpO1xyXG4gICAgICAgICQoXCIuaGVhZGVyX2NvbnRhaW5lcl9sZWZ0IGxpOmZpcnN0LWNoaWxkIGFcIikuYWRkQ2xhc3MoXCJhY3RpdmVcIik7XHJcbiAgICAgICAgJCgnLm1hc2tfZGl2X291dGVyJykucmVtb3ZlQXR0cihcInN0eWxlXCIpO1xyXG4gICAgICAgICQoJyNmb290ZXInKS5mYWRlT3V0KCk7XHJcbiAgICAgICAgJChcImlucHV0Lm1lbnUtYnRuXCIpLnByb3AoJ2NoZWNrZWQnLCBmYWxzZSk7XHJcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICQoJy5tYXNrX2Rpdl9vdXRlcicpLnJlbW92ZUNsYXNzKFwid2F2ZV9oaWRlXCIpO1xyXG4gICAgICAgIH0sIDIwMCk7XHJcblxyXG4gICAgfSk7XHJcblxyXG5cclxuICAgIC8qIOC4q+C4meC5ieC4siBzaGFrZSovXHJcbiAgICAvKiDguYHguKrguJTguIfguJvguLjguYjguKHguIHguJQqL1xyXG4gICAgJCgnI2J1c2luZXNzbmFtZScpLm9uKCdpbnB1dCcsIGZ1bmN0aW9uICgpIHtcclxuICAgICAgICBpZiAoJC50cmltKHRoaXMudmFsdWUpLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgJCgnI2J0bi1zaGFrZS1zdWJtaXQtanMnKS5yZW1vdmVDbGFzcyhcImhpZGRlblwiKTtcclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAkKCcjYnRuLXNoYWtlLXN1Ym1pdC1qcycpLmFkZENsYXNzKFwiaGlkZGVuXCIpO1xyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIC8qIOC4geC4lOC5geC4peC5ieC4p+C4i+C5iOC4reC4meC5gOC4quC4leC4iDAg4LmE4Lib4Liq4LmA4LiV4LiIMSovXHJcbiAgICAkKFwiI2J0bi1zaGFrZS1zdWJtaXQtanNcIikuY2xpY2soZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICQodGhpcykuYWRkQ2xhc3MoXCJoaWRkZW5cIik7XHJcbiAgICAgICAgJChcInAuYnVzaW5lc3NfbmFtZVwiKS5odG1sKFwiJ1wiICsgJChcImlucHV0I2J1c2luZXNzbmFtZVwiKS52YWwoKSArIFwiJ1wiKVxyXG4gICAgICAgIGJ1c2luZXNzTmFtZSA9ICQoXCJpbnB1dCNidXNpbmVzc25hbWVcIikudmFsKCkudG9TdHJpbmcoKS5yZXBsYWNlKFwiIFwiLCBcIi1cIik7XHJcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICQoXCJzcGFuLnN0YWdlLTBcIikuYWRkQ2xhc3MoXCJoaWRlLWVsbVwiKTtcclxuICAgICAgICAgICAgJChcImRpdi5idXNpbmVzc19uYW1lXCIpLmFkZENsYXNzKFwiaGlkZS1lbG1cIik7XHJcbiAgICAgICAgICAgICQoXCJwLmJ1c2luZXNzX25hbWVcIikucmVtb3ZlQ2xhc3MoXCJoaWRlLWVsbVwiKTtcclxuICAgICAgICAgICAgJChcIiNidG4tc2hvdy1yZXN1bHQtanNcIikucmVtb3ZlQ2xhc3MoXCJoaWRkZW5cIik7XHJcbiAgICAgICAgICAgICQoXCJkaXYjYnRuXCIpLnJlbW92ZUNsYXNzKFwiaGlkZVwiKTtcclxuXHJcbiAgICAgICAgfSwgNTAwKTtcclxuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgJChcInNwYW4uc3RhZ2UtMVwiKS5yZW1vdmVDbGFzcyhcImhpZGUtZWxtXCIpO1xyXG4gICAgICAgICAgICAkKFwiLmltZyBpbWdcIikucmVtb3ZlQ2xhc3MoXCJzdGFnZS0xXCIpLmFkZENsYXNzKFwic3RhZ2UtMlwiKTtcclxuICAgICAgICB9LCAzNTApO1xyXG4gICAgfSk7XHJcblxyXG4gICAgLyog4LiB4LiU4LmA4Lie4Li34LmI4LitIHNoYWtlKi9cclxuICAgICQoXCIjYnRuLXNob3ctcmVzdWx0LWpzXCIpLmNsaWNrKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAkKFwiI21iLXNoYWtlXCIpLmFkZENsYXNzKFwic2hha2VcIik7XHJcbiAgICAgICAgJChcIiNidG5cIikuY3NzKCd0cmFuc2Zvcm0nLCAndHJhbnNsYXRlKC01MCUsLTUwJSkgc2NhbGUoMCknKTtcclxuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uID0gXCJyZXN1bHQtcGFnZS5odG1sP2xldmVsPVwiICsgY2hlY2tMZXZlbCArIFwiJm5hbWU9XCIgKyBidXNpbmVzc05hbWUgKyBcIiZzY29yZT1cIiArIG92ZXJhbGxTY29yZSArIFwiJnByb2R1Y3Q9XCIgKyBwcm9kdWN0QXJyYXk7XHJcbiAgICAgICAgfSwgODAwKTtcclxuICAgIH0pO1xyXG5cclxuICAgIC8qIHdoZW4gY2xpY2sgYnViYmxlIEluc3VyYW5jZSAqL1xyXG4gICAgJChkb2N1bWVudCkub24oXCJjbGlja1wiLCBcIi5idWJibGV3cmFwXCIsIGZ1bmN0aW9uIChldmVudCkge1xyXG4gICAgICAgIGlmIChjb3VudCA9PSA5KSB7XHJcbiAgICAgICAgICAgIGJ1YmJsZUluc3VyYW5jZSgpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9KTtcclxuXHJcblxyXG5cclxuXHJcbiAgICAvLy8vLy8vLy8vLy8vLyBmdW5jdGlvbiAvLy8vLy8vLy8vLy8vLy8vXHJcbiAgICBmdW5jdGlvbiBkcmFnX3ZhbCgpIHtcclxuICAgICAgICB2YXIgcmFuZ2UgPSAkKCcuaW5wdXQtcmFuZ2UnKSxcclxuICAgICAgICAgICAgdmFsdWUgPSAkKCcucmFuZ2UtdmFsdWUnKTtcclxuXHJcbiAgICAgICAgdmFsdWUuaHRtbChyYW5nZS5hdHRyKCd2YWx1ZScpKTtcclxuXHJcbiAgICAgICAgcmFuZ2Uub24oJ2lucHV0JywgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICB2YWx1ZS5odG1sKHRoaXMudmFsdWUpO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgLy8vLy8vLy8vLy8vLyBxdWVzdGlvbiBmdW5jdGlvbiAvLy8vLy8vLy8vXHJcblxyXG4gICAgdmFyIHBvc0RyYWc7XHJcblxyXG4gICAgZnVuY3Rpb24gZ290b1F1ZXN0aW9uKG51bSkge1xyXG4gICAgICAgIGlmIChudW0gPT0gMSAmJiBxdWVzdGlvbnNbMF0uY3VycmVudGFuc3dlciA9PSAzKSB7XHJcbiAgICAgICAgICAgIHF1ZXN0aW9uc1syXS5hbnN3ZXJzWzBdLnByb2R1Y3RyZWxhdGUgPSBbJ2Q0J11cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBxdWVzdGlvbnNbMl0uYW5zd2Vyc1swXS5wcm9kdWN0cmVsYXRlID0gW11cclxuICAgICAgICB9XHJcbiAgICAgICAgaWYgKG51bSA9PSA4KSB7XHJcbiAgICAgICAgICAgIHByb2R1Y3RBcnJheS5wdXNoKFwiazFcIik7XHJcbiAgICAgICAgfVxyXG4gICAgICAgIGN1cnJlbnRRdWVzdGlvbiA9IG51bTtcclxuICAgICAgICB2YXIgcXVlc3Rpb24gPSBxdWVzdGlvbnNbbnVtXTtcclxuICAgICAgICBjb3VudFBhZ2UoY3VycmVudFF1ZXN0aW9uKTtcclxuICAgICAgICBpZiAoY3VycmVudFF1ZXN0aW9uICE9IDApIHtcclxuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAkKCcjYnRuLXByZXYtanMnKS5yZW1vdmVDbGFzcygnaGlkZScpO1xyXG4gICAgICAgICAgICB9LCA1MDApO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKHF1ZXN0aW9uLnR5cGUgPT0gXCJjaG9pY2VcIikge1xyXG4gICAgICAgICAgICAkKCcjc2xpZGVyQ2FudmFzJykuZmFkZU91dCgpO1xyXG4gICAgICAgICAgICAkKCcjYnViYmxlQ2FudmFzJykuZmFkZUluKCk7XHJcbiAgICAgICAgICAgIHZhciBodG1sYnViYmxlID0gXCJcIjtcclxuXHJcbiAgICAgICAgICAgIC8qZ2VuZXJhdGUgYnViYmxlIGFycmF5Ki9cclxuICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBxdWVzdGlvbi5hbnN3ZXJzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgYnViYmxlU3R5bGUgPSBxdWVzdGlvbi5hbnN3ZXJzW2ldLmJ1YmJsZXN0eWxlc2NhbGUuc3BsaXQoJywnKVswXTtcclxuICAgICAgICAgICAgICAgIGh0bWxidWJibGUgKz0gJzxkaXYgY2xhc3M9XCJidWJibGV3cmFwXCI+JztcclxuICAgICAgICAgICAgICAgIGh0bWxidWJibGUgKz0gJzxkaXYgY2xhc3M9XCJidWJibGVjaG9pY2UgJyArIGJ1YmJsZVN0eWxlICsgJ1wiIHN0eWxlPVwiYmFja2dyb3VuZDonICsgcXVlc3Rpb24uY29sb3JlbG0gKyAnXCI+PHNwYW4gY2xhc3M9XCJidWJibGVfdGV4dCB0ZXh0XCI+JyArIHF1ZXN0aW9uLmFuc3dlcnNbaV0udGl0bGUgKyAnPC9zcGFuPjwvZGl2Pic7XHJcbiAgICAgICAgICAgICAgICBodG1sYnViYmxlICs9ICc8L2Rpdj4nO1xyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB2YXIgaHRtbHRpdGxlYmIgPSBcIlwiO1xyXG4gICAgICAgICAgICBodG1sdGl0bGViYiArPSAnPGRpdiBjbGFzcz1cImhlYWRidWJibGUgdGV4dC1rdHJlZyBleHRyYWxhcmdlIHdoaXRlXCI+JztcclxuICAgICAgICAgICAgaHRtbHRpdGxlYmIgKz0gcXVlc3Rpb24udGl0bGU7XHJcbiAgICAgICAgICAgIGh0bWx0aXRsZWJiICs9ICc8L2Rpdj4nO1xyXG5cclxuXHJcbiAgICAgICAgICAgICQoXCIjYnViYmxlQ2FudmFzXCIpLmh0bWwoaHRtbGJ1YmJsZSk7XHJcbiAgICAgICAgICAgICQoXCIjYnViYmxlQ2FudmFzXCIpLnByZXBlbmQoaHRtbHRpdGxlYmIpO1xyXG5cclxuICAgICAgICAgICAgJChcIi5idWJibGVjaG9pY2VcIikub24oXCJjbGlja1wiLCBmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgICAgICAgICAgJCgnI2J1YmJsZUNhbnZhcycpLnNob3coKTtcclxuICAgICAgICAgICAgICAgIGNoZWNrQnViYmxlQWN0aXZlKGN1cnJlbnRRdWVzdGlvbilcclxuICAgICAgICAgICAgICAgIC8v4LiU4Li24LiH4LiC4LmJ4Lit4Lib4Lix4LiI4LiI4Li44Lia4Lix4LiZ4Lit4Lit4LiB4Lih4LiyXHJcbiAgICAgICAgICAgICAgICB2YXIgcXVlc3Rpb24gPSBxdWVzdGlvbnNbY3VycmVudFF1ZXN0aW9uXTtcclxuXHJcbiAgICAgICAgICAgICAgICB2YXIgY3VycmVudGFuc3dlckFycmF5ID0gcXVlc3Rpb24uY3VycmVudGFuc3dlci5zcGxpdCgnLCcpOyAvL2FycmF5IOC5gOC4geC5h+C4muC4leC4seC4p+C4l+C4teC5iOC5gOC4hOC4ouC5gOC4peC4t+C4reC4geC5hOC4p+C5ieC5g+C4mSBjdXJyZW50YW5zd2VyXHJcblxyXG4gICAgICAgICAgICAgICAgdmFyIHNlbGVjdEluZGV4ID0gXCJcIiArICQoJy5idWJibGVjaG9pY2UnKS5pbmRleCh0aGlzKTtcclxuICAgICAgICAgICAgICAgIHZhciBpc1NlbGVjdGVkID0gZmFsc2U7XHJcblxyXG4gICAgICAgICAgICAgICAgaXNTZWxlY3RlZCA9IGN1cnJlbnRhbnN3ZXJBcnJheS5pbmNsdWRlcyhcIlwiICsgc2VsZWN0SW5kZXgpOyAvLyBpcyBzZWxlY3Qgd2lsbCByZW1vdmUgICAgICAgICAgIFxyXG5cclxuICAgICAgICAgICAgICAgIGlmIChpc1NlbGVjdGVkKSB7IC8v4LiB4LiUIGJ1YmJsZSDguJfguLXguYjguIHguJTguYTguJvguYHguKXguYnguKcgPSBVbmNoZWNrZWQg4LiV4LmJ4Lit4LiH4Lil4LiaIHByb2R1Y3QgaWQg4LiX4Li14LmI4LmA4LiB4LmH4Lia4LmE4Lin4LmJ4LmD4LiZIHByb2R1Y3QgYXJyYXkg4Lit4Lit4LiBXHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8vIFxyXG4gICAgICAgICAgICAgICAgICAgIHZhciBjdXJyZW50Q2hvaWNlID0gcXVlc3Rpb24uYW5zd2Vyc1tzZWxlY3RJbmRleF1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy/guKfguJnguYDguK3guLIgaWQg4LiC4Lit4LiHIHJlbGF0ZSBwcm9kdWN0IOC5g+C4mSBjaG9pY2Ug4LiX4Li14LmI4LmA4Lil4Li34Lit4LiBXHJcbiAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBjdXJyZW50Q2hvaWNlLnByb2R1Y3RyZWxhdGUubGVuZ3RoOyBpKyspIHtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIC8v4LmA4Lit4LiyIGlkIOC4oeC4suC4q+C4siBpbmRleCDguYPguJkgcHJvZHVjdCBhcnJheSDguYHguKXguYnguKfguKXguJrguJfguLTguYnguIdcclxuICAgICAgICAgICAgICAgICAgICAgICAgcHJvZHVjdEFycmF5LnNwbGljZShwcm9kdWN0QXJyYXkuaW5kZXhPZihjdXJyZW50Q2hvaWNlLnByb2R1Y3RyZWxhdGVbaV0pLCAxKVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLy9cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgLyppZiBzZWxlY3RlZChub3QgZmlyc3QgdGltZSkqL1xyXG4gICAgICAgICAgICAgICAgICAgIGN1cnJlbnRhbnN3ZXJBcnJheS5zcGxpY2UoY3VycmVudGFuc3dlckFycmF5LmluZGV4T2YoXCJcIiArIHNlbGVjdEluZGV4KSwgMSk7XHJcbiAgICAgICAgICAgICAgICAgICAgcXVlc3Rpb24uY3VycmVudGFuc3dlciA9IGN1cnJlbnRhbnN3ZXJBcnJheS5qb2luKFwiLFwiKTtcclxuXHJcblxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBhZGQgdG8gcHJvZHVjdEFycmF5XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gcHJvZHVjdEFycmF5ID0gcHJvZHVjdEFycmF5LmNvbmNhdChxdWVzdGlvbi5hbnN3ZXJzW3NlbGVjdEluZGV4XS5wcm9kdWN0cmVsYXRlKVxyXG5cclxuICAgICAgICAgICAgICAgICAgICBpZiAocXVlc3Rpb24uY3VycmVudGFuc3dlciAhPSBcIlwiKSB7IC8v4LiW4LmJ4Liy4LiC4LmJ4Lit4LiZ4Lix4LmJ4LiZ4LmA4Lib4LmH4LiZIG11bHRpcGxlIGNob2ljZSDguYHguKXguYnguKfguIHguJQgYnViYmxlIOC4reC4seC4meC4l+C4teC5iOC4quC4reC4h+C4iOC4sOC5gOC4guC5ieC4suC4reC4seC4meC4meC4teC5iSB8fCDguKvguKPguLfguK3guJbguYnguLLguYDguJvguYfguJkgc2luZ2xlIGNob2ljZSDguIjguLDguYDguILguYnguLLguK3guLHguJnguJnguLXguYkg4LiW4LmJ4Liy4LmE4Lih4LmI4LmE4LiU4LmJIHVuY2hlY2sg4LiV4Lix4Lin4LmA4Lil4Li34Lit4LiB4LiB4LmI4Lit4LiZ4Lir4LiZ4LmJ4LiyXHJcblxyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKHF1ZXN0aW9uLm1heGNob2ljZSA9PSAxKSB7IC8v4LiB4Lij4LiT4Li1IFNpbmdsZSBjaG9pY2VcclxuXHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHZhciBwcmV2Q2hvaWNlID0gcXVlc3Rpb24uYW5zd2Vyc1twYXJzZUludChxdWVzdGlvbi5jdXJyZW50YW5zd2VyKV1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvL+C4p+C4meC5gOC4reC4siBpZCDguILguK3guIcgcmVsYXRlIHByb2R1Y3Qg4LmD4LiZIGNob2ljZSDguJfguLXguYjguYDguKXguLfguK3guIHguIHguYjguK3guJnguKvguJnguYnguLLguJnguLXguYlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcHJldkNob2ljZS5wcm9kdWN0cmVsYXRlLmxlbmd0aDsgaSsrKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8v4LmA4Lit4LiyIGlkIOC4oeC4suC4q+C4siBpbmRleCDguYPguJkgcHJvZHVjdCBhcnJheSDguYHguKXguYnguKfguKXguJrguJfguLTguYnguIdcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9kdWN0QXJyYXkuc3BsaWNlKHByb2R1Y3RBcnJheS5pbmRleE9mKHByZXZDaG9pY2UucHJvZHVjdHJlbGF0ZVtpXSksIDEpXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9kdWN0QXJyYXkgPSBwcm9kdWN0QXJyYXkuY29uY2F0KHF1ZXN0aW9uLmFuc3dlcnNbc2VsZWN0SW5kZXhdLnByb2R1Y3RyZWxhdGUpIC8v4LmA4Lit4LiyIHJlbGF0ZWQgcHJvZHVjdCDguILguK3guIcgYnViYmxlIOC4l+C4teC5iOC5gOC4peC4t+C4reC4geC5hOC4m+C5gOC4geC5h+C4muC5g+C4mSBwcm9kdWN0IGFycmF5XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgLy9cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAvKmZvciBtYXhjaG9pY2UgPSAxKi9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIuYnViYmxlY2hvaWNlXCIpLnJlbW92ZUNsYXNzKCdhY3RpdmUnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIHF1ZXN0aW9uLmN1cnJlbnRhbnN3ZXIgPSBcIlwiICsgc2VsZWN0SW5kZXg7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIC8qZm9yIG11bHRpcGxlY2hvaWNlKi9cclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGlmIChjdXJyZW50YW5zd2VyQXJyYXkubGVuZ3RoIDwgcXVlc3Rpb24ubWF4Y2hvaWNlKSB7IC8v4LmA4LiK4LmH4LiE4Lin4LmI4Liy4LmA4Lil4Li34Lit4LiB4Lih4Liy4LiB4LiB4Lin4LmI4LiyIG1heGltdW0g4Lir4Lij4Li34Lit4Lii4Lix4LiHXHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgY3VycmVudGFuc3dlckFycmF5LnB1c2goXCJcIiArIHNlbGVjdEluZGV4KTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBxdWVzdGlvbi5jdXJyZW50YW5zd2VyID0gY3VycmVudGFuc3dlckFycmF5LmpvaW4oXCIsXCIpOyAvL+C5gOC4reC4siBpbmRleCDguILguK3guIcgYnViYmxlIOC4l+C4teC5iOC4geC4lOC5gOC4geC5h+C4muC5gOC4guC5ieC4suC5hOC4m+C5g+C4mSBjdXJyZW50IGFuc3dlciDguILguK3guIfguILguYnguK3guJnguLHguYnguJlcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBwcm9kdWN0QXJyYXkgPSBwcm9kdWN0QXJyYXkuY29uY2F0KHF1ZXN0aW9uLmFuc3dlcnNbc2VsZWN0SW5kZXhdLnByb2R1Y3RyZWxhdGUpIC8v4LmA4Lit4LiyIHJlbGF0ZWQgcHJvZHVjdCDguILguK3guIcgYnViYmxlIOC4l+C4teC5iOC5gOC4peC4t+C4reC4geC5hOC4m+C5gOC4geC5h+C4muC5g+C4mSBwcm9kdWN0IGFycmF5XHJcblxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBpc1NlbGVjdGVkID0gdHJ1ZTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2UgeyAvL+C4luC5ieC4suC4guC5ieC4reC4meC4seC5ieC4mSDguYYgVW5jaGVjayBidWJibGUg4LiX4Lix4LmJ4LiH4Lir4Lih4LiUIOC5geC4peC5ieC4p+C4geC4lOC5gOC4peC4t+C4reC4geC5gOC4m+C5h+C4meC4reC4seC4meC5geC4o+C4geC4iOC4sOC5gOC4guC5ieC4suC4reC4seC4meC4meC4teC5iVxyXG4gICAgICAgICAgICAgICAgICAgICAgICBxdWVzdGlvbi5jdXJyZW50YW5zd2VyID0gXCJcIiArIHNlbGVjdEluZGV4OyAvL+C5gOC4reC4siBpbmRleCDguILguK3guIcgYnViYmxlIOC4l+C4teC5iOC4geC4lOC5gOC4geC5h+C4muC5gOC4guC5ieC4suC5hOC4m+C5g+C4mSBjdXJyZW50IGFuc3dlciDguILguK3guIfguILguYnguK3guJnguLHguYnguJlcclxuICAgICAgICAgICAgICAgICAgICAgICAgcHJvZHVjdEFycmF5ID0gcHJvZHVjdEFycmF5LmNvbmNhdChxdWVzdGlvbi5hbnN3ZXJzW3NlbGVjdEluZGV4XS5wcm9kdWN0cmVsYXRlKSAvL+C5gOC4reC4siBwcm9kdWN0IGlkIOC4iOC4suC4gSByZWxhdGVkIHByb2R1Y3Qg4LmA4LiB4LmH4Lia4LmD4Liq4LmIIHByb2R1Y3QgYXJyYXkgKHByb2R1Y3QgYXJyYXkg4LmD4LiK4LmJ4Liq4LmI4LiH4LmE4Lib4Lir4LiZ4LmJ4LiyIHJlc3VsdClcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgICAgIHZhciBlbGUgPSAkKHRoaXMpXHJcbiAgICAgICAgICAgICAgICBpZiAoIWlzU2VsZWN0ZWQpIHsgLy9mYWxzZVxyXG4gICAgICAgICAgICAgICAgICAgICQodGhpcykuYWRkQ2xhc3MoJ2JicG9wJyk7XHJcbiAgICAgICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGVsZS5yZW1vdmVDbGFzcygnYmJwb3AnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgZWxlLmFkZENsYXNzKCdhY3RpdmUnKTtcclxuICAgICAgICAgICAgICAgICAgICB9LCAxMDApO1xyXG4gICAgICAgICAgICAgICAgICAgIHBvcChlLnBhZ2VYLCBlLnBhZ2VZLCAxMik7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIGVsZS5yZW1vdmVDbGFzcygnYWN0aXZlJyk7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgc2V0Q3VycmVudEFjdGl2ZSgpO1xyXG4gICAgICAgICAgICBwb3NpdGlvbkJ1YmJsZSgpO1xyXG4gICAgICAgICAgICBpZiAobnVtID09IDEpIHtcclxuICAgICAgICAgICAgICAgIC8qIOC4guC5ieC4rSAyICovXHJcbiAgICAgICAgICAgICAgICBpZiAocXVlc3Rpb25zWzBdLmN1cnJlbnRhbnN3ZXIgPT0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8qIOC4hOC5ieC4suC4m+C4peC4teC4gSAqL1xyXG4gICAgICAgICAgICAgICAgICAgICQoJy5idWJibGV3cmFwOmVxKDcpJykuaGlkZSgpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvL+C5gOC4hOC4o+C4t+C5iOC4reC4h+C5g+C4iuC5ieC5hOC4n+C4n+C5ieC4si/guK3guLTguYDguKXguYfguIHguJfguKPguK3guJnguLTguIHguKrguYxcclxuICAgICAgICAgICAgICAgICAgICBxdWVzdGlvbnNbMV0uYW5zd2Vyc1szXS5tb2JpbGV0b3BsZWZ0ID0gXCI0NSUsMzAlXCI7XHJcbiAgICAgICAgICAgICAgICAgICAgLy/guK3guLLguKvguLLguKMv4LmA4LiE4Lij4Li34LmI4Lit4LiH4LiU4Li34LmI4LihXHJcbiAgICAgICAgICAgICAgICAgICAgcXVlc3Rpb25zWzFdLmFuc3dlcnNbNF0uZGVza3RvcHRvcGxlZnQgPSBcIjU2JSwzNiVcIjtcclxuICAgICAgICAgICAgICAgICAgICBxdWVzdGlvbnNbMV0uYW5zd2Vyc1s0XS5tb2JpbGV0b3BsZWZ0ID0gXCI1MCUsNjAlXCI7XHJcbiAgICAgICAgICAgICAgICAgICAgLy/guKfguLHguKrguJTguLjguIHguYjguK3guKrguKPguYnguLLguIdcclxuICAgICAgICAgICAgICAgICAgICBxdWVzdGlvbnNbMV0uYW5zd2Vyc1s2XS5kZXNrdG9wdG9wbGVmdCA9IFwiNjAlLDUwJVwiO1xyXG4gICAgICAgICAgICAgICAgICAgIHF1ZXN0aW9uc1sxXS5hbnN3ZXJzWzZdLm1vYmlsZXRvcGxlZnQgPSBcIjY2JSwzMiVcIjtcclxuICAgICAgICAgICAgICAgICAgICAvL+C4quC4tOC4meC4hOC5ieC4si/guJrguKPguLTguIHguLLguKPguK3guLfguYjguJnguYZcclxuICAgICAgICAgICAgICAgICAgICBxdWVzdGlvbnNbMV0uYW5zd2Vyc1s4XS5kZXNrdG9wdG9wbGVmdCA9IFwiNjAlLDY0JVwiO1xyXG4gICAgICAgICAgICAgICAgICAgIHF1ZXN0aW9uc1sxXS5hbnN3ZXJzWzhdLm1vYmlsZXRvcGxlZnQgPSBcIjY1JSw5NSVcIjtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAocXVlc3Rpb25zWzBdLmN1cnJlbnRhbnN3ZXIgPT0gMSkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8qIOC4hOC5ieC4suC4quC5iOC4hyAqL1xyXG4gICAgICAgICAgICAgICAgICAgICQoJy5idWJibGV3cmFwOmVxKDcpLC5idWJibGV3cmFwOmVxKDkpJykuaGlkZSgpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvL+C4quC4uOC4guC4oOC4suC4nuC5geC4peC4sOC4hOC4p+C4suC4oeC4h+C4suC4oVxyXG4gICAgICAgICAgICAgICAgICAgIHF1ZXN0aW9uc1sxXS5hbnN3ZXJzWzFdLmRlc2t0b3B0b3BsZWZ0ID0gXCIzMyUsNDMlXCI7XHJcbiAgICAgICAgICAgICAgICAgICAgLy/guYDguITguKPguLfguYjguK3guIfguYPguIrguYnguYTguJ/guJ/guYnguLIv4Lit4Li04LmA4Lil4LmH4LiB4LiX4Lij4Lit4LiZ4Li04LiB4Liq4LmMXHJcbiAgICAgICAgICAgICAgICAgICAgcXVlc3Rpb25zWzFdLmFuc3dlcnNbM10ubW9iaWxldG9wbGVmdCA9IFwiNDYlLDM3JVwiO1xyXG4gICAgICAgICAgICAgICAgICAgIC8v4Lit4Liy4Lir4Liy4LijL+C5gOC4hOC4o+C4t+C5iOC4reC4h+C4lOC4t+C5iOC4oVxyXG4gICAgICAgICAgICAgICAgICAgIHF1ZXN0aW9uc1sxXS5hbnN3ZXJzWzRdLmRlc2t0b3B0b3BsZWZ0ID0gXCI2MiUsNDElXCI7XHJcbiAgICAgICAgICAgICAgICAgICAgcXVlc3Rpb25zWzFdLmFuc3dlcnNbNF0ubW9iaWxldG9wbGVmdCA9IFwiNjElLDYwJVwiO1xyXG4gICAgICAgICAgICAgICAgICAgIC8v4Lin4Lix4Liq4LiU4Li44LiB4LmI4Lit4Liq4Lij4LmJ4Liy4LiHXHJcbiAgICAgICAgICAgICAgICAgICAgcXVlc3Rpb25zWzFdLmFuc3dlcnNbNl0uZGVza3RvcHRvcGxlZnQgPSBcIjYwJSw1OCVcIjtcclxuICAgICAgICAgICAgICAgICAgICBxdWVzdGlvbnNbMV0uYW5zd2Vyc1s2XS5tb2JpbGV0b3BsZWZ0ID0gXCI3NCUsMzYlXCI7XHJcbiAgICAgICAgICAgICAgICAgICAgLy/guKrguLTguJnguITguYnguLIv4Lia4Lij4Li04LiB4Liy4Lij4Lit4Li34LmI4LiZ4LmGXHJcbiAgICAgICAgICAgICAgICAgICAgcXVlc3Rpb25zWzFdLmFuc3dlcnNbOF0uZGVza3RvcHRvcGxlZnQgPSBcIjY1JSw3OCVcIjtcclxuICAgICAgICAgICAgICAgICAgICBxdWVzdGlvbnNbMV0uYW5zd2Vyc1s4XS5tb2JpbGV0b3BsZWZ0ID0gXCI2NSUsOTUlXCI7XHJcblxyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChxdWVzdGlvbnNbMF0uY3VycmVudGFuc3dlciA9PSAyKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLyog4Lia4Lij4Li04LiB4Liy4LijICovXHJcbiAgICAgICAgICAgICAgICAgICAgJCgnLmJ1YmJsZXdyYXA6ZXEoMCksLmJ1YmJsZXdyYXA6ZXEoMSksLmJ1YmJsZXdyYXA6ZXEoMiksLmJ1YmJsZXdyYXA6ZXEoMyksLmJ1YmJsZXdyYXA6ZXEoNSksLmJ1YmJsZXdyYXA6ZXEoNiksLmJ1YmJsZXdyYXA6ZXEoOCksLmJ1YmJsZXdyYXA6ZXEoOSknKS5oaWRlKCk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8v4Lit4Liy4Lir4Liy4LijL+C5gOC4hOC4o+C4t+C5iOC4reC4h+C4lOC4t+C5iOC4oVxyXG4gICAgICAgICAgICAgICAgICAgIHF1ZXN0aW9uc1sxXS5hbnN3ZXJzWzRdLmRlc2t0b3B0b3BsZWZ0ID0gXCI0MiUsNDIlXCI7XHJcbiAgICAgICAgICAgICAgICAgICAgcXVlc3Rpb25zWzFdLmFuc3dlcnNbNF0ubW9iaWxldG9wbGVmdCA9IFwiMzYlLDQ5JVwiO1xyXG4gICAgICAgICAgICAgICAgICAgIC8v4LmC4Lij4LiH4LmB4Lij4LihL+C4l+C4teC5iOC4nuC4seC4gVxyXG4gICAgICAgICAgICAgICAgICAgIHF1ZXN0aW9uc1sxXS5hbnN3ZXJzWzddLmRlc2t0b3B0b3BsZWZ0ID0gXCI1MSUsNjglXCI7XHJcbiAgICAgICAgICAgICAgICAgICAgcXVlc3Rpb25zWzFdLmFuc3dlcnNbN10ubW9iaWxldG9wbGVmdCA9IFwiNTYlLDc2JVwiO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmIChxdWVzdGlvbnNbMF0uY3VycmVudGFuc3dlciA9PSAzKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLyog4Lic4Lil4Li04LiV4Liq4Li04LiZ4LiE4LmJ4LiyICovXHJcbiAgICAgICAgICAgICAgICAgICAgJCgnLmJ1YmJsZXdyYXA6ZXEoNyksLmJ1YmJsZXdyYXA6ZXEoOSknKS5oaWRlKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgcXVlc3Rpb25zWzFdLmFuc3dlcnNbNF0uZGVza3RvcHRvcGxlZnQgPSBcIjQ4JSwzNSVcIjtcclxuXHJcblxyXG4gICAgICAgICAgICAgICAgICAgIC8v4LmA4LiE4Lij4Li34LmI4Lit4LiH4LmD4LiK4LmJ4LmE4Lif4Lif4LmJ4LiyL+C4reC4tOC5gOC4peC5h+C4geC4l+C4o+C4reC4meC4tOC4geC4quC5jFxyXG4gICAgICAgICAgICAgICAgICAgIHF1ZXN0aW9uc1sxXS5hbnN3ZXJzWzNdLm1vYmlsZXRvcGxlZnQgPSBcIjQ4JSw0OCVcIjtcclxuICAgICAgICAgICAgICAgICAgICAvL+C4quC4uOC4guC4oOC4suC4nuC5geC4peC4sOC4hOC4p+C4suC4oeC4h+C4suC4oVxyXG4gICAgICAgICAgICAgICAgICAgIHF1ZXN0aW9uc1sxXS5hbnN3ZXJzWzFdLmRlc2t0b3B0b3BsZWZ0ID0gXCI2MCUsNTAlXCI7XHJcbiAgICAgICAgICAgICAgICAgICAgLy/guKfguLHguKrguJTguLjguIHguYjguK3guKrguKPguYnguLLguIdcclxuICAgICAgICAgICAgICAgICAgICBxdWVzdGlvbnNbMV0uYW5zd2Vyc1s2XS5kZXNrdG9wdG9wbGVmdCA9IFwiNjIlLDY4JVwiO1xyXG4gICAgICAgICAgICAgICAgICAgIHF1ZXN0aW9uc1sxXS5hbnN3ZXJzWzZdLm1vYmlsZXRvcGxlZnQgPSBcIjY2JSwzMiVcIjtcclxuICAgICAgICAgICAgICAgICAgICAvL+C4quC4tOC4meC4hOC5ieC4si/guJrguKPguLTguIHguLLguKPguK3guLfguYjguJnguYZcclxuICAgICAgICAgICAgICAgICAgICBxdWVzdGlvbnNbMV0uYW5zd2Vyc1s4XS5kZXNrdG9wdG9wbGVmdCA9IFwiNjAlLDg3JVwiO1xyXG4gICAgICAgICAgICAgICAgICAgIHF1ZXN0aW9uc1sxXS5hbnN3ZXJzWzhdLm1vYmlsZXRvcGxlZnQgPSBcIjY1JSw5NSVcIjtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAocXVlc3Rpb25zWzBdLmN1cnJlbnRhbnN3ZXIgPT0gNCkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8qIOC4o+C5ieC4suC4meC4reC4suC4q+C4suC4o+C5geC4peC4sOC5gOC4hOC4o+C4t+C5iOC4reC4h+C4lOC4t+C5iOC4oSAqL1xyXG5cclxuICAgICAgICAgICAgICAgICAgICAvLyAkKCcuYnViYmxld3JhcDplcSg0KScpLmNsaWNrKCk7XHJcbiAgICAgICAgICAgICAgICAgICAgJCgnI2J0bi1uZXh0LWpzJykuY2xpY2soKTtcclxuICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAobnVtID09IDkpIHtcclxuICAgICAgICAgICAgICAgIGJ1YmJsZUluc3VyYW5jZSgpO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKG51bSA9PSAxMCkge1xyXG4gICAgICAgICAgICAgICAgLy8gdmFyIHRlbXAgPSBxdWVzdGlvbnNbM10uY3VycmVudGFuc3dlci5zcGxpdCgnLCcpO1xyXG4gICAgICAgICAgICAgICAgaWYgKHF1ZXN0aW9uc1szXS5jdXJyZW50YW5zd2VyID09ICcxJyB8fCBxdWVzdGlvbnNbM10uY3VycmVudGFuc3dlciA9PSAnMicpIHtcclxuICAgICAgICAgICAgICAgICAgICAkKCcuYnViYmxld3JhcDplcSgzKScpLmhpZGUoKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICB9IGVsc2UgaWYgKHF1ZXN0aW9uLnR5cGUgPT0gXCJzbGlkZVwiKSB7XHJcblxyXG4gICAgICAgICAgICAkKFwiLnF1ZXN0aW9uX2RyYWdnaW5nX2RlbW9cIikucHJlcGVuZChcIjxpbWcgY2xhc3M9J3RpcHNfc2hha2UnIHNyYz0nLi4vZGlzdC9yZXNvdXJjZXMvaW1hZ2VzL21haW4tcGFnZS9zbGlkZS1oYW5kLnBuZycvPlwiKTtcclxuICAgICAgICAgICAgJChcIi5xdWVzdGlvbl9kcmFnZ2luZ19kZW1vXCIpLmFkZENsYXNzKFwic2hvd190aXBzXCIpO1xyXG5cclxuICAgICAgICAgICAgJCgnLmhhbmRsZScpLmNsaWNrKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICQoXCIucXVlc3Rpb25fZHJhZ2dpbmdfZGVtbyBpbWcudGlwc19zaGFrZVwiKS5mYWRlT3V0KCk7XHJcbiAgICAgICAgICAgIH0pO1xyXG5cclxuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAkKFwiLnF1ZXN0aW9uX2RyYWdnaW5nX2RlbW8gaW1nLnRpcHNfc2hha2VcIikuZmFkZU91dChcInNsb3dcIiwgZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICQoXCIucXVlc3Rpb25fZHJhZ2dpbmdfZGVtbyBpbWcudGlwc19zaGFrZVwiKS5yZW1vdmUoKTtcclxuICAgICAgICAgICAgICAgICAgICAkKFwiLnF1ZXN0aW9uX2RyYWdnaW5nX2RlbW9cIikucmVtb3ZlQ2xhc3MoXCJzaG93X3RpcHNcIik7XHJcbiAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgfSwgMzAwMCk7XHJcblxyXG4gICAgICAgICAgICAkKCcubW9uaXRvcl90aXRsZScpLmh0bWwocXVlc3Rpb24udGl0bGUpO1xyXG4gICAgICAgICAgICByZW1vdmVBbGxCdWJibGUoKTtcclxuICAgICAgICAgICAgJCgnI3NsaWRlckNhbnZhcycpLnNob3coKTtcclxuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAkKCcjYnRuLW5leHQtanMsI2J0bi1wcmV2LWpzJykucmVtb3ZlQ2xhc3MoJ2hpZGUnKTtcclxuICAgICAgICAgICAgfSwgNTAwKTtcclxuICAgICAgICAgICAgdmFyIHNlbGVjdG9yO1xyXG5cclxuICAgICAgICAgICAgaWYgKHF1ZXN0aW9uLmN1cnJlbnRhbnN3ZXIgPT0gJycpIHtcclxuICAgICAgICAgICAgICAgIHNlbGVjdG9yID0gMVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG5cclxuICAgICAgICAgICAgICAgIHNlbGVjdG9yID0gcG9zRHJhZztcclxuICAgICAgICAgICAgICAgIGNvdW50ID0gZmFsc2U7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGlmIChudW0gPT0gMikge30gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHF1ZXN0aW9uLmFuc3dlcnMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgICAgICAkKCcub3ZlcmxheV9yYWRpdXMnKS5jc3Moe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBcImJhY2tncm91bmRcIjogXCJyZ2IoMjgsMTgzLDEzMilcIixcclxuICAgICAgICAgICAgICAgICAgICAgICAgXCJiYWNrZ3JvdW5kXCI6IHF1ZXN0aW9uLmNvbG9yZWxtLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgbmV3IERyYWdkZWFsZXIoJ2NvbnRlbnQtc2Nyb2xsZXInLCB7XHJcbiAgICAgICAgICAgICAgICBob3Jpem9udGFsOiBmYWxzZSxcclxuICAgICAgICAgICAgICAgIHZlcnRpY2FsOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgeTogc2VsZWN0b3IsXHJcbiAgICAgICAgICAgICAgICBhbmltYXRpb25DYWxsYmFjazogZnVuY3Rpb24gKHgsIHkpIHtcclxuICAgICAgICAgICAgICAgICAgICB2YXIgaGFuZGxlVG9wID0gJCgnLmhhbmRsZScpLm9mZnNldCgpLnRvcDtcclxuICAgICAgICAgICAgICAgICAgICB2YXIgdGl0bGVsZW5ndGhwZXJjZW50ID0gMTAwIC8gcXVlc3Rpb24uYW5zd2Vycy5sZW5ndGg7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmICgkKHdpbmRvdykud2lkdGgoKSA+IDc2Nykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKCcub3ZlcmxheV9yYWRpdXMnKS5jc3Moe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ3RvcCc6IGhhbmRsZVRvcCAtIDQ2XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJCgnLm92ZXJsYXlfcmFkaXVzJykuY3NzKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICd0b3AnOiBoYW5kbGVUb3AgLSAyNlxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBxdWVzdGlvbi5hbnN3ZXJzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmICgoKCh5ICogMTAwKSAtIDEwMCkgKiAoLTEpKSA8ICh0aXRsZWxlbmd0aHBlcmNlbnQgKiAoaSArIDEpKSkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJCgnLm92ZXJsYXlfaW5kaWNhdG9yJykuaHRtbChxdWVzdGlvbi5hbnN3ZXJzW2ldLnRpdGxlKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGNoZWNrQXZhaWxhYmxlID0gaTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGJyZWFrXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIGlmICh5IDwgMC4xKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICQoJy5vdmVybGF5X3JhZGl1cycpLmNzcyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYm9yZGVyLXJhZGl1cyc6ICc1MCUgNTAlIDBweCAwcHgnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ3dpZHRoJzogJzEyMCUnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICh5IDwgMC4yKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICQoJy5vdmVybGF5X3JhZGl1cycpLmNzcyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYm9yZGVyLXJhZGl1cyc6ICc1MCUgNTAlIDBweCAwcHgnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ3dpZHRoJzogJzEzMCUnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICh5IDwgMC4zKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICQoJy5vdmVybGF5X3JhZGl1cycpLmNzcyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYm9yZGVyLXJhZGl1cyc6ICc1MCUgNTAlIDBweCAwcHgnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ3dpZHRoJzogJzE0MCUnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICh5IDwgMC40KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICQoJy5vdmVybGF5X3JhZGl1cycpLmNzcyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYm9yZGVyLXJhZGl1cyc6ICc1MCUgNTAlIDBweCAwcHgnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ3dpZHRoJzogJzE1MCUnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICh5IDwgMC41KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICQoJy5vdmVybGF5X3JhZGl1cycpLmNzcyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYm9yZGVyLXJhZGl1cyc6ICc1MCUgNTAlIDBweCAwcHgnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ3dpZHRoJzogJzE2MCUnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICh5IDwgMC42KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICQoJy5vdmVybGF5X3JhZGl1cycpLmNzcyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYm9yZGVyLXJhZGl1cyc6ICc1MCUgNTAlIDBweCAwcHgnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ3dpZHRoJzogJzE3MCUnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICh5IDwgMC43KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICQoJy5vdmVybGF5X3JhZGl1cycpLmNzcyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYm9yZGVyLXJhZGl1cyc6ICc1MCUgNTAlIDBweCAwcHgnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ3dpZHRoJzogJzE4MCUnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICh5IDwgMC44KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICQoJy5vdmVybGF5X3JhZGl1cycpLmNzcyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYm9yZGVyLXJhZGl1cyc6ICc1MCUgNTAlIDBweCAwcHgnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ3dpZHRoJzogJzIwMCUnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIGlmICh5IDwgMC45KSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICQoJy5vdmVybGF5X3JhZGl1cycpLmNzcyh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAnYm9yZGVyLXJhZGl1cyc6ICc1MCUgNTAlIDBweCAwcHgnLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJ3dpZHRoJzogJzIyNSUnXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgJCgnLm92ZXJsYXlfcmFkaXVzJykuY3NzKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICdib3JkZXItcmFkaXVzJzogMFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgcG9zRHJhZyA9IHk7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgaWYgKCQoXCIubWFpbi1wYWdlXCIpLmhhc0NsYXNzKFwibm9uZS1ldmVudFwiKSkge1xyXG5cclxuICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAkKFwiLm1haW4tcGFnZVwiKS5yZW1vdmVDbGFzcyhcIm5vbmUtZXZlbnRcIik7XHJcbiAgICAgICAgICAgIH0sIDYwMCk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgIH1cclxuXHJcbiAgICBmdW5jdGlvbiBidWJibGVJbnN1cmFuY2UoKSB7XHJcbiAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgIGlmICgkKCcuYnViYmxld3JhcDplcSgwKScpLmZpbmQoJy5idWJibGVjaG9pY2UnKS5oYXNDbGFzcygnYWN0aXZlJykgfHwgJCgnLmJ1YmJsZXdyYXA6ZXEoMSknKS5maW5kKCcuYnViYmxlY2hvaWNlJykuaGFzQ2xhc3MoJ2FjdGl2ZScpIHx8ICQoJy5idWJibGV3cmFwOmVxKDIpJykuZmluZCgnLmJ1YmJsZWNob2ljZScpLmhhc0NsYXNzKCdhY3RpdmUnKSkge1xyXG4gICAgICAgICAgICAgICAgJCgnLmJ1YmJsZXdyYXA6ZXEoMyknKS5jc3Moe1xyXG4gICAgICAgICAgICAgICAgICAgIHBvaW50ZXJFdmVudHM6ICdub25lJyxcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoISQoJy5idWJibGV3cmFwOmVxKDApJykuZmluZCgnLmJ1YmJsZWNob2ljZScpLmhhc0NsYXNzKCdhY3RpdmUnKSAmJiAhJCgnLmJ1YmJsZXdyYXA6ZXEoMSknKS5maW5kKCcuYnViYmxlY2hvaWNlJykuaGFzQ2xhc3MoJ2FjdGl2ZScpICYmICEkKCcuYnViYmxld3JhcDplcSgyKScpLmZpbmQoJy5idWJibGVjaG9pY2UnKS5oYXNDbGFzcygnYWN0aXZlJykpIHtcclxuICAgICAgICAgICAgICAgICQoJy5idWJibGV3cmFwOmVxKDMpJykuY3NzKHtcclxuICAgICAgICAgICAgICAgICAgICBwb2ludGVyRXZlbnRzOiAnYXV0bycsXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICBpZiAoJCgnLmJ1YmJsZXdyYXA6ZXEoMyknKS5maW5kKCcuYnViYmxlY2hvaWNlJykuaGFzQ2xhc3MoJ2FjdGl2ZScpKSB7XHJcbiAgICAgICAgICAgICAgICAkKCcuYnViYmxld3JhcDplcSgwKSwgLmJ1YmJsZXdyYXA6ZXEoMSksIC5idWJibGV3cmFwOmVxKDIpJykuY3NzKHtcclxuICAgICAgICAgICAgICAgICAgICBwb2ludGVyRXZlbnRzOiAnbm9uZScsXHJcbiAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKCEkKCcuYnViYmxld3JhcDplcSgzKScpLmZpbmQoJy5idWJibGVjaG9pY2UnKS5oYXNDbGFzcygnYWN0aXZlJykpIHtcclxuICAgICAgICAgICAgICAgICQoJy5idWJibGV3cmFwOmVxKDApLCAuYnViYmxld3JhcDplcSgxKSwgLmJ1YmJsZXdyYXA6ZXEoMiknKS5jc3Moe1xyXG4gICAgICAgICAgICAgICAgICAgIHBvaW50ZXJFdmVudHM6ICdhdXRvJyxcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9LCAzMDApO1xyXG4gICAgfVxyXG5cclxuICAgIGZ1bmN0aW9uIGluaXRQb3NpdGlvbkJ1YmJsZSgpIHtcclxuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgdmFyIHF1ZXN0aW9uID0gcXVlc3Rpb25zW2N1cnJlbnRRdWVzdGlvbl07XHJcbiAgICAgICAgICAgIHZhciBiYlNjYWxlO1xyXG4gICAgICAgICAgICBpZiAoY3VycmVudFF1ZXN0aW9uID09IDEwKSB7XHJcbiAgICAgICAgICAgICAgICBiYlNjYWxlID0gMC4xO1xyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgYmJTY2FsZSA9IDA7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBxdWVzdGlvbi5hbnN3ZXJzLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgc2NhbGUgPSBxdWVzdGlvbi5hbnN3ZXJzW2ldLmJ1YmJsZXN0eWxlc2NhbGUuc3BsaXQoJywnKVsxXTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoJCh3aW5kb3cpLndpZHRoKCkgPj0gMTE1MCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBsZWZ0ID0gcXVlc3Rpb24uYW5zd2Vyc1tpXS5kZXNrdG9wdG9wbGVmdC5zcGxpdCgnLCcpWzFdO1xyXG4gICAgICAgICAgICAgICAgICAgICQoXCIuYnViYmxld3JhcDplcShcIiArIGkgKyBcIilcIikuY3NzKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGVmdDogbGVmdCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IHNjYWxlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0cmFuc2Zvcm06ICd0cmFuc2xhdGUoLTUwJSwtNTAlKSBzY2FsZSgwLjkpJyxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSBpZiAoJCh3aW5kb3cpLndpZHRoKCkgPiA3NjggJiYgJCh3aW5kb3cpLndpZHRoKCkgPCAxMTAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgdmFyIGxlZnQgPSBxdWVzdGlvbi5hbnN3ZXJzW2ldLmRlc2t0b3B0b3BsZWZ0LnNwbGl0KCcsJylbMV07XHJcbiAgICAgICAgICAgICAgICAgICAgJChcIi5idWJibGV3cmFwOmVxKFwiICsgaSArIFwiKVwiKS5jc3Moe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZWZ0OiBsZWZ0LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogc2NhbGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZSgtNTAlLC01MCUpIHNjYWxlKCcgKyAoMC44NSAtIGJiU2NhbGUpICsgJyknLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIGlmICgkKHdpbmRvdykud2lkdGgoKSA8PSA3NjggJiYgJCh3aW5kb3cpLndpZHRoKCkgPj0gMzc1ICYmICQod2luZG93KS5oZWlnaHQoKSA+IDc5MCkge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBsZWZ0ID0gcXVlc3Rpb24uYW5zd2Vyc1tpXS5tb2JpbGV0b3BsZWZ0LnNwbGl0KCcsJylbMV07XHJcbiAgICAgICAgICAgICAgICAgICAgJChcIi5idWJibGV3cmFwOmVxKFwiICsgaSArIFwiKVwiKS5jc3Moe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZWZ0OiBsZWZ0LFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB3aWR0aDogc2NhbGUsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRyYW5zZm9ybTogJ3RyYW5zbGF0ZSgtNTAlLC01MCUpIHNjYWxlKCcgKyAoMC42OCAtIGJiU2NhbGUpICsgJyknLFxyXG4gICAgICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgICAgICB2YXIgbGVmdCA9IHF1ZXN0aW9uLmFuc3dlcnNbaV0ubW9iaWxldG9wbGVmdC5zcGxpdCgnLCcpWzFdO1xyXG4gICAgICAgICAgICAgICAgICAgICQoXCIuYnViYmxld3JhcDplcShcIiArIGkgKyBcIilcIikuY3NzKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgbGVmdDogbGVmdCxcclxuICAgICAgICAgICAgICAgICAgICAgICAgd2lkdGg6IHNjYWxlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB0cmFuc2Zvcm06ICd0cmFuc2xhdGUoLTUwJSwtNTAlKSBzY2FsZSgnICsgKDAuNjIgLSBiYlNjYWxlKSArICcpJyxcclxuICAgICAgICAgICAgICAgICAgICB9KTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgICQoXCIuYnViYmxld3JhcDplcShcIiArIGkgKyBcIilcIikuZmluZChcIi5idWJibGVjaG9pY2VcIikuY3NzKHtcclxuICAgICAgICAgICAgICAgICAgICB3aWR0aDogc2NhbGUsXHJcbiAgICAgICAgICAgICAgICAgICAgaGVpZ2h0OiBzY2FsZSxcclxuICAgICAgICAgICAgICAgIH0pO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSwgMTAwKTtcclxuICAgIH1cclxuXHJcbiAgICBmdW5jdGlvbiBwb3NpdGlvbkJ1YmJsZSgpIHtcclxuICAgICAgICB2YXIgcXVlc3Rpb24gPSBxdWVzdGlvbnNbY3VycmVudFF1ZXN0aW9uXTtcclxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHF1ZXN0aW9uLmFuc3dlcnMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgLyogcmFuZG9tIHRpbWVvdXQgKGJ1YmJsZSBzbGlkZSB1cCkqL1xyXG4gICAgICAgICAgICAoZnVuY3Rpb24gKGkpIHtcclxuICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGlmICgkKHdpbmRvdykud2lkdGgoKSA+IDc2Nykge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgdG9wID0gcXVlc3Rpb24uYW5zd2Vyc1tpXS5kZXNrdG9wdG9wbGVmdC5zcGxpdCgnLCcpWzBdO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgbGVmdCA9IHF1ZXN0aW9uLmFuc3dlcnNbaV0uZGVza3RvcHRvcGxlZnQuc3BsaXQoJywnKVsxXTtcclxuICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YXIgdG9wID0gcXVlc3Rpb24uYW5zd2Vyc1tpXS5tb2JpbGV0b3BsZWZ0LnNwbGl0KCcsJylbMF07XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhciBsZWZ0ID0gcXVlc3Rpb24uYW5zd2Vyc1tpXS5tb2JpbGV0b3BsZWZ0LnNwbGl0KCcsJylbMV07XHJcbiAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgICQoXCIuYnViYmxld3JhcDplcShcIiArIGkgKyBcIilcIikuYW5pbWF0ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHRvcDogdG9wLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICBsZWZ0OiBsZWZ0LFxyXG4gICAgICAgICAgICAgICAgICAgIH0sIDcwMCk7XHJcbiAgICAgICAgICAgICAgICB9LCBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiA5MDEpICsgMjAwKTtcclxuXHJcbiAgICAgICAgICAgIH0pKGkpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICB9XHJcblxyXG4gICAgZnVuY3Rpb24gYnViYmxlRG93bigpIHtcclxuICAgICAgICB2YXIgcXVlc3Rpb24gPSBxdWVzdGlvbnNbY3VycmVudFF1ZXN0aW9uXTtcclxuICAgICAgICAkKCcjYnViYmxlQ2FudmFzJykuZmluZCgnLmJ1YmJsZWNob2ljZTpub3QoLmFjdGl2ZSknKS5hZGRDbGFzcygnaGlkZScpO1xyXG4gICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcXVlc3Rpb24uYW5zd2Vycy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAvKiByYW5kb20gdGltZW91dCAoYnViYmxlIHNsaWRlIHVwKSovXHJcbiAgICAgICAgICAgIChmdW5jdGlvbiAoaSkge1xyXG4gICAgICAgICAgICAgICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgJChcIi5idWJibGV3cmFwOmVxKFwiICsgaSArIFwiKVwiKS5hbmltYXRlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgdG9wOiAnMTE1JScsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIG9wYWNpdHk6ICcwJ1xyXG4gICAgICAgICAgICAgICAgICAgIH0sIDYwMCk7XHJcbiAgICAgICAgICAgICAgICB9LCBNYXRoLmZsb29yKE1hdGgucmFuZG9tKCkgKiA5MDEpICsgMjAwKTtcclxuXHJcbiAgICAgICAgICAgIH0pKGkpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxuXHJcbiAgICBmdW5jdGlvbiBzZXRDdXJyZW50QWN0aXZlKCkge1xyXG4gICAgICAgIHZhciBxdWVzdGlvbiA9IHF1ZXN0aW9uc1tjdXJyZW50UXVlc3Rpb25dO1xyXG5cclxuICAgICAgICB2YXIgY3VycmVudGFuc3dlckFycmF5ID0gcXVlc3Rpb24uY3VycmVudGFuc3dlci5zcGxpdCgnLCcpO1xyXG5cclxuICAgICAgICBmb3IgKHZhciBpID0gMDsgaSA8IHF1ZXN0aW9uLmFuc3dlcnMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgaWYgKGlzU2VsZWN0ZWQgPSBjdXJyZW50YW5zd2VyQXJyYXkuaW5jbHVkZXMoXCJcIiArIGkpKSB7XHJcbiAgICAgICAgICAgICAgICAkKFwiLmJ1YmJsZWNob2ljZTplcShcIiArIGkgKyBcIilcIikuYWRkQ2xhc3MoXCJhY3RpdmVcIik7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZnVuY3Rpb24gY2hlY2tCdWJibGVBY3RpdmUodmFsKSB7XHJcbiAgICAgICAgLypjaGVjayBidWJibGUgYWN0aXZlIChzaG93IGFuZCBoaWRlIGJ1dHRvbikgKi9cclxuICAgICAgICB2YXIgcXVlc3Rpb24gPSBxdWVzdGlvbnNbdmFsXTtcclxuICAgICAgICBpZiAodmFsIDwgMCkge1xyXG5cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICBpZiAocXVlc3Rpb24udHlwZSA9PSBcImNob2ljZVwiKSB7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKHF1ZXN0aW9uLm1heGNob2ljZSA9PSAxKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgJCgnI2J0bi1uZXh0LWpzJykuYWRkQ2xhc3MoJ2hpZGUnKTtcclxuICAgICAgICAgICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgaWYgKCQoJyNidWJibGVDYW52YXMnKS5maW5kKCcuYnViYmxlY2hvaWNlLmFjdGl2ZScpLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgJCgnI2J0bi1uZXh0LWpzJykucmVtb3ZlQ2xhc3MoJ2hpZGUnKTtcclxuICAgICAgICAgICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgICAgIH0sIDYwMCk7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHNldFRpbWVvdXQoZnVuY3Rpb24gKCkge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICAkKCcjYnRuLW5leHQtanMnKS5hZGRDbGFzcygnaGlkZScpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBpZiAoJCgnI2J1YmJsZUNhbnZhcycpLmZpbmQoJy5idWJibGVjaG9pY2UuYWN0aXZlJykubGVuZ3RoKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkKCcjYnRuLW5leHQtanMnKS5yZW1vdmVDbGFzcygnaGlkZScpO1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICAgICAgfSwgNjAwKTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9XHJcblxyXG4gICAgZnVuY3Rpb24gY291bnRQYWdlKGN1cikge1xyXG4gICAgICAgIHZhciBjb3VudGN1cnJlbnQgPSBjdXIgKyAxO1xyXG4gICAgICAgICQoJy5jb3VudGVyd3JhcCAuY3VycmVudC1jaG9pY2UnKS50ZXh0KGNvdW50Y3VycmVudCk7XHJcbiAgICAgICAgJCgnLmNvdW50ZXJ3cmFwIC5hbGwtY2hvaWNlJykudGV4dChxdWVzdGlvbnMubGVuZ3RoKTtcclxuICAgIH1cclxuXHJcbiAgICBmdW5jdGlvbiByZW1vdmVBbGxCdWJibGUoKSB7XHJcbiAgICAgICAgJCgnI2J1YmJsZUNhbnZhcycpLmVtcHR5KCkuaGlkZSgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qYW5pbWF0ZSBkZWJyaXMgd2hlbiBjbGljayBidWJibGUqL1xyXG4gICAgZnVuY3Rpb24gcjJkKHgpIHtcclxuICAgICAgICByZXR1cm4geCAvIChNYXRoLlBJIC8gMTgwKTtcclxuICAgIH1cclxuXHJcbiAgICBmdW5jdGlvbiBkMnIoeCkge1xyXG4gICAgICAgIHJldHVybiB4ICogKE1hdGguUEkgLyAxODApO1xyXG4gICAgfVxyXG5cclxuICAgIGZ1bmN0aW9uIHBvcChzdGFydF94LCBzdGFydF95LCBwYXJ0aWNsZV9jb3VudCkge1xyXG4gICAgICAgIGFyciA9IFtdO1xyXG4gICAgICAgIGFuZ2xlID0gMDtcclxuICAgICAgICBwYXJ0aWNsZXMgPSBbXTtcclxuICAgICAgICBvZmZzZXRfeCA9ICQoXCIjZHVtbXlfZGVicmlzXCIpLndpZHRoKCkgLyA1MDA7XHJcbiAgICAgICAgb2Zmc2V0X3kgPSAkKFwiI2R1bW15X2RlYnJpc1wiKS5oZWlnaHQoKSAvIDUwMDtcclxuXHJcbiAgICAgICAgZm9yIChpID0gMDsgaSA8IHBhcnRpY2xlX2NvdW50OyBpKyspIHtcclxuICAgICAgICAgICAgcmFkID0gZDJyKGFuZ2xlKTtcclxuICAgICAgICAgICAgeCA9IE1hdGguY29zKHJhZCkgKiAoMTAwICsgTWF0aC5yYW5kb20oKSAqIDIwKTtcclxuICAgICAgICAgICAgeSA9IE1hdGguc2luKHJhZCkgKiAoMTAwICsgTWF0aC5yYW5kb20oKSAqIDIwKTtcclxuICAgICAgICAgICAgYXJyLnB1c2goW3N0YXJ0X3ggKyB4LCBzdGFydF95ICsgeV0pO1xyXG4gICAgICAgICAgICB6ID0gJCgnPGRpdiBjbGFzcz1cImRlYnJpc1wiIC8+Jyk7XHJcbiAgICAgICAgICAgIHouY3NzKHtcclxuICAgICAgICAgICAgICAgIFwibGVmdFwiOiBzdGFydF94IC0gb2Zmc2V0X3gsXHJcbiAgICAgICAgICAgICAgICBcInRvcFwiOiBzdGFydF95IC0gb2Zmc2V0X3hcclxuICAgICAgICAgICAgfSkuYXBwZW5kVG8oJChcIiNkZWJyaXMtd3JhcFwiKSk7XHJcbiAgICAgICAgICAgIHBhcnRpY2xlcy5wdXNoKHopO1xyXG4gICAgICAgICAgICBhbmdsZSArPSAzNjAgLyAocGFydGljbGVfY291bnQgLSA2LjUpO1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgJC5lYWNoKHBhcnRpY2xlcywgZnVuY3Rpb24gKGksIHYpIHtcclxuICAgICAgICAgICAgJCh2KS5zaG93KCk7XHJcbiAgICAgICAgICAgICQodikuc3RvcCgpLmFuaW1hdGUoe1xyXG4gICAgICAgICAgICAgICAgdG9wOiBhcnJbaV1bMV0sXHJcbiAgICAgICAgICAgICAgICBsZWZ0OiBhcnJbaV1bMF0sXHJcbiAgICAgICAgICAgICAgICB3aWR0aDogNCxcclxuICAgICAgICAgICAgICAgIGhlaWdodDogNCxcclxuICAgICAgICAgICAgICAgIG9wYWNpdHk6IDBcclxuICAgICAgICAgICAgfSwgNjAwLCBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICAkKHYpLnJlbW92ZSgpXHJcbiAgICAgICAgICAgIH0pO1xyXG4gICAgICAgIH0pO1xyXG4gICAgfVxyXG4gICAgLyplbmQgYW5pbWF0ZSBkZWJyaXMgd2hlbiBjbGljayBidWJibGUqL1xyXG5cclxuICAgIC8qYm90dGxlKi9cclxuICAgIHZhciBjb3VudEFucyA9IDA7XHJcblxyXG4gICAgZnVuY3Rpb24gc3VibWl0QW5zKCkge1xyXG4gICAgICAgIGNvdW50QW5zICs9IDE7XHJcbiAgICAgICAgJChcIi5tYWluLXBhZ2UgW2RhdGEtbGF5ZXI9XCIgKyBjb3VudEFucyArIFwiXVwiKS5zaG93KCkuY3NzKHtcclxuICAgICAgICAgICAgb3BhY2l0eTogMSxcclxuICAgICAgICAgICAgdmlzaWJpbGl0eTogJ3Zpc2libGUnLFxyXG4gICAgICAgIH0pLmFkZENsYXNzKFwiYWN0aW9uXCIpO1xyXG4gICAgfVxyXG5cclxuICAgIGZ1bmN0aW9uIGJhY2tBbnMobnVtKSB7XHJcbiAgICAgICAgJChcIi5tYWluLXBhZ2UgW2RhdGEtbGF5ZXI9XCIgKyBjb3VudEFucyArIFwiXVwiKS5oaWRlKCkucmVtb3ZlQ2xhc3MoXCJhY3Rpb25cIik7XHJcbiAgICAgICAgdmFyIG1pbnVzID0gbnVtO1xyXG4gICAgICAgIGNvdW50QW5zIC09IG1pbnVzO1xyXG4gICAgICAgIC8vIGFsZXJ0KGNvdW50QW5zKTtcclxuICAgICAgICBpZiAoY291bnRBbnMgPT0gMCkge1xyXG4gICAgICAgICAgICAkKFwiLm1haW4tcGFnZSAgLmxheWVyXCIpLmhpZGUoKS5yZW1vdmVDbGFzcyhcImFjdGlvblwiKTtcclxuICAgICAgICB9XHJcbiAgICB9XHJcbiAgICAvKmJvdHRsZSovXHJcblxyXG5cclxuICAgIC8qIENhbGN1bGF0ZSBhbnN3ZXIgKi9cclxuICAgIHZhciByZXN1bHRBcnJheSA9IFtdO1xyXG5cclxuICAgIGZ1bmN0aW9uIHJlc3VsdENhbGN1bGF0b3IoKSB7XHJcbiAgICAgICAgcmVzdWx0QXJyYXkgPSBbXTtcclxuICAgICAgICBkaWdpdGFsQnVzaW5lc3NTY29yZSA9IDA7XHJcbiAgICAgICAgb3ZlcmFsbFNjb3JlID0gW107XHJcblxyXG5cclxuICAgICAgICB2YXIgcmVzdWx0ID0gbXVsdGlNb2RlKGNhbEFuc3dlcjMoKSwgY2FsQW5zd2VyNCgpLCBjYWxBbnN3ZXI1YW5kNigpLCBjYWxBbnN3ZXI3KCksIGNhbEFuc3dlcjgoKSk7IC8vc29ydCBtYXggc3RyaW5nIGluIGFycmF5XHJcbiAgICAgICAgY2FsQW5zd2VyOSgpXHJcbiAgICAgICAgY2FsQW5zd2VyMTAoKVxyXG4gICAgICAgIGNhbEFuc3dlcjExKClcclxuICAgICAgICBjaGVja0xldmVsID0gcmVzdWx0O1xyXG4gICAgfVxyXG5cclxuICAgIC8qIOC4guC5ieC4rSAxIChtYXhjaG9pY2UgPSAxKSovXHJcbiAgICBmdW5jdGlvbiBjYWxBbnN3ZXIxKCkge1xyXG5cclxuICAgICAgICB2YXIgZXhwciA9IHF1ZXN0aW9uc1swXS5jdXJyZW50YW5zd2VyOyAvL3BsZWFzZSBjaGFuZ2UgbnVtIGluIGFycmF5XHJcbiAgICAgICAgLyogMSBtYXhhbnN3ZXIqL1xyXG4gICAgICAgIHN3aXRjaCAoZXhwcikge1xyXG4gICAgICAgICAgICAvKnF1ZXN0aW9uW2ldLmFuc3dlcnMqL1xyXG4gICAgICAgICAgICBjYXNlICcwJzpcclxuICAgICAgICAgICAgICAgIHJlc3VsdEFycmF5LnB1c2goJ0JlZ2lubmVyJywgJ0ludGVybWVkaWF0ZScsICdBZHZhbmNlZCcpO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJzEnOlxyXG4gICAgICAgICAgICAgICAgcmVzdWx0QXJyYXkucHVzaCgnQmVnaW5uZXInLCAnSW50ZXJtZWRpYXRlJywgJ0FkdmFuY2VkJyk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgY2FzZSAnMic6XHJcbiAgICAgICAgICAgICAgICByZXN1bHRBcnJheS5wdXNoKCdBZHZhbmNlZCcsICdFeHBlcnQnLCAnUHJvZmVzc2lvbmFsJyk7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICB9XHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdEFycmF5O1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKiDguILguYnguK0gMiAobWF4Y2hvaWNlID49IDEpKi9cclxuICAgIGZ1bmN0aW9uIGNhbEFuc3dlcjIoKSB7XHJcbiAgICAgICAgLy8gQmVnaW5uZXJcclxuICAgICAgICAvLyBJbnRlcm1lZGlhdGVcclxuICAgICAgICAvLyBBZHZhbmNlZFxyXG4gICAgICAgIC8vIEV4cGVydCBcclxuICAgICAgICAvLyBQcm9mZXNzaW9uYWxcclxuICAgICAgICB2YXIgZXhwciA9IHF1ZXN0aW9uc1sxXS5jdXJyZW50YW5zd2VyOyAvL3BsZWFzZSBjaGFuZ2UgbnVtIGluIGFycmF5XHJcblxyXG4gICAgICAgIHZhciBhcnJheVRlbXAgPSBleHByLnNwbGl0KCcsJyk7XHJcbiAgICAgICAgLyptdWx0aXBsZSBhbnN3ZXIqL1xyXG4gICAgICAgIGZvciAoaSA9IDA7IGkgPCBhcnJheVRlbXAubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgc3dpdGNoIChhcnJheVRlbXBbaV0pIHtcclxuICAgICAgICAgICAgICAgIC8qIHF1ZXN0aW9uW2ldLmFuc3dlcnMgcmVzdWx0ICovXHJcbiAgICAgICAgICAgICAgICBjYXNlICcwJzpcclxuICAgICAgICAgICAgICAgICAgICByZXN1bHRBcnJheS5wdXNoKCdCZWdpbm5lcicsICdJbnRlcm1lZGlhdGUnLCAnQWR2YW5jZWQnKTtcclxuICAgICAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgICAgIGNhc2UgJzEnOlxyXG4gICAgICAgICAgICAgICAgICAgIHJlc3VsdEFycmF5LnB1c2goJ0JlZ2lubmVyJywgJ0ludGVybWVkaWF0ZScsICdBZHZhbmNlZCcpO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSAnMic6XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0QXJyYXkucHVzaCgnQWR2YW5jZWQnLCAnRXhwZXJ0JywgJ1Byb2Zlc3Npb25hbCcpO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgY2FzZSAnMyc6XHJcbiAgICAgICAgICAgICAgICAgICAgcmVzdWx0QXJyYXkucHVzaCgnQWR2YW5jZWQnLCAnRXhwZXJ0JywgJ1Byb2Zlc3Npb25hbCcpO1xyXG4gICAgICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgICAgICByZXR1cm4gcmVzdWx0QXJyYXk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC8qIOC4guC5ieC4rSAzIChtYXhjaG9pY2UgPSAxKSovXHJcbiAgICBmdW5jdGlvbiBjYWxBbnN3ZXIzKCkge1xyXG4gICAgICAgIC8vIEJlZ2lubmVyXHJcbiAgICAgICAgLy8gSW50ZXJtZWRpYXRlXHJcbiAgICAgICAgLy8gQWR2YW5jZWRcclxuICAgICAgICAvLyBFeHBlcnQgXHJcbiAgICAgICAgLy8gUHJvZmVzc2lvbmFsXHJcbiAgICAgICAgdmFyIGV4cHIgPSBxdWVzdGlvbnNbMl0uY3VycmVudGFuc3dlcjsgLy9wbGVhc2UgY2hhbmdlIG51bSBpbiBhcnJheVxyXG4gICAgICAgIC8qIDEgbWF4YW5zd2VyKi9cclxuICAgICAgICBzd2l0Y2ggKGV4cHIpIHtcclxuICAgICAgICAgICAgLypxdWVzdGlvbltpXS5hbnN3ZXJzKi9cclxuICAgICAgICAgICAgY2FzZSAnMCc6XHJcbiAgICAgICAgICAgICAgICByZXN1bHRBcnJheS5wdXNoKCdCZWdpbm5lcicsICdJbnRlcm1lZGlhdGUnLCAnQWR2YW5jZWQnKTtcclxuICAgICAgICAgICAgICAgIGRpZ2l0YWxCdXNpbmVzc1Njb3JlICs9IDEwMDtcclxuICAgICAgICAgICAgICAgIHEzU2NvcmUgPSA1MDtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICcxJzpcclxuICAgICAgICAgICAgICAgIHJlc3VsdEFycmF5LnB1c2goJ0JlZ2lubmVyJywgJ0ludGVybWVkaWF0ZScsICdBZHZhbmNlZCcpO1xyXG4gICAgICAgICAgICAgICAgZGlnaXRhbEJ1c2luZXNzU2NvcmUgKz0gMDtcclxuICAgICAgICAgICAgICAgIHEzU2NvcmUgPSAwO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJzInOlxyXG4gICAgICAgICAgICAgICAgcmVzdWx0QXJyYXkucHVzaCgnQWR2YW5jZWQnLCAnRXhwZXJ0JywgJ1Byb2Zlc3Npb25hbCcpO1xyXG4gICAgICAgICAgICAgICAgZGlnaXRhbEJ1c2luZXNzU2NvcmUgKz0gMTAwO1xyXG4gICAgICAgICAgICAgICAgcTNTY29yZSA9IDUwO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGRlZmF1bHQ6XHJcbiAgICAgICAgICAgICAgICAvLyBjb2RlIGJsb2NrXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gcmVzdWx0QXJyYXk7XHJcblxyXG4gICAgfVxyXG5cclxuICAgIC8qIOC4guC5ieC4rSA0IChtYXhjaG9pY2UgPSAxKSovXHJcbiAgICBmdW5jdGlvbiBjYWxBbnN3ZXI0KCkge1xyXG4gICAgICAgIC8vIEJlZ2lubmVyXHJcbiAgICAgICAgLy8gSW50ZXJtZWRpYXRlXHJcbiAgICAgICAgLy8gQWR2YW5jZWRcclxuICAgICAgICAvLyBFeHBlcnQgXHJcbiAgICAgICAgLy8gUHJvZmVzc2lvbmFsXHJcbiAgICAgICAgdmFyIGV4cHIgPSBxdWVzdGlvbnNbM10uY3VycmVudGFuc3dlcjsgLy9wbGVhc2UgY2hhbmdlIG51bSBpbiBhcnJheVxyXG4gICAgICAgIC8qIDEgbWF4YW5zd2VyKi9cclxuICAgICAgICBzd2l0Y2ggKGV4cHIpIHtcclxuICAgICAgICAgICAgLypxdWVzdGlvbltpXS5hbnN3ZXJzKi9cclxuICAgICAgICAgICAgY2FzZSAnMCc6XHJcbiAgICAgICAgICAgICAgICByZXN1bHRBcnJheS5wdXNoKCdCZWdpbm5lcicsICdJbnRlcm1lZGlhdGUnKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICcxJzpcclxuICAgICAgICAgICAgICAgIHJlc3VsdEFycmF5LnB1c2goJ0FkdmFuY2VkJywgJ0V4cGVydCcsICdQcm9mZXNzaW9uYWwnKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICcyJzpcclxuICAgICAgICAgICAgICAgIHJlc3VsdEFycmF5LnB1c2goJ0FkdmFuY2VkJywgJ0V4cGVydCcsICdQcm9mZXNzaW9uYWwnKTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBkZWZhdWx0OlxyXG4gICAgICAgICAgICAgICAgLy8gY29kZSBibG9ja1xyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgcmV0dXJuIHJlc3VsdEFycmF5O1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKiDguILguYnguK0gNSw2IOC4hOC4tOC4lOC4o+C4p+C4oeC4geC4seC4mSAobWF4Y2hvaWNlID49IDEpIOC4meC4seC4muC4guC5ieC4reC5gOC4guC5ieC4suC5gOC4hOC4qiBleC4g4LmA4Lil4Li34Lit4LiBIDIgKOC4iOC4suC4gSA4KSDguKfguLTguJjguLUgPSBCZWdpbm5lciAgKi9cclxuICAgIGZ1bmN0aW9uIGNhbEFuc3dlcjVhbmQ2KCkge1xyXG4gICAgICAgIC8vIEJlZ2lubmVyXHJcbiAgICAgICAgLy8gSW50ZXJtZWRpYXRlXHJcbiAgICAgICAgLy8gQWR2YW5jZWRcclxuICAgICAgICAvLyBFeHBlcnQgXHJcbiAgICAgICAgLy8gUHJvZmVzc2lvbmFsXHJcblxyXG4gICAgICAgIC8qIDEgbWF4YW5zd2VyIOC4meC4seC4muC4guC5ieC4reC5gOC4guC5ieC4suC5gOC4hOC4qiBleC4g4LmA4Lil4Li34Lit4LiBIDIgKOC4iOC4suC4gSA4KSDguKfguLTguJjguLUgPSBCZWdpbm5lciAqL1xyXG4gICAgICAgIHZhciBleHByMSA9IHF1ZXN0aW9uc1s0XS5jdXJyZW50YW5zd2VyOyAvL3BsZWFzZSBjaGFuZ2UgbnVtIGluIGFycmF5XHJcbiAgICAgICAgdmFyIGV4cHIyID0gcXVlc3Rpb25zWzVdLmN1cnJlbnRhbnN3ZXI7XHJcblxyXG4gICAgICAgIGlmIChleHByMSAhPSBcIlwiICYmIGV4cHIyICE9IFwiXCIpIHtcclxuICAgICAgICAgICAgdmFyIGV4cHIgPSBleHByMSArICcsJyArIGV4cHIyO1xyXG4gICAgICAgICAgICB2YXIgYXJyYXlUZW1wID0gZXhwci5zcGxpdCgnLCcpO1xyXG4gICAgICAgICAgICBpZiAoYXJyYXlUZW1wLmxlbmd0aCA+IDEgJiYgYXJyYXlUZW1wLmxlbmd0aCA8PSAzKSB7XHJcbiAgICAgICAgICAgICAgICByZXN1bHRBcnJheS5wdXNoKCdCZWdpbm5lcicpO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGFycmF5VGVtcC5sZW5ndGggPiAzICYmIGFycmF5VGVtcC5sZW5ndGggPD0gNCkge1xyXG4gICAgICAgICAgICAgICAgcmVzdWx0QXJyYXkucHVzaCgnSW50ZXJtZWRpYXRlJyk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoYXJyYXlUZW1wLmxlbmd0aCA+IDQgJiYgYXJyYXlUZW1wLmxlbmd0aCA8PSA2KSB7XHJcbiAgICAgICAgICAgICAgICByZXN1bHRBcnJheS5wdXNoKCdBZHZhbmNlZCcpO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGFycmF5VGVtcC5sZW5ndGggPT0gNykge1xyXG4gICAgICAgICAgICAgICAgcmVzdWx0QXJyYXkucHVzaCgnRXhwZXJ0Jyk7XHJcbiAgICAgICAgICAgIH0gZWxzZSBpZiAoYXJyYXlUZW1wLmxlbmd0aCA+PSA4KSB7XHJcbiAgICAgICAgICAgICAgICByZXN1bHRBcnJheS5wdXNoKCdQcm9mZXNzaW9uYWwnKTtcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgdmFyIHE1YW5zID0gZXhwcjEuc3BsaXQoJywnKTtcclxuICAgICAgICAgICAgdmFyIHE2YW5zID0gZXhwcjIuc3BsaXQoJywnKTtcclxuICAgICAgICAgICAgdmFyIGpvaW5hbnMgPSBcInE1KFwiO1xyXG5cclxuICAgICAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPCBxNWFucy5sZW5ndGg7IGkrKykge1xyXG4gICAgICAgICAgICAgICAgaWYgKGkgIT0gMCkge1xyXG4gICAgICAgICAgICAgICAgICAgIGpvaW5hbnMgKz0gJywnO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgam9pbmFucyArPSBxNWFuc1tpXTtcclxuXHJcbiAgICAgICAgICAgICAgICBpZiAoaSA9PSBxNWFucy5sZW5ndGggLSAxKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgam9pbmFucyArPSBcIikscTYoXCI7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIGZvciAodmFyIGkgPSAwOyBpIDwgcTZhbnMubGVuZ3RoOyBpKyspIHtcclxuICAgICAgICAgICAgICAgIGlmIChpICE9IDApIHtcclxuICAgICAgICAgICAgICAgICAgICBqb2luYW5zICs9ICcsJztcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgICAgIGpvaW5hbnMgKz0gcTZhbnNbaV07XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKGkgPT0gcTZhbnMubGVuZ3RoIC0gMSkge1xyXG4gICAgICAgICAgICAgICAgICAgIGpvaW5hbnMgKz0gXCIpXCI7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHZhciBzcGxpdGFuc3E1ID0gam9pbmFucy5zcGxpdCgnKSwnKVswXS5yZXBsYWNlKCdxNSgnLCAnJykuc3BsaXQoJywnKTtcclxuICAgICAgICAgICAgdmFyIHNwbGl0YW5zcTYgPSBqb2luYW5zLnNwbGl0KCcpLCcpWzFdLnJlcGxhY2UoJ3E2KCcsICcnKS5yZXBsYWNlKCcpJywgJycpLnNwbGl0KCcsJyk7XHJcbiAgICAgICAgICAgIHZhciBxNXNjb3JlID0gMDtcclxuICAgICAgICAgICAgdmFyIHE2c2NvcmUgPSAwO1xyXG4gICAgICAgICAgICB2YXIgdG90YWwgPSAwO1xyXG4gICAgICAgICAgICB2YXIgY291bnQgPSAwO1xyXG5cclxuICAgICAgICAgICAgdmFyIGRpZ2l0YWxQZXJjZW50YWdlID0gMDtcclxuXHJcbiAgICAgICAgICAgIC8vQ2FsIOC4hOC4sOC5geC4meC4meC4guC5ieC4rSA2XHJcbiAgICAgICAgICAgIGlmIChzcGxpdGFuc3E1LmluY2x1ZGVzKFwiMFwiKSkge1xyXG4gICAgICAgICAgICAgICAgcTVzY29yZSArPSAwO1xyXG4gICAgICAgICAgICAgICAgY291bnQrKztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoc3BsaXRhbnNxNS5pbmNsdWRlcyhcIjFcIikpIHtcclxuICAgICAgICAgICAgICAgIHE1c2NvcmUgKz0gMDtcclxuICAgICAgICAgICAgICAgIGNvdW50Kys7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHNwbGl0YW5zcTUuaW5jbHVkZXMoXCIyXCIpKSB7XHJcbiAgICAgICAgICAgICAgICBxNXNjb3JlICs9IDA7XHJcbiAgICAgICAgICAgICAgICBjb3VudCsrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChzcGxpdGFuc3E1LmluY2x1ZGVzKFwiM1wiKSkge1xyXG4gICAgICAgICAgICAgICAgcTVzY29yZSArPSAxMDA7XHJcbiAgICAgICAgICAgICAgICBjb3VudCsrO1xyXG4gICAgICAgICAgICAgICAgZGlnaXRhbFBlcmNlbnRhZ2UrKztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoc3BsaXRhbnNxNS5pbmNsdWRlcyhcIjRcIikpIHtcclxuICAgICAgICAgICAgICAgIHE1c2NvcmUgKz0gNTA7XHJcbiAgICAgICAgICAgICAgICBjb3VudCsrO1xyXG4gICAgICAgICAgICAgICAgZGlnaXRhbFBlcmNlbnRhZ2UrKztcclxuICAgICAgICAgICAgfVxyXG5cclxuICAgICAgICAgICAgcTVzY29yZSA9IHE1c2NvcmUgLyBjb3VudDtcclxuICAgICAgICAgICAgY291bnQgPSAwO1xyXG5cclxuICAgICAgICAgICAgLy9DYWwg4LiE4Liw4LmB4LiZ4LiZ4LiC4LmJ4LitIDZcclxuICAgICAgICAgICAgaWYgKHNwbGl0YW5zcTYuaW5jbHVkZXMoXCIwXCIpKSB7XHJcbiAgICAgICAgICAgICAgICBxNnNjb3JlICs9IDA7XHJcbiAgICAgICAgICAgICAgICBjb3VudCsrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChzcGxpdGFuc3E2LmluY2x1ZGVzKFwiMVwiKSkge1xyXG4gICAgICAgICAgICAgICAgcTZzY29yZSArPSA1MDtcclxuICAgICAgICAgICAgICAgIGNvdW50Kys7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgaWYgKHNwbGl0YW5zcTYuaW5jbHVkZXMoXCIyXCIpKSB7XHJcbiAgICAgICAgICAgICAgICBxNnNjb3JlICs9IDA7XHJcbiAgICAgICAgICAgICAgICBjb3VudCsrO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIGlmIChzcGxpdGFuc3E2LmluY2x1ZGVzKFwiM1wiKSkge1xyXG4gICAgICAgICAgICAgICAgcTZzY29yZSArPSAxMDA7XHJcbiAgICAgICAgICAgICAgICBjb3VudCsrO1xyXG4gICAgICAgICAgICAgICAgZGlnaXRhbFBlcmNlbnRhZ2UrKztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoc3BsaXRhbnNxNi5pbmNsdWRlcyhcIjRcIikpIHtcclxuICAgICAgICAgICAgICAgIHE2c2NvcmUgKz0gNTA7XHJcbiAgICAgICAgICAgICAgICBjb3VudCsrO1xyXG4gICAgICAgICAgICAgICAgZGlnaXRhbFBlcmNlbnRhZ2UrKztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoc3BsaXRhbnNxNi5pbmNsdWRlcyhcIjVcIikpIHtcclxuICAgICAgICAgICAgICAgIHE2c2NvcmUgKz0gMTAwO1xyXG4gICAgICAgICAgICAgICAgY291bnQrKztcclxuICAgICAgICAgICAgICAgIGRpZ2l0YWxQZXJjZW50YWdlKys7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgIHE2c2NvcmUgPSBxNnNjb3JlIC8gY291bnQ7XHJcbiAgICAgICAgICAgIHRvdGFsID0gKHE1c2NvcmUgKyBxNnNjb3JlKSAvIDI7XHJcbiAgICAgICAgICAgIGRpZ2l0YWxCdXNpbmVzc1Njb3JlID0gKGRpZ2l0YWxCdXNpbmVzc1Njb3JlICsgdG90YWwpIC8gMjtcclxuXHJcbiAgICAgICAgICAgIC8v4LiE4Liw4LmB4LiZ4LiZ4LiC4LmJ4LitIDUsNlxyXG4gICAgICAgICAgICBkaWdpdGFsUGVyY2VudGFnZSA9IGRpZ2l0YWxQZXJjZW50YWdlICogMTBcclxuICAgICAgICAgICAgaWYgKGRpZ2l0YWxQZXJjZW50YWdlID09IDQwKSB7XHJcbiAgICAgICAgICAgICAgICBkaWdpdGFsUGVyY2VudGFnZSArPSAxMFxyXG4gICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICAvL+C4hOC4sOC5geC4meC4mSBmaW5hbCDguYHguIHguJkgRGlnaXRhbCDguILguYnguK0gNSw2ICsg4LiC4LmJ4LitIDNcclxuICAgICAgICAgICAgb3ZlcmFsbFNjb3JlLnB1c2goJzA6JyArIE1hdGgucm91bmQoZGlnaXRhbFBlcmNlbnRhZ2UgKyBxM1Njb3JlKSk7XHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gcmVzdWx0QXJyYXk7XHJcblxyXG4gICAgfVxyXG4gICAgLyog4LiC4LmJ4LitIDcgKG1heGNob2ljZSA+PSAxKSDguJnguLHguJrguILguYnguK3guYDguILguYnguLLguYDguITguKogZXguIOC5gOC4peC4t+C4reC4gSAyICjguIjguLLguIEgOCkg4Lin4Li04LiY4Li1ID0gQmVnaW5uZXIgICovXHJcbiAgICBmdW5jdGlvbiBjYWxBbnN3ZXI3KCkge1xyXG4gICAgICAgIC8vIEJlZ2lubmVyXHJcbiAgICAgICAgLy8gSW50ZXJtZWRpYXRlXHJcbiAgICAgICAgLy8gQWR2YW5jZWRcclxuICAgICAgICAvLyBFeHBlcnQgXHJcbiAgICAgICAgLy8gUHJvZmVzc2lvbmFsXHJcblxyXG4gICAgICAgIHZhciBleHByID0gcXVlc3Rpb25zWzZdLmN1cnJlbnRhbnN3ZXIudG9TdHJpbmcoKTsgLy9wbGVhc2UgY2hhbmdlIG51bSBpbiBhcnJheVxyXG4gICAgICAgIGV4cHIgPSBleHByLnNwbGl0KCcsJylcclxuXHJcblxyXG4gICAgICAgIG92ZXJhbGxTY29yZS5wdXNoKCcyOicgKyBleHByLmxlbmd0aCAqIDIwKTtcclxuXHJcblxyXG4gICAgfVxyXG5cclxuICAgIC8qIOC4guC5ieC4rSA4ICovXHJcbiAgICBmdW5jdGlvbiBjYWxBbnN3ZXI4KCkge1xyXG4gICAgICAgIHZhciBleHByID0gcXVlc3Rpb25zWzddLmN1cnJlbnRhbnN3ZXI7IC8vcGxlYXNlIGNoYW5nZSBudW0gaW4gYXJyYXlcclxuXHJcbiAgICAgICAgb3ZlcmFsbFNjb3JlLnB1c2goJzM6JyArIChwYXJzZUludChleHByKSArIDEpICogMjUpO1xyXG5cclxuICAgIH1cclxuXHJcbiAgICAvKiDguILguYnguK0gOSAqL1xyXG4gICAgZnVuY3Rpb24gY2FsQW5zd2VyOSgpIHtcclxuICAgICAgICB2YXIgZXhwciA9IHF1ZXN0aW9uc1s4XS5jdXJyZW50YW5zd2VyOyAvL3BsZWFzZSBjaGFuZ2UgbnVtIGluIGFycmF5XHJcbiAgICAgICAgdmFyIHJlc3VsdFNjb3JlID0gMDtcclxuXHJcbiAgICAgICAgc3dpdGNoIChleHByKSB7XHJcbiAgICAgICAgICAgIC8qcXVlc3Rpb25baV0uYW5zd2VycyovXHJcbiAgICAgICAgICAgIGNhc2UgJzAnOlxyXG4gICAgICAgICAgICAgICAgcmVzdWx0U2NvcmUgPSAyNTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICcxJzpcclxuICAgICAgICAgICAgICAgIHJlc3VsdFNjb3JlID0gMTAwO1xyXG4gICAgICAgICAgICAgICAgYnJlYWs7XHJcbiAgICAgICAgICAgIGNhc2UgJzInOlxyXG4gICAgICAgICAgICAgICAgcmVzdWx0U2NvcmUgPSA3NTtcclxuICAgICAgICAgICAgICAgIGJyZWFrO1xyXG4gICAgICAgICAgICBjYXNlICczJzpcclxuICAgICAgICAgICAgICAgIHJlc3VsdFNjb3JlID0gNTA7XHJcbiAgICAgICAgICAgICAgICBicmVhaztcclxuICAgICAgICAgICAgZGVmYXVsdDpcclxuICAgICAgICAgICAgICAgIC8vIGNvZGUgYmxvY2tcclxuICAgICAgICB9XHJcblxyXG4gICAgICAgIG92ZXJhbGxTY29yZS5wdXNoKCcxOicgKyByZXN1bHRTY29yZSk7XHJcbiAgICB9XHJcblxyXG4gICAgLyog4LiC4LmJ4LitIDEwICovXHJcbiAgICBmdW5jdGlvbiBjYWxBbnN3ZXIxMCgpIHtcclxuICAgICAgICB2YXIgZXhwciA9IHF1ZXN0aW9uc1s5XS5jdXJyZW50YW5zd2VyOyAvL3BsZWFzZSBjaGFuZ2UgbnVtIGluIGFycmF5XHJcbiAgICAgICAgdmFyIGNvdW50ID0gZXhwci5zcGxpdCgnLCcpO1xyXG4gICAgICAgIHZhciByZXN1bHRTY29yZSA9IDA7XHJcblxyXG4gICAgICAgIGlmIChleHByLmluY2x1ZGVzKFwiM1wiKSkge1xyXG4gICAgICAgICAgICByZXN1bHRTY29yZSA9IDA7XHJcbiAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgaWYgKGNvdW50Lmxlbmd0aCA9PSAxKSB7XHJcbiAgICAgICAgICAgICAgICByZXN1bHRTY29yZSA9IDUwO1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGNvdW50Lmxlbmd0aCA9PSAyKSB7XHJcbiAgICAgICAgICAgICAgICByZXN1bHRTY29yZSA9IDc1O1xyXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGNvdW50Lmxlbmd0aCA9PSAzKSB7XHJcbiAgICAgICAgICAgICAgICByZXN1bHRTY29yZSA9IDEwMDtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuXHJcbiAgICAgICAgb3ZlcmFsbFNjb3JlLnB1c2goJzQ6JyArIHJlc3VsdFNjb3JlKTtcclxuICAgIH1cclxuXHJcbiAgICAvKiDguILguYnguK0gMTEgKi9cclxuICAgIGZ1bmN0aW9uIGNhbEFuc3dlcjExKCkge1xyXG4gICAgICAgIHZhciBleHByID0gcXVlc3Rpb25zWzEwXS5jdXJyZW50YW5zd2VyOyAvL3BsZWFzZSBjaGFuZ2UgbnVtIGluIGFycmF5XHJcbiAgICAgICAgdmFyIGNvdW50ID0gZXhwci5zcGxpdCgnLCcpO1xyXG4gICAgfVxyXG5cclxuICAgIC8qIHNvcnQgbWF4IHN0cmluZyBpbiBhcnJheSAqL1xyXG4gICAgaWYgKCFPYmplY3QudmFsdWVzKSBPYmplY3QudmFsdWVzID0gZnVuY3Rpb24gKG9iKSB7XHJcbiAgICAgICAgdmFyIGFyciA9IFtdO1xyXG4gICAgICAgIGZvciAodmFyIGsgaW4gb2IpXHJcbiAgICAgICAgICAgIGlmIChvYi5oYXNPd25Qcm9wZXJ0eShrKSlcclxuICAgICAgICAgICAgICAgIGFyci5wdXNoKG9iW2tdKTtcclxuICAgICAgICByZXR1cm4gYXJyO1xyXG4gICAgfTtcclxuXHJcbiAgICAvKiBmaW5kIHRoZSBsZXZlbCAqL1xyXG4gICAgZnVuY3Rpb24gbXVsdGlNb2RlKGFycikge1xyXG4gICAgICAgIHZhciBtYXAgPSBhcnIucmVkdWNlKGZ1bmN0aW9uIChtYXAsIGl0ZW0pIHtcclxuICAgICAgICAgICAgaWYgKCEoaXRlbSBpbiBtYXApKSBtYXBbaXRlbV0gPSAwO1xyXG4gICAgICAgICAgICByZXR1cm4gbWFwW2l0ZW1dKyssIG1hcDtcclxuICAgICAgICB9LCB7fSk7XHJcbiAgICAgICAgdmFyIG1heCA9IE1hdGgubWF4LmFwcGx5KG51bGwsIE9iamVjdC52YWx1ZXMobWFwKSksXHJcbiAgICAgICAgICAgIGFycjIgPSBbXTtcclxuICAgICAgICBPYmplY3Qua2V5cyhtYXApLmZvckVhY2goZnVuY3Rpb24gKGspIHtcclxuICAgICAgICAgICAgaWYgKG1hcFtrXSA9PT0gbWF4KSBhcnIyLnB1c2goayk7XHJcbiAgICAgICAgfSk7XHJcblxyXG5cclxuXHJcbiAgICAgICAgdmFyIHJlc3VsdGxldmVsID0gYXJyMjtcclxuICAgICAgICBpZiAocmVzdWx0bGV2ZWwuaW5jbHVkZXMoJ1Byb2Zlc3Npb25hbCcpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAnUHJvZmVzc2lvbmFsJ1xyXG4gICAgICAgIH0gZWxzZSBpZiAocmVzdWx0bGV2ZWwuaW5jbHVkZXMoJ0V4cGVydCcpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAnRXhwZXJ0J1xyXG4gICAgICAgIH0gZWxzZSBpZiAocmVzdWx0bGV2ZWwuaW5jbHVkZXMoJ0FkdmFuY2VkJykpIHtcclxuICAgICAgICAgICAgcmV0dXJuICdBZHZhbmNlZCdcclxuICAgICAgICB9IGVsc2UgaWYgKHJlc3VsdGxldmVsLmluY2x1ZGVzKCdJbnRlcm1lZGlhdGUnKSkge1xyXG4gICAgICAgICAgICByZXR1cm4gJ0ludGVybWVkaWF0ZSdcclxuICAgICAgICB9IGVsc2UgaWYgKHJlc3VsdGxldmVsLmluY2x1ZGVzKCdCZWdpbm5lcicpKSB7XHJcbiAgICAgICAgICAgIHJldHVybiAnQmVnaW5uZXInXHJcbiAgICAgICAgfVxyXG5cclxuICAgICAgICByZXR1cm4gYXJyMjtcclxuICAgIH1cclxuICAgIC8qIGVuZCBzb3J0IG1heCBzdHJpbmcgaW4gYXJyYXkgKi9cclxuXHJcbiAgICAvKiBDYWxjdWxhdGUgYW5zd2VyICovXHJcblxyXG4gICAgZnVuY3Rpb24gaW5pdEJhY2tncm91bmRBbmRFbG0oKSB7XHJcbiAgICAgICAgZm9yICh2YXIgaSA9IDA7IGkgPD0gcXVlc3Rpb25zLmxlbmd0aDsgaSsrKSB7XHJcbiAgICAgICAgICAgIC8qIGZvciBiYWNrZ3JvdW5kIGdyYWRpZW50ICovXHJcbiAgICAgICAgICAgIHZhciBodG1sYmdncmFkaWVudCA9IFwiXCI7XHJcbiAgICAgICAgICAgIHZhciBudW0gPSBpICsgMTtcclxuICAgICAgICAgICAgdmFyIG51bXEgPSBpICsgMjtcclxuICAgICAgICAgICAgaWYgKGkgPCBxdWVzdGlvbnMubGVuZ3RoKSB7XHJcblxyXG5cclxuICAgICAgICAgICAgICAgIGh0bWxiZ2dyYWRpZW50ICs9ICc8ZGl2IGlkPVwicXVlc3Rpb25fJyArIG51bSArICdcIiBjbGFzcz1cInF1ZXN0aW9uXCIgZGF0YS1xdWVzdGlvbj1cIicgKyBudW1xICsgJ1wiPic7XHJcbiAgICAgICAgICAgICAgICBodG1sYmdncmFkaWVudCArPSAnPGRpdiBjbGFzcz1cImxheWVyXzAgIGJnX2dyYWRpZW50IGJnX2dyYWRpZW50LScgKyBudW0gKyAnXCI+PC9kaXY+JztcclxuXHJcbiAgICAgICAgICAgICAgICBodG1sYmdncmFkaWVudCArPSAnPC9kaXY+JztcclxuXHJcbiAgICAgICAgICAgICAgICAvKiBmb3IgYm90dGxlIGluIG1haW4tcGFnZSAqL1xyXG4gICAgICAgICAgICAgICAgdmFyIGh0bWxib3R0bGUgPSBcIlwiO1xyXG4gICAgICAgICAgICAgICAgaHRtbGJvdHRsZSArPSAnPGltZyBjbGFzcz1cImxheWVyXCIgZGF0YS1sYXllcj1cIicgKyBudW0gKyAnXCIgc3JjPVwiLi4vZGlzdC9yZXNvdXJjZXMvaW1hZ2VzL2JvdHRsZS9sYXllcicgKyBudW0gKyAnLnBuZ1wiIGFsdD1cImxheWVyJyArIG51bSArICdcIj48L2ltZz4nXHJcbiAgICAgICAgICAgICAgICAkKFwiLm1haW4tcGFnZSAuYm90dGxld3JhcCAuYm90dGxlXCIpLmFwcGVuZChodG1sYm90dGxlKTtcclxuICAgICAgICAgICAgICAgIC8qIGZvciBib3R0bGUgKi9cclxuXHJcbiAgICAgICAgICAgICAgICAvKiBmb3IgYm90dGxlIGluIHJlc3VsdCAqL1xyXG4gICAgICAgICAgICAgICAgaWYgKGkgPCBxdWVzdGlvbnMubGVuZ3RoIC0gMSkge1xyXG4gICAgICAgICAgICAgICAgICAgIHZhciBodG1sYm90dGxlcmVzdWx0ID0gXCJcIjtcclxuICAgICAgICAgICAgICAgICAgICBodG1sYm90dGxlcmVzdWx0ICs9ICc8aW1nIGNsYXNzPVwibGF5ZXJcIiBkYXRhLWxheWVyPVwiJyArIG51bXEgKyAnXCIgc3JjPVwiLi4vZGlzdC9yZXNvdXJjZXMvaW1hZ2VzL2JvdHRsZS9sYXllcicgKyBudW1xICsgJy5wbmdcIiBhbHQ9XCJsYXllcicgKyBudW1xICsgJ1wiPjwvaW1nPidcclxuICAgICAgICAgICAgICAgICAgICAkKFwiI3Jlc3VsdC1wYWdlIC5ib3R0bGV3cmFwIC5ib3R0bGVcIikuYXBwZW5kKGh0bWxib3R0bGVyZXN1bHQpO1xyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICAgICAgLyogZm9yIGJvdHRsZSByZXN1bHQgKi9cclxuICAgICAgICAgICAgfSBlbHNlIHtcclxuICAgICAgICAgICAgICAgIGh0bWxiZ2dyYWRpZW50ICs9ICc8ZGl2IGlkPVwicXVlc3Rpb25fJyArIG51bSArICdcIiBjbGFzcz1cInF1ZXN0aW9uIHJlc3VsdC1wYWdlXCIgZGF0YS1xdWVzdGlvbj1cInJlc3VsdC1wYWdlXCI+JztcclxuICAgICAgICAgICAgICAgIGh0bWxiZ2dyYWRpZW50ICs9ICc8ZGl2IGNsYXNzPVwibGF5ZXJfMCAgYmdfZ3JhZGllbnQgYmdfZ3JhZGllbnQtJyArIG51bSArICdcIj48L2Rpdj4nO1xyXG4gICAgICAgICAgICAgICAgaHRtbGJnZ3JhZGllbnQgKz0gJzwvZGl2Pic7XHJcbiAgICAgICAgICAgIH1cclxuXHJcbiAgICAgICAgICAgICQoXCIucXVlc3Rpb25fbWFpblwiKS5hcHBlbmQoaHRtbGJnZ3JhZGllbnQpO1xyXG4gICAgICAgIH1cclxuICAgIH1cclxufSk7XHJcblxyXG5cclxuZnVuY3Rpb24gc2hha2Vfc3RnZTAoKSB7XHJcblxyXG4gICAgaWYgKCEvQW5kcm9pZC8udGVzdChuYXZpZ2F0b3IudXNlckFnZW50KSkge1xyXG4gICAgICAgICQoXCIjYnRuLTFcIikucmVtb3ZlKCk7XHJcbiAgICAgICAgJChcIiNidG4tMlwiKS5yZW1vdmUoKTtcclxuICAgIH07XHJcblxyXG4gICAgLy8gd2FpdGluZ0ZvclJlc3VsdFBhZ2UgPSB0cnVlXHJcbiAgICB2YXIgc2hha2VFdmVudCA9IG5ldyBTaGFrZSh7XHJcbiAgICAgICAgdGhyZXNob2xkOiAxNVxyXG4gICAgfSk7XHJcbiAgICBzaGFrZUV2ZW50LnN0YXJ0KCk7XHJcbiAgICB3aW5kb3cuYWRkRXZlbnRMaXN0ZW5lcignc2hha2UnLCBmdW5jdGlvbiAoZSkge1xyXG4gICAgICAgIGUucHJldmVudERlZmF1bHQoKTtcclxuICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgJChcIiNtYi1zaGFrZVwiKS5hZGRDbGFzcyhcInNoYWtlXCIpO1xyXG4gICAgICAgICAgICAkKFwiI2J0blwiKS5jc3MoJ3RyYW5zZm9ybScsICd0cmFuc2xhdGUoLTUwJSwtNTAlKSBzY2FsZSgwKScpO1xyXG4gICAgICAgICAgICBzZXRUaW1lb3V0KGZ1bmN0aW9uICgpIHtcclxuXHJcbiAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24gPSBcInJlc3VsdC1wYWdlLmh0bWw/bGV2ZWw9XCIgKyBjaGVja0xldmVsICsgXCImbmFtZT1cIiArIGJ1c2luZXNzTmFtZSArIFwiJnNjb3JlPVwiICsgb3ZlcmFsbFNjb3JlICsgXCImcHJvZHVjdD1cIiArIHByb2R1Y3RBcnJheTtcclxuXHJcbiAgICAgICAgICAgIH0sIDgwMCk7XHJcbiAgICAgICAgfSwgMjAwMCk7XHJcbiAgICB9LCBmYWxzZSk7XHJcblxyXG4gICAgLy9zdG9wIGxpc3RlbmluZ1xyXG4gICAgZnVuY3Rpb24gc3RvcFNoYWtlKCkge1xyXG4gICAgICAgIHNoYWtlRXZlbnQuc3RvcCgpO1xyXG4gICAgfVxyXG5cclxuICAgIC8vY2hlY2sgaWYgc2hha2UgaXMgc3VwcG9ydGVkIG9yIG5vdC5cclxuICAgIGlmICghKFwib25kZXZpY2Vtb3Rpb25cIiBpbiB3aW5kb3cpKSB7XHJcbiAgICAgICAgYWxlcnQoXCJOb3QgU3VwcG9ydGVkXCIpO1xyXG4gICAgfVxyXG5cclxuICAgICQoJyNyZXN1bHQtcGFnZScpLmZhZGVJbig2MDApO1xyXG5cclxuICAgIHZhciBpbWcxID0gJChcIiNpbWctc2hha2UtMVwiKTtcclxuICAgIHZhciBpbWcyID0gJChcIiNpbWctc2hha2UtMlwiKTtcclxuICAgIHZhciBpbnAgPSAkKFwiLmJ1c2luZXNzX25hbWVcIik7XHJcbiAgICB2YXIgc2xpZGUgPSAkKFwiI3Jlc3VsdC1wYWdlIGltZy5sYXllclwiKSxcclxuICAgICAgICBjdXIgPSAwO1xyXG4gICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgaW1nMS5yZW1vdmVDbGFzcyhcInN0YWdlLTBcIik7XHJcbiAgICAgICAgaW1nMi5yZW1vdmVDbGFzcyhcInN0YWdlLTBcIik7XHJcbiAgICAgICAgJChcImgxIHNwYW4uc3RhZ2UtMFwiKS5yZW1vdmVDbGFzcyhcImhpZGUtZWxtXCIpO1xyXG4gICAgICAgIHNldEludGVydmFsKGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgJChzbGlkZSkuZXEoKCsrY3VyKSkuYWRkQ2xhc3MoJ2FjdGlvbicpO1xyXG4gICAgICAgIH0sIDIwMCk7XHJcbiAgICB9LCA2MDApO1xyXG4gICAgc2V0VGltZW91dChmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgaW5wLnJlbW92ZUNsYXNzKFwic3RhZ2UtMFwiKTtcclxuICAgIH0sIDIwMDApO1xyXG5cclxufVxyXG5cclxuZnVuY3Rpb24gZ2V0VXJsVmFycygpIHtcclxuICAgIHZhciB2YXJzID0ge307XHJcbiAgICB2YXIgcGFydHMgPSB3aW5kb3cubG9jYXRpb24uaHJlZi5yZXBsYWNlKC9bPyZdKyhbXj0mXSspPShbXiZdKikvZ2ksIGZ1bmN0aW9uIChtLCBrZXksIHZhbHVlKSB7XHJcbiAgICAgICAgdmFyc1trZXldID0gdmFsdWU7XHJcbiAgICB9KTtcclxuICAgIHJldHVybiB2YXJzO1xyXG59Il0sImZpbGUiOiJsYW5kaW5nLmpzIn0=
