$(document).ready(function() {

    // Option change functionality
    $('.option').on('change', function() {
        var $radio = $(this),
            $currOptGroup = $($radio.data('target')),
            $optGroups = $('.option-group');

        $optGroups.addClass('hidden');
        $currOptGroup.removeClass('hidden');
    });

    // Validation 
    var parsleyInstance = $('form').parsley();
    
    parsleyInstance.options.successClass = 'test';
    parsleyInstance.options.errorsContainer = function (ParsleyField) {
        var $topParent = ParsleyField.$element.parentsUntil('form')[ParsleyField.$element.parentsUntil('form').length - 1];
       
        return $topParent;
    };
    
    parsleyInstance.options.errorsWrapper = '<div class="test"></div>';
    parsleyInstance.options.errorTemplate = '<span></span>';
    
    console.log(parsleyInstance.options.errorTemplate);
    
    window.ParsleyValidator
        .addValidator(
            // validate if the date is completed (all 3 selectors need to have a value)
            'dobrequired',

            // 'value' will be the value selected
            // 'requirements' will be the ids of the selects
            function(value, requirements) {
                var day = $(requirements[0]).val(),
                    month = $(requirements[1]).val(),
                    year = $(requirements[2]).val(),
                    /* 
                      Instantiate the date field as a parsley object
                      We do this because we need to add/remove error messages as all 
                      of them are attached to the date selector
                     */
                    $dobDayInstance = $(requirements[0]).parsley(),
                    errorMsg = 'Date is required';

                /* 
                  Remove all the existing messages
                  Because the required validator has the largest priority, all other
                  error messages have to be removed.
                 */
                window.ParsleyUI.removeError($dobDayInstance, 'dobrequired');
                window.ParsleyUI.removeError($dobDayInstance, 'date');
                window.ParsleyUI.removeError($dobDayInstance, 'age');

                if (day.length && month.length && year.length) {
                    // If the date is valid, remove the error msg
                    window.ParsleyUI.removeError($dobDayInstance, 'dobrequired');

                    return true;
                } else {
                    // If the date is not valid, add the error msg
                    window.ParsleyUI.addError($dobDayInstance, 'dobrequired', errorMsg);

                    return false;
                }

            },
            // priority - the validator with the highest priority will be run first
            34
        )
        .addValidator(
            // validate if the selected date is valid
            // ex.: 29/02/2010 will not be valid, 29/02/2012 will
            'date',

            // 'value' will be the value selected
            // 'requirements' will be the ids of the selects
            function(value, requirements) {
                var day = $(requirements[0]).val(),
                    month = $(requirements[1]).val(),
                    year = $(requirements[2]).val(),
                    mydate = new Date(),
                    /* 
                      Instantiate the date field as a parsley object
                      We do this because we need to add/remove error messages as all 
                      of them are attached to the date selector
                     */
                    $dobDayInstance = $(requirements[0]).parsley(),
                    errorMsg = 'Enter a valid date';

                /* 
                 Remove the error messages of the lower priority validators
                 Because the date validator has a lower priority, its
                 error messages have to be removed.
                */
                window.ParsleyUI.removeError($dobDayInstance, 'date');
                window.ParsleyUI.removeError($dobDayInstance, 'age');

                mydate.setFullYear(year, month - 1, day);

                if ((mydate.getDate() == day) && (mydate.getMonth() == (month - 1)) && (mydate.getFullYear() == year)) {
                    // If the current date is not identical to the converted date, it's not valid
                    window.ParsleyUI.removeError($dobDayInstance, 'date');

                    return true;
                } else {
                    window.ParsleyUI.addError($dobDayInstance, 'date', errorMsg);

                    return false;
                }
            },

            // priority
            33
        )
        .addValidator(
            // validate if the selected date coresponds to an age of at least 18
            'age',

            // 'value' will be the value selected
            // 'requirements' will be the ids of the selects
            function(value, requirements) {
                var day = $(requirements[0]).val(),
                    month = $(requirements[1]).val(),
                    year = $(requirements[2]).val(),
                    mydate = new Date(),
                    currdate = new Date(),
                    age = 18,
                    /* 
                      Instantiate the date field as a parsley object
                      We do this because we need to add/remove error messages as all 
                      of them are attached to the date selector
                     */
                    $dobDayInstance = $(requirements[0]).parsley(),
                    errorMsg = 'You must be over 18 to proceed';

                // Remove only the corresponding error msg as this is the last validator
                window.ParsleyUI.removeError($dobDayInstance, 'age');

                // Selected birth date
                mydate.setFullYear(year, month - 1, day);
                // We substract the minimum age from the current date
                currdate.setFullYear(currdate.getFullYear() - age);

                if (currdate >= mydate) {
                    /* 
                     We compare the minimum valid birth date (current date minus the minimum age)
                     to the current date. The first one needs to be greater or equal to the second
                     one in order to have a valid birth date.
                     */
                    window.ParsleyUI.removeError($dobDayInstance, 'age');

                    return true;
                } else {
                    window.ParsleyUI.addError($dobDayInstance, 'age', errorMsg);

                    return false;
                }
            },

            // priority
            32
        );

});
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJwYXJzbGV5LmpzIl0sInNvdXJjZXNDb250ZW50IjpbIiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xyXG5cclxuICAgIC8vIE9wdGlvbiBjaGFuZ2UgZnVuY3Rpb25hbGl0eVxyXG4gICAgJCgnLm9wdGlvbicpLm9uKCdjaGFuZ2UnLCBmdW5jdGlvbigpIHtcclxuICAgICAgICB2YXIgJHJhZGlvID0gJCh0aGlzKSxcclxuICAgICAgICAgICAgJGN1cnJPcHRHcm91cCA9ICQoJHJhZGlvLmRhdGEoJ3RhcmdldCcpKSxcclxuICAgICAgICAgICAgJG9wdEdyb3VwcyA9ICQoJy5vcHRpb24tZ3JvdXAnKTtcclxuXHJcbiAgICAgICAgJG9wdEdyb3Vwcy5hZGRDbGFzcygnaGlkZGVuJyk7XHJcbiAgICAgICAgJGN1cnJPcHRHcm91cC5yZW1vdmVDbGFzcygnaGlkZGVuJyk7XHJcbiAgICB9KTtcclxuXHJcbiAgICAvLyBWYWxpZGF0aW9uIFxyXG4gICAgdmFyIHBhcnNsZXlJbnN0YW5jZSA9ICQoJ2Zvcm0nKS5wYXJzbGV5KCk7XHJcbiAgICBcclxuICAgIHBhcnNsZXlJbnN0YW5jZS5vcHRpb25zLnN1Y2Nlc3NDbGFzcyA9ICd0ZXN0JztcclxuICAgIHBhcnNsZXlJbnN0YW5jZS5vcHRpb25zLmVycm9yc0NvbnRhaW5lciA9IGZ1bmN0aW9uIChQYXJzbGV5RmllbGQpIHtcclxuICAgICAgICB2YXIgJHRvcFBhcmVudCA9IFBhcnNsZXlGaWVsZC4kZWxlbWVudC5wYXJlbnRzVW50aWwoJ2Zvcm0nKVtQYXJzbGV5RmllbGQuJGVsZW1lbnQucGFyZW50c1VudGlsKCdmb3JtJykubGVuZ3RoIC0gMV07XHJcbiAgICAgICBcclxuICAgICAgICByZXR1cm4gJHRvcFBhcmVudDtcclxuICAgIH07XHJcbiAgICBcclxuICAgIHBhcnNsZXlJbnN0YW5jZS5vcHRpb25zLmVycm9yc1dyYXBwZXIgPSAnPGRpdiBjbGFzcz1cInRlc3RcIj48L2Rpdj4nO1xyXG4gICAgcGFyc2xleUluc3RhbmNlLm9wdGlvbnMuZXJyb3JUZW1wbGF0ZSA9ICc8c3Bhbj48L3NwYW4+JztcclxuICAgIFxyXG4gICAgY29uc29sZS5sb2cocGFyc2xleUluc3RhbmNlLm9wdGlvbnMuZXJyb3JUZW1wbGF0ZSk7XHJcbiAgICBcclxuICAgIHdpbmRvdy5QYXJzbGV5VmFsaWRhdG9yXHJcbiAgICAgICAgLmFkZFZhbGlkYXRvcihcclxuICAgICAgICAgICAgLy8gdmFsaWRhdGUgaWYgdGhlIGRhdGUgaXMgY29tcGxldGVkIChhbGwgMyBzZWxlY3RvcnMgbmVlZCB0byBoYXZlIGEgdmFsdWUpXHJcbiAgICAgICAgICAgICdkb2JyZXF1aXJlZCcsXHJcblxyXG4gICAgICAgICAgICAvLyAndmFsdWUnIHdpbGwgYmUgdGhlIHZhbHVlIHNlbGVjdGVkXHJcbiAgICAgICAgICAgIC8vICdyZXF1aXJlbWVudHMnIHdpbGwgYmUgdGhlIGlkcyBvZiB0aGUgc2VsZWN0c1xyXG4gICAgICAgICAgICBmdW5jdGlvbih2YWx1ZSwgcmVxdWlyZW1lbnRzKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgZGF5ID0gJChyZXF1aXJlbWVudHNbMF0pLnZhbCgpLFxyXG4gICAgICAgICAgICAgICAgICAgIG1vbnRoID0gJChyZXF1aXJlbWVudHNbMV0pLnZhbCgpLFxyXG4gICAgICAgICAgICAgICAgICAgIHllYXIgPSAkKHJlcXVpcmVtZW50c1syXSkudmFsKCksXHJcbiAgICAgICAgICAgICAgICAgICAgLyogXHJcbiAgICAgICAgICAgICAgICAgICAgICBJbnN0YW50aWF0ZSB0aGUgZGF0ZSBmaWVsZCBhcyBhIHBhcnNsZXkgb2JqZWN0XHJcbiAgICAgICAgICAgICAgICAgICAgICBXZSBkbyB0aGlzIGJlY2F1c2Ugd2UgbmVlZCB0byBhZGQvcmVtb3ZlIGVycm9yIG1lc3NhZ2VzIGFzIGFsbCBcclxuICAgICAgICAgICAgICAgICAgICAgIG9mIHRoZW0gYXJlIGF0dGFjaGVkIHRvIHRoZSBkYXRlIHNlbGVjdG9yXHJcbiAgICAgICAgICAgICAgICAgICAgICovXHJcbiAgICAgICAgICAgICAgICAgICAgJGRvYkRheUluc3RhbmNlID0gJChyZXF1aXJlbWVudHNbMF0pLnBhcnNsZXkoKSxcclxuICAgICAgICAgICAgICAgICAgICBlcnJvck1zZyA9ICdEYXRlIGlzIHJlcXVpcmVkJztcclxuXHJcbiAgICAgICAgICAgICAgICAvKiBcclxuICAgICAgICAgICAgICAgICAgUmVtb3ZlIGFsbCB0aGUgZXhpc3RpbmcgbWVzc2FnZXNcclxuICAgICAgICAgICAgICAgICAgQmVjYXVzZSB0aGUgcmVxdWlyZWQgdmFsaWRhdG9yIGhhcyB0aGUgbGFyZ2VzdCBwcmlvcml0eSwgYWxsIG90aGVyXHJcbiAgICAgICAgICAgICAgICAgIGVycm9yIG1lc3NhZ2VzIGhhdmUgdG8gYmUgcmVtb3ZlZC5cclxuICAgICAgICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICAgICAgd2luZG93LlBhcnNsZXlVSS5yZW1vdmVFcnJvcigkZG9iRGF5SW5zdGFuY2UsICdkb2JyZXF1aXJlZCcpO1xyXG4gICAgICAgICAgICAgICAgd2luZG93LlBhcnNsZXlVSS5yZW1vdmVFcnJvcigkZG9iRGF5SW5zdGFuY2UsICdkYXRlJyk7XHJcbiAgICAgICAgICAgICAgICB3aW5kb3cuUGFyc2xleVVJLnJlbW92ZUVycm9yKCRkb2JEYXlJbnN0YW5jZSwgJ2FnZScpO1xyXG5cclxuICAgICAgICAgICAgICAgIGlmIChkYXkubGVuZ3RoICYmIG1vbnRoLmxlbmd0aCAmJiB5ZWFyLmxlbmd0aCkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIElmIHRoZSBkYXRlIGlzIHZhbGlkLCByZW1vdmUgdGhlIGVycm9yIG1zZ1xyXG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5QYXJzbGV5VUkucmVtb3ZlRXJyb3IoJGRvYkRheUluc3RhbmNlLCAnZG9icmVxdWlyZWQnKTtcclxuXHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRydWU7XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIC8vIElmIHRoZSBkYXRlIGlzIG5vdCB2YWxpZCwgYWRkIHRoZSBlcnJvciBtc2dcclxuICAgICAgICAgICAgICAgICAgICB3aW5kb3cuUGFyc2xleVVJLmFkZEVycm9yKCRkb2JEYXlJbnN0YW5jZSwgJ2RvYnJlcXVpcmVkJywgZXJyb3JNc2cpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9XHJcblxyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICAvLyBwcmlvcml0eSAtIHRoZSB2YWxpZGF0b3Igd2l0aCB0aGUgaGlnaGVzdCBwcmlvcml0eSB3aWxsIGJlIHJ1biBmaXJzdFxyXG4gICAgICAgICAgICAzNFxyXG4gICAgICAgIClcclxuICAgICAgICAuYWRkVmFsaWRhdG9yKFxyXG4gICAgICAgICAgICAvLyB2YWxpZGF0ZSBpZiB0aGUgc2VsZWN0ZWQgZGF0ZSBpcyB2YWxpZFxyXG4gICAgICAgICAgICAvLyBleC46IDI5LzAyLzIwMTAgd2lsbCBub3QgYmUgdmFsaWQsIDI5LzAyLzIwMTIgd2lsbFxyXG4gICAgICAgICAgICAnZGF0ZScsXHJcblxyXG4gICAgICAgICAgICAvLyAndmFsdWUnIHdpbGwgYmUgdGhlIHZhbHVlIHNlbGVjdGVkXHJcbiAgICAgICAgICAgIC8vICdyZXF1aXJlbWVudHMnIHdpbGwgYmUgdGhlIGlkcyBvZiB0aGUgc2VsZWN0c1xyXG4gICAgICAgICAgICBmdW5jdGlvbih2YWx1ZSwgcmVxdWlyZW1lbnRzKSB7XHJcbiAgICAgICAgICAgICAgICB2YXIgZGF5ID0gJChyZXF1aXJlbWVudHNbMF0pLnZhbCgpLFxyXG4gICAgICAgICAgICAgICAgICAgIG1vbnRoID0gJChyZXF1aXJlbWVudHNbMV0pLnZhbCgpLFxyXG4gICAgICAgICAgICAgICAgICAgIHllYXIgPSAkKHJlcXVpcmVtZW50c1syXSkudmFsKCksXHJcbiAgICAgICAgICAgICAgICAgICAgbXlkYXRlID0gbmV3IERhdGUoKSxcclxuICAgICAgICAgICAgICAgICAgICAvKiBcclxuICAgICAgICAgICAgICAgICAgICAgIEluc3RhbnRpYXRlIHRoZSBkYXRlIGZpZWxkIGFzIGEgcGFyc2xleSBvYmplY3RcclxuICAgICAgICAgICAgICAgICAgICAgIFdlIGRvIHRoaXMgYmVjYXVzZSB3ZSBuZWVkIHRvIGFkZC9yZW1vdmUgZXJyb3IgbWVzc2FnZXMgYXMgYWxsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgb2YgdGhlbSBhcmUgYXR0YWNoZWQgdG8gdGhlIGRhdGUgc2VsZWN0b3JcclxuICAgICAgICAgICAgICAgICAgICAgKi9cclxuICAgICAgICAgICAgICAgICAgICAkZG9iRGF5SW5zdGFuY2UgPSAkKHJlcXVpcmVtZW50c1swXSkucGFyc2xleSgpLFxyXG4gICAgICAgICAgICAgICAgICAgIGVycm9yTXNnID0gJ0VudGVyIGEgdmFsaWQgZGF0ZSc7XHJcblxyXG4gICAgICAgICAgICAgICAgLyogXHJcbiAgICAgICAgICAgICAgICAgUmVtb3ZlIHRoZSBlcnJvciBtZXNzYWdlcyBvZiB0aGUgbG93ZXIgcHJpb3JpdHkgdmFsaWRhdG9yc1xyXG4gICAgICAgICAgICAgICAgIEJlY2F1c2UgdGhlIGRhdGUgdmFsaWRhdG9yIGhhcyBhIGxvd2VyIHByaW9yaXR5LCBpdHNcclxuICAgICAgICAgICAgICAgICBlcnJvciBtZXNzYWdlcyBoYXZlIHRvIGJlIHJlbW92ZWQuXHJcbiAgICAgICAgICAgICAgICAqL1xyXG4gICAgICAgICAgICAgICAgd2luZG93LlBhcnNsZXlVSS5yZW1vdmVFcnJvcigkZG9iRGF5SW5zdGFuY2UsICdkYXRlJyk7XHJcbiAgICAgICAgICAgICAgICB3aW5kb3cuUGFyc2xleVVJLnJlbW92ZUVycm9yKCRkb2JEYXlJbnN0YW5jZSwgJ2FnZScpO1xyXG5cclxuICAgICAgICAgICAgICAgIG15ZGF0ZS5zZXRGdWxsWWVhcih5ZWFyLCBtb250aCAtIDEsIGRheSk7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKChteWRhdGUuZ2V0RGF0ZSgpID09IGRheSkgJiYgKG15ZGF0ZS5nZXRNb250aCgpID09IChtb250aCAtIDEpKSAmJiAobXlkYXRlLmdldEZ1bGxZZWFyKCkgPT0geWVhcikpIHtcclxuICAgICAgICAgICAgICAgICAgICAvLyBJZiB0aGUgY3VycmVudCBkYXRlIGlzIG5vdCBpZGVudGljYWwgdG8gdGhlIGNvbnZlcnRlZCBkYXRlLCBpdCdzIG5vdCB2YWxpZFxyXG4gICAgICAgICAgICAgICAgICAgIHdpbmRvdy5QYXJzbGV5VUkucmVtb3ZlRXJyb3IoJGRvYkRheUluc3RhbmNlLCAnZGF0ZScpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgd2luZG93LlBhcnNsZXlVSS5hZGRFcnJvcigkZG9iRGF5SW5zdGFuY2UsICdkYXRlJywgZXJyb3JNc2cpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gZmFsc2U7XHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcblxyXG4gICAgICAgICAgICAvLyBwcmlvcml0eVxyXG4gICAgICAgICAgICAzM1xyXG4gICAgICAgIClcclxuICAgICAgICAuYWRkVmFsaWRhdG9yKFxyXG4gICAgICAgICAgICAvLyB2YWxpZGF0ZSBpZiB0aGUgc2VsZWN0ZWQgZGF0ZSBjb3Jlc3BvbmRzIHRvIGFuIGFnZSBvZiBhdCBsZWFzdCAxOFxyXG4gICAgICAgICAgICAnYWdlJyxcclxuXHJcbiAgICAgICAgICAgIC8vICd2YWx1ZScgd2lsbCBiZSB0aGUgdmFsdWUgc2VsZWN0ZWRcclxuICAgICAgICAgICAgLy8gJ3JlcXVpcmVtZW50cycgd2lsbCBiZSB0aGUgaWRzIG9mIHRoZSBzZWxlY3RzXHJcbiAgICAgICAgICAgIGZ1bmN0aW9uKHZhbHVlLCByZXF1aXJlbWVudHMpIHtcclxuICAgICAgICAgICAgICAgIHZhciBkYXkgPSAkKHJlcXVpcmVtZW50c1swXSkudmFsKCksXHJcbiAgICAgICAgICAgICAgICAgICAgbW9udGggPSAkKHJlcXVpcmVtZW50c1sxXSkudmFsKCksXHJcbiAgICAgICAgICAgICAgICAgICAgeWVhciA9ICQocmVxdWlyZW1lbnRzWzJdKS52YWwoKSxcclxuICAgICAgICAgICAgICAgICAgICBteWRhdGUgPSBuZXcgRGF0ZSgpLFxyXG4gICAgICAgICAgICAgICAgICAgIGN1cnJkYXRlID0gbmV3IERhdGUoKSxcclxuICAgICAgICAgICAgICAgICAgICBhZ2UgPSAxOCxcclxuICAgICAgICAgICAgICAgICAgICAvKiBcclxuICAgICAgICAgICAgICAgICAgICAgIEluc3RhbnRpYXRlIHRoZSBkYXRlIGZpZWxkIGFzIGEgcGFyc2xleSBvYmplY3RcclxuICAgICAgICAgICAgICAgICAgICAgIFdlIGRvIHRoaXMgYmVjYXVzZSB3ZSBuZWVkIHRvIGFkZC9yZW1vdmUgZXJyb3IgbWVzc2FnZXMgYXMgYWxsIFxyXG4gICAgICAgICAgICAgICAgICAgICAgb2YgdGhlbSBhcmUgYXR0YWNoZWQgdG8gdGhlIGRhdGUgc2VsZWN0b3JcclxuICAgICAgICAgICAgICAgICAgICAgKi9cclxuICAgICAgICAgICAgICAgICAgICAkZG9iRGF5SW5zdGFuY2UgPSAkKHJlcXVpcmVtZW50c1swXSkucGFyc2xleSgpLFxyXG4gICAgICAgICAgICAgICAgICAgIGVycm9yTXNnID0gJ1lvdSBtdXN0IGJlIG92ZXIgMTggdG8gcHJvY2VlZCc7XHJcblxyXG4gICAgICAgICAgICAgICAgLy8gUmVtb3ZlIG9ubHkgdGhlIGNvcnJlc3BvbmRpbmcgZXJyb3IgbXNnIGFzIHRoaXMgaXMgdGhlIGxhc3QgdmFsaWRhdG9yXHJcbiAgICAgICAgICAgICAgICB3aW5kb3cuUGFyc2xleVVJLnJlbW92ZUVycm9yKCRkb2JEYXlJbnN0YW5jZSwgJ2FnZScpO1xyXG5cclxuICAgICAgICAgICAgICAgIC8vIFNlbGVjdGVkIGJpcnRoIGRhdGVcclxuICAgICAgICAgICAgICAgIG15ZGF0ZS5zZXRGdWxsWWVhcih5ZWFyLCBtb250aCAtIDEsIGRheSk7XHJcbiAgICAgICAgICAgICAgICAvLyBXZSBzdWJzdHJhY3QgdGhlIG1pbmltdW0gYWdlIGZyb20gdGhlIGN1cnJlbnQgZGF0ZVxyXG4gICAgICAgICAgICAgICAgY3VycmRhdGUuc2V0RnVsbFllYXIoY3VycmRhdGUuZ2V0RnVsbFllYXIoKSAtIGFnZSk7XHJcblxyXG4gICAgICAgICAgICAgICAgaWYgKGN1cnJkYXRlID49IG15ZGF0ZSkge1xyXG4gICAgICAgICAgICAgICAgICAgIC8qIFxyXG4gICAgICAgICAgICAgICAgICAgICBXZSBjb21wYXJlIHRoZSBtaW5pbXVtIHZhbGlkIGJpcnRoIGRhdGUgKGN1cnJlbnQgZGF0ZSBtaW51cyB0aGUgbWluaW11bSBhZ2UpXHJcbiAgICAgICAgICAgICAgICAgICAgIHRvIHRoZSBjdXJyZW50IGRhdGUuIFRoZSBmaXJzdCBvbmUgbmVlZHMgdG8gYmUgZ3JlYXRlciBvciBlcXVhbCB0byB0aGUgc2Vjb25kXHJcbiAgICAgICAgICAgICAgICAgICAgIG9uZSBpbiBvcmRlciB0byBoYXZlIGEgdmFsaWQgYmlydGggZGF0ZS5cclxuICAgICAgICAgICAgICAgICAgICAgKi9cclxuICAgICAgICAgICAgICAgICAgICB3aW5kb3cuUGFyc2xleVVJLnJlbW92ZUVycm9yKCRkb2JEYXlJbnN0YW5jZSwgJ2FnZScpO1xyXG5cclxuICAgICAgICAgICAgICAgICAgICByZXR1cm4gdHJ1ZTtcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgd2luZG93LlBhcnNsZXlVSS5hZGRFcnJvcigkZG9iRGF5SW5zdGFuY2UsICdhZ2UnLCBlcnJvck1zZyk7XHJcblxyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiBmYWxzZTtcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuXHJcbiAgICAgICAgICAgIC8vIHByaW9yaXR5XHJcbiAgICAgICAgICAgIDMyXHJcbiAgICAgICAgKTtcclxuXHJcbn0pOyJdLCJmaWxlIjoicGFyc2xleS5qcyJ9
